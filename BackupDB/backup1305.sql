USE [master]
GO
/****** Object:  Database [Hotel]    Script Date: 05/13/2021 4:10:46 PM ******/
CREATE DATABASE [Hotel] ON  PRIMARY 
( NAME = N'Hotel', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Hotel.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Hotel_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Hotel_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Hotel] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Hotel].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Hotel] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Hotel] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Hotel] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Hotel] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Hotel] SET ARITHABORT OFF 
GO
ALTER DATABASE [Hotel] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Hotel] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Hotel] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Hotel] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Hotel] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Hotel] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Hotel] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Hotel] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Hotel] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Hotel] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Hotel] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Hotel] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Hotel] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Hotel] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Hotel] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Hotel] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Hotel] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Hotel] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Hotel] SET  MULTI_USER 
GO
ALTER DATABASE [Hotel] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Hotel] SET DB_CHAINING OFF 
GO
USE [Hotel]
GO
/****** Object:  Table [dbo].[HotelNews]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HotelNews](
	[HotelNewsID] [uniqueidentifier] NOT NULL,
	[NewsTypeID] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Brief] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Source] [nvarchar](max) NULL,
	[FileAttchment] [nvarchar](max) NULL,
	[OrderID] [int] NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NOT NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.HotelNews] PRIMARY KEY CLUSTERED 
(
	[HotelNewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsTypes]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTypes](
	[NewsTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [datetime] NULL,
 CONSTRAINT [PK_dbo.NewsTypes] PRIMARY KEY CLUSTERED 
(
	[NewsTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SentCommunication]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SentCommunication](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[MailTo] [varchar](50) NOT NULL,
	[MailCc] [varchar](200) NOT NULL,
	[MailBcc] [varchar](200) NOT NULL,
	[MailFrom] [varchar](50) NOT NULL,
	[MailFromName] [nvarchar](50) NOT NULL,
	[IsSent] [bit] NULL,
	[IsCheck] [bit] NULL,
	[DateCreate] [datetime] NULL,
	[DateCheck] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[LogError] [nvarchar](max) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](15) NOT NULL,
	[CountryPhoneCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_SentCommunication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Area]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Area](
	[AreaId] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [nvarchar](255) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Area] PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking](
	[BookingId] [uniqueidentifier] NOT NULL,
	[RoomPriceId] [uniqueidentifier] NOT NULL,
	[RoomQuantity] [int] NULL,
	[Amount] [float] NULL,
	[AmountCancel] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Email] [nvarchar](255) NOT NULL,
	[PhoneNumber] [nvarchar](255) NOT NULL,
	[ContactName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[SaleId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Booking_1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Country]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Country](
	[CountryId] [varchar](3) NOT NULL,
	[CountryName] [nvarchar](50) NOT NULL,
	[Symbol] [varchar](3) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Coupon]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Coupon](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[ShortCode] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[EffectiveDate] [date] NULL,
	[ExpirationDate] [date] NULL,
	[Price] [float] NULL,
	[Image] [nvarchar](1024) NULL,
	[Type] [int] NOT NULL,
	[PointExchange] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CouponType]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CouponType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeName] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CouponType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CustomerNo] [nvarchar](255) NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[Dob] [datetime] NULL,
	[Gender] [tinyint] NOT NULL,
	[Nationality] [varchar](3) NULL,
	[IdCard] [nvarchar](12) NULL,
	[DateOfIssue] [datetime] NULL,
	[PlaceOfIssue] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[CountryId] [int] NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Note] [nvarchar](255) NULL,
	[Password] [nvarchar](127) NULL,
	[TotalPoint] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerCoupon]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerCoupon](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CouponId] [uniqueidentifier] NOT NULL,
	[ReceiptId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CustomerCoupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Direction]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Direction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DirectionName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Direction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_District]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_District](
	[DistrictId] [int] IDENTITY(1,1) NOT NULL,
	[DistrictName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_District] PRIMARY KEY CLUSTERED 
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Hotel]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Hotel](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[AreaId] [int] NULL,
	[HotelRatingTypeId] [int] NULL,
	[HotelTypeId] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Address] [nvarchar](500) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Longitude] [nvarchar](255) NULL,
	[Latitude] [nvarchar](255) NULL,
	[Content] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Hotel_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelCancellationPolicy]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelCancellationPolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberDateCancel] [int] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[PercentAmount] [nchar](10) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelCancellationPolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelComment]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelComment](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[DateTimeComment] [datetime] NOT NULL,
	[PointLocation] [float] NULL,
	[PointServe] [float] NULL,
	[PointConvenient] [float] NULL,
	[PointCost] [float] NULL,
	[PointClean] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelConvenientType]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelConvenientType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConvenientName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[Type] [int] NOT NULL,
	[Order] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelConvenientType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelNotice]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelNotice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelNotice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelPriceType]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelPriceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PriceTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelPriceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelRatingType]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelRatingType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberStar] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelRatingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelServiceType]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelServiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelServiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelSurchargePolicy]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelSurchargePolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [nvarchar](255) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelSurchargePolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelType]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_PaymentType]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_PaymentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeCode] [nvarchar](255) NOT NULL,
	[PayTypeName] [nvarchar](255) NOT NULL,
	[Order] [int] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ProductImage]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductImage](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NULL,
	[RoomType] [uniqueidentifier] NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_HotelImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Province]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Province](
	[ProvinceId] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [varchar](3) NOT NULL,
	[ProvinceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Province] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Receipt]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Receipt](
	[Id] [uniqueidentifier] NOT NULL,
	[ReceiptNo] [nvarchar](255) NOT NULL,
	[ReceiptDate] [datetime] NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[Payer] [nvarchar](255) NULL,
	[PaymentType] [int] NOT NULL,
	[Company] [nvarchar](255) NULL,
	[VatCode] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Telephone] [nvarchar](255) NULL,
	[Mobile] [nvarchar](255) NULL,
	[Note] [nvarchar](255) NULL,
	[ReceiptOrder] [int] NULL,
	[Point] [float] NULL,
	[Status] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Room]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Room](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomName] [nvarchar](255) NOT NULL,
	[AvailableRoom] [int] NOT NULL,
	[RoomArea] [float] NULL,
	[Direction] [int] NULL,
	[SingleBed] [int] NULL,
	[DoubleBed] [int] NULL,
	[MaxPeople] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RoomType_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoomPrice]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoomPrice](
	[Id] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[RoomDate] [datetime] NOT NULL,
	[AvailableRoom] [int] NULL,
	[HotelPriceTypeId] [int] NULL,
	[PriceContract] [float] NOT NULL,
	[PricePromotion] [float] NULL,
	[PercentPromotion] [float] NULL,
	[HotelServiceType] [nvarchar](255) NULL,
	[IsCancel] [bit] NULL,
	[IsSurcharge] [bit] NULL,
	[IsPromotion] [bit] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[Tax] [float] NULL,
	[TransactionCosts] [float] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Room_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[UserId] [uniqueidentifier] NOT NULL,
	[LoginName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](127) NOT NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[CodeStaff] [nvarchar](255) NULL,
	[Active] [tinyint] NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User_Role]    Script Date: 05/13/2021 4:10:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User_Role](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_User_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbl_Area] ON 

INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ Tuyền Lâm', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Ven Hồ', 2, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Ga Xe Lửa Cũ', 3, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Trung Tâm Thành Phố Đà Lạt', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Thác Cam Ly', 2, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Thung Lũng Tình Yêu / Đồi Mộng Mơ', 3, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Thung Lũng Vàng', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Núi LangBiang', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Khu du lịch Trúc Lâm Viên', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'Núi Bà Rá', 16, NULL, 0, N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T14:06:12.257' AS DateTime), N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T14:06:12.703' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Area] OFF
GO
INSERT [dbo].[tbl_Booking] ([BookingId], [RoomPriceId], [RoomQuantity], [Amount], [AmountCancel], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [Email], [PhoneNumber], [ContactName], [ProvinceId], [SaleId], [Status]) VALUES (N'f004dcfe-d5ec-4989-afb3-c8a1f1c9a12f', N'74a7d97d-8d5a-49f0-8865-a8f09f0f7e12', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL, N'nguyenthanhdat.bc2310@gmail.com', N'0932980033', N'Nguyễn Thành Đạt', 1, NULL, 0)
GO
INSERT [dbo].[tbl_Country] ([CountryId], [CountryName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1', N'Việt Nam', N'VIE', 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Direction] ON 

INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hướng nội bộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Có cửa sổ (không hướng)', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Hướng phố', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Hướng núi', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Direction] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_District] ON 

INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Đà Lạt', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Bảo lộc', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Đạ Huoai', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Di Linh', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Đức Trọng', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Đơn Dương', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Đam Rông', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Cát Tiên', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Lâm Hà', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'Lạc Dương', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'Đạ Tẻh', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Bảo Lâm', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Bù Đốp', 3, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Phước Long', 3, NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_District] OFF
GO
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Khách Sạn LaDaLat', 1, 1, 7, 4, 1, N'4,18,16,3,5,15,10,6,7,9,11', N'106A  Mai Anh Đào, Phường 8, Đà Lạt, Lâm Đồng', NULL, NULL, NULL, NULL, N'Tại Ladalat, chúng tôi hoàn toàn tôn trọng và bảo tồn các đặc điểm riêng biệt đặc trưng của Đà Lạt. Hành lang rộng lớn đầy hoa anh đào – loài hoa được biết đến như biểu tượng của Đà Lạt. Tình yêu Lang và Biang được hồi sinh và kỷ niệm. Những đặc điểm bản chất của Đà Lạt tồn tại như một sự không thể tránh khỏi, tạo ra sự lãng mạn mơ màng của Ladalat.  
', NULL, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] ON 

INSERT [dbo].[tbl_HotelCancellationPolicy] ([Id], [NumberDateCancel], [HotelId], [PercentAmount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 7, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'50        ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] ON 

INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ bơi', NULL, 0, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Massage/Spa', NULL, 0, 2, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Wifi miễn phí', NULL, 0, 3, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Bãi đỗ xe', NULL, 0, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Giặt là', NULL, 0, 10, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Lễ tân 24/24', NULL, 0, 13, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Đưa đón sân bay', NULL, 0, 5, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'Cho thuê máy bay', NULL, 0, 6, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Phòng gym', NULL, 0, 7, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Nhà hàng', NULL, 0, 8, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Phục vụ đồ ăn tại phòng', NULL, 0, 9, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Chấp nhận thú cưng', NULL, 0, 11, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Hỗ trợ đặt tour', NULL, 0, 12, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Thang máy', NULL, 0, 14, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'Máy ATM trong khách sạn', NULL, 0, 15, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'Phòng họp', NULL, 0, 16, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'Tổ chức sự kiện', NULL, 0, 17, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'Vòi hoa sen', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N' Mấy sấy tóc', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'Quạt', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'Truyền hình vệ tinh/cáp', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'Két sắt trong phòng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'Nước đóng chai miễn phí', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'Dép đi trong phòng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'Sàn trải thảm', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'Dịch vụ báo thức', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'Buồng tắm đứng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'Đồ vệ sinh cá nhân miễn phí', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'Quầy bar nhỏ (MiniBar)', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'Cà phê/Trà', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N'Ấm đun nước điện', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'Tivi màn hình phẳng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'Điện thoại', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] ON 

INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Các mức phí trên chưa bao gồm thuế, phí', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Giường phụ không áp dụng cho loại phòng Basement Standard Triple No Window', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'1 phòng được thêm tối đa 1 người lớn và 1 trẻ em (tuỳ vào loại phòng)', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Trẻ từ 12 tuổi trở lên được tính như người lớn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Vui lòng nhập các yêu cầu đặc biệt vào mục Yêu cầu khác bên dưới Thông tin liên hệ để được hỗ trợ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] ON 

INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Ưu đãi đặc biệt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Đặt sớm giá tốt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Giá Shock giờ chót', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] ON 

INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 1, N'Khách sạn 1 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, 2, N'Khách sạn 2 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, 3, N'Khách sạn 3 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, 4, N'Khách sạn 4 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, 5, N'Khách sạn 5 sao', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] ON 

INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Bữa sáng miễn phí', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'3 Bữa ăn miễn phí', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Thêm giường phụ', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Miễn phí hủy phòng', NULL, 0, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] ON 

INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Từ 0 đến 5.9 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Từ 6 đến 11 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 152000, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Người lớn', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 523000, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelType] ON 

INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Khách sạn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Resort', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Homestay', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Căn hộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Biệt thự', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Du thuyền', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] ON 

INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'TTD', N'Thẻ tính dụng', 1, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'TND', N'Thẻ nội địa', 2, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'CK', N'Chuyển khoản', 3, 1, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Province] ON 

INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'1', N'Đà lạt', N'DLA', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'1', N'Bình Phước', NULL, 1, N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-12T17:04:40.360' AS DateTime), N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-12T17:08:29.860' AS DateTime))
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'1', N'Bình Phước', NULL, 0, N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T09:15:58.043' AS DateTime), N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T09:15:58.043' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Province] OFF
GO
INSERT [dbo].[tbl_Receipt] ([Id], [ReceiptNo], [ReceiptDate], [BookingId], [Amount], [Payer], [PaymentType], [Company], [VatCode], [Address], [Telephone], [Mobile], [Note], [ReceiptOrder], [Point], [Status], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'bc8e9172-baba-4d67-9826-f78cf4b914c0', N'TT0001', NULL, N'f004dcfe-d5ec-4989-afb3-c8a1f1c9a12f', 1161000, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Role] ON 

INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (1, N'Quản trị')
INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (2, N'Nhân viên')
SET IDENTITY_INSERT [dbo].[tbl_Role] OFF
GO
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Basement Standard Double No Window', 20, 25, NULL, NULL, 1, 2, N'20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30,
32,
33,
34,
35,
36', NULL, 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'74a7d97d-8d5a-49f0-8865-a8f09f0f7e12', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-05-05T00:00:00.000' AS DateTime), NULL, NULL, 822000, 528000, 36, N'1', 1, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'31626bac-ff21-47a3-8166-27ba5aabd1f5', N'hieunv', N'M/tTWwIES8wxgueZZcLelw==', N'Nguyễn Việt Hiếu', N'', 1, N'hieuviet.1103@gmail.com', N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', N'khanhpq', N'4QrcOUm6Wau+VuBX8g+IPg==', N'a khánh', N'', 0, N'khanh@gmail.com', N'', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'00000000-0000-0000-0000-000000000000', N'31626bac-ff21-47a3-8166-27ba5aabd1f5', 1, 1)
GO
ALTER TABLE [dbo].[tbl_Area] ADD  CONSTRAINT [DF_tbl_Area_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Country] ADD  CONSTRAINT [DF_tbl_Country_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Coupon] ADD  CONSTRAINT [DF_tbl_Coupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CouponType] ADD  CONSTRAINT [DF_tbl_CouponType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] ADD  CONSTRAINT [DF_tbl_CustomerCoupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Direction] ADD  CONSTRAINT [DF_tbl_Direction_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_District] ADD  CONSTRAINT [DF_tbl_District_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Hotel] ADD  CONSTRAINT [DF_tbl_Hotel_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] ADD  CONSTRAINT [DF_tbl_HotelCancellationPolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelComment] ADD  CONSTRAINT [DF_tbl_HotelComment_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelConvenientType] ADD  CONSTRAINT [DF_tbl_HotelConvenientType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelNotice] ADD  CONSTRAINT [DF_tbl_HotelNotice_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelPriceType] ADD  CONSTRAINT [DF_tbl_HotelPriceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelRatingType] ADD  CONSTRAINT [DF_tbl_HotelRatingType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelServiceType] ADD  CONSTRAINT [DF_tbl_HotelServiceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] ADD  CONSTRAINT [DF_tbl_HotelSurchargePolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelType] ADD  CONSTRAINT [DF_tbl_HotelType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_PaymentType] ADD  CONSTRAINT [DF_tbl_PaymentType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_HotelImage_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_ProductImage_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Province] ADD  CONSTRAINT [DF_tbl_Province_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Room] ADD  CONSTRAINT [DF_tbl_RoomType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_RoomPrice] ADD  CONSTRAINT [DF_tbl_Room_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_User_Role] ADD  CONSTRAINT [DF_tbl_User_Role_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[HotelNews]  WITH CHECK ADD  CONSTRAINT [FK_HotelNews_NewsTypes] FOREIGN KEY([NewsTypeID])
REFERENCES [dbo].[NewsTypes] ([NewsTypeID])
GO
ALTER TABLE [dbo].[HotelNews] CHECK CONSTRAINT [FK_HotelNews_NewsTypes]
GO
ALTER TABLE [dbo].[tbl_Area]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Area_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Area] CHECK CONSTRAINT [FK_tbl_Area_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Booking]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Booking_tbl_RoomPrice] FOREIGN KEY([RoomPriceId])
REFERENCES [dbo].[tbl_RoomPrice] ([Id])
GO
ALTER TABLE [dbo].[tbl_Booking] CHECK CONSTRAINT [FK_tbl_Booking_tbl_RoomPrice]
GO
ALTER TABLE [dbo].[tbl_Coupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Coupon_tbl_CouponType] FOREIGN KEY([Type])
REFERENCES [dbo].[tbl_CouponType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Coupon] CHECK CONSTRAINT [FK_tbl_Coupon_tbl_CouponType]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon] FOREIGN KEY([CouponId])
REFERENCES [dbo].[tbl_Coupon] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[tbl_Receipt] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt]
GO
ALTER TABLE [dbo].[tbl_District]  WITH CHECK ADD  CONSTRAINT [FK_tbl_District_tbl_Province] FOREIGN KEY([ProvinceId])
REFERENCES [dbo].[tbl_Province] ([ProvinceId])
GO
ALTER TABLE [dbo].[tbl_District] CHECK CONSTRAINT [FK_tbl_District_tbl_Province]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[tbl_Area] ([AreaId])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_Area]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_HotelRatingType] FOREIGN KEY([HotelRatingTypeId])
REFERENCES [dbo].[tbl_HotelRatingType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_HotelRatingType]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_HotelType] FOREIGN KEY([HotelTypeId])
REFERENCES [dbo].[tbl_HotelType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_HotelType]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] CHECK CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelNotice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelNotice] CHECK CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] CHECK CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_Province]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Province_tbl_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[tbl_Country] ([CountryId])
GO
ALTER TABLE [dbo].[tbl_Province] CHECK CONSTRAINT [FK_tbl_Province_tbl_Country]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_Booking] FOREIGN KEY([BookingId])
REFERENCES [dbo].[tbl_Booking] ([BookingId])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_Booking]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType] FOREIGN KEY([PaymentType])
REFERENCES [dbo].[tbl_PaymentType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Direction] FOREIGN KEY([Direction])
REFERENCES [dbo].[tbl_Direction] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Direction]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_RoomPrice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RoomPrice_tbl_Room] FOREIGN KEY([RoomId])
REFERENCES [dbo].[tbl_Room] ([Id])
GO
ALTER TABLE [dbo].[tbl_RoomPrice] CHECK CONSTRAINT [FK_tbl_RoomPrice_tbl_Room]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_Role]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_User]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Ảnh đại diện KS; 1: Ds ảnh Ks; 2Ảnh đại diện phòng; 3 Ds ảnh phòng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ProductImage', @level2type=N'COLUMN',@level2name=N'Type'
GO
USE [master]
GO
ALTER DATABASE [Hotel] SET  READ_WRITE 
GO
