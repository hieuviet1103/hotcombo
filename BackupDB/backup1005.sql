USE [Hotel]
GO
/****** Object:  Table [dbo].[HotelNews]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HotelNews](
	[HotelNewsID] [uniqueidentifier] NOT NULL,
	[NewsTypeID] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Brief] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Source] [nvarchar](max) NULL,
	[FileAttchment] [nvarchar](max) NULL,
	[OrderID] [int] NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NOT NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.HotelNews] PRIMARY KEY CLUSTERED 
(
	[HotelNewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsTypes]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTypes](
	[NewsTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [datetime] NULL,
 CONSTRAINT [PK_dbo.NewsTypes] PRIMARY KEY CLUSTERED 
(
	[NewsTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SentCommunication]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SentCommunication](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[MailTo] [nchar](50) NOT NULL,
	[MailCc] [nchar](200) NOT NULL,
	[MailBcc] [nchar](200) NOT NULL,
	[MailFrom] [nchar](50) NOT NULL,
	[MailFromName] [nchar](200) NOT NULL,
	[IsSent] [bit] NULL,
	[IsCheck] [bit] NULL,
	[DateCreate] [datetime] NULL,
	[DateCheck] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[LogError] [nvarchar](200) NOT NULL,
	[Type] [nchar](50) NOT NULL,
	[PhoneNumber] [nchar](15) NOT NULL,
	[CountryPhoneCode] [nchar](10) NOT NULL,
 CONSTRAINT [PK_SentCommunication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Area]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Area](
	[AreaId] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [nvarchar](255) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Area] PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking](
	[BookingId] [uniqueidentifier] NOT NULL,
	[RoomId] [int] NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
	[Email] [nvarchar](255) NOT NULL,
	[PhoneNumber] [nvarchar](255) NOT NULL,
	[ContactName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[SaleId] [nvarchar](255) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Booking_1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Country]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](50) NOT NULL,
	[Symbol] [varchar](3) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Coupon]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Coupon](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[ShortCode] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Type] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CouponType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CouponType](
	[Id] [int] NOT NULL,
	[CouponTypeName] [nvarchar](255) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CustomerNo] [nvarchar](255) NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[Dob] [datetime] NULL,
	[Gender] [tinyint] NOT NULL,
	[Nationality] [varchar](3) NULL,
	[IdCard] [nvarchar](12) NULL,
	[DateOfIssue] [datetime] NULL,
	[PlaceOfIssue] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[CountryId] [int] NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Note] [nvarchar](255) NULL,
	[Password] [nvarchar](127) NULL,
	[TotalPoint] [float] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerCoupon]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerCoupon](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CouponId] [uniqueidentifier] NOT NULL,
	[ReceiptId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CustomerCoupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Direction]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Direction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DirectionName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Direction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_District]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_District](
	[DistrictId] [int] IDENTITY(1,1) NOT NULL,
	[DistrictName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_District] PRIMARY KEY CLUSTERED 
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Hotel]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Hotel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[AreaId] [int] NULL,
	[HotelRatingTypeId] [int] NULL,
	[HotelTypeId] [int] NULL,
	[HotelServiceType] [nvarchar](255) NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Address] [nvarchar](500) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Hotel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelCancellationPolicy]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelCancellationPolicy](
	[Id] [int] NOT NULL,
	[NumberDateCancel] [int] NOT NULL,
	[HotelId] [int] NOT NULL,
	[PercentAmount] [nchar](10) NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelComment]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelComment](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[DateTimeComment] [datetime] NOT NULL,
	[PointLocation] [float] NULL,
	[PointServe] [float] NULL,
	[PointConvenient] [float] NULL,
	[PointCost] [float] NULL,
	[PointClean] [float] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelConvenientType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelConvenientType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConvenientName] [nvarchar](255) NOT NULL,
	[Type] [int] NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelConvenientType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelNotice]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelNotice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[Content] [nvarchar](500) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelNotice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelPriceType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelPriceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PriceTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelPriceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelRatingType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelRatingType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberStar] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelRatingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelServiceType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelServiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelServiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelSurchargePolicy]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelSurchargePolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SurchargeName] [nvarchar](255) NOT NULL,
	[HotelId] [int] NOT NULL,
	[Amount] [float] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelSurchargePolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_PaymentType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_PaymentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeCode] [nvarchar](255) NOT NULL,
	[PayTypeName] [nvarchar](255) NOT NULL,
	[Order] [int] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ProductImage]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductImage](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NULL,
	[RoomType] [uniqueidentifier] NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_tbl_HotelImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Province]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Province](
	[ProvinceId] [int] IDENTITY(1,1) NOT NULL,
	[ProvinceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Province] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Receipt]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Receipt](
	[Id] [uniqueidentifier] NOT NULL,
	[ReceiptNo] [nvarchar](255) NOT NULL,
	[ReceiptDate] [datetime] NOT NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[Payer] [nvarchar](255) NULL,
	[PaymentType] [int] NOT NULL,
	[Company] [nvarchar](255) NULL,
	[VatCode] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Telephone] [nvarchar](255) NULL,
	[Mobile] [nvarchar](255) NULL,
	[Note] [nvarchar](255) NULL,
	[ReceiptOrder] [int] NULL,
	[Status] [int] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Room]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Room](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomTypeId] [int] NOT NULL,
	[HotelPriceTypeId] [int] NULL,
	[Date] [datetime] NOT NULL,
	[PriceContact] [float] NOT NULL,
	[PricePromotion] [float] NULL,
	[PercentPromotion] [float] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Room] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoomPrice]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoomPrice](
	[Id] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[RoomDate] [datetime] NOT NULL,
	[AvailableRoom] [int] NULL,
	[HotelPriceTypeId] [int] NULL,
	[PriceContract] [float] NOT NULL,
	[PricePromotion] [float] NULL,
	[PercentPromotion] [float] NULL,
	[HotelServiceType] [nvarchar](255) NULL,
	[IsCancel] [bit] NULL,
	[IsSurcharge] [bit] NULL,
	[IsPromotion] [bit] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[IsDelete] [bit] NULL,
	[Tax] [float] NULL,
	[TransactionCosts] [float] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Room_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoomType]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoomType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [int] NOT NULL,
	[RoomName] [nvarchar](255) NOT NULL,
	[AvailableRoom] [int] NOT NULL,
	[RoomArea] [float] NULL,
	[Derection] [int] NULL,
	[SingleBed] [int] NULL,
	[DoubleBed] [int] NULL,
	[MaxPeople] [int] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RoomType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nchar](20) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[Email] [nchar](30) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[Password] [nvarchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User_Role]    Script Date: 10/5/2021 9:36:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User_Role](
	[Id] [bigint] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_User_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[NewsTypes] ON 

INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (1, 1, N'Tin Tức', 1, NULL, N'hieunv', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[NewsTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Area] ON 

INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ Tuyền Lâm', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Ga Xe Lửa Cũ', 3, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Thác Cam Ly', 2, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Trung Tâm Thành Phố Đà Lạt', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Thác Cam Ly', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Thung Lũng Tình Yêu / Đồi Mộng Mơ', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Thung Lũng Vàng', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Núi LangBiang', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Khu du lịch Trúc Lâm Viên', 1, NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Area] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Country] ON 

INSERT [dbo].[tbl_Country] ([CountryId], [CountryName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Việt Nam', N'VIE', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Country] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Direction] ON 

INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hướng nội bộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Có cửa sổ (không hướng)', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Hướng phố', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Hướng núi', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Direction] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_District] ON 

INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Đà Lạt', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Bảo lộc', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Đức Trọng', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Di Linh', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Đức Trọng', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Đơn Dương', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Đam Rông', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Cát Tiên', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Lâm Hà', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'Lạc Dương', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'Đạ Tẻh', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Bảo Lâm', 1, NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_District] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Hotel] ON 

INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelServiceType], [HotelConvenientType], [Address], [Telephone], [Mobile], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Khách sạn Kings', 1, 2, 1, 4, 1, N'1,2,3,4', N'1,3', N'10 Bùi Thi Xuân, Phường 2, Đà Lạt Lâm Đồng', N'02471099966', NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Hotel] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] ON 

INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ bơi', 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Massage/Spa', 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Wifi miễn phí', 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Bãi đỗ xe', 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Giặt là', 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Lễ tân 24/24', 0, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] ON 

INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Ưu đãi đặc biệt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Đặt sớm giá tốt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Giá Shock giờ chót', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] ON 

INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 1, N'Một sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, 2, N'Hai sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, 3, N'Ba sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, 4, N'Bốn sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, 5, N'Năm sao', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] ON 

INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Bữa sáng miễn phí', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'3 Bữa ăn miễn phí', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Thêm giường phụ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelType] ON 

INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Khách sạn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Resort', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Homestay', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Căn hộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Biệt thự', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Du thuyền', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] ON 

INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'TTD', N'Thẻ tính dụng', 1, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'TND', N'Thẻ nội địa', 2, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'CK', N'Chuyển khoản', 3, 0, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Province] ON 

INSERT [dbo].[tbl_Province] ([ProvinceId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Đà lạt', N'DLA', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Province] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Role] ON 

INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (1, N'Quản trị')
INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (2, N'Nhân viên')
INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (3, N'Điều hành')
SET IDENTITY_INSERT [dbo].[tbl_Role] OFF
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'74a7d97d-8d5a-49f0-8865-a8f09f0f7e12', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-05-05T00:00:00.000' AS DateTime), NULL, NULL, 822000, 528000, 36, N'1', 1, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_User] ([UserId], [UserName], [FullName], [Email], [ApplicationId], [RoleId], [Password], [IsActive]) VALUES (N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', N'hieunv              ', N'Nguyễn Việt Hiếu', N'hieuviet.1103@gmail.com       ', 1, 1, N'M/tTWwIES8wxgueZZcLelw==', 1)
GO
ALTER TABLE [dbo].[tbl_Hotel] ADD  CONSTRAINT [DF_tbl_Hotel_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelComment] ADD  CONSTRAINT [DF_tbl_HotelComment_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelNotice] ADD  CONSTRAINT [DF_tbl_HotelNotice_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelServiceType] ADD  CONSTRAINT [DF_tbl_HotelServiceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] ADD  CONSTRAINT [DF_tbl_HotelSurchargePolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_HotelImage_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[tbl_Room] ADD  CONSTRAINT [DF_tbl_RoomType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_RoomPrice] ADD  CONSTRAINT [DF_tbl_Room_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_User] ADD  CONSTRAINT [DF_tbl_User_ApplicationId]  DEFAULT ((0)) FOR [ApplicationId]
GO
ALTER TABLE [dbo].[tbl_User] ADD  CONSTRAINT [DF_tbl_User_IsActive]  DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tbl_User_Role] ADD  CONSTRAINT [DF_tbl_User_Role_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tbl_Area]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Area_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Area] CHECK CONSTRAINT [FK_tbl_Area_tbl_District]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon] FOREIGN KEY([CouponId])
REFERENCES [dbo].[tbl_Coupon] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[tbl_Receipt] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt]
GO
ALTER TABLE [dbo].[tbl_District]  WITH CHECK ADD  CONSTRAINT [FK_tbl_District_tbl_Province] FOREIGN KEY([ProvinceId])
REFERENCES [dbo].[tbl_Province] ([ProvinceId])
GO
ALTER TABLE [dbo].[tbl_District] CHECK CONSTRAINT [FK_tbl_District_tbl_Province]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[tbl_Area] ([AreaId])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_Area]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_HotelRatingType] FOREIGN KEY([HotelRatingTypeId])
REFERENCES [dbo].[tbl_HotelRatingType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_HotelRatingType]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_HotelType] FOREIGN KEY([HotelTypeId])
REFERENCES [dbo].[tbl_HotelType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_HotelType]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] CHECK CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_HotelNotice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelNotice] CHECK CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] CHECK CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_Booking] FOREIGN KEY([BookingId])
REFERENCES [dbo].[tbl_Booking] ([BookingId])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_Booking]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType] FOREIGN KEY([PaymentType])
REFERENCES [dbo].[tbl_PaymentType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_Role]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_User]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Ảnh đại diện KS; 1: Ds ảnh Ks; 2Ảnh đại diện phòng; 3 Ds ảnh phòng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ProductImage', @level2type=N'COLUMN',@level2name=N'Type'
GO
