USE [master]
GO
/****** Object:  Database [Combo]    Script Date: 4/9/2021 8:40:44 PM ******/
CREATE DATABASE [Combo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Combo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Combo.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Combo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Combo_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Combo] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Combo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Combo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Combo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Combo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Combo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Combo] SET ARITHABORT OFF 
GO
ALTER DATABASE [Combo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Combo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Combo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Combo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Combo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Combo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Combo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Combo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Combo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Combo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Combo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Combo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Combo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Combo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Combo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Combo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Combo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Combo] SET RECOVERY FULL 
GO
ALTER DATABASE [Combo] SET  MULTI_USER 
GO
ALTER DATABASE [Combo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Combo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Combo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Combo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Combo] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Combo', N'ON'
GO
ALTER DATABASE [Combo] SET QUERY_STORE = OFF
GO
USE [Combo]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Combo]
GO
/****** Object:  Table [dbo].[tbl_Room]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Room](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomName] [nvarchar](255) NOT NULL,
	[AvailableRoom] [int] NOT NULL,
	[RoomArea] [float] NULL,
	[Direction] [nvarchar](255) NULL,
	[SingleBed] [int] NULL,
	[DoubleBed] [int] NULL,
	[MaxPeople] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[HotelServiceType] [nvarchar](255) NULL,
	[IsCancel] [bit] NULL,
	[IsSurcharge] [bit] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RoomType_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelRatingType]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelRatingType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberStar] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelRatingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Hotel]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Hotel](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelName] [nvarchar](255) NOT NULL,
	[CountryId] [int] NOT NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[AreaId] [nvarchar](255) NULL,
	[HotelRatingTypeId] [int] NULL,
	[HotelTypeId] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Address] [nvarchar](500) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Longitude] [nvarchar](255) NULL,
	[Latitude] [nvarchar](255) NULL,
	[Content] [nvarchar](max) NULL,
	[Email] [nvarchar](255) NULL,
	[Url] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[IsActive] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[PriceFrom] [float] NULL,
	[PricePromotion] [float] NULL,
	[CancelTime] [int] NULL,
 CONSTRAINT [PK_tbl_Hotel_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoomPrice]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoomPrice](
	[Id] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomDate] [datetime] NOT NULL,
	[AvailableRoom] [int] NULL,
	[HotelPriceTypeId] [int] NULL,
	[PriceContract] [float] NOT NULL,
	[PricePromotion] [float] NULL,
	[PercentPromotion] [float] NULL,
	[IsActive] [bit] NULL,
	[IsPromotion] [bit] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[Tax] [float] NULL,
	[TransactionCosts] [float] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Room_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_roomprice_profile]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Datnt>
-- Create date: <5/6/2021>
-- Description:	<Lấy danh sách roomprice ở trang Danh sách>
-- =============================================
CREATE VIEW [dbo].[vw_roomprice_profile]
AS
	SELECT rp.Id
      ,rp.RoomId
      ,rp.HotelId
      ,rp.RoomDate
      ,rp.AvailableRoom
      ,rp.HotelPriceTypeId
      ,rp.PriceContract
      ,rp.PricePromotion
      ,rp.PercentPromotion
      --,rp.HotelServiceType
      --,rp.IsCancel
      --,rp.IsSurcharge
      ,rp.IsPromotion
      --,rp.HotelConvenientType
      ,rp.IsDelete
      ,rp.Tax
      ,rp.TransactionCosts
      ,rp.UserCreate
      ,rp.DateCreate
      ,rp.UserUpdate
      ,rp.DateUpdate
	  ,r.RoomName
	  ,h.HotelName
	  ,h.ProvinceId
	  ,h.DistrictId
	  ,h.AreaId
	  ,h.Address
	  ,h.Latitude
	  ,h.Longitude
	  ,h.Content
	  ,h.Image
	  ,h.HotelConvenientType AS HotelConvenientTypeHotel
	  ,h.HotelRatingTypeId
	  ,h.HotelTypeId
	  ,r.Direction
	  --,d.DirectionName
	  ,r.SingleBed
	  ,r.DoubleBed
	  ,r.MaxPeople
	  ,r.HotelConvenientType AS HotelConvenientTypeRoom
	  ,r.HotelServiceType
      ,r.IsCancel
      ,r.IsSurcharge
	  ,r.Image AS ImageRoom
	  ,r.RoomArea
	  ,hrt.NumberStar
	FROM tbl_RoomPrice rp
	LEFT JOIN tbl_Room r ON r.Id = rp.RoomId
	LEFT JOIN tbl_Hotel h ON h.Id = r.HotelId
	--LEFT JOIN tbl_Direction d ON d.Id = r.Direction
	LEFT JOIN tbl_HotelRatingType hrt ON hrt.Id = h.HotelRatingTypeId
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[PhoneNumber] [varchar](15) NULL,
	[Email] [nvarchar](100) NULL,
	[Address] [nvarchar](250) NULL,
	[Type] [int] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsShowHeader] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HotelNews]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HotelNews](
	[HotelNewsID] [uniqueidentifier] NOT NULL,
	[NewsTypeID] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Brief] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Source] [nvarchar](max) NULL,
	[FileAttchment] [nvarchar](max) NULL,
	[OrderID] [int] NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NOT NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [int] NOT NULL,
	[ProvinceID] [int] NULL,
 CONSTRAINT [PK_dbo.HotelNews] PRIMARY KEY CLUSTERED 
(
	[HotelNewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsAndCategories]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsAndCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[NewsId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_NewsAndCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsCategory]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsCategory](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_NewsCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsTypes]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTypes](
	[NewsTypeID] [int] NOT NULL,
	[Code] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [datetime] NULL,
 CONSTRAINT [PK_dbo.NewsTypes] PRIMARY KEY CLUSTERED 
(
	[NewsTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SentCommunication]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SentCommunication](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[MailTo] [varchar](50) NOT NULL,
	[MailCc] [varchar](200) NOT NULL,
	[MailBcc] [varchar](200) NOT NULL,
	[MailFrom] [varchar](50) NOT NULL,
	[MailFromName] [nvarchar](50) NOT NULL,
	[IsSent] [bit] NULL,
	[IsCheck] [bit] NULL,
	[DateCreate] [datetime] NULL,
	[DateCheck] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[LogError] [nvarchar](max) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](15) NOT NULL,
	[CountryPhoneCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_SentCommunication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Area]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Area](
	[AreaId] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [nvarchar](255) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Area] PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_banner]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_banner](
	[banner_id] [int] IDENTITY(1,1) NOT NULL,
	[banner_name] [nvarchar](500) NULL,
	[banner_src] [nvarchar](500) NULL,
	[banner_desc] [nvarchar](500) NULL,
	[banner_pos] [tinyint] NULL,
	[banner_width] [int] NULL,
	[banner_height] [int] NULL,
	[banner_dura] [int] NULL,
	[banner_hyperlink] [nvarchar](500) NULL,
	[banner_target] [varchar](50) NULL,
	[isflash] [tinyint] NULL,
	[priority] [int] NULL,
	[active] [tinyint] NULL,
	[language_id] [tinyint] NULL,
	[visits] [int] NULL,
	[date_start] [datetime] NULL,
	[date_end] [datetime] NULL,
	[listpage] [nvarchar](10) NULL,
	[img_apple] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_banner] PRIMARY KEY CLUSTERED 
(
	[banner_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Banner_Position]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Banner_Position](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
 CONSTRAINT [PK_tbl_Banner_Position] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking](
	[BookingId] [uniqueidentifier] NOT NULL,
	[BookingCode] [varchar](6) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[CheckIn] [datetime] NOT NULL,
	[CheckOut] [datetime] NOT NULL,
	[DateCreate] [datetime] NOT NULL,
	[CODCancel] [datetime] NOT NULL,
	[CountDate] [int] NOT NULL,
	[RoomCount] [int] NOT NULL,
	[PersonCount] [int] NOT NULL,
	[Tax] [float] NULL,
	[Surcharge] [float] NULL,
	[Penalty] [float] NULL,
	[RoomAmount] [float] NOT NULL,
	[Discount] [float] NULL,
	[TotalAmount] [float] NOT NULL,
	[NetPrice] [float] NULL,
	[PaymentType] [int] NOT NULL,
	[IsInvoceExport] [bit] NOT NULL,
	[CustomerId] [uniqueidentifier] NULL,
	[CustomerEmail] [varchar](100) NULL,
	[CustomerPhoneNumber] [varchar](15) NULL,
	[CustomerName] [nvarchar](50) NULL,
	[CustomerProvinceId] [int] NULL,
	[CustomerDistrictId] [int] NULL,
	[CustomerAddress] [nvarchar](200) NULL,
	[CustomerNote] [nvarchar](max) NULL,
	[CompanyName] [nvarchar](200) NULL,
	[TaxCode] [varchar](50) NULL,
	[CompanyAddress] [nvarchar](200) NULL,
	[BillingAddress] [nvarchar](200) NULL,
	[CustomerCheckPayment] [bit] NOT NULL,
	[DateCheckPayment] [datetime] NULL,
	[IsPaymentSuccess] [bit] NOT NULL,
	[UserConfirm] [varchar](50) NULL,
	[DateConfirm] [datetime] NULL,
	[Status] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_Booking1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking2]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking2](
	[BookingId] [uniqueidentifier] NOT NULL,
	[RoomPriceId] [uniqueidentifier] NOT NULL,
	[RoomQuantity] [int] NULL,
	[Amount] [float] NULL,
	[AmountCancel] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Email] [nvarchar](255) NOT NULL,
	[PhoneNumber] [nvarchar](255) NOT NULL,
	[ContactName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[SaleId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Booking_1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_BookingAllot]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BookingAllot](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Status] [int] NULL,
	[UserCreate] [uniqueidentifier] NOT NULL,
	[DateCreate] [datetime] NULL,
	[DeadLine] [datetime] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_BookingAllot] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_BookingPoints]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BookingPoints](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Point] [float] NULL,
	[DateCreate] [datetime] NOT NULL,
	[UserCreate] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tbl_BookingPoints] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_BookingStatus]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BookingStatus](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Content]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Content](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ContenCode] [nvarchar](50) NOT NULL,
	[ContentName] [nvarchar](200) NULL,
	[ContentDetail] [ntext] NULL,
	[DateCreate] [datetime] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Content] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Country]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](50) NOT NULL,
	[Symbol] [varchar](3) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Coupon]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Coupon](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[ShortCode] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[EffectiveDate] [date] NULL,
	[ExpirationDate] [date] NULL,
	[Price] [float] NULL,
	[Image] [nvarchar](1024) NULL,
	[Type] [int] NOT NULL,
	[PointExchange] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CouponType]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CouponType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeName] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CouponType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CustomerNo] [nvarchar](255) NULL,
	[IsActive] [bit] NOT NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[Dob] [datetime] NULL,
	[Gender] [tinyint] NOT NULL,
	[Nationality] [varchar](3) NULL,
	[IdCard] [nvarchar](12) NULL,
	[DateOfIssue] [datetime] NULL,
	[PlaceOfIssue] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[CountryId] [int] NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Image] [nvarchar](500) NULL,
	[Note] [nvarchar](255) NULL,
	[Password] [nvarchar](127) NULL,
	[TotalPoint] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerCoupon]    Script Date: 4/9/2021 8:40:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerCoupon](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CouponId] [uniqueidentifier] NOT NULL,
	[ReceiptId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CustomerCoupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Direction]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Direction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DirectionName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Direction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_DiscountHistory]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_DiscountHistory](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Discount] [float] NULL,
	[CustomerName] [nvarchar](255) NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_DiscountHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_District]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_District](
	[DistrictId] [int] IDENTITY(1,1) NOT NULL,
	[DistrictName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_District] PRIMARY KEY CLUSTERED 
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelCancellationPolicy]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelCancellationPolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberDateCancel] [int] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[PercentAmount] [nchar](10) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelCancellationPolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelComment]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelComment](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[DateTimeComment] [datetime] NOT NULL,
	[PointLocation] [float] NULL,
	[PointServe] [float] NULL,
	[PointConvenient] [float] NULL,
	[PointCost] [float] NULL,
	[PointClean] [float] NULL,
	[Type] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelConvenientType]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelConvenientType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConvenientName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[Type] [int] NOT NULL,
	[Order] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelConvenientType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelNotice]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelNotice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelNotice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelPriceType]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelPriceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PriceTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelPriceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelServiceType]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelServiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelServiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelSurchargePolicy]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelSurchargePolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [nvarchar](255) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelSurchargePolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelType]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_PaymentType]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_PaymentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeCode] [nvarchar](255) NOT NULL,
	[PayTypeName] [nvarchar](255) NOT NULL,
	[Order] [int] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ProductImage]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductImage](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NULL,
	[RoomId] [uniqueidentifier] NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_tbl_HotelImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Province]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Province](
	[ProvinceId] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[ProvinceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Province] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Receipt]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Receipt](
	[Id] [uniqueidentifier] NOT NULL,
	[ReceiptNo] [nvarchar](255) NOT NULL,
	[ReceiptDate] [datetime] NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[Payer] [nvarchar](255) NULL,
	[PaymentType] [int] NOT NULL,
	[Company] [nvarchar](255) NULL,
	[VatCode] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Telephone] [nvarchar](255) NULL,
	[Mobile] [nvarchar](255) NULL,
	[Note] [nvarchar](255) NULL,
	[ReceiptOrder] [int] NULL,
	[Point] [float] NULL,
	[Status] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[UserId] [uniqueidentifier] NOT NULL,
	[LoginName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](127) NOT NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[CodeStaff] [nvarchar](255) NULL,
	[Active] [tinyint] NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User_Role]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User_Role](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_User_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Area] ADD  CONSTRAINT [DF_tbl_Area_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_isflash]  DEFAULT ((0)) FOR [isflash]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_priority]  DEFAULT ((1)) FOR [priority]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_active]  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_language_id]  DEFAULT ((2)) FOR [language_id]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking1_IsInvoceExport]  DEFAULT ((0)) FOR [IsInvoceExport]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking_CustomerCheckPayment]  DEFAULT ((0)) FOR [CustomerCheckPayment]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking_IsPaymentSuccess]  DEFAULT ((0)) FOR [IsPaymentSuccess]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking1_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Booking2] ADD  CONSTRAINT [DF_tbl_Booking_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Country] ADD  CONSTRAINT [DF_tbl_Country_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Coupon] ADD  CONSTRAINT [DF_tbl_Coupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CouponType] ADD  CONSTRAINT [DF_tbl_CouponType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] ADD  CONSTRAINT [DF_tbl_CustomerCoupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Direction] ADD  CONSTRAINT [DF_tbl_Direction_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_District] ADD  CONSTRAINT [DF_tbl_District_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Hotel] ADD  CONSTRAINT [DF_tbl_Hotel_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] ADD  CONSTRAINT [DF_tbl_HotelCancellationPolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelComment] ADD  CONSTRAINT [DF_tbl_HotelComment_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelConvenientType] ADD  CONSTRAINT [DF_tbl_HotelConvenientType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelNotice] ADD  CONSTRAINT [DF_tbl_HotelNotice_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelPriceType] ADD  CONSTRAINT [DF_tbl_HotelPriceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelRatingType] ADD  CONSTRAINT [DF_tbl_HotelRatingType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelServiceType] ADD  CONSTRAINT [DF_tbl_HotelServiceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] ADD  CONSTRAINT [DF_tbl_HotelSurchargePolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelType] ADD  CONSTRAINT [DF_tbl_HotelType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_PaymentType] ADD  CONSTRAINT [DF_tbl_PaymentType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_HotelImage_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_ProductImage_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Province] ADD  CONSTRAINT [DF_tbl_Province_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Room] ADD  CONSTRAINT [DF_tbl_RoomType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_RoomPrice] ADD  CONSTRAINT [DF_tbl_Room_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_User_Role] ADD  CONSTRAINT [DF_tbl_User_Role_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[HotelNews]  WITH CHECK ADD  CONSTRAINT [FK_HotelNews_NewsTypes] FOREIGN KEY([NewsTypeID])
REFERENCES [dbo].[NewsTypes] ([NewsTypeID])
GO
ALTER TABLE [dbo].[HotelNews] CHECK CONSTRAINT [FK_HotelNews_NewsTypes]
GO
ALTER TABLE [dbo].[tbl_Area]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Area_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Area] CHECK CONSTRAINT [FK_tbl_Area_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Coupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Coupon_tbl_CouponType] FOREIGN KEY([Type])
REFERENCES [dbo].[tbl_CouponType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Coupon] CHECK CONSTRAINT [FK_tbl_Coupon_tbl_CouponType]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon] FOREIGN KEY([CouponId])
REFERENCES [dbo].[tbl_Coupon] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[tbl_Receipt] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt]
GO
ALTER TABLE [dbo].[tbl_District]  WITH CHECK ADD  CONSTRAINT [FK_tbl_District_tbl_Province] FOREIGN KEY([ProvinceId])
REFERENCES [dbo].[tbl_Province] ([ProvinceId])
GO
ALTER TABLE [dbo].[tbl_District] CHECK CONSTRAINT [FK_tbl_District_tbl_Province]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] CHECK CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelNotice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelNotice] CHECK CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] CHECK CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_Province]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Province_tbl_Country1] FOREIGN KEY([CountryId])
REFERENCES [dbo].[tbl_Country] ([CountryId])
GO
ALTER TABLE [dbo].[tbl_Province] CHECK CONSTRAINT [FK_tbl_Province_tbl_Country1]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_Booking] FOREIGN KEY([BookingId])
REFERENCES [dbo].[tbl_Booking2] ([BookingId])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_Booking]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType] FOREIGN KEY([PaymentType])
REFERENCES [dbo].[tbl_PaymentType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_RoomPrice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RoomPrice_tbl_Room] FOREIGN KEY([RoomId])
REFERENCES [dbo].[tbl_Room] ([Id])
GO
ALTER TABLE [dbo].[tbl_RoomPrice] CHECK CONSTRAINT [FK_tbl_RoomPrice_tbl_Room]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_Role]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_User]
GO
/****** Object:  StoredProcedure [dbo].[sp_hotel_list]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Datnt>
-- Create date: <5/6/2021>
-- Description:	<Lấy danh sách hotel ở trang Danh sách hoặc trang Detail (check biến @typeInput chuyền vào)>
-- =============================================
CREATE PROCEDURE [dbo].[sp_hotel_list]
	-- Add the parameters for the stored procedure here
	@typeInput INT = NUll, -- 0: Hotel; 1: Province; 
	@itemId nvarchar(255) = NULL, 
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@numberRoom int = NULL,
	@numberAdul INT = NULL,
	@numberChildren INT = NULL,
	@priceFrom float = NULL,
	@priceTo float = NULL,
	@hotelPriceType NVARCHAR(100) = NULL,
	@hotelRatingType NVARCHAR(100) = NULL,
	@hotelType INT = NULL,
	@hotelServiceType nvarchar(255) = NULL,
	@hotelConvenientType nvarchar(255) = NULL,
	@hotelArea INT = NULL,
	@orderPrice INT = NULL, -- 0: Tang : giam
	@orderRating INT = NULL -- 0: Tang : giam
AS
BEGIN
	SET NOCOUNT ON;

	

	SELECT ROW_NUMBER () OVER (ORDER BY vrp.DateCreate DESC) STT
		  ,vrp.Id
		  ,vrp.RoomId
		  ,vrp.HotelId
		  ,vrp.RoomDate
		  ,vrp.AvailableRoom
		  ,vrp.HotelPriceTypeId
		  ,vrp.PriceContract
		  ,vrp.PricePromotion
		  ,vrp.PercentPromotion
		  ,vrp.HotelServiceType
		  ,vrp.IsCancel
		  ,vrp.IsSurcharge
		  ,vrp.IsPromotion
		  --,vrp.HotelConvenientType
		  ,vrp.IsDelete
		  ,vrp.Tax
		  ,vrp.TransactionCosts
		  ,vrp.UserCreate
		  ,vrp.DateCreate
		  ,vrp.UserUpdate
		  ,vrp.DateUpdate
		  ,vrp.RoomName
		  ,vrp.HotelName
		  ,vrp.ProvinceId
		  ,vrp.DistrictId
		  ,vrp.AreaId
		  ,vrp.Address
		  ,vrp.Latitude
		  ,vrp.Longitude
		  ,vrp.Content
		  ,vrp.Image
		  ,vrp.ImageRoom
		  ,vrp.RoomArea
		  ,vrp.HotelConvenientTypeHotel
		  ,vrp.HotelRatingTypeId
		  ,vrp.NumberStar
		  ,vrp.HotelTypeId
		  ,vrp.Direction
		  --,vrp.DirectionName
		  ,vrp.SingleBed
		  ,vrp.DoubleBed
		  ,vrp.MaxPeople
		  ,vrp.HotelConvenientTypeRoom
		INTO #KhachSanGia
		FROM vw_roomprice_profile vrp 
		WHERE vrp.IsDelete <> 1

	IF(@typeInput = 0)
	BEGIN
		SELECT
			temp.RoomId
			,temp.HotelId
			,temp.AvailableRoom
			,temp.IsCancel
			,temp.HotelServiceType
			,temp.HotelConvenientTypeHotel
			,temp.RoomName
			,temp.HotelName
			,temp.Address
			,temp.Latitude
			,temp.Longitude
			,temp.Content
			,temp.Image
			,temp.ImageRoom
			,temp.Direction
			--,temp.DirectionName
			,temp.SingleBed
			,temp.DoubleBed
			,temp.MaxPeople
			,temp.RoomArea
			,(SUM(temp.PriceContract)/COUNT(temp.Id)) AS PriceContract
			,(SUM(temp.PricePromotion)/COUNT(temp.Id)) AS PricePromotion
		FROM #KhachSanGia temp
		WHERE temp.HotelId = @itemId
		AND temp.RoomDate >= @startDate AND temp.RoomDate < @endDate
		GROUP BY RoomId, HotelId, HotelName, RoomName, temp.AvailableRoom, temp.IsCancel, temp.HotelServiceType, temp.HotelConvenientTypeHotel, temp.Address, temp.Latitude, temp.Longitude, temp.Content, temp.Image, temp.Direction, temp.SingleBed, temp.DoubleBed, temp.MaxPeople, temp.ImageRoom, temp.RoomArea
	END
	ELSE IF(@typeInput = 1)
	BEGIN
		SELECT
			temp.RoomId
			,temp.HotelId
			,temp.HotelRatingTypeId
			,temp.NumberStar
			,temp.HotelTypeId
			,temp.AvailableRoom
			,temp.IsCancel
			,temp.HotelServiceType
			,temp.HotelConvenientTypeHotel
			,temp.RoomName
			,temp.HotelName
			,temp.Address
			,temp.Latitude
			,temp.Longitude
			,temp.Content
			,temp.Image
			,temp.Direction
			--,temp.DirectionName
			,temp.SingleBed
			,temp.DoubleBed
			,temp.MaxPeople
			,(SUM(temp.PriceContract)/COUNT(temp.Id)) AS PriceContract
			,(SUM(temp.PricePromotion)/COUNT(temp.Id)) AS PricePromotion
		INTO #tableRusult
		FROM #KhachSanGia temp 
		WHERE (@itemId IS NULL OR temp.ProvinceId = @itemId) 
		AND temp.RoomDate >= @startDate AND temp.RoomDate < @endDate
		AND (@hotelPriceType IS NULL OR temp.HotelPriceTypeId IN (SELECT value FROM STRING_SPLIT(@hotelPriceType, ',')))
		AND (@hotelRatingType IS NULL OR temp.HotelRatingTypeId IN (SELECT value FROM STRING_SPLIT(@hotelRatingType, ',')))
		AND (@hotelType IS NULL OR temp.HotelTypeId = @hotelType)
		AND (@hotelServiceType IS NULL OR temp.HotelServiceType IN (SELECT value FROM STRING_SPLIT(@hotelServiceType, ',')))
		AND (@hotelConvenientType IS NULL OR temp.HotelConvenientTypeHotel IN (SELECT value FROM STRING_SPLIT(@hotelConvenientType, ',')))
		AND (@hotelArea IS NULL OR temp.AreaId = CAST(@hotelArea as nvarchar(255)))
		AND (@priceFrom IS NULL OR temp.PriceContract >= @priceFrom)
		AND (@priceTo IS NULL OR temp.PriceContract <= @priceTo)
		GROUP BY RoomId, HotelId, temp.HotelRatingTypeId, temp.HotelTypeId, HotelName, RoomName, temp.AvailableRoom, temp.IsCancel, temp.HotelServiceType, temp.HotelConvenientTypeHotel, temp.Address, temp.Latitude, temp.Longitude, temp.Content, temp.Image, temp.Direction, temp.SingleBed, temp.DoubleBed, temp.MaxPeople, temp.HotelRatingTypeId, temp.NumberStar--, temp.DirectionName

		--Lay ra record co gia thap nhat cua moi 1 khach san
		SELECT * FROM #tableRusult t1
		INNER JOIN (
			SELECT HotelId, MIN(PriceContract) AS MinV 
			FROM #tableRusult 
			GROUP BY HotelId
		) t2 ON t1.HotelId = t2.HotelId AND t1.PriceContract = t2.MinV
		DROP TABLE #tableRusult
	END

	DROP TABLE #KhachSanGia
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_hotel_review]    Script Date: 4/9/2021 8:40:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Datnt>
-- Create date: <19/06/2021>
-- Description:	<Lấy ra đánh giá chung bình của một khách sạn>
-- Tra ra 6 record trong do lan luoc la: TatCa; CongTac; CapDoi; GiaDinh; BanBe; CaNhan
-- =============================================
CREATE PROCEDURE [dbo].[sp_hotel_review]
	@id nvarchar(255) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	SELECT COUNT(*) AS NumAll
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 0) AS NumCongTac 
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 1) AS NumCapDoi 
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 2) AS NumGiaDinh 
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 3) AS NumBanBe
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 4) AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM tbl_HotelComment WHERE HotelId = @id
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM tbl_HotelComment WHERE HotelId = @id AND Type = 0
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM tbl_HotelComment WHERE HotelId = @id AND Type = 1
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM tbl_HotelComment WHERE HotelId = @id AND Type = 2
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM tbl_HotelComment WHERE HotelId = @id AND Type = 3
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM tbl_HotelComment WHERE HotelId = @id AND Type = 4
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Thời gian huỷ sau khi đặt booking' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Hotel', @level2type=N'COLUMN',@level2name=N'CancelTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Công tác; 1: Cặp đôi; 2: Gia Đình; 3: Ban Be; 4: Cá nhân' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_HotelComment', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Sảnh lễ tân; 2: Ảnh ngoại cảnh; 3: Ảnh khác; 4: Ảnh Phòng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ProductImage', @level2type=N'COLUMN',@level2name=N'Type'
GO
USE [master]
GO
ALTER DATABASE [Combo] SET  READ_WRITE 
GO
