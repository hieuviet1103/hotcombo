USE [master]
GO
/****** Object:  Database [Hotel]    Script Date: 19/5/2021 11:10:37 PM ******/
CREATE DATABASE [Hotel]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Hotel', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\Hotel.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Hotel_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\Hotel_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Hotel] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Hotel].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Hotel] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Hotel] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Hotel] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Hotel] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Hotel] SET ARITHABORT OFF 
GO
ALTER DATABASE [Hotel] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Hotel] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Hotel] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Hotel] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Hotel] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Hotel] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Hotel] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Hotel] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Hotel] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Hotel] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Hotel] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Hotel] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Hotel] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Hotel] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Hotel] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Hotel] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Hotel] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Hotel] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Hotel] SET  MULTI_USER 
GO
ALTER DATABASE [Hotel] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Hotel] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Hotel] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Hotel] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Hotel] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Hotel] SET QUERY_STORE = OFF
GO
USE [Hotel]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Hotel]
GO
/****** Object:  Table [dbo].[HotelNews]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HotelNews](
	[HotelNewsID] [uniqueidentifier] NOT NULL,
	[NewsTypeID] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Brief] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Source] [nvarchar](max) NULL,
	[FileAttchment] [nvarchar](max) NULL,
	[OrderID] [int] NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NOT NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.HotelNews] PRIMARY KEY CLUSTERED 
(
	[HotelNewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsTypes]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTypes](
	[NewsTypeID] [int] NOT NULL,
	[Code] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [datetime] NULL,
 CONSTRAINT [PK_dbo.NewsTypes] PRIMARY KEY CLUSTERED 
(
	[NewsTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SentCommunication]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SentCommunication](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[MailTo] [varchar](50) NOT NULL,
	[MailCc] [varchar](200) NOT NULL,
	[MailBcc] [varchar](200) NOT NULL,
	[MailFrom] [varchar](50) NOT NULL,
	[MailFromName] [nvarchar](50) NOT NULL,
	[IsSent] [bit] NULL,
	[IsCheck] [bit] NULL,
	[DateCreate] [datetime] NULL,
	[DateCheck] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[LogError] [nvarchar](max) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](15) NOT NULL,
	[CountryPhoneCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_SentCommunication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Area]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Area](
	[AreaId] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [nvarchar](255) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Area] PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking](
	[BookingId] [uniqueidentifier] NOT NULL,
	[RoomPriceId] [uniqueidentifier] NOT NULL,
	[RoomQuantity] [int] NULL,
	[Amount] [float] NULL,
	[AmountCancel] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Email] [nvarchar](255) NOT NULL,
	[PhoneNumber] [nvarchar](255) NOT NULL,
	[ContactName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[SaleId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Booking_1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Country]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Country](
	[CountryId] [varchar](3) NOT NULL,
	[CountryName] [nvarchar](50) NOT NULL,
	[Symbol] [varchar](3) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Coupon]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Coupon](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[ShortCode] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[EffectiveDate] [date] NULL,
	[ExpirationDate] [date] NULL,
	[Price] [float] NULL,
	[Image] [nvarchar](1024) NULL,
	[Type] [int] NOT NULL,
	[PointExchange] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CouponType]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CouponType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeName] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CouponType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CustomerNo] [nvarchar](255) NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[Dob] [datetime] NULL,
	[Gender] [tinyint] NOT NULL,
	[Nationality] [varchar](3) NULL,
	[IdCard] [nvarchar](12) NULL,
	[DateOfIssue] [datetime] NULL,
	[PlaceOfIssue] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[CountryId] [int] NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Note] [nvarchar](255) NULL,
	[Password] [nvarchar](127) NULL,
	[TotalPoint] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerCoupon]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerCoupon](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CouponId] [uniqueidentifier] NOT NULL,
	[ReceiptId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CustomerCoupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Direction]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Direction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DirectionName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Direction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_District]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_District](
	[DistrictId] [int] IDENTITY(1,1) NOT NULL,
	[DistrictName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_District] PRIMARY KEY CLUSTERED 
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Hotel]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Hotel](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[AreaId] [int] NULL,
	[HotelRatingTypeId] [int] NULL,
	[HotelTypeId] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Address] [nvarchar](500) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Longitude] [nvarchar](255) NULL,
	[Latitude] [nvarchar](255) NULL,
	[Content] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Hotel_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelCancellationPolicy]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelCancellationPolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberDateCancel] [int] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[PercentAmount] [nchar](10) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelCancellationPolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelComment]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelComment](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[DateTimeComment] [datetime] NOT NULL,
	[PointLocation] [float] NULL,
	[PointServe] [float] NULL,
	[PointConvenient] [float] NULL,
	[PointCost] [float] NULL,
	[PointClean] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelConvenientType]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelConvenientType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConvenientName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[Type] [int] NOT NULL,
	[Order] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelConvenientType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelNotice]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelNotice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelNotice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelPriceType]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelPriceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PriceTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelPriceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelRatingType]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelRatingType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberStar] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelRatingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelServiceType]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelServiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelServiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelSurchargePolicy]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelSurchargePolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [nvarchar](255) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelSurchargePolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelType]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_PaymentType]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_PaymentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeCode] [nvarchar](255) NOT NULL,
	[PayTypeName] [nvarchar](255) NOT NULL,
	[Order] [int] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ProductImage]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductImage](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NULL,
	[RoomType] [uniqueidentifier] NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_HotelImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Province]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Province](
	[ProvinceId] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [varchar](3) NOT NULL,
	[ProvinceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Province] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Receipt]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Receipt](
	[Id] [uniqueidentifier] NOT NULL,
	[ReceiptNo] [nvarchar](255) NOT NULL,
	[ReceiptDate] [datetime] NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[Payer] [nvarchar](255) NULL,
	[PaymentType] [int] NOT NULL,
	[Company] [nvarchar](255) NULL,
	[VatCode] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Telephone] [nvarchar](255) NULL,
	[Mobile] [nvarchar](255) NULL,
	[Note] [nvarchar](255) NULL,
	[ReceiptOrder] [int] NULL,
	[Point] [float] NULL,
	[Status] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Room]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Room](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomName] [nvarchar](255) NOT NULL,
	[AvailableRoom] [int] NOT NULL,
	[RoomArea] [float] NULL,
	[Direction] [int] NULL,
	[SingleBed] [int] NULL,
	[DoubleBed] [int] NULL,
	[MaxPeople] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RoomType_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoomPrice]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoomPrice](
	[Id] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[RoomDate] [datetime] NOT NULL,
	[AvailableRoom] [int] NULL,
	[HotelPriceTypeId] [int] NULL,
	[PriceContract] [float] NOT NULL,
	[PricePromotion] [float] NULL,
	[PercentPromotion] [float] NULL,
	[HotelServiceType] [nvarchar](255) NULL,
	[IsCancel] [bit] NULL,
	[IsSurcharge] [bit] NULL,
	[IsPromotion] [bit] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[Tax] [float] NULL,
	[TransactionCosts] [float] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Room_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[UserId] [uniqueidentifier] NOT NULL,
	[LoginName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](127) NOT NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[CodeStaff] [nvarchar](255) NULL,
	[Active] [tinyint] NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User_Role]    Script Date: 19/5/2021 11:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User_Role](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_User_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'cd6dfbac-874f-49d6-a594-3b6cececa2fa', 2, N'Tin tức du lịch mùa covid', N'Tin tức du lịch mùa covid năm thứ 2 liên tiếp', N'<html>
<head>
	<title></title>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="2">
			<h1>Cần bản đồ an to&agrave;n để du lịch &#39;sống chung với dịch&#39;</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2">Đ&agrave;o Loan</td>
		</tr>
		<tr>
			<td colspan="2">Thứ&nbsp;Ba,&nbsp;&nbsp;4/5/2021,&nbsp;17:13&nbsp;</td>
		</tr>
		<tr>
			<td style="vertical-align:top">&nbsp;</td>
			<td style="vertical-align:top">
			<p>(KTSG Online) - Khi dịch Covid-19 c&oacute; nguy cơ b&ugrave;ng l&ecirc;n như hiện nay, việc x&acirc;y dựng bản đồ du lịch an to&agrave;n cho du kh&aacute;ch v&agrave; doanh nghiệp tiếp tục được đặt ra như một trong những yếu tố gi&uacute;p ng&agrave;nh du lịch n&oacute;i chung đủ khả năng để c&oacute; thể &quot;sống chung với dịch&quot;.</p>

			<p><em>KTSG Online</em>&nbsp;đ&atilde; phỏng vấn &ocirc;ng Nguyễn Quốc Kỳ, Chủ tịch Vietravel Holdings về vấn đề n&agrave;y.</p>

			<table align="middle" cellpadding="0" cellspacing="0" style="width:99%">
				<tbody>
					<tr>
						<td><img alt="" src="https://cdn.thesaigontimes.vn/Uploads/Articles/315994/75248_dsc07591.jpg" /></td>
					</tr>
					<tr>
						<td>Du kh&aacute;ch ở Y&ecirc;n Tử, tỉnh Quảng Ninh. Ảnh: Đ&agrave;o Loan</td>
					</tr>
				</tbody>
			</table>

			<p>KTSG Online: Tại sao &ocirc;ng cho rằng với diễn biến phức tạp v&agrave; kh&ocirc;n lường của dịch bệnh như hiện nay, ng&agrave;nh du lịch phải c&oacute; bản đồ an to&agrave;n th&igrave; mới c&oacute; thể &quot;sống chung với dịch&quot;?</p>

			<p><strong>- &Ocirc;NG NGUYỄN QUỐC KỲ</strong>: C&oacute; thể thấy, cả doanh nghiệp lẫn du kh&aacute;ch đều rất bối rối trong những lần b&ugrave;ng dịch vừa qua.</p>

			<p>Mỗi khi c&oacute; ca nhiễm, d&ugrave; chỉ l&agrave; một ca ở một khu vực ri&ecirc;ng lẻ l&agrave; kh&aacute;ch h&agrave;ng đ&atilde; cực kỳ hoang mang v&igrave; th&ocirc;ng tin về dịch bệnh tr&agrave;n ngập tr&ecirc;n c&aacute;c phương tiện truyền th&ocirc;ng, mạng x&atilde; hội, truyền miệng...&nbsp; Những điều n&agrave;y khiến nhiều người tưởng chừng như tất cả điểm đến đều c&oacute; dịch.</p>

			<p>Chẳng hạn, khi n&oacute;i H&agrave; Nội c&oacute; dịch th&igrave; ngay lập tức nhiều kh&aacute;ch hủy tour đến đ&acirc;y v&igrave; cho rằng kh&ocirc;ng an to&agrave;n trong khi thực tế th&agrave;nh phố n&agrave;y rất rộng lớn, c&oacute; rất nhiều điểm vẫn an to&agrave;n nhưng th&ocirc;ng tin n&agrave;y lại lọt thỏm trong d&ograve;ng th&ocirc;ng tin về dịch bệnh, khiến du kh&aacute;ch lo sợ.</p>

			<p>T&igrave;nh trạng hủy, ho&atilde;n dịch vụ trong những đợt b&ugrave;ng dịch vừa qua cũng c&oacute; nguy&ecirc;n nh&acirc;n từ t&acirc;m l&yacute; n&agrave;y. V&igrave; thế, nếu c&oacute; bản đồ du lịch an to&agrave;n, cho người d&acirc;n biết nơi n&agrave;o đang c&oacute; dịch kh&ocirc;ng n&ecirc;n đến, nơi n&agrave;o đang trong diện cảnh b&aacute;o, điểm n&agrave;o vẫn an to&agrave;n để vui chơi th&igrave; sẽ gi&uacute;p ổn định t&acirc;m l&yacute; kh&aacute;ch h&agrave;ng, đồng thời cũng gi&uacute;p cơ quan chức năng ngăn những tin đồn thất thiệt về dịch bệnh.</p>

			<p>C&oacute; bản đồ n&agrave;y, c&aacute;c nh&agrave; điều h&agrave;nh dịch vụ sẽ được hỗ trợ rất nhiều. Ch&uacute;ng t&ocirc;i sẽ kh&ocirc;ng c&ograve;n cảnh l&uacute;ng t&uacute;ng v&agrave; bị động khi dự b&aacute;o t&igrave;nh h&igrave;nh để sắp xếp dịch vụ m&agrave; c&oacute; thể chủ động biết được nơi n&agrave;o c&oacute; thể đưa kh&aacute;ch đến, nơi n&agrave;o cần phải gi&atilde;n ra để t&iacute;nh to&aacute;n kế hoạch kinh doanh.</p>

			<p>Việc n&agrave;y cũng sẽ gi&uacute;p doanh nghiệp tiết kiệm chi ph&iacute; v&agrave; giữ d&ograve;ng tiền. Nếu cứ đặt dịch vụ rồi lại phải hủy, ho&atilde;n đột ngột như thời gian qua th&igrave; doanh nghiệp sẽ kh&oacute; khăn v&igrave; số tiền đặt cọc bị ng&acirc;m rất l&acirc;u, trong khi tiền mặt lại đang thiếu.</p>

			<p>Du kh&aacute;ch y&ecirc;n t&acirc;m, doanh nghiệp chủ động điều h&agrave;nh dịch vụ th&igrave; du lịch mới vận h&agrave;nh được trong l&uacute;c dịch vẫn c&ograve;n như thế n&agrave;y.</p>

			<p>KTSG Online: Hiện nay, doanh nghiệp du lịch lấy th&ocirc;ng tin từ đ&acirc;u để chuẩn bị kế hoạch ứng ph&oacute; khi b&ugrave;ng dịch?</p>

			<table align="right" cellpadding="0" cellspacing="0" style="width:99%">
				<tbody>
					<tr>
						<td><img alt="" src="https://cdn.thesaigontimes.vn/Uploads/Articles/315994/2b333_anhky_1.jpg" /></td>
					</tr>
					<tr>
						<td>&Ocirc;ng Nguyễn Quốc Kỳ</td>
					</tr>
				</tbody>
			</table>

			<p>- Ch&uacute;ng t&ocirc;i phải lấy từ nhiều nguồn, trong đ&oacute; chủ yếu l&agrave; đợi th&ocirc;ng b&aacute;o từ c&aacute;c địa phương để biết c&aacute;c điểm du lịch, c&aacute;c dịch vụ cho du kh&aacute;ch c&oacute; được ph&eacute;p hoạt động hay kh&ocirc;ng.</p>

			<p>Tuy nhi&ecirc;n, kh&ocirc;ng phải l&uacute;c n&agrave;o th&ocirc;ng b&aacute;o cũng đến đ&uacute;ng thời điểm cho n&ecirc;n đ&atilde; c&oacute; trường hợp, c&oacute; c&ocirc;ng ty đưa kh&aacute;ch đến nơi rồi mới biết điểm đến đ&atilde; đ&oacute;ng cửa.</p>

			<p>KTSG Online: Theo &ocirc;ng, bản đồ du lịch n&ecirc;n l&agrave;m như thế n&agrave;o v&agrave; cơ quan n&agrave;o n&ecirc;n cầm trịch việc n&agrave;y?</p>

			<p>- Về mặt kỹ thuật, c&oacute; thể thể hiện bằng trang web hoặc bằng phần mềm ứng dụng. Với nội dung thể hiện, để đảm bảo t&iacute;nh ch&iacute;nh x&aacute;c v&agrave; tổng qu&aacute;t tr&ecirc;n cả nước, Ban Chỉ đạo quốc gia ph&ograve;ng chống dịch Covid-19 n&ecirc;n l&agrave; đơn vị cập nhật th&ocirc;ng tin về dịch bệnh c&ugrave;ng những cảnh b&aacute;o li&ecirc;n quan.</p>

			<p>Ban chỉ đạo cũng c&oacute; thể ủy quyền cho một đơn vị chuy&ecirc;n tr&aacute;ch như Bộ Y tế cập nhật th&ocirc;ng tin. Hiện nay, Bộ Y tế đang cập nhật th&ocirc;ng tin c&aacute;c ca nhiễm mới 2 lần/ng&agrave;y. Đ&acirc;y cũng l&agrave; c&aacute;ch c&oacute; thể tham khảo khi thực hiện bản đồ.</p>

			<p>C&oacute; thể h&igrave;nh dung bản đồ du lịch an to&agrave;n như bản đồ cảnh b&aacute;o c&aacute;c điểm ch&aacute;y rừng m&agrave; cơ quan kiểm l&acirc;m đ&atilde; l&agrave;m. Nh&igrave;n v&agrave;o bản đồ, du kh&aacute;ch sẽ thấy tổng thể cả điểm đến v&agrave; thấy được trong đ&oacute; nơi n&agrave;o l&agrave; v&ugrave;ng đỏ kh&ocirc;ng n&ecirc;n đến, nơi n&agrave;o c&oacute; nguy cơ vừa phải, nơi n&agrave;o ho&agrave;n to&agrave;n xanh để c&oacute; thể y&ecirc;n t&acirc;m du lịch...</p>

			<p>Điều quan trọng l&agrave; th&ocirc;ng tin cần phải cập nhật li&ecirc;n tục. Với sự hỗ trợ của c&ocirc;ng nghệ, c&aacute;c tỉnh, th&agrave;nh tr&ecirc;n cả nước cũng c&oacute; thể cập nhật th&ocirc;ng tin l&ecirc;n đ&acirc;y để du kh&aacute;ch biết t&igrave;nh h&igrave;nh dịch ở tỉnh A đang như thế n&agrave;o, tỉnh B đang cho mở dịch vụ n&agrave;o, cấm dịch vụ n&agrave;o, khu vực n&agrave;o đang gi&atilde;n c&aacute;ch, nơi n&agrave;o buộc phải đeo khẩu trang... Điều n&agrave;y thực sự rất quan trọng v&agrave; hữu &iacute;ch.</p>

			<p>T&ocirc;i cho rằng nếu l&agrave;m tốt th&igrave; bản đồ du lịch an to&agrave;n kh&ocirc;ng chỉ l&agrave; k&ecirc;nh cung cấp th&ocirc;ng tin quan trọng cho du kh&aacute;ch trong đại dịch m&agrave; c&ograve;n l&agrave; sau n&agrave;y, khi mở cửa thị trường. Khi muốn đi du lịch Việt Nam hay khi c&oacute; sự cố, dịch bệnh, du kh&aacute;ch nước ngo&agrave;i c&oacute; thể v&agrave;o để t&igrave;m th&ocirc;ng tin.</p>

			<p>Th&ecirc;m v&agrave;o đ&oacute;, bản đồ n&agrave;y cũng c&oacute; thể ph&aacute;t triển th&ecirc;m tiện &iacute;ch như l&agrave; bản đồ th&ocirc;ng b&aacute;o mật độ kh&aacute;ch, gi&uacute;p điểm đến giải tỏa qu&aacute; tải trong c&aacute;c cao điểm du lịch. Chẳng hạn như việc Đ&agrave; Lạt tắc nghẽn trong dịp lễ vừa rồi, nếu c&oacute; th&ocirc;ng tin về sức chứa của điểm đến, của c&aacute;c cơ sở dịch vụ ngay tại thời điểm đ&oacute; c&ugrave;ng khuyến c&aacute;o của cơ quan chức năng th&igrave; chắc rằng nhiều người sẽ chọn hướng kh&aacute;c để tr&aacute;nh tắc nghẽn, gi&uacute;p điểm đến giảm &aacute;p lực.</p>
			</td>
		</tr>
	</tbody>
</table>
<!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', NULL, NULL, N'19052021_104053yh_advertising1617262142.jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:40:37.950' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:40:53.650' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0f2', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_080417yh_img-1.jpeg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:16:14.710' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0f3', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_091154yh_o-d1-(1).jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:16:09.707' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0f4', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_091513yh_advertising1617262142.jpg', 3, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:15:36.290' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0fd', 2, N'Chương trình Ưu đãi VNPay', N'Nhằm gia tăng tiện tích cho khách hàng khi mua tour, Công ty Du lịch Vietravel tổ chức chương trình ưu đãi hấp dẫn cho khách hàng khi thanh toán bằng dịch vụ VNPay QR code.', N'<html>
<head>
	<title></title>
</head>
<body>
<p style="text-align:center"><span style="color:#d35400"><strong><span style="font-size:28px">Thể lệ chương tr&igrave;nh ưu đ&atilde;i VNPAY</span></strong></span></p>

<p><br />
05/10/2020<br />
Nhằm gia tăng tiện t&iacute;ch cho kh&aacute;ch h&agrave;ng khi mua tour, C&ocirc;ng ty Du lịch Vietravel tổ chức chương tr&igrave;nh ưu đ&atilde;i hấp dẫn cho kh&aacute;ch h&agrave;ng khi thanh to&aacute;n bằng dịch vụ VNPay QR code.<br />
&nbsp;</p>

<figure class="easyimage easyimage-full"><img alt="" src="blob:http://localhost:13863/087dd565-d7ef-414d-b156-977f5f29b0c0" width="560" />
<figcaption></figcaption>
</figure>

<p><img src="http://localhost:13863/FileUploads/News/18052021_104109yb_shutterstock.jpg" /></p>

<p><br />
<br />
Lưu<br />
1. Khi mua c&aacute;c sản phẩm tại Vietravel v&agrave; thanh to&aacute;n th&agrave;nh c&ocirc;ng bằng dịch vụ VNPAYQR (sử dụng t&iacute;nh năng QR Pay) tr&ecirc;n ứng dụng Mobile Banking của c&aacute;c ng&acirc;n h&agrave;ng: Agribank, BIDV, VietinBank, Vietcombank, SCB, IVB, ABBank, Eximbank, HDBank, NCB, Nam A Bank, VietBank, BIDC, SaiGonBank, SeABank, Ocean Bank, KienLongBank: Kh&aacute;ch h&agrave;ng nhập m&atilde; khuyến mại VTR25 sẽ được giảm 5% gi&aacute; trị giao dịch, tối đa 100.000đ (Gi&aacute; trị khuyến mại), cho mỗi giao dịch th&agrave;nh c&ocirc;ng (kh&ocirc;ng t&iacute;nh c&aacute;c giao dịch ho&agrave;n, hủy).<br />
2. Chỉ &aacute;p dụng cho 1.000 kh&aacute;ch h&agrave;ng (tương ứng với mỗi số t&agrave;i khoản ng&acirc;n h&agrave;ng v&agrave;/hoặc số điện thoại đăng k&yacute; dịch vụ Mobile Banking) đầu ti&ecirc;n thanh to&aacute;n th&agrave;nh c&ocirc;ng qua dịch vụ VNPAY-QR. Mỗi kh&aacute;ch h&agrave;ng được hưởng duy nhất 01 (một) lần gi&aacute; trị khuyến mại/01 th&aacute;ng trong thời gian diễn ra chương tr&igrave;nh.<br />
3. Chương tr&igrave;nh c&oacute; thể kết th&uacute;c trước thời hạn<br />
4. Gi&aacute; trị khuyến mại sẽ được trừ trực tiếp v&agrave;o gi&aacute; trị giao dịch thanh to&aacute;n của Kh&aacute;ch h&agrave;ng khi thực hiện thanh to&aacute;n th&agrave;nh c&ocirc;ng.&nbsp;<br />
5. Gi&aacute; trị khuyến mại sẽ được l&agrave;m tr&ograve;n theo đơn vị: ngh&igrave;n đồng. Cụ thể, &lt;500 đồng l&agrave;m tr&ograve;n xuống 0đ; &gt;=500đ l&agrave;m tr&ograve;n l&ecirc;n 1.000đ.<br />
6. Kh&ocirc;ng &aacute;p dụng t&aacute;ch h&oacute;a đơn dưới mọi h&igrave;nh thức hoặc ho&agrave;n tiền một phần với c&aacute;c giao dịch đ&atilde; hưởng khuyến mại.&nbsp;<br />
7. Chỉ &aacute;p dụng cho kh&aacute;ch h&agrave;ng thanh to&aacute;n 100% gi&aacute; trị đơn h&agrave;ng qua VNPAYQR.<br />
8. Chỉ &aacute;p dụng cho kh&aacute;ch lẻ, kh&ocirc;ng &aacute;p dụng cho kh&aacute;ch đo&agrave;n v&agrave; đại l&yacute;<br />
9. Chi tiết danh s&aacute;ch ng&acirc;n h&agrave;ng v&agrave; c&aacute;c đơn vị &aacute;p dụng khuyến mại được cập nhật tại: https://coupons.vnpay.vn/<br />
&nbsp;</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', N'https://google.com', N'vtv', N'19052021_102648yh_advertising1617262142.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-14T23:43:31.440' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:30:09.127' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b1f4', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_080417yh_img-1.jpeg', 4, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:15:41.833' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'd866e92f-04d6-4745-8937-70dcd4ba1e74', 4, N'Chương trình Ưu đãi VNPay', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_091936yh_advertising1617262142.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:19:31.893' AS DateTime), NULL, NULL, 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'3a76d88b-0494-4c6a-8b72-d22a86bcee7d', 2, N' Du lịch Kon Tum khám phá những điều thú vị về nhà Rông', N'Được coi là biểu tượng văn hóa của cộng đồng các dân tộc ở Tây Nguyên, nhà Rông thể hiện giá trị vật chất và tinh thần trong đời sống đồng bào nơi đây. Tuổi trẻ hãy một lần đi du lịch Kon Tum để khám phá những điều thú vị về ngôi nhà độc đáo này', N'<html>
<head>
	<title></title>
</head>
<body>
<h2><strong>10 ĐIỀU VỀ NH&Agrave; R&Ocirc;NG T&Acirc;Y NGUY&Ecirc;N</strong></h2>

<h2><strong>&nbsp;</strong></h2>

<p>Nh&agrave; r&ocirc;ng được xem l&agrave; biểu tượng văn h&oacute;a của cộng đồng c&aacute;c d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n, thể hiện gi&aacute; trị vật chất v&agrave; tinh thần trong đời sống đồng b&agrave;o nơi đ&acirc;y.</p>

<p><img alt="Lễ hội ở nhà Rông của người dân tộc Ba Na" src="https://hfvtravel.com/wp-content/uploads/2020/10/le-hoi-nha-rong-cua-nguoi-dan-toc-ba-na.jpg" /></p>

<p>1. Kh&ocirc;ng phải d&acirc;n tộc n&agrave;o ở T&acirc;y Nguy&ecirc;n cũng c&oacute; nh&agrave; r&ocirc;ng. Nh&agrave; r&ocirc;ng xuất hiện nhiều tại c&aacute;c bu&ocirc;n l&agrave;ng d&acirc;n tộc khu vực ph&iacute;a bắc T&acirc;y Nguy&ecirc;n, đặc biệt ở hai tỉnh Gia Lai v&agrave; Kon Tum. Ph&iacute;a nam T&acirc;y Nguy&ecirc;n từ &ETH;ắk Lắk trở v&agrave;o, nh&agrave; r&ocirc;ng xuất hiện thưa thớt dần.</p>

<p>2. Nh&agrave; r&ocirc;ng l&agrave; kh&ocirc;ng gian sinh hoạt cộng đồng lớn nhất mỗi l&agrave;ng; nơi người d&acirc;n trao đổi, thảo luận về c&aacute;c lĩnh vực h&agrave;nh ch&iacute;nh, qu&acirc;n sự; nơi thực thi c&aacute;c luật tục, bảo tồn truyền thống v&agrave; diễn ra những nghi thức t&ocirc;n gi&aacute;o, t&iacute;n ngưỡng.</p>

<p>3. Nh&agrave; r&ocirc;ng kh&ocirc;ng phải d&ugrave;ng để lưu tr&uacute;, mặc d&ugrave; c&oacute; kết cấu v&agrave; vật liệu tương tự nh&agrave; s&agrave;n d&ugrave;ng để ở (được x&acirc;y dựng bằng gỗ, tre, cỏ tranh&hellip;), nh&agrave; r&ocirc;ng mang c&aacute;c n&eacute;t kiến tr&uacute;c đặc sắc v&agrave; cao, rộng hơn nhiều. Nh&agrave; r&ocirc;ng c&agrave;ng cao v&agrave; rộng th&igrave; c&agrave;ng thể hiện sự gi&agrave;u c&oacute;, thịnh vượng, sung t&uacute;c, h&ugrave;ng mạnh của l&agrave;ng.</p>

<p><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-1.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-15.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-4.jpg" />4. Đồng b&agrave;o T&acirc;y Nguy&ecirc;n quan niệm nh&agrave; r&ocirc;ng l&agrave; nơi thu h&uacute;t kh&iacute; thi&ecirc;ng đất trời để bảo trợ cho d&acirc;n l&agrave;ng. Do đ&oacute; trong mỗi nh&agrave; r&ocirc;ng đều c&oacute; một nơi trang trọng để thờ c&aacute;c vật được người d&acirc;n cho l&agrave; thần linh tr&uacute; ngụ như con dao, h&ograve;n đ&aacute;, sừng tr&acirc;u&hellip; Ngo&agrave;i ra, nơi n&agrave;y c&ograve;n như một bảo t&agrave;ng lưu giữ c&aacute;c hiện vật truyền thống gắn liền với lịch sử h&igrave;nh th&agrave;nh bu&ocirc;n l&agrave;ng như cồng chi&ecirc;ng, trống, vũ kh&iacute;, đầu c&aacute;c con vật hiến sinh trong ng&agrave;y lễ.</p>

<p>5. Nh&agrave; r&ocirc;ng l&agrave; nơi quan trọng nhất l&agrave;ng n&ecirc;n đ&agrave;n &ocirc;ng trong l&agrave;ng phải thay nhau ngủ qua đ&ecirc;m tại đ&acirc;y để tr&ocirc;ng coi. Một số l&agrave;ng l&agrave;m đến hai nh&agrave; r&ocirc;ng: &ldquo;nh&agrave; r&ocirc;ng c&aacute;i&rdquo; nhỏ v&agrave; c&oacute; m&aacute;i thấp d&agrave;nh cho phụ nữ, &ldquo;nh&agrave; r&ocirc;ng đực&rdquo; d&agrave;nh cho đ&agrave;n &ocirc;ng c&oacute; quy m&ocirc; lớn hơn v&agrave; trang tr&iacute; c&ocirc;ng phu. Ngo&agrave;i mục đ&iacute;ch g&igrave;n giữ kh&ocirc;ng gian thi&ecirc;ng, nh&agrave; r&ocirc;ng l&agrave; nơi người d&acirc;n trao đổi những c&acirc;u chuyện, kinh nghiệm trong đời sống. Nam nữ độc th&acirc;n trong l&agrave;ng c&oacute; thể qu&acirc;y quần tại nh&agrave; r&ocirc;ng để thăm hỏi, t&igrave;m bạn đời, tuy nhi&ecirc;n kh&ocirc;ng đi được ph&eacute;p đi qu&aacute; giới hạn.</p>

<p>6. Mỗi d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n c&oacute; kiểu l&agrave;m nh&agrave; r&ocirc;ng kh&aacute;c nhau. K&iacute;ch thước nh&agrave; r&ocirc;ng nhỏ v&agrave; thấp nhất l&agrave; của người Giẻ Tri&ecirc;ng. Nh&agrave; r&ocirc;ng của người X&ecirc; &ETH;ăng cao v&uacute;t. Nh&agrave; r&ocirc;ng của người Gia Rai c&oacute; m&aacute;i mảnh, dẹt như lưỡi r&igrave;u. Nh&agrave; r&ocirc;ng của người Ba Na to hơn nh&agrave; Gia Rai, c&oacute; đường n&eacute;t mềm mại v&agrave; thường c&oacute; c&aacute;c nh&agrave; s&agrave;n xung quanh. Điểm chung của c&aacute;c ng&ocirc;i nh&agrave; r&ocirc;ng l&agrave; được x&acirc;y cất tr&ecirc;n một khoảng đất rộng, nằm ngay tại khu vực trung t&acirc;m của bu&ocirc;n l&agrave;ng.</p>

<p>&nbsp;</p>

<p>7. S&agrave;n nh&agrave; r&ocirc;ng được thiết kế gắn liền với văn h&oacute;a qu&acirc;y quần uống rượu cần của đồng b&agrave;o. S&agrave;n thường được l&agrave;m từ v&aacute;n gỗ hay ống tre nứa đập dập, khi gh&eacute;p kh&ocirc;ng kh&iacute;t nhau m&agrave; c&aacute;c tấm c&aacute;ch nhau khoảng 1 cm. Nhờ thế m&agrave; khi người d&acirc;n tập trung ăn uống, nước kh&ocirc;ng bị chảy l&ecirc;nh l&aacute;ng ra s&agrave;n. Mặt kh&aacute;c, kiểu s&agrave;n n&agrave;y gi&uacute;p cho việc vệ sinh nh&agrave; dễ d&agrave;ng hơn.</p>

<p>8. Cầu thang nh&agrave; r&ocirc;ng thường c&oacute; 7 đến 9 bậc, tuy nhi&ecirc;n mỗi d&acirc;n tộc lại c&oacute; trang tr&iacute; kh&aacute;c nhau. Tr&ecirc;n th&agrave;nh v&agrave; cột của cầu thang, người Gia Rai hay tạc h&igrave;nh quả bầu đựng nước, người Bana khắc h&igrave;nh ngọn c&acirc;y rau dớn, c&ograve;n người Giẻ Tri&ecirc;ng, Xơ Đăng thường đẽo h&igrave;nh n&uacute;m chi&ecirc;ng hoặc mũi thuyền.</p>

<p>9. Nếu như m&aacute;i đ&igrave;nh miền xu&ocirc;i gắn liền với h&igrave;nh ảnh c&acirc;y đa, th&igrave; nh&agrave; r&ocirc;ng T&acirc;y Nguy&ecirc;n c&oacute; c&acirc;y n&ecirc;u. C&acirc;y n&ecirc;u được trang tr&iacute; nhiều họa tiết, đặt ở ph&iacute;a trước s&acirc;n ch&iacute;nh giữa của ng&ocirc;i nh&agrave; r&ocirc;ng để phục vụ c&aacute;c lễ hội lớn của bu&ocirc;n l&agrave;ng. Theo quan niệm, c&acirc;y n&ecirc;u l&agrave; nơi hội tụ c&aacute;c vị thần linh. Ở từng lễ hội, c&acirc;y n&ecirc;u mang một h&igrave;nh ảnh biểu tượng kh&aacute;c nhau như c&acirc;y n&ecirc;u trong lễ đ&acirc;m tr&acirc;u c&oacute; 4 nh&aacute;nh, lễ mừng l&uacute;a mới c&acirc;y n&ecirc;u chỉ c&oacute; 1 nh&aacute;nh&hellip;</p>

<p>10. Nh&agrave; r&ocirc;ng chỉ gắn với bu&ocirc;n l&agrave;ng, kh&ocirc;ng c&oacute; nh&agrave; r&ocirc;ng cấp tỉnh, cấp huyện hoặc nh&agrave; r&ocirc;ng chung nhiều l&agrave;ng. Hiện nay, nh&agrave; r&ocirc;ng truyền thống lu&ocirc;n được g&igrave;n giữ tại trung t&acirc;m l&agrave;ng. Một số địa điểm bu&ocirc;n l&agrave;ng c&oacute; nh&agrave; r&ocirc;ng hiện nay l&agrave; nh&agrave; r&ocirc;ng Kon Klor ở th&agrave;nh phố Kon Tum, l&agrave;ng Plei Phung, l&agrave;ng Kon So Lăl (huyện Chư Pah) v&agrave; l&agrave;ng Đ&ecirc; K&rsquo;tu (huyện Mang Yang) ở Gia Lai.</p>

<p>&nbsp;</p>
</body>
</html>
', NULL, NULL, N'19052021_110520yh_2.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:05:20.213' AS DateTime), NULL, NULL, 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'b7ff61ba-d9c4-43b7-ba2b-daf174c30bd6', 4, N'Chương trình Ưu đãi VNPay', N'Nhằm gia tăng tiện tích cho khách hàng khi mua tour, Công ty Du lịch Vietravel tổ chức chương trình ưu đãi hấp dẫn cho khách hàng khi thanh toán bằng dịch vụ VNPay QR code.', NULL, NULL, NULL, N'19052021_102059yh_advertising1616146679.jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:20:58.263' AS DateTime), NULL, NULL, 1)
GO
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (1, 1, N'Tin tức ', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (2, 2, N'Cẩm nang du lịch', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (3, 3, N'Banner header', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (4, 4, N'Banner bottom', 1, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Area] ON 

INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ Tuyền Lâm', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Ven Hồ', 2, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Ga Xe Lửa Cũ', 3, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Trung Tâm Thành Phố Đà Lạt', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Thác Cam Ly', 2, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Thung Lũng Tình Yêu / Đồi Mộng Mơ', 3, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Thung Lũng Vàng', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Núi LangBiang', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Khu du lịch Trúc Lâm Viên', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'Núi Bà Rá', 16, NULL, 0, N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T14:06:12.257' AS DateTime), N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T14:06:12.703' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Area] OFF
GO
INSERT [dbo].[tbl_Booking] ([BookingId], [RoomPriceId], [RoomQuantity], [Amount], [AmountCancel], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [Email], [PhoneNumber], [ContactName], [ProvinceId], [SaleId], [Status]) VALUES (N'f004dcfe-d5ec-4989-afb3-c8a1f1c9a12f', N'74a7d97d-8d5a-49f0-8865-a8f09f0f7e12', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL, N'nguyenthanhdat.bc2310@gmail.com', N'0932980033', N'Nguyễn Thành Đạt', 1, NULL, 0)
GO
INSERT [dbo].[tbl_Country] ([CountryId], [CountryName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1', N'Việt Nam', N'VIE', 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Direction] ON 

INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hướng nội bộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Có cửa sổ (không hướng)', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Hướng phố', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Hướng núi', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Direction] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_District] ON 

INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Đà Lạt', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Bảo lộc', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Đạ Huoai', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Di Linh', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Đức Trọng', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Đơn Dương', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Đam Rông', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Cát Tiên', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Lâm Hà', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'Lạc Dương', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'Đạ Tẻh', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Bảo Lâm', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Bù Đốp', 3, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Phước Long', 3, NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_District] OFF
GO
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Khách Sạn LaDaLat', 1, 1, 7, 4, 1, N'4,18,16,3,5,15,10,6,7,9,11', N'106A  Mai Anh Đào, Phường 8, Đà Lạt, Lâm Đồng', NULL, NULL, NULL, NULL, N'Tại Ladalat, chúng tôi hoàn toàn tôn trọng và bảo tồn các đặc điểm riêng biệt đặc trưng của Đà Lạt. Hành lang rộng lớn đầy hoa anh đào – loài hoa được biết đến như biểu tượng của Đà Lạt. Tình yêu Lang và Biang được hồi sinh và kỷ niệm. Những đặc điểm bản chất của Đà Lạt tồn tại như một sự không thể tránh khỏi, tạo ra sự lãng mạn mơ màng của Ladalat.  
', NULL, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] ON 

INSERT [dbo].[tbl_HotelCancellationPolicy] ([Id], [NumberDateCancel], [HotelId], [PercentAmount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 7, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'50        ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] ON 

INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ bơi', NULL, 0, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Massage/Spa', NULL, 0, 2, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Wifi miễn phí', NULL, 0, 3, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Bãi đỗ xe', NULL, 0, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Giặt là', NULL, 0, 10, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Lễ tân 24/24', NULL, 0, 13, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Đưa đón sân bay', NULL, 0, 5, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'Cho thuê máy bay', NULL, 0, 6, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Phòng gym', NULL, 0, 7, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Nhà hàng', NULL, 0, 8, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Phục vụ đồ ăn tại phòng', NULL, 0, 9, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Chấp nhận thú cưng', NULL, 0, 11, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Hỗ trợ đặt tour', NULL, 0, 12, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Thang máy', NULL, 0, 14, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'Máy ATM trong khách sạn', NULL, 0, 15, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'Phòng họp', NULL, 0, 16, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'Tổ chức sự kiện', NULL, 0, 17, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'Vòi hoa sen', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N' Mấy sấy tóc', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'Quạt', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'Truyền hình vệ tinh/cáp', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'Két sắt trong phòng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'Nước đóng chai miễn phí', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'Dép đi trong phòng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'Sàn trải thảm', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'Dịch vụ báo thức', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'Buồng tắm đứng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'Đồ vệ sinh cá nhân miễn phí', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'Quầy bar nhỏ (MiniBar)', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'Cà phê/Trà', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N'Ấm đun nước điện', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'Tivi màn hình phẳng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'Điện thoại', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] ON 

INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Các mức phí trên chưa bao gồm thuế, phí', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Giường phụ không áp dụng cho loại phòng Basement Standard Triple No Window', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'1 phòng được thêm tối đa 1 người lớn và 1 trẻ em (tuỳ vào loại phòng)', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Trẻ từ 12 tuổi trở lên được tính như người lớn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Vui lòng nhập các yêu cầu đặc biệt vào mục Yêu cầu khác bên dưới Thông tin liên hệ để được hỗ trợ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] ON 

INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Ưu đãi đặc biệt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Đặt sớm giá tốt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Giá Shock giờ chót', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] ON 

INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 1, N'Khách sạn 1 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, 2, N'Khách sạn 2 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, 3, N'Khách sạn 3 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, 4, N'Khách sạn 4 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, 5, N'Khách sạn 5 sao', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] ON 

INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Bữa sáng miễn phí', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'3 Bữa ăn miễn phí', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Thêm giường phụ', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Miễn phí hủy phòng', NULL, 0, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] ON 

INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Từ 0 đến 5.9 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Từ 6 đến 11 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 152000, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Người lớn', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 523000, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelType] ON 

INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Khách sạn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Resort', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Homestay', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Căn hộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Biệt thự', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Du thuyền', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] ON 

INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'TTD', N'Thẻ tính dụng', 1, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'TND', N'Thẻ nội địa', 2, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'CK', N'Chuyển khoản', 3, 1, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Province] ON 

INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'1', N'Đà lạt', N'DLA', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'1', N'Bình Phước', NULL, 1, N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-12T17:04:40.360' AS DateTime), N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-12T17:08:29.860' AS DateTime))
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'1', N'Bình Phước', NULL, 0, N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T09:15:58.043' AS DateTime), N'fe0a2a29-fcc6-4a32-99c1-60e78ea7c919', CAST(N'2021-05-13T09:15:58.043' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Province] OFF
GO
INSERT [dbo].[tbl_Receipt] ([Id], [ReceiptNo], [ReceiptDate], [BookingId], [Amount], [Payer], [PaymentType], [Company], [VatCode], [Address], [Telephone], [Mobile], [Note], [ReceiptOrder], [Point], [Status], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'bc8e9172-baba-4d67-9826-f78cf4b914c0', N'TT0001', NULL, N'f004dcfe-d5ec-4989-afb3-c8a1f1c9a12f', 1161000, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Role] ON 

INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (1, N'Quản trị')
INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (2, N'Nhân viên')
SET IDENTITY_INSERT [dbo].[tbl_Role] OFF
GO
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Basement Standard Double No Window', 20, 25, NULL, NULL, 1, 2, N'20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30,
32,
33,
34,
35,
36', NULL, 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'74a7d97d-8d5a-49f0-8865-a8f09f0f7e12', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-05-05T00:00:00.000' AS DateTime), NULL, NULL, 822000, 528000, 36, N'1', 1, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'31626bac-ff21-47a3-8166-27ba5aabd1f5', N'hieunv', N'M/tTWwIES8wxgueZZcLelw==', N'Nguyễn Việt Hiếu', N'', 1, N'hieuviet.1103@gmail.com', N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', N'khanhpq', N'4QrcOUm6Wau+VuBX8g+IPg==', N'a khánh', N'', 1, N'khanh@gmail.com', N'', NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-13T21:51:18.410' AS DateTime))
GO
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'00000000-0000-0000-0000-000000000000', N'31626bac-ff21-47a3-8166-27ba5aabd1f5', 1, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'ce5e0195-3cb5-4753-b6f6-c1b23ff0adcb', N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', 2, 1)
GO
ALTER TABLE [dbo].[tbl_Area] ADD  CONSTRAINT [DF_tbl_Area_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Country] ADD  CONSTRAINT [DF_tbl_Country_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Coupon] ADD  CONSTRAINT [DF_tbl_Coupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CouponType] ADD  CONSTRAINT [DF_tbl_CouponType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] ADD  CONSTRAINT [DF_tbl_CustomerCoupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Direction] ADD  CONSTRAINT [DF_tbl_Direction_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_District] ADD  CONSTRAINT [DF_tbl_District_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Hotel] ADD  CONSTRAINT [DF_tbl_Hotel_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] ADD  CONSTRAINT [DF_tbl_HotelCancellationPolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelComment] ADD  CONSTRAINT [DF_tbl_HotelComment_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelConvenientType] ADD  CONSTRAINT [DF_tbl_HotelConvenientType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelNotice] ADD  CONSTRAINT [DF_tbl_HotelNotice_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelPriceType] ADD  CONSTRAINT [DF_tbl_HotelPriceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelRatingType] ADD  CONSTRAINT [DF_tbl_HotelRatingType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelServiceType] ADD  CONSTRAINT [DF_tbl_HotelServiceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] ADD  CONSTRAINT [DF_tbl_HotelSurchargePolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelType] ADD  CONSTRAINT [DF_tbl_HotelType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_PaymentType] ADD  CONSTRAINT [DF_tbl_PaymentType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_HotelImage_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_ProductImage_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Province] ADD  CONSTRAINT [DF_tbl_Province_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Room] ADD  CONSTRAINT [DF_tbl_RoomType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_RoomPrice] ADD  CONSTRAINT [DF_tbl_Room_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_User_Role] ADD  CONSTRAINT [DF_tbl_User_Role_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tbl_Area]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Area_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Area] CHECK CONSTRAINT [FK_tbl_Area_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Booking]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Booking_tbl_RoomPrice] FOREIGN KEY([RoomPriceId])
REFERENCES [dbo].[tbl_RoomPrice] ([Id])
GO
ALTER TABLE [dbo].[tbl_Booking] CHECK CONSTRAINT [FK_tbl_Booking_tbl_RoomPrice]
GO
ALTER TABLE [dbo].[tbl_Coupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Coupon_tbl_CouponType] FOREIGN KEY([Type])
REFERENCES [dbo].[tbl_CouponType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Coupon] CHECK CONSTRAINT [FK_tbl_Coupon_tbl_CouponType]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon] FOREIGN KEY([CouponId])
REFERENCES [dbo].[tbl_Coupon] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[tbl_Receipt] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt]
GO
ALTER TABLE [dbo].[tbl_District]  WITH CHECK ADD  CONSTRAINT [FK_tbl_District_tbl_Province] FOREIGN KEY([ProvinceId])
REFERENCES [dbo].[tbl_Province] ([ProvinceId])
GO
ALTER TABLE [dbo].[tbl_District] CHECK CONSTRAINT [FK_tbl_District_tbl_Province]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[tbl_Area] ([AreaId])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_Area]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_HotelRatingType] FOREIGN KEY([HotelRatingTypeId])
REFERENCES [dbo].[tbl_HotelRatingType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_HotelRatingType]
GO
ALTER TABLE [dbo].[tbl_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Hotel_tbl_HotelType] FOREIGN KEY([HotelTypeId])
REFERENCES [dbo].[tbl_HotelType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Hotel] CHECK CONSTRAINT [FK_tbl_Hotel_tbl_HotelType]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] CHECK CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelNotice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelNotice] CHECK CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] CHECK CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_Province]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Province_tbl_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[tbl_Country] ([CountryId])
GO
ALTER TABLE [dbo].[tbl_Province] CHECK CONSTRAINT [FK_tbl_Province_tbl_Country]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_Booking] FOREIGN KEY([BookingId])
REFERENCES [dbo].[tbl_Booking] ([BookingId])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_Booking]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType] FOREIGN KEY([PaymentType])
REFERENCES [dbo].[tbl_PaymentType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Direction] FOREIGN KEY([Direction])
REFERENCES [dbo].[tbl_Direction] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Direction]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_RoomPrice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RoomPrice_tbl_Room] FOREIGN KEY([RoomId])
REFERENCES [dbo].[tbl_Room] ([Id])
GO
ALTER TABLE [dbo].[tbl_RoomPrice] CHECK CONSTRAINT [FK_tbl_RoomPrice_tbl_Room]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_Role]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_User]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Ảnh đại diện KS; 1: Ds ảnh Ks; 2Ảnh đại diện phòng; 3 Ds ảnh phòng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ProductImage', @level2type=N'COLUMN',@level2name=N'Type'
GO
USE [master]
GO
ALTER DATABASE [Hotel] SET  READ_WRITE 
GO
