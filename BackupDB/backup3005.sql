USE [master]
GO
/****** Object:  Database [Combo]    Script Date: 30/5/2021 9:54:07 AM ******/
CREATE DATABASE [Combo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Combo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Combo.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Combo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Combo_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Combo] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Combo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Combo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Combo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Combo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Combo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Combo] SET ARITHABORT OFF 
GO
ALTER DATABASE [Combo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Combo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Combo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Combo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Combo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Combo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Combo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Combo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Combo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Combo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Combo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Combo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Combo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Combo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Combo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Combo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Combo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Combo] SET RECOVERY FULL 
GO
ALTER DATABASE [Combo] SET  MULTI_USER 
GO
ALTER DATABASE [Combo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Combo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Combo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Combo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Combo] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Combo', N'ON'
GO
ALTER DATABASE [Combo] SET QUERY_STORE = OFF
GO
USE [Combo]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Combo]
GO
/****** Object:  Table [dbo].[HotelNews]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HotelNews](
	[HotelNewsID] [uniqueidentifier] NOT NULL,
	[NewsTypeID] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Brief] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Source] [nvarchar](max) NULL,
	[FileAttchment] [nvarchar](max) NULL,
	[OrderID] [int] NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NOT NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.HotelNews] PRIMARY KEY CLUSTERED 
(
	[HotelNewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsTypes]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTypes](
	[NewsTypeID] [int] NOT NULL,
	[Code] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [datetime] NULL,
 CONSTRAINT [PK_dbo.NewsTypes] PRIMARY KEY CLUSTERED 
(
	[NewsTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SentCommunication]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SentCommunication](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[MailTo] [varchar](50) NOT NULL,
	[MailCc] [varchar](200) NOT NULL,
	[MailBcc] [varchar](200) NOT NULL,
	[MailFrom] [varchar](50) NOT NULL,
	[MailFromName] [nvarchar](50) NOT NULL,
	[IsSent] [bit] NULL,
	[IsCheck] [bit] NULL,
	[DateCreate] [datetime] NULL,
	[DateCheck] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[LogError] [nvarchar](max) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](15) NOT NULL,
	[CountryPhoneCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_SentCommunication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Area]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Area](
	[AreaId] [int] NOT NULL,
	[AreaName] [nvarchar](255) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Area] PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking](
	[BookingId] [uniqueidentifier] NOT NULL,
	[RoomPriceId] [uniqueidentifier] NOT NULL,
	[RoomQuantity] [int] NULL,
	[Amount] [float] NULL,
	[AmountCancel] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Email] [nvarchar](255) NOT NULL,
	[PhoneNumber] [nvarchar](255) NOT NULL,
	[ContactName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[SaleId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Booking_1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Content]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Content](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ContenCode] [nvarchar](50) NOT NULL,
	[ContentName] [nvarchar](200) NULL,
	[ContentDetail] [ntext] NULL,
	[DateCreate] [datetime] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Content] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Country]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Country](
	[CountryId] [varchar](3) NOT NULL,
	[CountryName] [nvarchar](50) NOT NULL,
	[Symbol] [varchar](3) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Coupon]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Coupon](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[ShortCode] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[EffectiveDate] [date] NULL,
	[ExpirationDate] [date] NULL,
	[Price] [float] NULL,
	[Image] [nvarchar](1024) NULL,
	[Type] [int] NOT NULL,
	[PointExchange] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CouponType]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CouponType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeName] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CouponType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CustomerNo] [nvarchar](255) NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[Dob] [datetime] NULL,
	[Gender] [tinyint] NOT NULL,
	[Nationality] [varchar](3) NULL,
	[IdCard] [nvarchar](12) NULL,
	[DateOfIssue] [datetime] NULL,
	[PlaceOfIssue] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[CountryId] [int] NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Note] [nvarchar](255) NULL,
	[Password] [nvarchar](127) NULL,
	[TotalPoint] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerCoupon]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerCoupon](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CouponId] [uniqueidentifier] NOT NULL,
	[ReceiptId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CustomerCoupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Direction]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Direction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DirectionName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Direction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_District]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_District](
	[DistrictId] [int] NOT NULL,
	[DistrictName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_District] PRIMARY KEY CLUSTERED 
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Hotel]    Script Date: 30/5/2021 9:54:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Hotel](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelName] [nvarchar](255) NOT NULL,
	[CountryId] [varchar](3) NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[AreaId] [nvarchar](255) NULL,
	[HotelRatingTypeId] [int] NULL,
	[HotelTypeId] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Address] [nvarchar](500) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Longitude] [nvarchar](255) NULL,
	[Latitude] [nvarchar](255) NULL,
	[Content] [nvarchar](max) NULL,
	[Email] [nvarchar](255) NULL,
	[Url] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[IsActive] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[PriceFrom] [float] NULL,
	[PricePromotion] [float] NULL,
 CONSTRAINT [PK_tbl_Hotel_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelCancellationPolicy]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelCancellationPolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberDateCancel] [int] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[PercentAmount] [nchar](10) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelCancellationPolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelComment]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelComment](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[DateTimeComment] [datetime] NOT NULL,
	[PointLocation] [float] NULL,
	[PointServe] [float] NULL,
	[PointConvenient] [float] NULL,
	[PointCost] [float] NULL,
	[PointClean] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelConvenientType]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelConvenientType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConvenientName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[Type] [int] NOT NULL,
	[Order] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelConvenientType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelNotice]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelNotice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelNotice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelPriceType]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelPriceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PriceTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelPriceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelRatingType]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelRatingType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberStar] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelRatingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelServiceType]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelServiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelServiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelSurchargePolicy]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelSurchargePolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [nvarchar](255) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelSurchargePolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelType]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_PaymentType]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_PaymentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeCode] [nvarchar](255) NOT NULL,
	[PayTypeName] [nvarchar](255) NOT NULL,
	[Order] [int] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ProductImage]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductImage](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NULL,
	[RoomType] [uniqueidentifier] NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_HotelImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Province]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Province](
	[ProvinceId] [int] NOT NULL,
	[CountryId] [varchar](3) NOT NULL,
	[ProvinceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Province] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Receipt]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Receipt](
	[Id] [uniqueidentifier] NOT NULL,
	[ReceiptNo] [nvarchar](255) NOT NULL,
	[ReceiptDate] [datetime] NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[Payer] [nvarchar](255) NULL,
	[PaymentType] [int] NOT NULL,
	[Company] [nvarchar](255) NULL,
	[VatCode] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Telephone] [nvarchar](255) NULL,
	[Mobile] [nvarchar](255) NULL,
	[Note] [nvarchar](255) NULL,
	[ReceiptOrder] [int] NULL,
	[Point] [float] NULL,
	[Status] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Room]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Room](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomName] [nvarchar](255) NOT NULL,
	[AvailableRoom] [int] NOT NULL,
	[RoomArea] [float] NULL,
	[Direction] [int] NULL,
	[SingleBed] [int] NULL,
	[DoubleBed] [int] NULL,
	[MaxPeople] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RoomType_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoomPrice]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoomPrice](
	[Id] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[RoomDate] [datetime] NOT NULL,
	[AvailableRoom] [int] NULL,
	[HotelPriceTypeId] [int] NULL,
	[PriceContract] [float] NOT NULL,
	[PricePromotion] [float] NULL,
	[PercentPromotion] [float] NULL,
	[HotelServiceType] [nvarchar](255) NULL,
	[IsCancel] [bit] NULL,
	[IsSurcharge] [bit] NULL,
	[IsPromotion] [bit] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[Tax] [float] NULL,
	[TransactionCosts] [float] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Room_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[UserId] [uniqueidentifier] NOT NULL,
	[LoginName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](127) NOT NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[CodeStaff] [nvarchar](255) NULL,
	[Active] [tinyint] NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User_Role]    Script Date: 30/5/2021 9:54:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User_Role](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_User_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'cd6dfbac-874f-49d6-a594-3b6cececa2fa', 2, N'Tin tức du lịch mùa covid', N'Tin tức du lịch mùa covid năm thứ 2 liên tiếp', N'<html>
<head>
	<title></title>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="2">
			<h1>Cần bản đồ an to&agrave;n để du lịch &#39;sống chung với dịch&#39;</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2">Đ&agrave;o Loan</td>
		</tr>
		<tr>
			<td colspan="2">Thứ&nbsp;Ba,&nbsp;&nbsp;4/5/2021,&nbsp;17:13&nbsp;</td>
		</tr>
		<tr>
			<td style="vertical-align:top">&nbsp;</td>
			<td style="vertical-align:top">
			<p>(KTSG Online) - Khi dịch Covid-19 c&oacute; nguy cơ b&ugrave;ng l&ecirc;n như hiện nay, việc x&acirc;y dựng bản đồ du lịch an to&agrave;n cho du kh&aacute;ch v&agrave; doanh nghiệp tiếp tục được đặt ra như một trong những yếu tố gi&uacute;p ng&agrave;nh du lịch n&oacute;i chung đủ khả năng để c&oacute; thể &quot;sống chung với dịch&quot;.</p>

			<p><em>KTSG Online</em>&nbsp;đ&atilde; phỏng vấn &ocirc;ng Nguyễn Quốc Kỳ, Chủ tịch Vietravel Holdings về vấn đề n&agrave;y.</p>

			<table align="middle" cellpadding="0" cellspacing="0" style="width:99%">
				<tbody>
					<tr>
						<td><img alt="" src="https://cdn.thesaigontimes.vn/Uploads/Articles/315994/75248_dsc07591.jpg" /></td>
					</tr>
					<tr>
						<td>Du kh&aacute;ch ở Y&ecirc;n Tử, tỉnh Quảng Ninh. Ảnh: Đ&agrave;o Loan</td>
					</tr>
				</tbody>
			</table>

			<p>KTSG Online: Tại sao &ocirc;ng cho rằng với diễn biến phức tạp v&agrave; kh&ocirc;n lường của dịch bệnh như hiện nay, ng&agrave;nh du lịch phải c&oacute; bản đồ an to&agrave;n th&igrave; mới c&oacute; thể &quot;sống chung với dịch&quot;?</p>

			<p><strong>- &Ocirc;NG NGUYỄN QUỐC KỲ</strong>: C&oacute; thể thấy, cả doanh nghiệp lẫn du kh&aacute;ch đều rất bối rối trong những lần b&ugrave;ng dịch vừa qua.</p>

			<p>Mỗi khi c&oacute; ca nhiễm, d&ugrave; chỉ l&agrave; một ca ở một khu vực ri&ecirc;ng lẻ l&agrave; kh&aacute;ch h&agrave;ng đ&atilde; cực kỳ hoang mang v&igrave; th&ocirc;ng tin về dịch bệnh tr&agrave;n ngập tr&ecirc;n c&aacute;c phương tiện truyền th&ocirc;ng, mạng x&atilde; hội, truyền miệng...&nbsp; Những điều n&agrave;y khiến nhiều người tưởng chừng như tất cả điểm đến đều c&oacute; dịch.</p>

			<p>Chẳng hạn, khi n&oacute;i H&agrave; Nội c&oacute; dịch th&igrave; ngay lập tức nhiều kh&aacute;ch hủy tour đến đ&acirc;y v&igrave; cho rằng kh&ocirc;ng an to&agrave;n trong khi thực tế th&agrave;nh phố n&agrave;y rất rộng lớn, c&oacute; rất nhiều điểm vẫn an to&agrave;n nhưng th&ocirc;ng tin n&agrave;y lại lọt thỏm trong d&ograve;ng th&ocirc;ng tin về dịch bệnh, khiến du kh&aacute;ch lo sợ.</p>

			<p>T&igrave;nh trạng hủy, ho&atilde;n dịch vụ trong những đợt b&ugrave;ng dịch vừa qua cũng c&oacute; nguy&ecirc;n nh&acirc;n từ t&acirc;m l&yacute; n&agrave;y. V&igrave; thế, nếu c&oacute; bản đồ du lịch an to&agrave;n, cho người d&acirc;n biết nơi n&agrave;o đang c&oacute; dịch kh&ocirc;ng n&ecirc;n đến, nơi n&agrave;o đang trong diện cảnh b&aacute;o, điểm n&agrave;o vẫn an to&agrave;n để vui chơi th&igrave; sẽ gi&uacute;p ổn định t&acirc;m l&yacute; kh&aacute;ch h&agrave;ng, đồng thời cũng gi&uacute;p cơ quan chức năng ngăn những tin đồn thất thiệt về dịch bệnh.</p>

			<p>C&oacute; bản đồ n&agrave;y, c&aacute;c nh&agrave; điều h&agrave;nh dịch vụ sẽ được hỗ trợ rất nhiều. Ch&uacute;ng t&ocirc;i sẽ kh&ocirc;ng c&ograve;n cảnh l&uacute;ng t&uacute;ng v&agrave; bị động khi dự b&aacute;o t&igrave;nh h&igrave;nh để sắp xếp dịch vụ m&agrave; c&oacute; thể chủ động biết được nơi n&agrave;o c&oacute; thể đưa kh&aacute;ch đến, nơi n&agrave;o cần phải gi&atilde;n ra để t&iacute;nh to&aacute;n kế hoạch kinh doanh.</p>

			<p>Việc n&agrave;y cũng sẽ gi&uacute;p doanh nghiệp tiết kiệm chi ph&iacute; v&agrave; giữ d&ograve;ng tiền. Nếu cứ đặt dịch vụ rồi lại phải hủy, ho&atilde;n đột ngột như thời gian qua th&igrave; doanh nghiệp sẽ kh&oacute; khăn v&igrave; số tiền đặt cọc bị ng&acirc;m rất l&acirc;u, trong khi tiền mặt lại đang thiếu.</p>

			<p>Du kh&aacute;ch y&ecirc;n t&acirc;m, doanh nghiệp chủ động điều h&agrave;nh dịch vụ th&igrave; du lịch mới vận h&agrave;nh được trong l&uacute;c dịch vẫn c&ograve;n như thế n&agrave;y.</p>

			<p>KTSG Online: Hiện nay, doanh nghiệp du lịch lấy th&ocirc;ng tin từ đ&acirc;u để chuẩn bị kế hoạch ứng ph&oacute; khi b&ugrave;ng dịch?</p>

			<table align="right" cellpadding="0" cellspacing="0" style="width:99%">
				<tbody>
					<tr>
						<td><img alt="" src="https://cdn.thesaigontimes.vn/Uploads/Articles/315994/2b333_anhky_1.jpg" /></td>
					</tr>
					<tr>
						<td>&Ocirc;ng Nguyễn Quốc Kỳ</td>
					</tr>
				</tbody>
			</table>

			<p>- Ch&uacute;ng t&ocirc;i phải lấy từ nhiều nguồn, trong đ&oacute; chủ yếu l&agrave; đợi th&ocirc;ng b&aacute;o từ c&aacute;c địa phương để biết c&aacute;c điểm du lịch, c&aacute;c dịch vụ cho du kh&aacute;ch c&oacute; được ph&eacute;p hoạt động hay kh&ocirc;ng.</p>

			<p>Tuy nhi&ecirc;n, kh&ocirc;ng phải l&uacute;c n&agrave;o th&ocirc;ng b&aacute;o cũng đến đ&uacute;ng thời điểm cho n&ecirc;n đ&atilde; c&oacute; trường hợp, c&oacute; c&ocirc;ng ty đưa kh&aacute;ch đến nơi rồi mới biết điểm đến đ&atilde; đ&oacute;ng cửa.</p>

			<p>KTSG Online: Theo &ocirc;ng, bản đồ du lịch n&ecirc;n l&agrave;m như thế n&agrave;o v&agrave; cơ quan n&agrave;o n&ecirc;n cầm trịch việc n&agrave;y?</p>

			<p>- Về mặt kỹ thuật, c&oacute; thể thể hiện bằng trang web hoặc bằng phần mềm ứng dụng. Với nội dung thể hiện, để đảm bảo t&iacute;nh ch&iacute;nh x&aacute;c v&agrave; tổng qu&aacute;t tr&ecirc;n cả nước, Ban Chỉ đạo quốc gia ph&ograve;ng chống dịch Covid-19 n&ecirc;n l&agrave; đơn vị cập nhật th&ocirc;ng tin về dịch bệnh c&ugrave;ng những cảnh b&aacute;o li&ecirc;n quan.</p>

			<p>Ban chỉ đạo cũng c&oacute; thể ủy quyền cho một đơn vị chuy&ecirc;n tr&aacute;ch như Bộ Y tế cập nhật th&ocirc;ng tin. Hiện nay, Bộ Y tế đang cập nhật th&ocirc;ng tin c&aacute;c ca nhiễm mới 2 lần/ng&agrave;y. Đ&acirc;y cũng l&agrave; c&aacute;ch c&oacute; thể tham khảo khi thực hiện bản đồ.</p>

			<p>C&oacute; thể h&igrave;nh dung bản đồ du lịch an to&agrave;n như bản đồ cảnh b&aacute;o c&aacute;c điểm ch&aacute;y rừng m&agrave; cơ quan kiểm l&acirc;m đ&atilde; l&agrave;m. Nh&igrave;n v&agrave;o bản đồ, du kh&aacute;ch sẽ thấy tổng thể cả điểm đến v&agrave; thấy được trong đ&oacute; nơi n&agrave;o l&agrave; v&ugrave;ng đỏ kh&ocirc;ng n&ecirc;n đến, nơi n&agrave;o c&oacute; nguy cơ vừa phải, nơi n&agrave;o ho&agrave;n to&agrave;n xanh để c&oacute; thể y&ecirc;n t&acirc;m du lịch...</p>

			<p>Điều quan trọng l&agrave; th&ocirc;ng tin cần phải cập nhật li&ecirc;n tục. Với sự hỗ trợ của c&ocirc;ng nghệ, c&aacute;c tỉnh, th&agrave;nh tr&ecirc;n cả nước cũng c&oacute; thể cập nhật th&ocirc;ng tin l&ecirc;n đ&acirc;y để du kh&aacute;ch biết t&igrave;nh h&igrave;nh dịch ở tỉnh A đang như thế n&agrave;o, tỉnh B đang cho mở dịch vụ n&agrave;o, cấm dịch vụ n&agrave;o, khu vực n&agrave;o đang gi&atilde;n c&aacute;ch, nơi n&agrave;o buộc phải đeo khẩu trang... Điều n&agrave;y thực sự rất quan trọng v&agrave; hữu &iacute;ch.</p>

			<p>T&ocirc;i cho rằng nếu l&agrave;m tốt th&igrave; bản đồ du lịch an to&agrave;n kh&ocirc;ng chỉ l&agrave; k&ecirc;nh cung cấp th&ocirc;ng tin quan trọng cho du kh&aacute;ch trong đại dịch m&agrave; c&ograve;n l&agrave; sau n&agrave;y, khi mở cửa thị trường. Khi muốn đi du lịch Việt Nam hay khi c&oacute; sự cố, dịch bệnh, du kh&aacute;ch nước ngo&agrave;i c&oacute; thể v&agrave;o để t&igrave;m th&ocirc;ng tin.</p>

			<p>Th&ecirc;m v&agrave;o đ&oacute;, bản đồ n&agrave;y cũng c&oacute; thể ph&aacute;t triển th&ecirc;m tiện &iacute;ch như l&agrave; bản đồ th&ocirc;ng b&aacute;o mật độ kh&aacute;ch, gi&uacute;p điểm đến giải tỏa qu&aacute; tải trong c&aacute;c cao điểm du lịch. Chẳng hạn như việc Đ&agrave; Lạt tắc nghẽn trong dịp lễ vừa rồi, nếu c&oacute; th&ocirc;ng tin về sức chứa của điểm đến, của c&aacute;c cơ sở dịch vụ ngay tại thời điểm đ&oacute; c&ugrave;ng khuyến c&aacute;o của cơ quan chức năng th&igrave; chắc rằng nhiều người sẽ chọn hướng kh&aacute;c để tr&aacute;nh tắc nghẽn, gi&uacute;p điểm đến giảm &aacute;p lực.</p>
			</td>
		</tr>
	</tbody>
</table>
<!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', NULL, NULL, N'19052021_104053yh_advertising1617262142.jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:40:37.950' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:40:53.650' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0f2', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_080417yh_img-1.jpeg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:16:14.710' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0f3', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_091154yh_o-d1-(1).jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:16:09.707' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0f4', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_091513yh_advertising1617262142.jpg', 3, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:15:36.290' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0fd', 2, N'Chương trình Ưu đãi VNPay', N'Nhằm gia tăng tiện tích cho khách hàng khi mua tour, Công ty Du lịch Vietravel tổ chức chương trình ưu đãi hấp dẫn cho khách hàng khi thanh toán bằng dịch vụ VNPay QR code.', N'<html>
<head>
	<title></title>
</head>
<body>
<p style="text-align:center"><span style="color:#d35400"><strong><span style="font-size:28px">Thể lệ chương tr&igrave;nh ưu đ&atilde;i VNPAY</span></strong></span></p>

<p><br />
05/10/2020<br />
Nhằm gia tăng tiện t&iacute;ch cho kh&aacute;ch h&agrave;ng khi mua tour, C&ocirc;ng ty Du lịch Vietravel tổ chức chương tr&igrave;nh ưu đ&atilde;i hấp dẫn cho kh&aacute;ch h&agrave;ng khi thanh to&aacute;n bằng dịch vụ VNPay QR code.<br />
&nbsp;</p>

<figure class="easyimage easyimage-full"><img alt="" src="blob:http://localhost:13863/087dd565-d7ef-414d-b156-977f5f29b0c0" width="560" />
<figcaption></figcaption>
</figure>

<p><img src="http://localhost:13863/FileUploads/News/18052021_104109yb_shutterstock.jpg" /></p>

<p><br />
<br />
Lưu<br />
1. Khi mua c&aacute;c sản phẩm tại Vietravel v&agrave; thanh to&aacute;n th&agrave;nh c&ocirc;ng bằng dịch vụ VNPAYQR (sử dụng t&iacute;nh năng QR Pay) tr&ecirc;n ứng dụng Mobile Banking của c&aacute;c ng&acirc;n h&agrave;ng: Agribank, BIDV, VietinBank, Vietcombank, SCB, IVB, ABBank, Eximbank, HDBank, NCB, Nam A Bank, VietBank, BIDC, SaiGonBank, SeABank, Ocean Bank, KienLongBank: Kh&aacute;ch h&agrave;ng nhập m&atilde; khuyến mại VTR25 sẽ được giảm 5% gi&aacute; trị giao dịch, tối đa 100.000đ (Gi&aacute; trị khuyến mại), cho mỗi giao dịch th&agrave;nh c&ocirc;ng (kh&ocirc;ng t&iacute;nh c&aacute;c giao dịch ho&agrave;n, hủy).<br />
2. Chỉ &aacute;p dụng cho 1.000 kh&aacute;ch h&agrave;ng (tương ứng với mỗi số t&agrave;i khoản ng&acirc;n h&agrave;ng v&agrave;/hoặc số điện thoại đăng k&yacute; dịch vụ Mobile Banking) đầu ti&ecirc;n thanh to&aacute;n th&agrave;nh c&ocirc;ng qua dịch vụ VNPAY-QR. Mỗi kh&aacute;ch h&agrave;ng được hưởng duy nhất 01 (một) lần gi&aacute; trị khuyến mại/01 th&aacute;ng trong thời gian diễn ra chương tr&igrave;nh.<br />
3. Chương tr&igrave;nh c&oacute; thể kết th&uacute;c trước thời hạn<br />
4. Gi&aacute; trị khuyến mại sẽ được trừ trực tiếp v&agrave;o gi&aacute; trị giao dịch thanh to&aacute;n của Kh&aacute;ch h&agrave;ng khi thực hiện thanh to&aacute;n th&agrave;nh c&ocirc;ng.&nbsp;<br />
5. Gi&aacute; trị khuyến mại sẽ được l&agrave;m tr&ograve;n theo đơn vị: ngh&igrave;n đồng. Cụ thể, &lt;500 đồng l&agrave;m tr&ograve;n xuống 0đ; &gt;=500đ l&agrave;m tr&ograve;n l&ecirc;n 1.000đ.<br />
6. Kh&ocirc;ng &aacute;p dụng t&aacute;ch h&oacute;a đơn dưới mọi h&igrave;nh thức hoặc ho&agrave;n tiền một phần với c&aacute;c giao dịch đ&atilde; hưởng khuyến mại.&nbsp;<br />
7. Chỉ &aacute;p dụng cho kh&aacute;ch h&agrave;ng thanh to&aacute;n 100% gi&aacute; trị đơn h&agrave;ng qua VNPAYQR.<br />
8. Chỉ &aacute;p dụng cho kh&aacute;ch lẻ, kh&ocirc;ng &aacute;p dụng cho kh&aacute;ch đo&agrave;n v&agrave; đại l&yacute;<br />
9. Chi tiết danh s&aacute;ch ng&acirc;n h&agrave;ng v&agrave; c&aacute;c đơn vị &aacute;p dụng khuyến mại được cập nhật tại: https://coupons.vnpay.vn/<br />
&nbsp;</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', N'https://google.com', N'vtv', N'19052021_102648yh_advertising1617262142.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-14T23:43:31.440' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:30:09.127' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b1f4', 3, N'Ưu đãi du lịch', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_080417yh_img-1.jpeg', 4, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T20:04:17.900' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:15:41.833' AS DateTime), 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'd866e92f-04d6-4745-8937-70dcd4ba1e74', 4, N'Chương trình Ưu đãi VNPay', N'Ưu dãi du lịch', NULL, NULL, NULL, N'16052021_091936yh_advertising1617262142.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-16T21:19:31.893' AS DateTime), NULL, NULL, 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'3a76d88b-0494-4c6a-8b72-d22a86bcee7d', 2, N' Du lịch Kon Tum khám phá những điều thú vị về nhà Rông', N'Được coi là biểu tượng văn hóa của cộng đồng các dân tộc ở Tây Nguyên, nhà Rông thể hiện giá trị vật chất và tinh thần trong đời sống đồng bào nơi đây. Tuổi trẻ hãy một lần đi du lịch Kon Tum để khám phá những điều thú vị về ngôi nhà độc đáo này', N'<html>
<head>
	<title></title>
</head>
<body>
<h2><strong>10 ĐIỀU VỀ NH&Agrave; R&Ocirc;NG T&Acirc;Y NGUY&Ecirc;N</strong></h2>

<h2><strong>&nbsp;</strong></h2>

<p>Nh&agrave; r&ocirc;ng được xem l&agrave; biểu tượng văn h&oacute;a của cộng đồng c&aacute;c d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n, thể hiện gi&aacute; trị vật chất v&agrave; tinh thần trong đời sống đồng b&agrave;o nơi đ&acirc;y.</p>

<p><img alt="Lễ hội ở nhà Rông của người dân tộc Ba Na" src="https://hfvtravel.com/wp-content/uploads/2020/10/le-hoi-nha-rong-cua-nguoi-dan-toc-ba-na.jpg" /></p>

<p>1. Kh&ocirc;ng phải d&acirc;n tộc n&agrave;o ở T&acirc;y Nguy&ecirc;n cũng c&oacute; nh&agrave; r&ocirc;ng. Nh&agrave; r&ocirc;ng xuất hiện nhiều tại c&aacute;c bu&ocirc;n l&agrave;ng d&acirc;n tộc khu vực ph&iacute;a bắc T&acirc;y Nguy&ecirc;n, đặc biệt ở hai tỉnh Gia Lai v&agrave; Kon Tum. Ph&iacute;a nam T&acirc;y Nguy&ecirc;n từ &ETH;ắk Lắk trở v&agrave;o, nh&agrave; r&ocirc;ng xuất hiện thưa thớt dần.</p>

<p>2. Nh&agrave; r&ocirc;ng l&agrave; kh&ocirc;ng gian sinh hoạt cộng đồng lớn nhất mỗi l&agrave;ng; nơi người d&acirc;n trao đổi, thảo luận về c&aacute;c lĩnh vực h&agrave;nh ch&iacute;nh, qu&acirc;n sự; nơi thực thi c&aacute;c luật tục, bảo tồn truyền thống v&agrave; diễn ra những nghi thức t&ocirc;n gi&aacute;o, t&iacute;n ngưỡng.</p>

<p>3. Nh&agrave; r&ocirc;ng kh&ocirc;ng phải d&ugrave;ng để lưu tr&uacute;, mặc d&ugrave; c&oacute; kết cấu v&agrave; vật liệu tương tự nh&agrave; s&agrave;n d&ugrave;ng để ở (được x&acirc;y dựng bằng gỗ, tre, cỏ tranh&hellip;), nh&agrave; r&ocirc;ng mang c&aacute;c n&eacute;t kiến tr&uacute;c đặc sắc v&agrave; cao, rộng hơn nhiều. Nh&agrave; r&ocirc;ng c&agrave;ng cao v&agrave; rộng th&igrave; c&agrave;ng thể hiện sự gi&agrave;u c&oacute;, thịnh vượng, sung t&uacute;c, h&ugrave;ng mạnh của l&agrave;ng.</p>

<p><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-1.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-15.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-4.jpg" />4. Đồng b&agrave;o T&acirc;y Nguy&ecirc;n quan niệm nh&agrave; r&ocirc;ng l&agrave; nơi thu h&uacute;t kh&iacute; thi&ecirc;ng đất trời để bảo trợ cho d&acirc;n l&agrave;ng. Do đ&oacute; trong mỗi nh&agrave; r&ocirc;ng đều c&oacute; một nơi trang trọng để thờ c&aacute;c vật được người d&acirc;n cho l&agrave; thần linh tr&uacute; ngụ như con dao, h&ograve;n đ&aacute;, sừng tr&acirc;u&hellip; Ngo&agrave;i ra, nơi n&agrave;y c&ograve;n như một bảo t&agrave;ng lưu giữ c&aacute;c hiện vật truyền thống gắn liền với lịch sử h&igrave;nh th&agrave;nh bu&ocirc;n l&agrave;ng như cồng chi&ecirc;ng, trống, vũ kh&iacute;, đầu c&aacute;c con vật hiến sinh trong ng&agrave;y lễ.</p>

<p>5. Nh&agrave; r&ocirc;ng l&agrave; nơi quan trọng nhất l&agrave;ng n&ecirc;n đ&agrave;n &ocirc;ng trong l&agrave;ng phải thay nhau ngủ qua đ&ecirc;m tại đ&acirc;y để tr&ocirc;ng coi. Một số l&agrave;ng l&agrave;m đến hai nh&agrave; r&ocirc;ng: &ldquo;nh&agrave; r&ocirc;ng c&aacute;i&rdquo; nhỏ v&agrave; c&oacute; m&aacute;i thấp d&agrave;nh cho phụ nữ, &ldquo;nh&agrave; r&ocirc;ng đực&rdquo; d&agrave;nh cho đ&agrave;n &ocirc;ng c&oacute; quy m&ocirc; lớn hơn v&agrave; trang tr&iacute; c&ocirc;ng phu. Ngo&agrave;i mục đ&iacute;ch g&igrave;n giữ kh&ocirc;ng gian thi&ecirc;ng, nh&agrave; r&ocirc;ng l&agrave; nơi người d&acirc;n trao đổi những c&acirc;u chuyện, kinh nghiệm trong đời sống. Nam nữ độc th&acirc;n trong l&agrave;ng c&oacute; thể qu&acirc;y quần tại nh&agrave; r&ocirc;ng để thăm hỏi, t&igrave;m bạn đời, tuy nhi&ecirc;n kh&ocirc;ng đi được ph&eacute;p đi qu&aacute; giới hạn.</p>

<p>6. Mỗi d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n c&oacute; kiểu l&agrave;m nh&agrave; r&ocirc;ng kh&aacute;c nhau. K&iacute;ch thước nh&agrave; r&ocirc;ng nhỏ v&agrave; thấp nhất l&agrave; của người Giẻ Tri&ecirc;ng. Nh&agrave; r&ocirc;ng của người X&ecirc; &ETH;ăng cao v&uacute;t. Nh&agrave; r&ocirc;ng của người Gia Rai c&oacute; m&aacute;i mảnh, dẹt như lưỡi r&igrave;u. Nh&agrave; r&ocirc;ng của người Ba Na to hơn nh&agrave; Gia Rai, c&oacute; đường n&eacute;t mềm mại v&agrave; thường c&oacute; c&aacute;c nh&agrave; s&agrave;n xung quanh. Điểm chung của c&aacute;c ng&ocirc;i nh&agrave; r&ocirc;ng l&agrave; được x&acirc;y cất tr&ecirc;n một khoảng đất rộng, nằm ngay tại khu vực trung t&acirc;m của bu&ocirc;n l&agrave;ng.</p>

<p>&nbsp;</p>

<p>7. S&agrave;n nh&agrave; r&ocirc;ng được thiết kế gắn liền với văn h&oacute;a qu&acirc;y quần uống rượu cần của đồng b&agrave;o. S&agrave;n thường được l&agrave;m từ v&aacute;n gỗ hay ống tre nứa đập dập, khi gh&eacute;p kh&ocirc;ng kh&iacute;t nhau m&agrave; c&aacute;c tấm c&aacute;ch nhau khoảng 1 cm. Nhờ thế m&agrave; khi người d&acirc;n tập trung ăn uống, nước kh&ocirc;ng bị chảy l&ecirc;nh l&aacute;ng ra s&agrave;n. Mặt kh&aacute;c, kiểu s&agrave;n n&agrave;y gi&uacute;p cho việc vệ sinh nh&agrave; dễ d&agrave;ng hơn.</p>

<p>8. Cầu thang nh&agrave; r&ocirc;ng thường c&oacute; 7 đến 9 bậc, tuy nhi&ecirc;n mỗi d&acirc;n tộc lại c&oacute; trang tr&iacute; kh&aacute;c nhau. Tr&ecirc;n th&agrave;nh v&agrave; cột của cầu thang, người Gia Rai hay tạc h&igrave;nh quả bầu đựng nước, người Bana khắc h&igrave;nh ngọn c&acirc;y rau dớn, c&ograve;n người Giẻ Tri&ecirc;ng, Xơ Đăng thường đẽo h&igrave;nh n&uacute;m chi&ecirc;ng hoặc mũi thuyền.</p>

<p>9. Nếu như m&aacute;i đ&igrave;nh miền xu&ocirc;i gắn liền với h&igrave;nh ảnh c&acirc;y đa, th&igrave; nh&agrave; r&ocirc;ng T&acirc;y Nguy&ecirc;n c&oacute; c&acirc;y n&ecirc;u. C&acirc;y n&ecirc;u được trang tr&iacute; nhiều họa tiết, đặt ở ph&iacute;a trước s&acirc;n ch&iacute;nh giữa của ng&ocirc;i nh&agrave; r&ocirc;ng để phục vụ c&aacute;c lễ hội lớn của bu&ocirc;n l&agrave;ng. Theo quan niệm, c&acirc;y n&ecirc;u l&agrave; nơi hội tụ c&aacute;c vị thần linh. Ở từng lễ hội, c&acirc;y n&ecirc;u mang một h&igrave;nh ảnh biểu tượng kh&aacute;c nhau như c&acirc;y n&ecirc;u trong lễ đ&acirc;m tr&acirc;u c&oacute; 4 nh&aacute;nh, lễ mừng l&uacute;a mới c&acirc;y n&ecirc;u chỉ c&oacute; 1 nh&aacute;nh&hellip;</p>

<p>10. Nh&agrave; r&ocirc;ng chỉ gắn với bu&ocirc;n l&agrave;ng, kh&ocirc;ng c&oacute; nh&agrave; r&ocirc;ng cấp tỉnh, cấp huyện hoặc nh&agrave; r&ocirc;ng chung nhiều l&agrave;ng. Hiện nay, nh&agrave; r&ocirc;ng truyền thống lu&ocirc;n được g&igrave;n giữ tại trung t&acirc;m l&agrave;ng. Một số địa điểm bu&ocirc;n l&agrave;ng c&oacute; nh&agrave; r&ocirc;ng hiện nay l&agrave; nh&agrave; r&ocirc;ng Kon Klor ở th&agrave;nh phố Kon Tum, l&agrave;ng Plei Phung, l&agrave;ng Kon So Lăl (huyện Chư Pah) v&agrave; l&agrave;ng Đ&ecirc; K&rsquo;tu (huyện Mang Yang) ở Gia Lai.</p>

<p>&nbsp;</p>
</body>
</html>
', NULL, NULL, N'19052021_110520yh_2.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:05:20.213' AS DateTime), NULL, NULL, 1)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (N'b7ff61ba-d9c4-43b7-ba2b-daf174c30bd6', 4, N'Chương trình Ưu đãi VNPay', N'Nhằm gia tăng tiện tích cho khách hàng khi mua tour, Công ty Du lịch Vietravel tổ chức chương trình ưu đãi hấp dẫn cho khách hàng khi thanh toán bằng dịch vụ VNPay QR code.', NULL, NULL, NULL, N'19052021_102059yh_advertising1616146679.jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:20:58.263' AS DateTime), NULL, NULL, 1)
GO
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (1, 1, N'Tin tức ', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (2, 2, N'Cẩm nang du lịch', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (3, 3, N'Banner header', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (4, 4, N'Banner bottom', 1, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ Tuyền Lâm', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Ven Hồ', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Ga Xe Lửa Cũ', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Trung Tâm Thành Phố Đà Lạt', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Thác Cam Ly', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Thung Lũng Tình Yêu / Đồi Mộng Mơ', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Thung Lũng Vàng', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Núi LangBiang', 9, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Khu du lịch Trúc Lâm Viên', 9, NULL, 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_Booking] ([BookingId], [RoomPriceId], [RoomQuantity], [Amount], [AmountCancel], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [Email], [PhoneNumber], [ContactName], [ProvinceId], [SaleId], [Status]) VALUES (N'f004dcfe-d5ec-4989-afb3-c8a1f1c9a12f', N'74a7d97d-8d5a-49f0-8865-a8f09f0f7e12', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL, N'nguyenthanhdat.bc2310@gmail.com', N'0932980033', N'Nguyễn Thành Đạt', 1, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[tbl_Content] ON 

INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (2, N'privacy-cookies', N'Chính sách riêng tư', N'<html>', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Content] OFF
GO
INSERT [dbo].[tbl_Country] ([CountryId], [CountryName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1', N'Việt Nam', N'VIE', 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Direction] ON 

INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hướng nội bộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Có cửa sổ (không hướng)', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Hướng phố', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Hướng núi', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Direction] OFF
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Huyện Hoài Nhơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Huyện Vân Canh', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Huyện Hoài Ân', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Huyện Tuy Phước', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Tp.Qui Nhơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Huyện Tây Sơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Huyện Phù Cát', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'Huyện An Nhơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Huyện Vĩnh Thạnh', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Huyện Phù Mỹ', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Huyện An Lão', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'Tam Đường', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'Tp.Lai Châu', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Sìn Hồ', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Mường Tè', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Tân Uyên', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'Than Uyên', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'Phong Thổ', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'Nậm Nhùn', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'Cam Lâm', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N'Trường Sa', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'Diên Khánh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'Vạn Ninh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'Khánh Sơn', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'Thị xã Ninh Hòa', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'Tp.Cam Ranh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'Tp.Nha Trang', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'Huyện Khánh Vĩnh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'Thị xã Hà Tiên', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'Huyện Phú Quốc', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (31, N'Hòn Đất', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'An Minh', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'Tp.Rạch Giá', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N' Huyện Gò Quao', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'Tân Hiệp', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'Huyện Châu Thành', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (37, N'Huyện Vĩnh Thuận', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (38, N'Kiên Lương', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (39, N'An Biên', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (40, N'Huyện Giồng Riềng', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (41, N'Giang Thành', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (42, N'Kiên Hải', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (43, N'U Minh Thượng', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (44, N'Huyện Trảng Bàng', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (45, N'Huyện Châu Thành', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (46, N'Tân Biên', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (47, N'Dương Minh Châu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (48, N'Huyện Gò Dầu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (49, N'Hòa Thành', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (50, N'Huyện Bến Cầu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (51, N'Huyện Tân Châu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (52, N'Tp.Tây Ninh', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (53, N'Huyện Tân Thành', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (54, N'Huyện Đất Đỏ', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (55, N'Huyện Châu Đức', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (56, N'Huyện Côn Đảo', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (57, N'Thị xã Bà Rịa', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (58, N'Huyện Long Đất (Xóa)', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (59, N'Tp.Vũng Tàu', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (60, N'Huyện Xuyên Mộc', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (61, N'Huyện Long Điền', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (62, N'Xuân Tây', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (63, N'Hà Đông', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (64, N'Huyện Thạch Nhất', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (65, N'Quận Thanh Trì', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (66, N'Tp.Hà Tĩnh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (67, N'Can Lộc', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (68, N'Nghi Xuân', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (69, N'Cẩm Xuyên', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (70, N'Đức Thọ', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (71, N'Thạch Hà', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (72, N'Kỳ Anh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (73, N'Thị xã Hồng Lĩnh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (74, N'Hương Sơn', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (75, N'Hương Khê', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (76, N'Lộc Hà', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (77, N'Thị xã Kỳ Anh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (78, N'Vũ Quang', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (79, N'Thị xã Bình Long', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (80, N'Huyện Lộc Ninh', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (81, N'Huyện Đồng Phú', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (82, N'Thị Xã Đồng Xoài', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (83, N'Huyện Phú Riềng', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (84, N'Huyện Chơn Thành', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (85, N'Huyện Hớn Quản', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (86, N'Huyện Bù Đăng', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (87, N'Thị xã Phước Long', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (88, N'Huyện Bù Gia Mập', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (89, N'Huyện Bù Đốp', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (90, N'Thị xã Vĩnh Châu', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (91, N'Huyện Minh Châu', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (92, N'Long Phú', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (93, N'Huyện Kế Sách', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (94, N'Huyện Trần Đề', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (95, N'Huyện Mỹ Xuyên', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (96, N'Huyện Thạnh Trị', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (97, N'Huyện Mỹ Tú', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (98, N'Thị xã Ngã Năm', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (99, N'Huyện Châu Thành', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (100, N'Tp.Sóc Trăng', 13, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (101, N'Phú Vang', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (102, N'A Lưới', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (103, N'Phong Điền', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (104, N'Phú Lộc', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (105, N'Hương Thuỷ', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (106, N'Tp.Huế', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (107, N'Quảng Điền', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (108, N'Hương Trà', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (109, N'Nam Đông', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (110, N'Thái Thụy', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (111, N'Hưng Hà', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (112, N'Tp.Thái Bình', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (113, N'Đông Hưng', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (114, N'Kiến Xương', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (115, N'Vũ Thư', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (116, N'Quỳnh Phụ', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (117, N'Tiền Hải', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (118, N'Tp.Yên Bái', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (119, N'Yên Bình', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (120, N'Thị xã Nghĩa Lộ', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (121, N'Văn Chấn', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (122, N'Văn Yên', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (123, N'Trạm Tấu', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (124, N'Lục Yên', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (125, N'Trấn Yên', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (126, N'Mù Căng Chải', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (127, N'Đắk Glei', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (128, N'Ia H'' Drai', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (129, N'Kon Rẫy', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (130, N'Tp.Kom Tum', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (131, N'Ngọc Hồi', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (132, N'Đắk Tô', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (133, N'Tu Mơ Rông', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (134, N'Kon Plông', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (135, N'Đắk Hà', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (136, N'Sa Thầy', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (137, N'Hải Hà', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (138, N'Hoành Bồ', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (139, N'Tp.Uông Bí', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (140, N'Tp.Cẩm Phả', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (141, N'Bình Liêu', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (142, N'Tp.Hạ Long', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (143, N'Tiên Yên', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (144, N'Ba Chẽ', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (145, N'Cô Tô', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (146, N'Đầm Hà', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (147, N'Thị xã Quảng Yên', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (148, N'Thị xã Đông Triều', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (149, N'Vân Đồn', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (150, N'Tp.Móng Cái', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (151, N'Quận 4', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (152, N'Quận 8', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (153, N'Quận 2', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (154, N'Quận Tân Bình', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (155, N'Quận Bình Thạnh', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (156, N'Huyện Củ Chi', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (157, N'Quận 10', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (158, N'Huyện Nhà Bè', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (159, N'Quận 7', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (160, N'Huyện Hóc Môn', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (161, N'Quận 6', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (162, N'Quận Bình Tân', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (163, N'Quận 12', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (164, N'Quận Tân Phú', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (165, N'Bình Chánh', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (166, N'Quận 3', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (167, N'Quận Thủ Đức', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (168, N'Quận 9', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (169, N'Quận Phú Nhuận', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (170, N'Quận 1', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (171, N'Quận Gò Vấp', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (172, N'Quận 11', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (173, N'Huyện Cần Giờ', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (174, N'Quận 5', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (175, N'Ngọc Hiển', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (176, N'Huyện Năm Căn', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (177, N'Phú Tân', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (178, N'Huyện Trần Văn Thời', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (179, N'Cái Nước', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (180, N'Huyện Thới Bình ', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (181, N'U Minh', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (182, N'Tp.Cà Mau', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (183, N'Đầm Dơi', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (184, N'Con Cuông', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (185, N'Huyện Nghi Lộc', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (186, N'Yên Thành', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (187, N'Nam Đàn', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (188, N'Thanh Chương', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (189, N'Kỳ Sơn', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (190, N'Quỳnh Lưu', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (191, N'Quỳ Hợp', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (192, N'Diễn Châu', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (193, N'Thị xã Cửa Lò', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (194, N'Tương Dương', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (195, N'Tân Kỳ', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (196, N'Tp.Vinh', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (197, N'Anh Sơn', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (198, N'Thị xã Thái Hoà', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (199, N'Đô Lương', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (200, N'Nghĩa Đàn', 21, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (201, N'Hoàng Mai', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (202, N'Quế Phong', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (203, N'Hưng Nguyên', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (204, N'Quỳ Châu', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (205, N'Huyện Yên Thế', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (206, N'Huyện Yên Dũng', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (207, N'Huyện Việt Yên', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (208, N'Huyện Lục Ngạn', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (209, N'Tp.Bắc Giang', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (210, N'Huyện Hiệp Hoà', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (211, N'Huyện Lạng Giang', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (212, N'Huyện Tân Yên', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (213, N'Huyện Sơn Động', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (214, N'Huyện Lục Nam', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (215, N'Huyện Gia Bình', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (216, N'Huyện Giá Rai', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (217, N'Huyện Hồng Dân', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (218, N'Huyện Đông Hải', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (219, N'Huyện Phước Long', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (220, N'Huyện Vĩnh Lợi', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (221, N'Thị xã Bạc Liêu', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (222, N'Huyện Hoà Bình', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (223, N'Yên Mô', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (224, N'Tp.Ninh Bình', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (225, N'Yên Khánh', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (226, N'Nho Quan', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (227, N'Hoa Lư', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (228, N'Tp.Tam Điệp', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (229, N'Kim Sơn', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (230, N'Gia Viễn', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (231, N'Yên Lập', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (232, N'Cẩm Khê', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (233, N'Đoan Hùng', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (234, N'Tam Nông', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (235, N'Thanh Thuỷ', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (236, N'Thanh Ba', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (237, N'Tp.Việt Trì', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (238, N'Thanh Sơn', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (239, N'Phù Ninh', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (240, N'Hạ Hoà', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (241, N'Tân Sơn', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (242, N'Thị xã Phú Thọ', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (243, N'Lâm Thao', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (244, N'Vĩnh Tường', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (245, N'Sông Lô', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (246, N'VĨNH TƯỜNG (Xóa)', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (247, N'Tam Dương', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (248, N'Bình Xuyên', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (249, N'Thị xã Phúc Yên', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (250, N'Lập Thạch', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (251, N'Yên Lạc', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (252, N'Tp.Vĩnh Yên', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (253, N'Tam Đảo', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (254, N'Tân Lạc', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (255, N'Kỳ Sơn', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (256, N'Đà Bắc', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (257, N'Lạc Thủy', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (258, N'Tp.Hòa Bình', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (259, N'Mai Châu', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (260, N'Kim Bôi', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (261, N'Huyện Lương Sơn', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (262, N'Yên Thủy', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (263, N'Huyện Cao Phong', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (264, N'Lạc Sơn', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (265, N'M''Đrắk', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (266, N'Ea H''leo', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (267, N'Ea Súp', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (268, N'Krông A Na', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (269, N'Ea Kar', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (270, N'Cư M''gar', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (271, N'Thị xã Gia Nghĩa (Xóa)', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (272, N'Buôn Đôn', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (273, N'Thị Xã Buôn Hồ', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (274, N'Krông Búk', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (275, N'Krông Năng', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (276, N'Lắk', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (277, N'Krông Bông', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (278, N'Tp. Buôn Mê Thuộc', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (279, N'Cư Kuin', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (280, N'Krông Pắc', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (281, N'Ninh Sơn', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (282, N'Ninh Phước', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (283, N'Tp.Phan Rang-Tháp Chàm', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (284, N'Bác Ái', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (285, N'Thuận Nam', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (286, N'Ninh Hải', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (287, N'Thuận Bắc', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (288, N'Hàm Thuận Bắc', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (289, N'Huyện Tuy Phong', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (290, N'Tp.Phan Thiết', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (291, N'Huyện Thánh Linh', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (292, N'Phú Quí', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (293, N'Bắc Bình', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (294, N'Đức Linh', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (295, N'Hàm Tân', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (296, N'Thị xã La Gi', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (297, N'Hàm Thuận Nam', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (298, N'Giao Thủy', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (299, N'Tp.Nam Định', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (300, N'Xuân Trường', 33, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (301, N'Nghĩa Hưng', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (302, N'Nam Trực', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (303, N'Mỹ Lộc', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (304, N'Ý Yên', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (305, N'Trực Ninh', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (306, N'Vụ Bản', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (307, N'Hải Hậu', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (308, N'Đắk Song', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (309, N'Đắk Mil', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (310, N'Đắk R''Lấp', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (311, N'Krông Nô', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (312, N'Đăk Glong', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (313, N'Tuy Đức', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (314, N'Cư Jút', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (315, N'Thị xã Gia Nghĩa', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (316, N'Nà Hang', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (317, N'Lâm Bình', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (318, N'Yên Sơn', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (319, N'Sơn Dương', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (320, N'Hàm Yên', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (321, N'Tp.Tuyên Quang', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (322, N'Chiêm Hóa', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (323, N'Cái Răng', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (324, N'Vĩnh Thạnh', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (325, N'Phong Điền', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (326, N'Vĩnh Thạnh (Xóa)', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (327, N'Bình Thủy', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (328, N'Tp.Cần Thơ', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (329, N'Thốt Nốt', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (330, N'Thới Lai', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (331, N'An Phú (Xóa)', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (332, N'Ô Môn', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (333, N'Cờ Đỏ', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (334, N'Ninh Kiều', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (335, N'Tp.Bắc Ninh', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (336, N'Huyện Từ Sơn', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (337, N'Huyện Lương Tài', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (338, N'Huyện Thuận Thành', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (339, N'Huyện Tiên Du', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (340, N'Huyện Yên Phong', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (341, N'Huyện Quế Võ', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (342, N'Ba Tri', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (343, N'Bình Đại', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (344, N'Thị xã Bến Tre', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (345, N'Giồng Trôm', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (346, N'Chợ Lách', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (347, N'Thạnh Phú', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (348, N'Mỏ Cày Nam', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (349, N'Mỏ Cày Bắc', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (350, N'Châu Thành', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (351, N'Thiệu Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (352, N'Nông Cống', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (353, N'Thị xã Bỉm Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (354, N'Hà Trung', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (355, N'Triệu Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (356, N'Vĩnh Lộc', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (357, N'Yên Định', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (358, N'Tp Thanh Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (359, N'Thường Xuân', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (360, N'Đông Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (361, N'Quảng Xương', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (362, N'Quan Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (363, N'Hoằng Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (364, N'Thị xã Sầm Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (365, N'Thọ Xuân', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (366, N'Cẩm Thủy', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (367, N'Hậu Lộc', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (368, N'Ngọc Lặc', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (369, N'Lang Chánh', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (370, N'Như Xuân', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (371, N'Bá Thước', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (372, N'Như Thanh', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (373, N'Mường Lát', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (374, N'Nga Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (375, N'Quan Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (376, N'Tĩnh Gia', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (377, N'Thạch Thành', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (378, N'Huyện Kiến Thụy', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (379, N'Huyện Tiên Lãng', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (380, N'Quận Hồng Bàng', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (381, N'Huyện Thủy Nguyên', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (382, N'Quận Đồ Sơn', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (383, N'Quận Lê Chân', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (384, N'Huyện Cát Hải', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (385, N'Huyện An Dương', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (386, N'Quận Ngô Quyền', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (387, N'Quận Dương Kinh', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (388, N'Huyện Bạch Long Vĩ', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (389, N'Quận Kiến An', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (390, N'Vĩnh Bảo', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (391, N'Quận Hải An', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (392, N'Huyện An Lão', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (393, N'Cồn Cỏ', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (394, N'Cam Lộ', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (395, N'Gio Linh', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (396, N'Triệu Phong', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (397, N'Thị xã Quảng Trị', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (398, N'Hướng Hóa', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (399, N'Tp.Đồng Hà', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (400, N'Đa Krông', 41, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (401, N'Vĩnh Linh', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (402, N'Hải Lăng', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (403, N'Tam Bình', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (404, N'Mang Thít', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (405, N'Huyện Vũng Liêm', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (406, N'Trà Ôn', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (407, N'Bình Minh', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (408, N'Cái Vồn (Xóa)', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (409, N'Cái Răng (Xóa)', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (410, N'Tp.Vĩnh Long', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (411, N'Huyện Long Hồ', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (412, N'Huyện Bình Tân', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (413, N'Trà Cú', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (414, N'Huyện Tiểu Cần', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (415, N'Thị xã Duyện Hải', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (416, N'Càng Long', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (417, N'Duyên Hải', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (418, N'Thị xã Trà Vinh', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (419, N'Cầu Kè', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (420, N'Huyện Cầu Ngang', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (421, N'Châu Thành', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (422, N'Thanh Liêm', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (423, N'Bình Lục', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (424, N'Kim Bảng', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (425, N'Duy Tiên', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (426, N'Huyện Lý Nhân', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (427, N'Tp.Phủ Lý', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (428, N'Huyện Đồng Hỷ', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (429, N'Tp.Sông Công', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (430, N'Huyện Phú Bình', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (431, N'Huyện Võ Nhai', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (432, N'Huyện Đại Từ', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (433, N'Huyện Phú Lương', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (434, N'Huyện Phổ Yên', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (435, N'Tp.Thái Nguyên', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (436, N'Huyện Định Hóa', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (437, N'Tp.Vị Thanh', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (438, N'Huyện Châu Thành', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (439, N'Thị xã Ngã Bảy', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (440, N'Huyện Vị Thủy', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (441, N'Thị xã Long Mỹ', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (442, N'Huyện Châu Thành A', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (443, N'Long Mỹ', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (444, N'Phụng Hiệp', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (445, N'Vân Hồ', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (446, N'Sốp Cộp', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (447, N'Yên Châu', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (448, N'Mường La', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (449, N'Mai Sơn', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (450, N'Tp.Sơn La', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (451, N'Quỳnh Nhai', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (452, N'Thuận Châu', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (453, N'Sông Mã', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (454, N'Bắc Yên', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (455, N'Phù Yên', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (456, N'Mộc Châu', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (457, N'Đam Rông', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (458, N'Lạc Dương', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (459, N'Đạ Tẻh', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (460, N'Huyện Bảo Lâm', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (461, N'Đơn Dương', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (462, N'Huyện Lâm Hà', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (463, N'Di Linh', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (464, N'Đức Trọng', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (465, N'Tp.Bảo Lộc', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (466, N'Tp.Đà Lạt', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (467, N'Huyện Đạ Huoai', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (468, N'Cát Tiên', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (469, N'Thị xã Long Khánh', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (470, N'Huyện Nhơn Trạch', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (471, N'Xuân Lộc', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (472, N'Trảng Bom', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (473, N'Long Thành (Xóa)', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (474, N'Cẩm Mỹ', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (475, N'Tp.Biên Hòa', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (476, N'HUYỆN HỐ NAI (Xóa)', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (477, N'Định Quán', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (478, N'Long Thành', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (479, N'Huyện Vĩnh Cửu', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (480, N'Tân Phú', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (481, N'Thống Nhất', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (482, N'Huyện Văn Lâm', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (483, N'Văn Giang', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (484, N'Tp.Hưng Yên', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (485, N'Khoái Châu', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (486, N'Mỹ Hào', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (487, N'Kim Động', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (488, N'Tiên Lữ', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (489, N'Yên Mỹ', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (490, N'Ân Thi', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (491, N'Huyện Phù Cừ', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (492, N'Phù Cừ (Xóa)', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (493, N'Phúc Thọ', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (494, N'Hoàn Kiếm', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (495, N'Hà Đông', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (496, N'Thanh Oai', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (497, N'Phú Xuyên', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (498, N'Cầu Giấy', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (499, N'Huyện Thanh Trì', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (500, N'Huyện Gia Lâm', 53, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (501, N'Mê Linh', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (502, N'Thạch Thất', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (503, N'Huyện Sóc Sơn', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (504, N'Mỹ Đức', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (505, N'Quốc Oai', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (506, N'Sơn Tây', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (507, N'Long Biên', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (508, N'Huyện Đông Anh', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (509, N'Ứng Hòa', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (510, N'Hoài Đức', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (511, N'Hoàng Mai', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (512, N'Đan Phượng', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (513, N'Tây Hồ', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (514, N'Chương Mỹ', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (515, N'Hai Bà Trưng', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (516, N'Thanh Xuân', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (517, N'Đống Đa', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (518, N'Ba Đình', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (519, N'Thường Tín', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (520, N'Từ Liêm', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (521, N'Huyện Ba Vì', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (522, N'Thị xã Bến Cát', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (523, N'Dĩ An', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (524, N'THỊ TRẤN LÁI THIÊU (Xóa)', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (525, N'Dầu Tiếng', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (526, N'Bắc Tân Uyên', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (527, N'Thuận An', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (528, N'DĨ AN (Xóa)', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (529, N'Thị xã Tân Uyên', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (530, N'Bàu Bàng', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (531, N'Huyện Dầu Tiếng (Xóa)', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (532, N'Thủ Dầu Một', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (533, N'Phú Giáo', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (534, N'BMT', 56, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (535, N'Tân Hưng', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (536, N'Huyện Bến Lức', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (537, N'Đức Huệ', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (538, N'Cần Đước', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (539, N'Huyện Châu Thành', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (540, N'Tp.Tân An', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (541, N'Tân Trụ', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (542, N'Tân Thạnh', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (543, N'Thạnh Hóa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (544, N'Mộc Hóa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (545, N'Huyện Cần Giuộc', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (546, N'Huyện Đức Hòa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (547, N'Thủ Thừa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (548, N'Huyện Thủ Thừa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (549, N'Vĩnh Hưng', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (550, N'Điện Biên Đông', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (551, N'Tp.Điện Biên', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (552, N'Thị Xã Mường Lay', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (553, N'Tủa Chùa', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (554, N'Mường Chà', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (555, N'Điện Biên', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (556, N'Nậm Pồ', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (557, N'Mường Ảng', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (558, N'Mường Nhé', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (559, N'Tuần Giáo', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (560, N'Kim Thành', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (561, N'Thanh Miện', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (562, N'Nam Sách', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (563, N'Thị xã Chí Linh', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (564, N'Tp.Hải Dương', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (565, N'Gia Lộc', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (566, N'Bình Giang', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (567, N'Kinh Môn', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (568, N'Cẩm Giàng', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (569, N'Tứ Kỳ', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (570, N'Ninh Giang', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (571, N'Thanh Hà', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (572, N'SaPa', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (573, N'Văn Bàn', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (574, N'Bát Xát', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (575, N'Mường Khương', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (576, N'Tp.Lào Cai', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (577, N'Bảo Yên', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (578, N'Bảo Thắng', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (579, N'Si Ma Cai', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (580, N'Bắc Hà', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (581, N'Thạch An', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (582, N'Trà Lĩnh', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (583, N'Bảo Lạc', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (584, N'Hà Quảng', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (585, N'Phục Hoà', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (586, N'Tp.Cao Bằng', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (587, N'Quảng Uyên', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (588, N'Nguyên Bình', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (589, N'Bảo Lâm', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (590, N'Hoà An', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (591, N'Thông Nông', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (592, N'Hạ Lang', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (593, N'Trùng Khánh', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (594, N'Lộc Bình', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (595, N'Bình Gia', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (596, N'Cao Lộc', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (597, N'Hữu Lũng', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (598, N'Chi Lăng', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (599, N'Văn Lãng', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (600, N'Văn Quan', 62, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (601, N'Bắc Sơn', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (602, N'Tp.Lạng Sơn', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (603, N'Đình Lập', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (604, N'Tràng Định', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (605, N'Thị xã Cai Lậy', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (606, N'Thị xã Gò Công', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (607, N'Huyện Tân Phú Đông', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (608, N'Gò Công Đông', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (609, N'Tp.Mỹ Tho', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (610, N'Châu Thành', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (611, N'Chợ Gạo', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (612, N'Cai Lậy', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (613, N'Cái Bè', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (614, N'Gò Công Tây', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (615, N'Huyện Tân Phước', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (616, N'Ia Grai', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (617, N'Phú Thiện', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (618, N'Mang Yang', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (619, N'Kông Chro', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (620, N'Tp.Pleiku', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (621, N'Thị xã Ayun Pa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (622, N'KBang', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (623, N'Thị xã An Khê', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (624, N'Đăk Đoa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (625, N'Đăk Pơ', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (626, N'Chư Păh', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (627, N'Chư Pưh', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (628, N'Chư Prông', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (629, N'Ia Pa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (630, N'Đức Cơ', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (631, N'Chư Sê', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (632, N'Krông Pa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (633, N'Tp.Sa Đét', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (634, N'Hồng Ngự', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (635, N'Huyện Tam Nông', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (636, N'Cao Lãnh', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (637, N'Thị xã Hồng Ngự', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (638, N'Huyện Thanh Bình', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (639, N'Huyện Lấp Vò', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (640, N'Tp.Cao Lãnh', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (641, N'Huyện Lai Vung', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (642, N'Huyện Tân Hồng', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (643, N'Tháp Mười', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (644, N'Châu Thành', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (645, N'Huyện Núi Thành', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (646, N'Bắc Trà My', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (647, N'Tp.Hội An', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (648, N'Hiệp Đức', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (649, N'Nông Sơn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (650, N'Nam Giang', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (651, N'Phú Ninh', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (652, N'Phước Sơn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (653, N'Tây Giang', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (654, N'Nam Trà My', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (655, N'Thăng Bình', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (656, N'Thị xã Điện Bàn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (657, N'Tp.Tam Kỳ', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (658, N'Quế Sơn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (659, N'Đại Lộc', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (660, N'Đông Giang', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (661, N'Tiên Phước', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (662, N'Duy Xuyên', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (663, N'Huyện Bạch Thông', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (664, N'Thị xã Bắc Cạn', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (665, N'Huyện Ngân Sơn', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (666, N'Huyện Ba Bể', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (667, N'Huyện Pác Nặm', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (668, N'Huyện Chợ Đồn', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (669, N'Huyện Chợ Mới', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (670, N'Huyện Na Rì', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (671, N'Tp.Tuy Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (672, N'Phú Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (673, N'Sông Hinh', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (674, N'Đồng Xuân', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (675, N'Thị xã Sông Cầu', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (676, N'Sơn Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (677, N'Tây Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (678, N'Huyện Đông Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (679, N'Tuy An', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (680, N'Quận Thanh Khê', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (681, N'Quận Liên Chiểu', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (682, N'Quận Hải Châu', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (683, N'Quận Cẩm Lệ', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (684, N'Huyện Hòa Vang', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (685, N'Huyện Hoàng Sa', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (686, N'Huyện Sơn Trà', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (687, N'Quận Ngũ Hành Sơn', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (688, N'Tp.Đồng Hới', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (689, N'Quảng Ninh', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (690, N'Tuyên Hóa', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (691, N'Minh Hóa', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (692, N'Bố Trạch', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (693, N'Thị xã Ba Đồn', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (694, N'Quảng Trạch', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (695, N'Lệ Thủy', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (696, N'Nghĩa Hành', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (697, N'Sơn Hà', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (698, N'Lý Sơn', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (699, N'Mộ Đức', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (700, N'Tư Nghĩa', 74, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (701, N'Bình Sơn', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (702, N'Sơn Tây', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (703, N'Sơn Tịnh', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (704, N'Minh Long', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (705, N'Tp.Quảng Ngãi', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (706, N'Tây Trà', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (707, N'Đức Phổ', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (708, N'Trà Bồng', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (709, N'Ba Tơ', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (710, N'Tp.Hà Giang', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (711, N'Yên Minh', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (712, N'Quản Bạ', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (713, N'Bắc Mê', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (714, N'Huyện Mèo Vạc', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (715, N'Hoàng Su Phì', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (716, N'Vị Xuyên', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (717, N'Quang Bình', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (718, N'Bắc Quang', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (719, N'Huyện Đồng Văn', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (720, N'Xín Mần', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (721, N'Huyện Phú Tân', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (722, N'Huyện Tịnh Biên', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (723, N'TP.Châu Đốc', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (724, N'Huyện Thoại Sơn', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (725, N'Huyện Tri Tôn', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (726, N'Huyện Tân Châu', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (727, N'Huyện An Phú', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (728, N'Huyện Châu Thành', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (729, N'Huyện Chợ Mới', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (730, N'Huyện Tân Châu (Xóa)', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (731, N'Huyện Châu Phú', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (732, N'TP.Long Xuyên', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (733, N'Rạch Giá', 77, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Khách Sạn LaDaLat', N'1', 1, 14, N'11,10,7,9,4', 4, 1, N'4,7,5,15,6,10,9,18,11,16,3', N'106A  Mai Anh Đào, Phường 8, Đà Lạt, Lâm Đồng', N'54545', NULL, NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p><strong>Tại Ladalat</strong>, ch&uacute;ng t&ocirc;i ho&agrave;n to&agrave;n t&ocirc;n trọng v&agrave; bảo tồn c&aacute;c đặc điểm ri&ecirc;ng biệt đặc trưng của Đ&agrave; Lạt. H&agrave;nh lang rộng lớn đầy hoa anh đ&agrave;o &ndash; lo&agrave;i hoa được biết đến như biểu tượng của Đ&agrave; Lạt. <span style="color:#2980b9">T&igrave;nh y&ecirc;u Lang v&agrave; Biang được hồi sinh v&agrave; kỷ niệm. Những đặc điểm bản chất của Đ&agrave; Lạt tồn tại như một sự kh&ocirc;ng thể tr&aacute;nh khỏi, tạo ra sự l&atilde;ng mạn mơ m&agrave;ng của L<span style="background-color:#8e44ad">adala</span>t.</span></p>
</body>
</html>
', N'fghdhd', NULL, N'25052021_055019yh_hinh4.jpg', 0, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T17:55:17.187' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'd4258d99-6163-410b-a1c7-2e2fe6334a4d', N'KS3', N'1', 1, NULL, NULL, 4, 1, N'34,29,14', N'200', N'200', N'200', NULL, NULL, N'200', N'200', N'200', NULL, 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T18:07:33.937' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'21985eda-87ba-4176-932e-3194d286b4b7', N'Khach san 1', N'1', 1, 14, N'3', 2, 1, N'4,29,8', N'119 le van khuong', N'14654321534', N'3574642314', NULL, NULL, N'asdfasdfasdfasd', N'tranglth@saigoncenterreal.vn', N'asdfasdfasdf', N'21052021_021200yb_cbr20rr.PNG', NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-21T02:12:00.970' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'99a40b13-16e5-4a17-9f18-417566705967', N'Khách sạn A', NULL, 1, 1, N'7', 3, 1, N'4,5,6', N'20 SR', NULL, NULL, NULL, NULL, N'nguye thadfádf', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'77c99374-bd74-4378-8011-4361c2df7db5', N'KS 100', N'1', 1, NULL, NULL, 3, 1, N'34,29,33', N'100', N'100', N'100', NULL, NULL, N'100', N'100', N'100', NULL, 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-18T23:52:18.853' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'535a5b23-6a2f-4d8e-8260-79152ff81f11', N'Khach san 2', N'1', 1, NULL, NULL, 4, 1, N'34,33,8,28', N'23asdf', N'23452345', N'23452345', NULL, NULL, N'sdfgsdgdfg', N'sdgsdfgsdfg', N'sdfgsdfgsdfg', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'48c71873-dfb8-4dfd-9be2-8500bc481cfb', N'ks5', N'1', 1, NULL, NULL, 4, 1, N'21,4', N'500', N'500', N'500', NULL, NULL, N'sdfádf', N'vp174', N'asdfasdfasdf', N'19052021_062828yh_cbr20rr.PNG', 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T18:28:21.363' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion]) VALUES (N'64477619-22da-45d0-9d24-8c63124e0d82', N'Khách sạn Holiday', N'1', 1, 13, N'11,10', 1, 1, N'29,14,28', N'H59-h60, Nguyễn Thị Nghĩa, Thành Phố Đà Lạt, Lâm Đồng, Việt Nam', N'0932980033', NULL, NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p>Tọa lạc ở th&agrave;nh phố Đ&agrave; Lạt, c&aacute;ch Vườn hoa Đ&agrave; Lạt 2,3 km v&agrave; Quảng trường L&acirc;m Vi&ecirc;n 2,7 km, Dalat Holiday Hotel cung cấp chỗ nghỉ với sảnh kh&aacute;ch chung v&agrave; WiFi miễn ph&iacute; trong to&agrave;n bộ khu&ocirc;n vi&ecirc;n cũng như chỗ đỗ xe ri&ecirc;ng miễn ph&iacute; cho kh&aacute;ch l&aacute;i xe. Chỗ nghỉ n&agrave;y nằm trong b&aacute;n k&iacute;nh khoảng 2,9 km từ Hồ Xu&acirc;n Hương, 3 km từ C&ocirc;ng vi&ecirc;n Yersin v&agrave; 7 km từ Thiền viện Tr&uacute;c L&acirc;m. Chỗ nghỉ c&oacute; dịch vụ ph&ograve;ng, b&agrave;n đặt tour v&agrave; dịch vụ giữ h&agrave;nh l&yacute; cho kh&aacute;ch.</p>

<p>Tất cả ph&ograve;ng nghỉ tại kh&aacute;ch sạn đều được trang bị TV truyền h&igrave;nh c&aacute;p m&agrave;n h&igrave;nh phẳng, ấm đun nước, v&ograve;i sen, đồ vệ sinh c&aacute; nh&acirc;n miễn ph&iacute; v&agrave; b&agrave;n l&agrave;m việc. Mỗi ph&ograve;ng c&ograve;n c&oacute; tủ để quần &aacute;o v&agrave; ph&ograve;ng tắm ri&ecirc;ng.</p>
</body>
</html>
', N'tranglth@saigoncenterreal.vn', NULL, N'25052021_042118yh_khach-san-da-lat-tot-nhat.jpg', 1, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T16:21:18.683' AS DateTime), NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] ON 

INSERT [dbo].[tbl_HotelCancellationPolicy] ([Id], [NumberDateCancel], [HotelId], [PercentAmount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 7, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'60        ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelCancellationPolicy] ([Id], [NumberDateCancel], [HotelId], [PercentAmount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, 5, N'99a40b13-16e5-4a17-9f18-417566705967', N'30        ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] ON 

INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ bơi 1', NULL, 0, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Massage/Spa', NULL, 0, 2, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Wifi miễn phí', NULL, 0, 3, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Bãi đỗ xe', NULL, 0, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Giặt là', NULL, 0, 10, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Lễ tân 24/24', NULL, 0, 13, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Đưa đón sân bay', NULL, 0, 5, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'Cho thuê máy bay', NULL, 0, 6, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Phòng gym', NULL, 0, 7, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Nhà hàng', NULL, 0, 8, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Phục vụ đồ ăn tại phòng', NULL, 0, 9, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Chấp nhận thú cưng', NULL, 0, 11, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Hỗ trợ đặt tour', NULL, 0, 12, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Thang máy', NULL, 0, 14, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'Máy ATM trong khách sạn', NULL, 0, 15, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'Phòng họp', NULL, 0, 16, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'Tổ chức sự kiện', NULL, 0, 17, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'Vòi hoa sen', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N' Mấy sấy tóc', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'Quạt', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'Truyền hình vệ tinh/cáp', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'Két sắt trong phòng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'Nước đóng chai miễn phí', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'Dép đi trong phòng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'Sàn trải thảm', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'Dịch vụ báo thức', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'Buồng tắm đứng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'Đồ vệ sinh cá nhân miễn phí', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'Quầy bar nhỏ (MiniBar)', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'Cà phê/Trà', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N'Ấm đun nước điện', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'Tivi màn hình phẳng', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'Điện thoại', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (37, N'test 2', NULL, 0, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (38, N'test1', NULL, 0, 2, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] ON 

INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Các mức phí trên chưa bao gồm thuế, phí', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Giường phụ không áp dụng cho loại phòng Basement Standard Triple No Window', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'dấdfádfsadfádf', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Trẻ từ 12 tuổi trở lên được tính như người lớn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Vui lòng nhập các yêu cầu đặc biệt vào mục Yêu cầu khác bên dưới Thông tin liên hệ để được hỗ trợ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'sdfádfádfsđàádfádf', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] ON 

INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Ưu đãi đặc biệt 1', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Đặt sớm giá tốt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Giá Shock giờ chót', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Gia thuong', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] ON 

INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 1, N'Khách sạn 1 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, 2, N'Khách sạn 2 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, 3, N'Khách sạn 3 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, 4, N'Khách sạn 4 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, 5, N'Khách sạn 5 sao', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] ON 

INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Bữa sáng miễn phí', NULL, 0, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'3 Bữa ăn miễn phí', NULL, 0, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Thêm giường phụ', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Miễn phí hủy phòng', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'An trua', NULL, 0, 3, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] ON 

INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Từ 0 đến 5.9 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Từ 6 đến 11 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 1520001, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Người lớn', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 523000, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'dADá', N'99a40b13-16e5-4a17-9f18-417566705967', 34243, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelType] ON 

INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Khách sạn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Resort', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Homestay', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Căn hộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Biệt thự', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Du thuyền', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] ON 

INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'TTD', N'Thẻ tính dụng', 1, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'TND', N'Thẻ nội địa', 2, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'CK', N'Chuyển khoản', 3, 1, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] OFF
GO
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'1', N'Bình Định', N'BDI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'1', N'Lai Châu', N'LCH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'1', N'Khánh Hòa', N'NHA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'1', N'Bình Định', N'BDI       ', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'1', N'Kiên Giang', N'KGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'1', N'Đà Lạt', N'DLA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'1', N'Nha Trang', NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'1', N'Tây Ninh', N'TNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'1', N'Bà Rịa - Vũng Tàu', N'BRV       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'1', N'Hà Tây', N'HTA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'1', N'Hà Tĩnh', N'HTI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'1', N'Bình Phước', N'BPU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'1', N'Sóc Trăng', N'STR       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'1', N'Huế', N'HUE       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'1', N'Thái Bình', N'TBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'1', N'Yên Bái', N'YBA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'1', N'Kon Tum', N'KTU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'1', N'Quảng Ninh', N'QNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'1', N'Tp. HCM', N'SGN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'1', N'Cà Mau', N'CMA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N'1', N'Nghệ An', N'NAN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'1', N'Bắc Giang', N'BGA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'1', N'Bạc Liêu', N'BLI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'1', N'Hạ Long', N'QNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'1', N'Ninh Bình', N'NBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'1', N'Phú Thọ', N'PTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'1', N'Vĩnh Phúc', N'VPH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'1', N'Hoà Bình', N'HBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'1', N'Đắk Lắk', N'DLA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'1', N'Ninh Thuận', N'NTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (31, N'1', N'Bình Thuận', N'BTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'1', N'Phan Thiết', N'BTH       ', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'1', N'Nam Định', N'NDI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N'1', N'Đắk Nông', N'DKN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'1', N'Tuyên Quang', N'TQU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'1', N'Cần Thơ', N'CTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (37, N'1', N'Bắc Ninh', N'BNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (38, N'1', N'Bến Tre', N'BTR       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (39, N'1', N'Thanh Hoá', N'THO       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (40, N'1', N'Hải Phòng', N'HPH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (41, N'1', N'Quảng Trị', N'QTR       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (42, N'1', N'Vĩnh Long', N'VLO       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (43, N'1', N'Trà Vinh', N'TVI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (44, N'1', N'Côn Đảo', NULL, 0, N'4a2ee6fc-e1e9-4643-bb36-025425223b41', NULL, N'4a2ee6fc-e1e9-4643-bb36-025425223b41', NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (45, N'1', N'Trường Sa', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (46, N'1', N'Hà Nam', N'HNA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (47, N'1', N'Thái Nguyên', N'TNG       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (48, N'1', N'Hậu Giang', N'HGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (49, N'1', N'Sơn La', N'SLA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (50, N'1', N'Lâm Đồng', N'DAT       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (51, N'1', N'Đồng Nai', N'DNA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (52, N'1', N'Hưng Yên', N'HYE       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (53, N'1', N'Hà Nội', N'HAN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (54, N'1', N'Trên tàu hỏa', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (55, N'1', N'Bình Dương', N'BDU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (56, N'1', N'Đak Lak', N'DAK       ', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (57, N'1', N'Long An', N'LAN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (58, N'1', N'Điện Biên', N'DBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (59, N'1', N'Hải Dương', N'HDU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (60, N'1', N'Lào Cai', N'LCA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (61, N'1', N'Cao Bằng', N'CBA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (62, N'1', N'Lạng Sơn', N'LSO       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (63, N'1', N'Tiền Giang', N'TGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (64, N'1', N'Gia Lai', N'GLA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (65, N'1', N'Đồng Tháp', N'DTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (66, N'1', N'Quảng Nam', N'QNA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (67, N'1', N'Hà Bắc', N'HBC       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (68, N'1', N'Bắc Cạn', N'BCA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (69, N'1', N'Phú Yên', N'PYE       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (70, N'1', N'Đà Nẵng', N'DNG       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (71, N'1', N'Hà Tiên', N'KGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (72, N'1', N'Quảng Bình', N'QBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (73, N'1', N'Sapa', NULL, 0, N'7990014b-94be-4b4b-939b-fc1057a1047c', NULL, N'7990014b-94be-4b4b-939b-fc1057a1047c', NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (74, N'1', N'Quảng Ngãi', N'QNG       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (75, N'1', N'Hà Giang', N'HGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (76, N'1', N'An Giang', N'AGN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (77, N'1', N'Rạch Giá', N'KGI       ', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_Receipt] ([Id], [ReceiptNo], [ReceiptDate], [BookingId], [Amount], [Payer], [PaymentType], [Company], [VatCode], [Address], [Telephone], [Mobile], [Note], [ReceiptOrder], [Point], [Status], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'bc8e9172-baba-4d67-9826-f78cf4b914c0', N'TT0001', NULL, N'f004dcfe-d5ec-4989-afb3-c8a1f1c9a12f', 1161000, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Role] ON 

INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (1, N'Quản trị')
INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (2, N'Nhân viên')
SET IDENTITY_INSERT [dbo].[tbl_Role] OFF
GO
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5599da0c-e1f6-4bde-877b-163b566960e5', N'21985eda-87ba-4176-932e-3194d286b4b7', N'phong4', 4, 23, 3, 1, 3, 3, N'', NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-21T02:11:26.463' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', N'phong thu 3', 2, 23, 1, 1, 2, 3, N'29,33', NULL, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-21T02:06:37.987' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'039a3642-7e52-4435-ab3e-52e89aabc909', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Phòng Superior', 0, 45, 4, 0, 1, 4, N'21,34', N'25052021_124745yb_hinh-1.jpg', 1, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T00:47:45.077' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'29a64c25-4c7d-48aa-911d-57a3f4b62148', N'21985eda-87ba-4176-932e-3194d286b4b7', N'aaa', 2, 23, 4, 1, 3, 3, N'29,33', NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-21T01:46:10.650' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Phòng đôi', 2, 23, 4, 1, 2, 3, N'34,29,33', N'25052021_055637yh_khach-san-da-lat-2018-so-dien-thoai-cac-khach-san-da-lat-gan-cho-va-ho-xuan-huong.jpg', 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T17:56:42.573' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', N'Phòng đơn', 3, 23, 1, 1, 3, 3, N'4,14,8', N'25052021_042215yh_khach-san-da-lat-4-sao-sammy-da-lat.jpg', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T16:22:15.550' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd140945b-992f-4dc7-9c87-cbec213e22e2', N'21985eda-87ba-4176-932e-3194d286b4b7', N'aaa', 1, 23, 2, 1, 2, 3, N'4,33', N'19052021_113021yh_cbr250rr.PNG', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:30:21.683' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'65e570fa-e0e3-41d2-8fc2-e2193db5e0b4', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'aaa', 4, 23, 1, 1, 2, 3, N'34', N'25052021_022404yh_hinh3.jpg', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T14:24:04.007' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', N'Basement Standard Double No Window', 20, 25, NULL, 1, 1, 2, N'21,34,29,33,26,28,36,30,24,25,22,32,27,35,23,20', NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:35:17.923' AS DateTime))
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9d2258c9-03f4-43ea-9f83-2184714331a4', N'd140945b-992f-4dc7-9c87-cbec213e22e2', CAST(N'2021-06-02T00:00:00.000' AS DateTime), 5, NULL, 550000, 500000, 9.09, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:34:15.677' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'badc9859-1c57-42c7-aa0c-3366898090dd', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-08T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:58:41.340' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3ee06314-1e2f-4da6-b701-51767cdfb9a2', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-10T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:58:41.860' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b665f734-b49f-458d-9ee9-5a4c5796c204', N'5599da0c-e1f6-4bde-877b-163b566960e5', CAST(N'2021-06-02T00:00:00.000' AS DateTime), 5, NULL, 590000, 510000, 13.56, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:33:13.877' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ddad049b-6f3a-4433-978e-5fc0b2ebc08b', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-05T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:41:11.550' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'643c41fa-9b2e-42f3-b18e-6cbe6bd27050', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-03T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:41:10.693' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd595894e-0e4c-4e64-8b91-7a65157b7a41', N'5599da0c-e1f6-4bde-877b-163b566960e5', CAST(N'2021-06-01T00:00:00.000' AS DateTime), 5, NULL, 590000, 510000, 13.56, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:33:10.993' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'917e8cb0-c885-4dee-a6e6-84b39bf39458', N'5599da0c-e1f6-4bde-877b-163b566960e5', CAST(N'2021-06-05T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:33:16.140' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f04eceef-1fa4-4915-a12c-89acfb9a3beb', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-09T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:58:41.557' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd648bc31-f88f-439f-aa94-960a5728ed88', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-07T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:58:40.847' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5d2c87b3-4ef1-49b0-b1c5-98596a471d1f', N'd140945b-992f-4dc7-9c87-cbec213e22e2', CAST(N'2021-06-01T00:00:00.000' AS DateTime), 5, NULL, 550000, 500000, 9.09, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:34:15.613' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'74a7d97d-8d5a-49f0-8865-a8f09f0f7e12', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-05-05T00:00:00.000' AS DateTime), NULL, NULL, 822000, 528000, 36, N'1', 1, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1fa26648-a119-4149-9bdf-a9224900790d', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-13T00:00:00.000' AS DateTime), 5, NULL, 5800000, 500000, 8.62, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T23:05:07.730' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'10c4f1f0-81c7-4791-a237-acdbba3939a8', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-04T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:41:11.133' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'585f2375-2547-4ac8-a9c3-b7028e8d544c', N'd140945b-992f-4dc7-9c87-cbec213e22e2', CAST(N'2021-06-04T00:00:00.000' AS DateTime), 5, NULL, 550000, 500000, 9.09, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:34:15.800' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b1932ae8-d3fb-4b4c-9b84-bbfd96de1fc1', N'5599da0c-e1f6-4bde-877b-163b566960e5', CAST(N'2021-06-03T00:00:00.000' AS DateTime), 5, NULL, 590000, 510000, 13.56, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:33:16.067' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2db4d30d-0358-476a-9491-bd0639a9d743', N'5599da0c-e1f6-4bde-877b-163b566960e5', CAST(N'2021-06-04T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:33:16.103' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'05451bb1-d678-44dd-a48b-c1eceea13a89', N'd140945b-992f-4dc7-9c87-cbec213e22e2', CAST(N'2021-06-05T00:00:00.000' AS DateTime), 5, NULL, 550000, 500000, 9.09, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:34:15.837' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e946cf8a-63a4-40ba-8e70-c423ab63c2fb', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-06T00:00:00.000' AS DateTime), 4, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:59:54.103' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0076d596-8822-4ec3-a48a-e5351e55f4b2', N'd140945b-992f-4dc7-9c87-cbec213e22e2', CAST(N'2021-06-03T00:00:00.000' AS DateTime), 5, NULL, 550000, 500000, 9.09, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-27T23:34:15.750' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a85cdacc-3963-4de4-ade0-e7ca0ad97f2f', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-02T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:41:10.257' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [HotelServiceType], [IsCancel], [IsSurcharge], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'09418a3b-1e01-4aac-9a1c-f9b43a1b258b', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', CAST(N'2021-06-01T00:00:00.000' AS DateTime), 5, NULL, 590000, 500000, 15.25, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-26T22:41:09.697' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'31626bac-ff21-47a3-8166-27ba5aabd1f5', N'hieunv', N'M/tTWwIES8wxgueZZcLelw==', N'Nguyễn Việt Hiếu', N'', 1, N'hieuviet.1103@gmail.com', N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', N'khanhpq', N'Z13nyDTQaQAst65Ic/lBUA==', N'a khánh', N'', 1, N'khanh@gmail.com', N'', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'00000000-0000-0000-0000-000000000000', N'31626bac-ff21-47a3-8166-27ba5aabd1f5', 1, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', 1, 1)
GO
ALTER TABLE [dbo].[tbl_Area] ADD  CONSTRAINT [DF_tbl_Area_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Country] ADD  CONSTRAINT [DF_tbl_Country_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Coupon] ADD  CONSTRAINT [DF_tbl_Coupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CouponType] ADD  CONSTRAINT [DF_tbl_CouponType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] ADD  CONSTRAINT [DF_tbl_CustomerCoupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Direction] ADD  CONSTRAINT [DF_tbl_Direction_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_District] ADD  CONSTRAINT [DF_tbl_District_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Hotel] ADD  CONSTRAINT [DF_tbl_Hotel_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] ADD  CONSTRAINT [DF_tbl_HotelCancellationPolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelComment] ADD  CONSTRAINT [DF_tbl_HotelComment_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelConvenientType] ADD  CONSTRAINT [DF_tbl_HotelConvenientType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelNotice] ADD  CONSTRAINT [DF_tbl_HotelNotice_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelPriceType] ADD  CONSTRAINT [DF_tbl_HotelPriceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelRatingType] ADD  CONSTRAINT [DF_tbl_HotelRatingType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelServiceType] ADD  CONSTRAINT [DF_tbl_HotelServiceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] ADD  CONSTRAINT [DF_tbl_HotelSurchargePolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelType] ADD  CONSTRAINT [DF_tbl_HotelType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_PaymentType] ADD  CONSTRAINT [DF_tbl_PaymentType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_HotelImage_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_ProductImage_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Province] ADD  CONSTRAINT [DF_tbl_Province_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Room] ADD  CONSTRAINT [DF_tbl_RoomType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_RoomPrice] ADD  CONSTRAINT [DF_tbl_Room_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_User_Role] ADD  CONSTRAINT [DF_tbl_User_Role_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[HotelNews]  WITH CHECK ADD  CONSTRAINT [FK_HotelNews_NewsTypes] FOREIGN KEY([NewsTypeID])
REFERENCES [dbo].[NewsTypes] ([NewsTypeID])
GO
ALTER TABLE [dbo].[HotelNews] CHECK CONSTRAINT [FK_HotelNews_NewsTypes]
GO
ALTER TABLE [dbo].[tbl_Area]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Area_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Area] CHECK CONSTRAINT [FK_tbl_Area_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Booking]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Booking_tbl_RoomPrice] FOREIGN KEY([RoomPriceId])
REFERENCES [dbo].[tbl_RoomPrice] ([Id])
GO
ALTER TABLE [dbo].[tbl_Booking] CHECK CONSTRAINT [FK_tbl_Booking_tbl_RoomPrice]
GO
ALTER TABLE [dbo].[tbl_Coupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Coupon_tbl_CouponType] FOREIGN KEY([Type])
REFERENCES [dbo].[tbl_CouponType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Coupon] CHECK CONSTRAINT [FK_tbl_Coupon_tbl_CouponType]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon] FOREIGN KEY([CouponId])
REFERENCES [dbo].[tbl_Coupon] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[tbl_Receipt] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt]
GO
ALTER TABLE [dbo].[tbl_District]  WITH CHECK ADD  CONSTRAINT [FK_tbl_District_tbl_Province] FOREIGN KEY([ProvinceId])
REFERENCES [dbo].[tbl_Province] ([ProvinceId])
GO
ALTER TABLE [dbo].[tbl_District] CHECK CONSTRAINT [FK_tbl_District_tbl_Province]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] CHECK CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelNotice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelNotice] CHECK CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] CHECK CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_Province]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Province_tbl_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[tbl_Country] ([CountryId])
GO
ALTER TABLE [dbo].[tbl_Province] CHECK CONSTRAINT [FK_tbl_Province_tbl_Country]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_Booking] FOREIGN KEY([BookingId])
REFERENCES [dbo].[tbl_Booking] ([BookingId])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_Booking]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType] FOREIGN KEY([PaymentType])
REFERENCES [dbo].[tbl_PaymentType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Direction] FOREIGN KEY([Direction])
REFERENCES [dbo].[tbl_Direction] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Direction]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_RoomPrice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RoomPrice_tbl_Room] FOREIGN KEY([RoomId])
REFERENCES [dbo].[tbl_Room] ([Id])
GO
ALTER TABLE [dbo].[tbl_RoomPrice] CHECK CONSTRAINT [FK_tbl_RoomPrice_tbl_Room]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_Role]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_User]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Ảnh đại diện KS; 1: Ds ảnh Ks; 2Ảnh đại diện phòng; 3 Ds ảnh phòng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ProductImage', @level2type=N'COLUMN',@level2name=N'Type'
GO
USE [master]
GO
ALTER DATABASE [Combo] SET  READ_WRITE 
GO
