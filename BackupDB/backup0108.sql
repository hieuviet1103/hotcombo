USE [master]
GO
/****** Object:  Database [Combo]    Script Date: 1/8/2021 6:05:20 PM ******/
CREATE DATABASE [Combo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Combo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Combo.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Combo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Combo_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Combo] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Combo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Combo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Combo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Combo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Combo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Combo] SET ARITHABORT OFF 
GO
ALTER DATABASE [Combo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Combo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Combo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Combo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Combo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Combo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Combo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Combo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Combo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Combo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Combo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Combo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Combo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Combo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Combo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Combo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Combo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Combo] SET RECOVERY FULL 
GO
ALTER DATABASE [Combo] SET  MULTI_USER 
GO
ALTER DATABASE [Combo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Combo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Combo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Combo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Combo] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Combo', N'ON'
GO
ALTER DATABASE [Combo] SET QUERY_STORE = OFF
GO
USE [Combo]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Combo]
GO
/****** Object:  Table [dbo].[tbl_Room]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Room](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomName] [nvarchar](255) NOT NULL,
	[AvailableRoom] [int] NOT NULL,
	[RoomArea] [float] NULL,
	[Direction] [nvarchar](255) NULL,
	[SingleBed] [int] NULL,
	[DoubleBed] [int] NULL,
	[MaxPeople] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[HotelServiceType] [nvarchar](255) NULL,
	[IsCancel] [bit] NULL,
	[IsSurcharge] [bit] NULL,
	[IsDelete] [bit] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RoomType_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelRatingType]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelRatingType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberStar] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelRatingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RoomPrice]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RoomPrice](
	[Id] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomDate] [datetime] NOT NULL,
	[AvailableRoom] [int] NULL,
	[HotelPriceTypeId] [int] NULL,
	[PriceContract] [float] NOT NULL,
	[PricePromotion] [float] NULL,
	[PercentPromotion] [float] NULL,
	[IsActive] [bit] NULL,
	[IsPromotion] [bit] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[Tax] [float] NULL,
	[TransactionCosts] [float] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Room_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Hotel]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Hotel](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelName] [nvarchar](255) NOT NULL,
	[CountryId] [varchar](3) NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[AreaId] [nvarchar](255) NULL,
	[HotelRatingTypeId] [int] NULL,
	[HotelTypeId] [int] NULL,
	[HotelConvenientType] [nvarchar](255) NULL,
	[Address] [nvarchar](500) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Longitude] [nvarchar](255) NULL,
	[Latitude] [nvarchar](255) NULL,
	[Content] [nvarchar](max) NULL,
	[Email] [nvarchar](255) NULL,
	[Url] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[IsActive] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[PriceFrom] [float] NULL,
	[PricePromotion] [float] NULL,
	[CancelTime] [int] NULL,
 CONSTRAINT [PK_tbl_Hotel_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_roomprice_profile]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Datnt>
-- Create date: <5/6/2021>
-- Description:	<Lấy danh sách roomprice ở trang Danh sách>
-- =============================================
CREATE VIEW [dbo].[vw_roomprice_profile]
AS
	SELECT rp.Id
      ,rp.RoomId
      ,rp.HotelId
      ,rp.RoomDate
      ,rp.AvailableRoom
      ,rp.HotelPriceTypeId
      ,rp.PriceContract
      ,rp.PricePromotion
      ,rp.PercentPromotion
      --,rp.HotelServiceType
      --,rp.IsCancel
      --,rp.IsSurcharge
      ,rp.IsPromotion
      --,rp.HotelConvenientType
      ,rp.IsDelete
      ,rp.Tax
      ,rp.TransactionCosts
      ,rp.UserCreate
      ,rp.DateCreate
      ,rp.UserUpdate
      ,rp.DateUpdate
	  ,r.RoomName
	  ,h.HotelName
	  ,h.ProvinceId
	  ,h.DistrictId
	  ,h.AreaId
	  ,h.Address
	  ,h.Latitude
	  ,h.Longitude
	  ,h.Content
	  ,h.Image
	  ,h.HotelConvenientType AS HotelConvenientTypeHotel
	  ,h.HotelRatingTypeId
	  ,h.HotelTypeId
	  ,r.Direction
	  --,d.DirectionName
	  ,r.SingleBed
	  ,r.DoubleBed
	  ,r.MaxPeople
	  ,r.HotelConvenientType AS HotelConvenientTypeRoom
	  ,r.HotelServiceType
      ,r.IsCancel
      ,r.IsSurcharge
	  ,r.Image AS ImageRoom
	  ,r.RoomArea
	  ,hrt.NumberStar
	FROM tbl_RoomPrice rp
	LEFT JOIN tbl_Room r ON r.Id = rp.RoomId
	LEFT JOIN tbl_Hotel h ON h.Id = r.HotelId
	--LEFT JOIN tbl_Direction d ON d.Id = r.Direction
	LEFT JOIN tbl_HotelRatingType hrt ON hrt.Id = h.HotelRatingTypeId
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[PhoneNumber] [varchar](15) NULL,
	[Email] [nvarchar](100) NULL,
	[Address] [nvarchar](250) NULL,
	[Type] [int] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsShowHeader] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HotelNews]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HotelNews](
	[HotelNewsID] [uniqueidentifier] NOT NULL,
	[NewsTypeID] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Brief] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[Source] [nvarchar](max) NULL,
	[FileAttchment] [nvarchar](max) NULL,
	[OrderID] [int] NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NOT NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [int] NOT NULL,
	[ProvinceID] [int] NULL,
 CONSTRAINT [PK_dbo.HotelNews] PRIMARY KEY CLUSTERED 
(
	[HotelNewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsAndCategories]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsAndCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[NewsId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_NewsAndCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsCategory]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsCategory](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_NewsCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsTypes]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTypes](
	[NewsTypeID] [int] NOT NULL,
	[Code] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[CrtBy] [nvarchar](max) NULL,
	[CrtDate] [datetime] NULL,
	[UpdBy] [nvarchar](max) NULL,
	[UpdDate] [datetime] NULL,
	[CompID] [datetime] NULL,
 CONSTRAINT [PK_dbo.NewsTypes] PRIMARY KEY CLUSTERED 
(
	[NewsTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SentCommunication]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SentCommunication](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[MailTo] [varchar](50) NOT NULL,
	[MailCc] [varchar](200) NOT NULL,
	[MailBcc] [varchar](200) NOT NULL,
	[MailFrom] [varchar](50) NOT NULL,
	[MailFromName] [nvarchar](50) NOT NULL,
	[IsSent] [bit] NULL,
	[IsCheck] [bit] NULL,
	[DateCreate] [datetime] NULL,
	[DateCheck] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[LogError] [nvarchar](max) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](15) NOT NULL,
	[CountryPhoneCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_SentCommunication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Area]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Area](
	[AreaId] [int] NOT NULL,
	[AreaName] [nvarchar](255) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Area] PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_banner]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_banner](
	[banner_id] [int] IDENTITY(1,1) NOT NULL,
	[banner_name] [nvarchar](500) NULL,
	[banner_src] [nvarchar](500) NULL,
	[banner_desc] [nvarchar](500) NULL,
	[banner_pos] [tinyint] NULL,
	[banner_width] [int] NULL,
	[banner_height] [int] NULL,
	[banner_dura] [int] NULL,
	[banner_hyperlink] [nvarchar](500) NULL,
	[banner_target] [varchar](50) NULL,
	[isflash] [tinyint] NULL,
	[priority] [int] NULL,
	[active] [tinyint] NULL,
	[language_id] [tinyint] NULL,
	[visits] [int] NULL,
	[date_start] [datetime] NULL,
	[date_end] [datetime] NULL,
	[listpage] [nvarchar](10) NULL,
	[img_apple] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_banner] PRIMARY KEY CLUSTERED 
(
	[banner_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Banner_Position]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Banner_Position](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
 CONSTRAINT [PK_tbl_Banner_Position] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking](
	[BookingId] [uniqueidentifier] NOT NULL,
	[BookingCode] [varchar](6) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[CheckIn] [datetime] NOT NULL,
	[CheckOut] [datetime] NOT NULL,
	[DateCreate] [datetime] NOT NULL,
	[CODCancel] [datetime] NOT NULL,
	[CountDate] [int] NOT NULL,
	[RoomCount] [int] NOT NULL,
	[PersonCount] [int] NOT NULL,
	[Tax] [float] NULL,
	[Surcharge] [float] NULL,
	[Penalty] [float] NULL,
	[RoomAmount] [float] NOT NULL,
	[TotalAmount] [float] NOT NULL,
	[PaymentType] [int] NOT NULL,
	[IsInvoceExport] [bit] NOT NULL,
	[CustomerId] [uniqueidentifier] NULL,
	[CustomerEmail] [varchar](100) NULL,
	[CustomerPhoneNumber] [varchar](15) NULL,
	[CustomerName] [nvarchar](50) NULL,
	[CustomerProvinceId] [int] NULL,
	[CustomerDistrictId] [int] NULL,
	[CustomerAddress] [nvarchar](200) NULL,
	[CustomerNote] [nvarchar](max) NULL,
	[CompanyName] [nvarchar](200) NULL,
	[TaxCode] [varchar](50) NULL,
	[CompanyAddress] [nvarchar](200) NULL,
	[BillingAddress] [nvarchar](200) NULL,
	[CustomerCheckPayment] [bit] NOT NULL,
	[DateCheckPayment] [datetime] NULL,
	[IsPaymentSuccess] [bit] NOT NULL,
	[UserConfirm] [varchar](50) NULL,
	[DateConfirm] [datetime] NULL,
	[Status] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_Booking1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Booking2]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Booking2](
	[BookingId] [uniqueidentifier] NOT NULL,
	[RoomPriceId] [uniqueidentifier] NOT NULL,
	[RoomQuantity] [int] NULL,
	[Amount] [float] NULL,
	[AmountCancel] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Email] [nvarchar](255) NOT NULL,
	[PhoneNumber] [nvarchar](255) NOT NULL,
	[ContactName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NULL,
	[SaleId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Booking_1] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_BookingAllot]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BookingAllot](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Status] [int] NULL,
	[UserCreate] [uniqueidentifier] NOT NULL,
	[DateCreate] [datetime] NULL,
	[DeadLine] [datetime] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_BookingAllot] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_BookingStatus]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BookingStatus](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Content]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Content](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ContenCode] [nvarchar](50) NOT NULL,
	[ContentName] [nvarchar](200) NULL,
	[ContentDetail] [ntext] NULL,
	[DateCreate] [datetime] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Content] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Country]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Country](
	[CountryId] [varchar](3) NOT NULL,
	[CountryName] [nvarchar](50) NOT NULL,
	[Symbol] [varchar](3) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Coupon]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Coupon](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[ShortCode] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[EffectiveDate] [date] NULL,
	[ExpirationDate] [date] NULL,
	[Price] [float] NULL,
	[Image] [nvarchar](1024) NULL,
	[Type] [int] NOT NULL,
	[PointExchange] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_tbl_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CouponType]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CouponType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponTypeName] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CouponType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CustomerNo] [nvarchar](255) NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[Dob] [datetime] NULL,
	[Gender] [tinyint] NOT NULL,
	[Nationality] [varchar](3) NULL,
	[IdCard] [nvarchar](12) NULL,
	[DateOfIssue] [datetime] NULL,
	[PlaceOfIssue] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[CountryId] [int] NULL,
	[ProvinceId] [int] NULL,
	[DistrictId] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Note] [nvarchar](255) NULL,
	[Password] [nvarchar](127) NULL,
	[TotalPoint] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerCoupon]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerCoupon](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[CouponId] [uniqueidentifier] NOT NULL,
	[ReceiptId] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CustomerCoupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Direction]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Direction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DirectionName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Direction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_District]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_District](
	[DistrictId] [int] NOT NULL,
	[DistrictName] [nvarchar](255) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_District] PRIMARY KEY CLUSTERED 
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelCancellationPolicy]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelCancellationPolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberDateCancel] [int] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[PercentAmount] [nchar](10) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelCancellationPolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelComment]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelComment](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[DateTimeComment] [datetime] NOT NULL,
	[PointLocation] [float] NULL,
	[PointServe] [float] NULL,
	[PointConvenient] [float] NULL,
	[PointCost] [float] NULL,
	[PointClean] [float] NULL,
	[Type] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelConvenientType]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelConvenientType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConvenientName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[Type] [int] NOT NULL,
	[Order] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelConvenientType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelNotice]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelNotice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](500) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelNotice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelPriceType]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelPriceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PriceTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelPriceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelServiceType]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelServiceType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelServiceType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelSurchargePolicy]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelSurchargePolicy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [nvarchar](255) NOT NULL,
	[HotelId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelSurchargePolicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_HotelType]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HotelType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HotelTypeName] [nvarchar](255) NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_HotelType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_PaymentType]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_PaymentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeCode] [nvarchar](255) NOT NULL,
	[PayTypeName] [nvarchar](255) NOT NULL,
	[Order] [int] NULL,
	[Status] [int] NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [nvarchar](255) NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [nvarchar](255) NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ProductImage]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductImage](
	[Id] [uniqueidentifier] NOT NULL,
	[HotelId] [uniqueidentifier] NULL,
	[RoomId] [uniqueidentifier] NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[Order] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_tbl_HotelImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Province]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Province](
	[ProvinceId] [int] NOT NULL,
	[CountryId] [varchar](3) NOT NULL,
	[ProvinceName] [nvarchar](255) NOT NULL,
	[Symbol] [nvarchar](255) NULL,
	[IsDelete] [bit] NOT NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Province] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Receipt]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Receipt](
	[Id] [uniqueidentifier] NOT NULL,
	[ReceiptNo] [nvarchar](255) NOT NULL,
	[ReceiptDate] [datetime] NULL,
	[BookingId] [uniqueidentifier] NOT NULL,
	[Amount] [float] NULL,
	[Payer] [nvarchar](255) NULL,
	[PaymentType] [int] NOT NULL,
	[Company] [nvarchar](255) NULL,
	[VatCode] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Telephone] [nvarchar](255) NULL,
	[Mobile] [nvarchar](255) NULL,
	[Note] [nvarchar](255) NULL,
	[ReceiptOrder] [int] NULL,
	[Point] [float] NULL,
	[Status] [int] NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[UserId] [uniqueidentifier] NOT NULL,
	[LoginName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](127) NOT NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[CodeStaff] [nvarchar](255) NULL,
	[Active] [tinyint] NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[UserCreate] [uniqueidentifier] NULL,
	[DateCreate] [datetime] NULL,
	[UserUpdate] [uniqueidentifier] NULL,
	[DateUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User_Role]    Script Date: 1/8/2021 6:05:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User_Role](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_User_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'66310ef2-e8c8-4c0b-b26f-29a1a59dbfd0', 1, N'Nhiếp ảnh gia mạo hiểm săn hình trên vách đá', N'Simon Carter thích chụp những người mạo hiểm chinh phục các vách đá cheo leo ở nhiều điểm du lịch nổi tiếng thế giới.', N'<p>Simon Carter người Canberra, Australia, từng được tạp ch&iacute;&nbsp;<em>Men&rsquo;s Journal</em>&nbsp;(Mỹ) vinh danh l&agrave; một trong những nhiếp ảnh gia phi&ecirc;u lưu mạo hiểm nhất thế giới v&agrave; tạp ch&iacute;&nbsp;<em>Rock and Ice</em>&nbsp;(Mỹ) m&ocirc; tả l&agrave; &quot;nhiếp ảnh gia leo n&uacute;i vĩ đại nhất mọi thời đại&quot;.</p>

<p>Từng c&oacute; dịp du lịch Hạ Long, Việt Nam, Simon Carter chụp nữ vận động vi&ecirc;n Monique Forestier (ảnh) treo m&igrave;nh giữa kh&ocirc;ng gian c&aacute;ch mặt biển 10 m, với một tay b&aacute;m chặt khối thạch nhũ c&oacute; k&iacute;ch cỡ bằng người bu&ocirc;ng xuống từ trần hang Kim Quy.</p>

<p>Ngo&agrave;i hệ thống đảo đ&aacute; v&ocirc;i, Hạ Long c&ograve;n m&ecirc; hoặc du kh&aacute;ch với c&aacute;c hang động tự nhi&ecirc;n tuyệt đẹp, trong đ&oacute; c&oacute; động Kim Quy, nằm tr&ecirc;n h&ograve;n Dầm Nam c&oacute; đỉnh cao 187 m, nổi tiếng với những nhũ đ&aacute; từ trần hang bu&ocirc;ng xuống v&agrave; sự t&iacute;ch li&ecirc;n quan đến R&ugrave;a V&agrave;ng gi&uacute;p vua L&ecirc; Lợi đ&aacute;nh tan qu&acirc;n giặc, sau n&agrave;y đặt t&ecirc;n l&agrave; động Kim Quy.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/3-1626430974.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=nTGxbxlIUvGoLFrfU4K1rw" /></p>

<p>Simon Carter treo m&igrave;nh tr&ecirc;n ch&oacute;p n&uacute;i thiết lập hệ thống, thiết bị m&aacute;y m&oacute;c để vươn ra c&aacute;ch v&aacute;ch n&uacute;i khoảng 8 m v&agrave; t&aacute;c nghiệp d&atilde;y n&uacute;i Blue, bang New South Wales, Australia.</p>

<p>Đam m&ecirc; nhiếp ảnh từ thời thiếu ni&ecirc;n v&agrave; cả bộ m&ocirc;n leo n&uacute;i, Simon Carter sau đ&oacute; định h&igrave;nh phong c&aacute;ch ri&ecirc;ng l&agrave; chụp ảnh leo n&uacute;i mạo hiểm. Trong 38 năm qua, &ocirc;ng đ&atilde; chinh phục v&agrave; chụp ảnh c&aacute;c điểm leo n&uacute;i nổi tiếng tại Australia như c&aacute;c d&atilde;y n&uacute;i Blue, Arapiles, Grampians, Cape Hauy c&ugrave;ng nhiều điểm leo n&uacute;i nổi tiếng kh&aacute;c tr&ecirc;n thế giới.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/4-1626430979.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=_q8LN-AW3cDOIiPWHRYl9Q" /></p>

<p>Nh&agrave; leo n&uacute;i John Smoothy b&aacute;m chặt leo l&ecirc;n v&aacute;ch đ&aacute; dựng đứng tại trạm leo Charlie, b&ecirc;n dưới l&agrave; biển m&acirc;y phủ v&ugrave;ng rừng quanh d&atilde;y n&uacute;i Blue, bang New South Wales.</p>

<p><iframe frameborder="0" height="250" id="google_ads_iframe_/27973503/Vnexpress/Desktop/Breakpage1/Dulich/Dulich.anh.detail_0" name="" scrolling="no" src="https://14547108eae05f41f718ef356c5c3fcd.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html" title="3rd party ad content" width="970"></iframe></p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/5-1626431023.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=w-nG4g7pigH_mReLh1fqrA" /></p>

<p>Simon Carter chụp ngược s&aacute;ng cảnh vận động vi&ecirc;n Tony Barron b&aacute;m chặt v&agrave;o đường r&atilde;nh giữa hai sườn n&uacute;i để leo l&ecirc;n tr&ecirc;n cao tại điểm Atridae, thuộc n&uacute;i Arapiles, bang Victoria, Australia.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/6-1626431028.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=0nhdk1H3GroaIBCsIxvTZw" /></p>

<p>Nh&agrave; leo n&uacute;i Lee Cossey chinh phục bức tường đ&aacute; Taipan Wall cao 55 m tại khu vực n&uacute;i Grampians, bang Victoria, bằng tay kh&ocirc;ng.</p>

<p>Bộ m&ocirc;n Free Soloing hay c&ograve;n gọi l&agrave; tay kh&ocirc;ng leo n&uacute;i l&agrave; một c&aacute;ch leo n&uacute;i mạo hiểm mang t&iacute;nh sống. Người chơi kh&ocirc;ng d&ugrave;ng bất kỳ thiết bị hỗ trợ n&agrave;o, chỉ sử dụng sự kh&eacute;o l&eacute;o của cơ thể v&agrave; một t&uacute;i đựng bột magie để b&ocirc;i l&ecirc;n tay nhằm tăng ma s&aacute;t khi b&aacute;m.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/7-1626431032.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=jKVdurx2pYgkL0fYTjWTfw" /></p>

<p>Pha mạo hiểm đu m&igrave;nh cheo leo v&aacute;ch đ&aacute; của hai nữ vận động leo n&uacute;i Ashlee Hendy (tr&ecirc;n) v&agrave; Elizabeth Chong tại điểm leo Clean Cuts, vườn quốc gia Grampians, bang Victoria.</p>
', NULL, NULL, N'17072021_062506yb_h4.jpg', NULL, 1, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-17T06:25:06.333' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-17T06:25:25.980' AS DateTime), 1, NULL)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'66310ef2-e8c8-4c0b-b26f-29a1a59dbfd1', 1, N'Nhiếp ảnh gia mạo hiểm săn hình trên vách đá', N'Simon Carter thích chụp những người mạo hiểm chinh phục các vách đá cheo leo ở nhiều điểm du lịch nổi tiếng thế giới.', N'<p>Simon Carter người Canberra, Australia, từng được tạp ch&iacute;&nbsp;<em>Men&rsquo;s Journal</em>&nbsp;(Mỹ) vinh danh l&agrave; một trong những nhiếp ảnh gia phi&ecirc;u lưu mạo hiểm nhất thế giới v&agrave; tạp ch&iacute;&nbsp;<em>Rock and Ice</em>&nbsp;(Mỹ) m&ocirc; tả l&agrave; &quot;nhiếp ảnh gia leo n&uacute;i vĩ đại nhất mọi thời đại&quot;.</p>

<p>Từng c&oacute; dịp du lịch Hạ Long, Việt Nam, Simon Carter chụp nữ vận động vi&ecirc;n Monique Forestier (ảnh) treo m&igrave;nh giữa kh&ocirc;ng gian c&aacute;ch mặt biển 10 m, với một tay b&aacute;m chặt khối thạch nhũ c&oacute; k&iacute;ch cỡ bằng người bu&ocirc;ng xuống từ trần hang Kim Quy.</p>

<p>Ngo&agrave;i hệ thống đảo đ&aacute; v&ocirc;i, Hạ Long c&ograve;n m&ecirc; hoặc du kh&aacute;ch với c&aacute;c hang động tự nhi&ecirc;n tuyệt đẹp, trong đ&oacute; c&oacute; động Kim Quy, nằm tr&ecirc;n h&ograve;n Dầm Nam c&oacute; đỉnh cao 187 m, nổi tiếng với những nhũ đ&aacute; từ trần hang bu&ocirc;ng xuống v&agrave; sự t&iacute;ch li&ecirc;n quan đến R&ugrave;a V&agrave;ng gi&uacute;p vua L&ecirc; Lợi đ&aacute;nh tan qu&acirc;n giặc, sau n&agrave;y đặt t&ecirc;n l&agrave; động Kim Quy.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/3-1626430974.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=nTGxbxlIUvGoLFrfU4K1rw" /></p>

<p>Simon Carter treo m&igrave;nh tr&ecirc;n ch&oacute;p n&uacute;i thiết lập hệ thống, thiết bị m&aacute;y m&oacute;c để vươn ra c&aacute;ch v&aacute;ch n&uacute;i khoảng 8 m v&agrave; t&aacute;c nghiệp d&atilde;y n&uacute;i Blue, bang New South Wales, Australia.</p>

<p>Đam m&ecirc; nhiếp ảnh từ thời thiếu ni&ecirc;n v&agrave; cả bộ m&ocirc;n leo n&uacute;i, Simon Carter sau đ&oacute; định h&igrave;nh phong c&aacute;ch ri&ecirc;ng l&agrave; chụp ảnh leo n&uacute;i mạo hiểm. Trong 38 năm qua, &ocirc;ng đ&atilde; chinh phục v&agrave; chụp ảnh c&aacute;c điểm leo n&uacute;i nổi tiếng tại Australia như c&aacute;c d&atilde;y n&uacute;i Blue, Arapiles, Grampians, Cape Hauy c&ugrave;ng nhiều điểm leo n&uacute;i nổi tiếng kh&aacute;c tr&ecirc;n thế giới.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/4-1626430979.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=_q8LN-AW3cDOIiPWHRYl9Q" /></p>

<p>Nh&agrave; leo n&uacute;i John Smoothy b&aacute;m chặt leo l&ecirc;n v&aacute;ch đ&aacute; dựng đứng tại trạm leo Charlie, b&ecirc;n dưới l&agrave; biển m&acirc;y phủ v&ugrave;ng rừng quanh d&atilde;y n&uacute;i Blue, bang New South Wales.</p>

<p><iframe frameborder="0" height="250" id="google_ads_iframe_/27973503/Vnexpress/Desktop/Breakpage1/Dulich/Dulich.anh.detail_0" name="" scrolling="no" src="https://14547108eae05f41f718ef356c5c3fcd.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html" title="3rd party ad content" width="970"></iframe></p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/5-1626431023.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=w-nG4g7pigH_mReLh1fqrA" /></p>

<p>Simon Carter chụp ngược s&aacute;ng cảnh vận động vi&ecirc;n Tony Barron b&aacute;m chặt v&agrave;o đường r&atilde;nh giữa hai sườn n&uacute;i để leo l&ecirc;n tr&ecirc;n cao tại điểm Atridae, thuộc n&uacute;i Arapiles, bang Victoria, Australia.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/6-1626431028.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=0nhdk1H3GroaIBCsIxvTZw" /></p>

<p>Nh&agrave; leo n&uacute;i Lee Cossey chinh phục bức tường đ&aacute; Taipan Wall cao 55 m tại khu vực n&uacute;i Grampians, bang Victoria, bằng tay kh&ocirc;ng.</p>

<p>Bộ m&ocirc;n Free Soloing hay c&ograve;n gọi l&agrave; tay kh&ocirc;ng leo n&uacute;i l&agrave; một c&aacute;ch leo n&uacute;i mạo hiểm mang t&iacute;nh sống. Người chơi kh&ocirc;ng d&ugrave;ng bất kỳ thiết bị hỗ trợ n&agrave;o, chỉ sử dụng sự kh&eacute;o l&eacute;o của cơ thể v&agrave; một t&uacute;i đựng bột magie để b&ocirc;i l&ecirc;n tay nhằm tăng ma s&aacute;t khi b&aacute;m.</p>

<p><img alt="" src="https://i1-dulich.vnecdn.net/2021/07/16/7-1626431032.jpg?w=1200&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=jKVdurx2pYgkL0fYTjWTfw" /></p>

<p>Pha mạo hiểm đu m&igrave;nh cheo leo v&aacute;ch đ&aacute; của hai nữ vận động leo n&uacute;i Ashlee Hendy (tr&ecirc;n) v&agrave; Elizabeth Chong tại điểm leo Clean Cuts, vườn quốc gia Grampians, bang Victoria.</p>
', NULL, NULL, N'17072021_062506yb_h4.jpg', NULL, 1, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-17T06:25:00.000' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-17T06:25:00.000' AS DateTime), 1, NULL)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'cd6dfbac-874f-49d6-a594-3b6cececa2fa', 2, N'Tin tức du lịch mùa covid', N'Tin tức du lịch mùa covid năm thứ 2 liên tiếp', N'<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="vertical-align:top">&nbsp;</td>
			<td style="vertical-align:top">&nbsp;</td>
		</tr>
	</tbody>
</table>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link -->', NULL, NULL, N'05072021_063014yb_h1.jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:40:37.950' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-24T16:28:01.213' AS DateTime), 1, 19)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'cd6dfbac-874f-49d6-a594-3b6cececa2fb', 2, N'Tin tức du lịch mùa covid', N'Tin tức du lịch mùa covid năm thứ 2 liên tiếp', N'<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="vertical-align:top">&nbsp;</td>
			<td style="vertical-align:top">&nbsp;</td>
		</tr>
	</tbody>
</table>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link -->', NULL, NULL, N'05072021_063014yb_h1.jpg', 2, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T22:40:00.000' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-24T16:28:00.000' AS DateTime), 1, 19)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'ab7c739d-6632-4002-bfe4-5c036c1dae28', 1, N'Hà Nội dự kiến lập 22 chốt kiểm soát các cửa ngõ', N'Từ 14/7, 22 chốt kiểm soát gồm lực lượng công an, quân đội, thanh tra giao thông, y tế, dân phòng bắt đầu hoạt động tại các cửa ngõ của Hà Nội.', N'<p>Từ 14/7, 22 chốt kiểm so&aacute;t gồm lực lượng c&ocirc;ng an, qu&acirc;n đội, thanh tra giao th&ocirc;ng, y tế, d&acirc;n ph&ograve;ng bắt đầu hoạt động tại c&aacute;c cửa ng&otilde; của H&agrave; Nội.</p>

<p>Chiều 12/7, tại cuộc họp Ban chỉ đạo ph&ograve;ng, chống Covid-19 của TP H&agrave; Nội, đại t&aacute; Trần Ngọc Dương, Ph&oacute; gi&aacute;m đốc C&ocirc;ng an th&agrave;nh phố, cho biết c&ocirc;ng an sẽ chia l&agrave;m 4 ca trực, chịu tr&aacute;ch nhiệm dựng lều, bạt, b&agrave;n ghế, nước uống tại chốt trực.</p>

<p>Người d&acirc;n từ c&aacute;c tỉnh th&agrave;nh qua chốt sẽ phải khai y tế, đo th&acirc;n nhiệt. Trường hợp nghi vấn, biển số tỉnh v&ugrave;ng dịch sẽ phải quay lại hoặc tr&igrave;nh x&eacute;t nghiệm &acirc;m t&iacute;nh.</p>

<p><img alt="Chốt kiểm soát lưu động của Thanh tra giao thông sáng 12/7 (Sở Giao thông Vận tải Hà Nội) tại địa phận thị trấn Xuân Mai, Chương Mỹ. Khi 22 chốt liên ngành đi vào hoạt động, 9 chốt của Thanh tra giao thông sẽ được gọp vào các chốt liên ngành. Ảnh: Võ Hải." src="https://i1-vnexpress.vnecdn.net/2021/07/12/cho-t-2-8113-1626096073.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=f1LP_tg4gjNeamPCQblI4w" /></p>

<p>Chốt kiểm so&aacute;t lưu động của Thanh tra giao th&ocirc;ng s&aacute;ng 12/7 (Sở Giao th&ocirc;ng Vận tải H&agrave; Nội) tại địa phận thị trấn Xu&acirc;n Mai, Chương Mỹ. Khi 22 chốt li&ecirc;n ng&agrave;nh đi v&agrave;o hoạt động, 9 chốt của Thanh tra giao th&ocirc;ng sẽ được gộp v&agrave;o. Ảnh:<em>&nbsp;V&otilde; Hải.</em></p>

<p>Ph&oacute; chủ tịch th&agrave;nh phố Chử Xu&acirc;n Dũng lưu &yacute;, việc lập chốt kiểm so&aacute;t cần th&ocirc;ng tin để người d&acirc;n nắm bắt r&otilde;, kh&ocirc;ng để &ugrave;n tắc, &quot;ngăn s&ocirc;ng cấm chợ&quot;.</p>

<p>Theo kế hoạch, 22 chốt sẽ được lập tại c&aacute;c tuyến đường cửa ng&otilde; thủ đ&ocirc; gồm: Từ H&agrave; Nam về H&agrave; Nội tuyến quốc lộ 1A, 1B; từ Hưng Y&ecirc;n, Bắc Ninh, Bắc Giang về H&agrave; Nội theo tuyến quốc lộ 5, cao tốc H&agrave; Nội - Lạng Sơn; từ H&ograve;a B&igrave;nh, Ph&uacute; Thọ, Vĩnh Ph&uacute;c, Th&aacute;i Nguy&ecirc;n về H&agrave; Nội.</p>

<p>Ngo&agrave;i ra, căn cứ t&igrave;nh h&igrave;nh thực tế, quận, huyện, thị x&atilde; bố tr&iacute; c&aacute;c chốt tại đường ngang, ng&otilde; tắt, đường nh&aacute;nh, bến thủy nội địa ra v&agrave;o th&agrave;nh phố.</p>

<p>Đ&acirc;y l&agrave; lần thứ hai th&agrave;nh phố lập chốt kiểm so&aacute;t ở c&aacute;c tuyến đường cửa ng&otilde; thủ đ&ocirc;. Trước đ&oacute; th&aacute;ng 4/2020, thực hiện Chỉ thị 16 c&aacute;ch ly x&atilde; hội, H&agrave; Nội lập 30 chốt tr&ecirc;n c&aacute;c trục đường ra v&agrave;o th&agrave;nh phố. Sau gần một th&aacute;ng, c&aacute;c chốt được dỡ khi dịch được khống chế, th&agrave;nh phố &aacute;p dụng trạng th&aacute;i b&igrave;nh thường mới.</p>

<p>Từ ng&agrave;y 5/7 đến nay, H&agrave; Nội ghi nhận 24 ca bệnh li&ecirc;n quan TP HCM. Ngo&agrave;i ra, c&oacute; 31 trường hợp li&ecirc;n quan đến Bắc Giang v&agrave; 9 ca bệnh ở huyện Mỹ Đức. Tổng số ca nhiễm từ 5/7 đến nay l&agrave; 64, tổng số ca nhiễm ghi nhận từ 29/4 (đợt dịch thứ tư) l&agrave; 322, chưa t&iacute;nh số ca nhiễm ở c&aacute;c bệnh viện trung ương.</p>

<p><strong>Mở rộng x&eacute;t nghiệm s&agrave;ng lọc</strong></p>

<p>Theo Sở Y tế H&agrave; Nội, từ ng&agrave;y 16/6 đến nay H&agrave; Nội đ&atilde; x&eacute;t nghiệm ngẫu nhi&ecirc;n h&agrave;nh kh&aacute;ch tại tất cả chuyến bay từ T&acirc;n Sơn Nhất về Nội B&agrave;i, đ&atilde; lấy tr&ecirc;n 2.000 mẫu, kết quả đều &acirc;m t&iacute;nh; lấy mẫu x&eacute;t nghiệm 112 l&aacute;i xe đường d&agrave;i về từ TP HCM v&agrave; c&aacute;c tuyến đường d&agrave;i từ ph&iacute;a Nam qua H&agrave; Nội, kết quả tất cả đều &acirc;m t&iacute;nh.</p>

<p>Th&agrave;nh phố r&agrave; so&aacute;t được tr&ecirc;n 6.600 người về từ TP HCM từ ng&agrave;y 23/6, đ&atilde; lấy mẫu x&eacute;t nghiệm cho hơn 6.500 người. Kết quả 13 dương t&iacute;nh, hơn 5.400 &acirc;m t&iacute;nh, c&ograve;n lại chưa c&oacute; kết quả.</p>

<p>Gi&aacute;m đốc Sở Y tế Trần Thị Nhị H&agrave; cho biết, thời gian tới th&agrave;nh phố tiếp tục mở rộng x&eacute;t nghiệm s&agrave;ng lọc tới l&aacute;i xe c&ocirc;ng nghệ, nh&acirc;n vi&ecirc;n thu gom r&aacute;c, nh&acirc;n vi&ecirc;n vệ sinh.</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link -->', N'https://vnexpress.net/ha-noi-du-kien-lap-22-chot-kiem-soat-cac-cua-ngo-4308483.html', N'VNExpress', N'12072021_100350yh_h4.jpg', NULL, 1, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-12T22:03:00.000' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-24T16:26:00.000' AS DateTime), 1, 19)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'ab7c739d-6632-4002-bfe4-5c036c1dae29', 1, N'Hà Nội dự kiến lập 22 chốt kiểm soát các cửa ngõ', N'Từ 14/7, 22 chốt kiểm soát gồm lực lượng công an, quân đội, thanh tra giao thông, y tế, dân phòng bắt đầu hoạt động tại các cửa ngõ của Hà Nội.', N'<p>Từ 14/7, 22 chốt kiểm so&aacute;t gồm lực lượng c&ocirc;ng an, qu&acirc;n đội, thanh tra giao th&ocirc;ng, y tế, d&acirc;n ph&ograve;ng bắt đầu hoạt động tại c&aacute;c cửa ng&otilde; của H&agrave; Nội.</p>

<p>Chiều 12/7, tại cuộc họp Ban chỉ đạo ph&ograve;ng, chống Covid-19 của TP H&agrave; Nội, đại t&aacute; Trần Ngọc Dương, Ph&oacute; gi&aacute;m đốc C&ocirc;ng an th&agrave;nh phố, cho biết c&ocirc;ng an sẽ chia l&agrave;m 4 ca trực, chịu tr&aacute;ch nhiệm dựng lều, bạt, b&agrave;n ghế, nước uống tại chốt trực.</p>

<p>Người d&acirc;n từ c&aacute;c tỉnh th&agrave;nh qua chốt sẽ phải khai y tế, đo th&acirc;n nhiệt. Trường hợp nghi vấn, biển số tỉnh v&ugrave;ng dịch sẽ phải quay lại hoặc tr&igrave;nh x&eacute;t nghiệm &acirc;m t&iacute;nh.</p>

<p><img alt="Chốt kiểm soát lưu động của Thanh tra giao thông sáng 12/7 (Sở Giao thông Vận tải Hà Nội) tại địa phận thị trấn Xuân Mai, Chương Mỹ. Khi 22 chốt liên ngành đi vào hoạt động, 9 chốt của Thanh tra giao thông sẽ được gọp vào các chốt liên ngành. Ảnh: Võ Hải." src="https://i1-vnexpress.vnecdn.net/2021/07/12/cho-t-2-8113-1626096073.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=f1LP_tg4gjNeamPCQblI4w" /></p>

<p>Chốt kiểm so&aacute;t lưu động của Thanh tra giao th&ocirc;ng s&aacute;ng 12/7 (Sở Giao th&ocirc;ng Vận tải H&agrave; Nội) tại địa phận thị trấn Xu&acirc;n Mai, Chương Mỹ. Khi 22 chốt li&ecirc;n ng&agrave;nh đi v&agrave;o hoạt động, 9 chốt của Thanh tra giao th&ocirc;ng sẽ được gộp v&agrave;o. Ảnh:<em>&nbsp;V&otilde; Hải.</em></p>

<p>Ph&oacute; chủ tịch th&agrave;nh phố Chử Xu&acirc;n Dũng lưu &yacute;, việc lập chốt kiểm so&aacute;t cần th&ocirc;ng tin để người d&acirc;n nắm bắt r&otilde;, kh&ocirc;ng để &ugrave;n tắc, &quot;ngăn s&ocirc;ng cấm chợ&quot;.</p>

<p>Theo kế hoạch, 22 chốt sẽ được lập tại c&aacute;c tuyến đường cửa ng&otilde; thủ đ&ocirc; gồm: Từ H&agrave; Nam về H&agrave; Nội tuyến quốc lộ 1A, 1B; từ Hưng Y&ecirc;n, Bắc Ninh, Bắc Giang về H&agrave; Nội theo tuyến quốc lộ 5, cao tốc H&agrave; Nội - Lạng Sơn; từ H&ograve;a B&igrave;nh, Ph&uacute; Thọ, Vĩnh Ph&uacute;c, Th&aacute;i Nguy&ecirc;n về H&agrave; Nội.</p>

<p>Ngo&agrave;i ra, căn cứ t&igrave;nh h&igrave;nh thực tế, quận, huyện, thị x&atilde; bố tr&iacute; c&aacute;c chốt tại đường ngang, ng&otilde; tắt, đường nh&aacute;nh, bến thủy nội địa ra v&agrave;o th&agrave;nh phố.</p>

<p>Đ&acirc;y l&agrave; lần thứ hai th&agrave;nh phố lập chốt kiểm so&aacute;t ở c&aacute;c tuyến đường cửa ng&otilde; thủ đ&ocirc;. Trước đ&oacute; th&aacute;ng 4/2020, thực hiện Chỉ thị 16 c&aacute;ch ly x&atilde; hội, H&agrave; Nội lập 30 chốt tr&ecirc;n c&aacute;c trục đường ra v&agrave;o th&agrave;nh phố. Sau gần một th&aacute;ng, c&aacute;c chốt được dỡ khi dịch được khống chế, th&agrave;nh phố &aacute;p dụng trạng th&aacute;i b&igrave;nh thường mới.</p>

<p>Từ ng&agrave;y 5/7 đến nay, H&agrave; Nội ghi nhận 24 ca bệnh li&ecirc;n quan TP HCM. Ngo&agrave;i ra, c&oacute; 31 trường hợp li&ecirc;n quan đến Bắc Giang v&agrave; 9 ca bệnh ở huyện Mỹ Đức. Tổng số ca nhiễm từ 5/7 đến nay l&agrave; 64, tổng số ca nhiễm ghi nhận từ 29/4 (đợt dịch thứ tư) l&agrave; 322, chưa t&iacute;nh số ca nhiễm ở c&aacute;c bệnh viện trung ương.</p>

<p><strong>Mở rộng x&eacute;t nghiệm s&agrave;ng lọc</strong></p>

<p>Theo Sở Y tế H&agrave; Nội, từ ng&agrave;y 16/6 đến nay H&agrave; Nội đ&atilde; x&eacute;t nghiệm ngẫu nhi&ecirc;n h&agrave;nh kh&aacute;ch tại tất cả chuyến bay từ T&acirc;n Sơn Nhất về Nội B&agrave;i, đ&atilde; lấy tr&ecirc;n 2.000 mẫu, kết quả đều &acirc;m t&iacute;nh; lấy mẫu x&eacute;t nghiệm 112 l&aacute;i xe đường d&agrave;i về từ TP HCM v&agrave; c&aacute;c tuyến đường d&agrave;i từ ph&iacute;a Nam qua H&agrave; Nội, kết quả tất cả đều &acirc;m t&iacute;nh.</p>

<p>Th&agrave;nh phố r&agrave; so&aacute;t được tr&ecirc;n 6.600 người về từ TP HCM từ ng&agrave;y 23/6, đ&atilde; lấy mẫu x&eacute;t nghiệm cho hơn 6.500 người. Kết quả 13 dương t&iacute;nh, hơn 5.400 &acirc;m t&iacute;nh, c&ograve;n lại chưa c&oacute; kết quả.</p>

<p>Gi&aacute;m đốc Sở Y tế Trần Thị Nhị H&agrave; cho biết, thời gian tới th&agrave;nh phố tiếp tục mở rộng x&eacute;t nghiệm s&agrave;ng lọc tới l&aacute;i xe c&ocirc;ng nghệ, nh&acirc;n vi&ecirc;n thu gom r&aacute;c, nh&acirc;n vi&ecirc;n vệ sinh.</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link -->', N'https://vnexpress.net/ha-noi-du-kien-lap-22-chot-kiem-soat-cac-cua-ngo-4308483.html', N'VNExpress', N'12072021_100350yh_h4.jpg', NULL, 1, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-12T22:03:37.920' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-24T16:26:00.293' AS DateTime), 1, 19)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0f7', 2, N'Chương trình Ưu đãi VNPay', N'Nhằm gia tăng tiện tích cho khách hàng khi mua tour, Công ty Du lịch Vietravel tổ chức chương trình ưu đãi hấp dẫn cho khách hàng khi thanh toán bằng dịch vụ VNPay QR code.', N'<p style="text-align:center"><span style="color:#d35400"><strong><span style="font-size:28px">Thể lệ chương tr&igrave;nh ưu đ&atilde;i VNPAY</span></strong></span></p>

<p><br />
05/10/2020<br />
Nhằm gia tăng tiện t&iacute;ch cho kh&aacute;ch h&agrave;ng khi mua tour, C&ocirc;ng ty Du lịch Vietravel tổ chức chương tr&igrave;nh ưu đ&atilde;i hấp dẫn cho kh&aacute;ch h&agrave;ng khi thanh to&aacute;n bằng dịch vụ VNPay QR code.<br />
&nbsp;</p>

<figure class="easyimage easyimage-full"><img alt="" src="blob:http://localhost:13863/087dd565-d7ef-414d-b156-977f5f29b0c0" width="560" />
<figcaption></figcaption>
</figure>

<p><img src="http://localhost:13863/FileUploads/News/18052021_104109yb_shutterstock.jpg" /></p>

<p><br />
<br />
Lưu<br />
1. Khi mua c&aacute;c sản phẩm tại Vietravel v&agrave; thanh to&aacute;n th&agrave;nh c&ocirc;ng bằng dịch vụ VNPAYQR (sử dụng t&iacute;nh năng QR Pay) tr&ecirc;n ứng dụng Mobile Banking của c&aacute;c ng&acirc;n h&agrave;ng: Agribank, BIDV, VietinBank, Vietcombank, SCB, IVB, ABBank, Eximbank, HDBank, NCB, Nam A Bank, VietBank, BIDC, SaiGonBank, SeABank, Ocean Bank, KienLongBank: Kh&aacute;ch h&agrave;ng nhập m&atilde; khuyến mại VTR25 sẽ được giảm 5% gi&aacute; trị giao dịch, tối đa 100.000đ (Gi&aacute; trị khuyến mại), cho mỗi giao dịch th&agrave;nh c&ocirc;ng (kh&ocirc;ng t&iacute;nh c&aacute;c giao dịch ho&agrave;n, hủy).<br />
2. Chỉ &aacute;p dụng cho 1.000 kh&aacute;ch h&agrave;ng (tương ứng với mỗi số t&agrave;i khoản ng&acirc;n h&agrave;ng v&agrave;/hoặc số điện thoại đăng k&yacute; dịch vụ Mobile Banking) đầu ti&ecirc;n thanh to&aacute;n th&agrave;nh c&ocirc;ng qua dịch vụ VNPAY-QR. Mỗi kh&aacute;ch h&agrave;ng được hưởng duy nhất 01 (một) lần gi&aacute; trị khuyến mại/01 th&aacute;ng trong thời gian diễn ra chương tr&igrave;nh.<br />
3. Chương tr&igrave;nh c&oacute; thể kết th&uacute;c trước thời hạn<br />
4. Gi&aacute; trị khuyến mại sẽ được trừ trực tiếp v&agrave;o gi&aacute; trị giao dịch thanh to&aacute;n của Kh&aacute;ch h&agrave;ng khi thực hiện thanh to&aacute;n th&agrave;nh c&ocirc;ng.&nbsp;<br />
5. Gi&aacute; trị khuyến mại sẽ được l&agrave;m tr&ograve;n theo đơn vị: ngh&igrave;n đồng. Cụ thể, &lt;500 đồng l&agrave;m tr&ograve;n xuống 0đ; &gt;=500đ l&agrave;m tr&ograve;n l&ecirc;n 1.000đ.<br />
6. Kh&ocirc;ng &aacute;p dụng t&aacute;ch h&oacute;a đơn dưới mọi h&igrave;nh thức hoặc ho&agrave;n tiền một phần với c&aacute;c giao dịch đ&atilde; hưởng khuyến mại.&nbsp;<br />
7. Chỉ &aacute;p dụng cho kh&aacute;ch h&agrave;ng thanh to&aacute;n 100% gi&aacute; trị đơn h&agrave;ng qua VNPAYQR.<br />
8. Chỉ &aacute;p dụng cho kh&aacute;ch lẻ, kh&ocirc;ng &aacute;p dụng cho kh&aacute;ch đo&agrave;n v&agrave; đại l&yacute;<br />
9. Chi tiết danh s&aacute;ch ng&acirc;n h&agrave;ng v&agrave; c&aacute;c đơn vị &aacute;p dụng khuyến mại được cập nhật tại: https://coupons.vnpay.vn/<br />
&nbsp;</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link -->', N'https://google.com', N'vtv', N'19052021_102648yh_advertising1617262142.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-14T23:43:00.000' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-13T22:44:00.000' AS DateTime), 1, 70)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'a538e2d4-7e8f-4f04-b80b-6b2e0697b0fd', 2, N'Chương trình Ưu đãi VNPay', N'Nhằm gia tăng tiện tích cho khách hàng khi mua tour, Công ty Du lịch Vietravel tổ chức chương trình ưu đãi hấp dẫn cho khách hàng khi thanh toán bằng dịch vụ VNPay QR code.', N'<p style="text-align:center"><span style="color:#d35400"><strong><span style="font-size:28px">Thể lệ chương tr&igrave;nh ưu đ&atilde;i VNPAY</span></strong></span></p>

<p><br />
05/10/2020<br />
Nhằm gia tăng tiện t&iacute;ch cho kh&aacute;ch h&agrave;ng khi mua tour, C&ocirc;ng ty Du lịch Vietravel tổ chức chương tr&igrave;nh ưu đ&atilde;i hấp dẫn cho kh&aacute;ch h&agrave;ng khi thanh to&aacute;n bằng dịch vụ VNPay QR code.<br />
&nbsp;</p>

<figure class="easyimage easyimage-full"><img alt="" src="blob:http://localhost:13863/087dd565-d7ef-414d-b156-977f5f29b0c0" width="560" />
<figcaption></figcaption>
</figure>

<p><img src="http://localhost:13863/FileUploads/News/18052021_104109yb_shutterstock.jpg" /></p>

<p><br />
<br />
Lưu<br />
1. Khi mua c&aacute;c sản phẩm tại Vietravel v&agrave; thanh to&aacute;n th&agrave;nh c&ocirc;ng bằng dịch vụ VNPAYQR (sử dụng t&iacute;nh năng QR Pay) tr&ecirc;n ứng dụng Mobile Banking của c&aacute;c ng&acirc;n h&agrave;ng: Agribank, BIDV, VietinBank, Vietcombank, SCB, IVB, ABBank, Eximbank, HDBank, NCB, Nam A Bank, VietBank, BIDC, SaiGonBank, SeABank, Ocean Bank, KienLongBank: Kh&aacute;ch h&agrave;ng nhập m&atilde; khuyến mại VTR25 sẽ được giảm 5% gi&aacute; trị giao dịch, tối đa 100.000đ (Gi&aacute; trị khuyến mại), cho mỗi giao dịch th&agrave;nh c&ocirc;ng (kh&ocirc;ng t&iacute;nh c&aacute;c giao dịch ho&agrave;n, hủy).<br />
2. Chỉ &aacute;p dụng cho 1.000 kh&aacute;ch h&agrave;ng (tương ứng với mỗi số t&agrave;i khoản ng&acirc;n h&agrave;ng v&agrave;/hoặc số điện thoại đăng k&yacute; dịch vụ Mobile Banking) đầu ti&ecirc;n thanh to&aacute;n th&agrave;nh c&ocirc;ng qua dịch vụ VNPAY-QR. Mỗi kh&aacute;ch h&agrave;ng được hưởng duy nhất 01 (một) lần gi&aacute; trị khuyến mại/01 th&aacute;ng trong thời gian diễn ra chương tr&igrave;nh.<br />
3. Chương tr&igrave;nh c&oacute; thể kết th&uacute;c trước thời hạn<br />
4. Gi&aacute; trị khuyến mại sẽ được trừ trực tiếp v&agrave;o gi&aacute; trị giao dịch thanh to&aacute;n của Kh&aacute;ch h&agrave;ng khi thực hiện thanh to&aacute;n th&agrave;nh c&ocirc;ng.&nbsp;<br />
5. Gi&aacute; trị khuyến mại sẽ được l&agrave;m tr&ograve;n theo đơn vị: ngh&igrave;n đồng. Cụ thể, &lt;500 đồng l&agrave;m tr&ograve;n xuống 0đ; &gt;=500đ l&agrave;m tr&ograve;n l&ecirc;n 1.000đ.<br />
6. Kh&ocirc;ng &aacute;p dụng t&aacute;ch h&oacute;a đơn dưới mọi h&igrave;nh thức hoặc ho&agrave;n tiền một phần với c&aacute;c giao dịch đ&atilde; hưởng khuyến mại.&nbsp;<br />
7. Chỉ &aacute;p dụng cho kh&aacute;ch h&agrave;ng thanh to&aacute;n 100% gi&aacute; trị đơn h&agrave;ng qua VNPAYQR.<br />
8. Chỉ &aacute;p dụng cho kh&aacute;ch lẻ, kh&ocirc;ng &aacute;p dụng cho kh&aacute;ch đo&agrave;n v&agrave; đại l&yacute;<br />
9. Chi tiết danh s&aacute;ch ng&acirc;n h&agrave;ng v&agrave; c&aacute;c đơn vị &aacute;p dụng khuyến mại được cập nhật tại: https://coupons.vnpay.vn/<br />
&nbsp;</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link -->', N'https://google.com', N'vtv', N'19052021_102648yh_advertising1617262142.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-14T23:43:31.440' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-13T22:44:43.393' AS DateTime), 1, 70)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'3a76d88b-0494-4c6a-8b72-d22a86bcee79', 2, N'Du lịch Kon Tum khám phá những điều thú vị về nhà Rông', N'Được coi là biểu tượng văn hóa của cộng đồng các dân tộc ở Tây Nguyên, nhà Rông thể hiện giá trị vật chất và tinh thần trong đời sống đồng bào nơi đây. Tuổi trẻ hãy một lần đi du lịch Kon Tum để khám phá những điều thú vị về ngôi nhà độc đáo này', N'<h2><strong>10 ĐIỀU VỀ NH&Agrave; R&Ocirc;NG T&Acirc;Y NGUY&Ecirc;N</strong></h2>

<h2><strong>&nbsp;</strong></h2>

<p>Nh&agrave; r&ocirc;ng được xem l&agrave; biểu tượng văn h&oacute;a của cộng đồng c&aacute;c d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n, thể hiện gi&aacute; trị vật chất v&agrave; tinh thần trong đời sống đồng b&agrave;o nơi đ&acirc;y.</p>

<p><img alt="Lễ hội ở nhà Rông của người dân tộc Ba Na" src="https://hfvtravel.com/wp-content/uploads/2020/10/le-hoi-nha-rong-cua-nguoi-dan-toc-ba-na.jpg" /></p>

<p>1. Kh&ocirc;ng phải d&acirc;n tộc n&agrave;o ở T&acirc;y Nguy&ecirc;n cũng c&oacute; nh&agrave; r&ocirc;ng. Nh&agrave; r&ocirc;ng xuất hiện nhiều tại c&aacute;c bu&ocirc;n l&agrave;ng d&acirc;n tộc khu vực ph&iacute;a bắc T&acirc;y Nguy&ecirc;n, đặc biệt ở hai tỉnh Gia Lai v&agrave; Kon Tum. Ph&iacute;a nam T&acirc;y Nguy&ecirc;n từ &ETH;ắk Lắk trở v&agrave;o, nh&agrave; r&ocirc;ng xuất hiện thưa thớt dần.</p>

<p>2. Nh&agrave; r&ocirc;ng l&agrave; kh&ocirc;ng gian sinh hoạt cộng đồng lớn nhất mỗi l&agrave;ng; nơi người d&acirc;n trao đổi, thảo luận về c&aacute;c lĩnh vực h&agrave;nh ch&iacute;nh, qu&acirc;n sự; nơi thực thi c&aacute;c luật tục, bảo tồn truyền thống v&agrave; diễn ra những nghi thức t&ocirc;n gi&aacute;o, t&iacute;n ngưỡng.</p>

<p>3. Nh&agrave; r&ocirc;ng kh&ocirc;ng phải d&ugrave;ng để lưu tr&uacute;, mặc d&ugrave; c&oacute; kết cấu v&agrave; vật liệu tương tự nh&agrave; s&agrave;n d&ugrave;ng để ở (được x&acirc;y dựng bằng gỗ, tre, cỏ tranh&hellip;), nh&agrave; r&ocirc;ng mang c&aacute;c n&eacute;t kiến tr&uacute;c đặc sắc v&agrave; cao, rộng hơn nhiều. Nh&agrave; r&ocirc;ng c&agrave;ng cao v&agrave; rộng th&igrave; c&agrave;ng thể hiện sự gi&agrave;u c&oacute;, thịnh vượng, sung t&uacute;c, h&ugrave;ng mạnh của l&agrave;ng.</p>

<p><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-1.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-15.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-4.jpg" />4. Đồng b&agrave;o T&acirc;y Nguy&ecirc;n quan niệm nh&agrave; r&ocirc;ng l&agrave; nơi thu h&uacute;t kh&iacute; thi&ecirc;ng đất trời để bảo trợ cho d&acirc;n l&agrave;ng. Do đ&oacute; trong mỗi nh&agrave; r&ocirc;ng đều c&oacute; một nơi trang trọng để thờ c&aacute;c vật được người d&acirc;n cho l&agrave; thần linh tr&uacute; ngụ như con dao, h&ograve;n đ&aacute;, sừng tr&acirc;u&hellip; Ngo&agrave;i ra, nơi n&agrave;y c&ograve;n như một bảo t&agrave;ng lưu giữ c&aacute;c hiện vật truyền thống gắn liền với lịch sử h&igrave;nh th&agrave;nh bu&ocirc;n l&agrave;ng như cồng chi&ecirc;ng, trống, vũ kh&iacute;, đầu c&aacute;c con vật hiến sinh trong ng&agrave;y lễ.</p>

<p>5. Nh&agrave; r&ocirc;ng l&agrave; nơi quan trọng nhất l&agrave;ng n&ecirc;n đ&agrave;n &ocirc;ng trong l&agrave;ng phải thay nhau ngủ qua đ&ecirc;m tại đ&acirc;y để tr&ocirc;ng coi. Một số l&agrave;ng l&agrave;m đến hai nh&agrave; r&ocirc;ng: &ldquo;nh&agrave; r&ocirc;ng c&aacute;i&rdquo; nhỏ v&agrave; c&oacute; m&aacute;i thấp d&agrave;nh cho phụ nữ, &ldquo;nh&agrave; r&ocirc;ng đực&rdquo; d&agrave;nh cho đ&agrave;n &ocirc;ng c&oacute; quy m&ocirc; lớn hơn v&agrave; trang tr&iacute; c&ocirc;ng phu. Ngo&agrave;i mục đ&iacute;ch g&igrave;n giữ kh&ocirc;ng gian thi&ecirc;ng, nh&agrave; r&ocirc;ng l&agrave; nơi người d&acirc;n trao đổi những c&acirc;u chuyện, kinh nghiệm trong đời sống. Nam nữ độc th&acirc;n trong l&agrave;ng c&oacute; thể qu&acirc;y quần tại nh&agrave; r&ocirc;ng để thăm hỏi, t&igrave;m bạn đời, tuy nhi&ecirc;n kh&ocirc;ng đi được ph&eacute;p đi qu&aacute; giới hạn.</p>

<p>6. Mỗi d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n c&oacute; kiểu l&agrave;m nh&agrave; r&ocirc;ng kh&aacute;c nhau. K&iacute;ch thước nh&agrave; r&ocirc;ng nhỏ v&agrave; thấp nhất l&agrave; của người Giẻ Tri&ecirc;ng. Nh&agrave; r&ocirc;ng của người X&ecirc; &ETH;ăng cao v&uacute;t. Nh&agrave; r&ocirc;ng của người Gia Rai c&oacute; m&aacute;i mảnh, dẹt như lưỡi r&igrave;u. Nh&agrave; r&ocirc;ng của người Ba Na to hơn nh&agrave; Gia Rai, c&oacute; đường n&eacute;t mềm mại v&agrave; thường c&oacute; c&aacute;c nh&agrave; s&agrave;n xung quanh. Điểm chung của c&aacute;c ng&ocirc;i nh&agrave; r&ocirc;ng l&agrave; được x&acirc;y cất tr&ecirc;n một khoảng đất rộng, nằm ngay tại khu vực trung t&acirc;m của bu&ocirc;n l&agrave;ng.</p>

<p>&nbsp;</p>

<p>7. S&agrave;n nh&agrave; r&ocirc;ng được thiết kế gắn liền với văn h&oacute;a qu&acirc;y quần uống rượu cần của đồng b&agrave;o. S&agrave;n thường được l&agrave;m từ v&aacute;n gỗ hay ống tre nứa đập dập, khi gh&eacute;p kh&ocirc;ng kh&iacute;t nhau m&agrave; c&aacute;c tấm c&aacute;ch nhau khoảng 1 cm. Nhờ thế m&agrave; khi người d&acirc;n tập trung ăn uống, nước kh&ocirc;ng bị chảy l&ecirc;nh l&aacute;ng ra s&agrave;n. Mặt kh&aacute;c, kiểu s&agrave;n n&agrave;y gi&uacute;p cho việc vệ sinh nh&agrave; dễ d&agrave;ng hơn.</p>

<p>8. Cầu thang nh&agrave; r&ocirc;ng thường c&oacute; 7 đến 9 bậc, tuy nhi&ecirc;n mỗi d&acirc;n tộc lại c&oacute; trang tr&iacute; kh&aacute;c nhau. Tr&ecirc;n th&agrave;nh v&agrave; cột của cầu thang, người Gia Rai hay tạc h&igrave;nh quả bầu đựng nước, người Bana khắc h&igrave;nh ngọn c&acirc;y rau dớn, c&ograve;n người Giẻ Tri&ecirc;ng, Xơ Đăng thường đẽo h&igrave;nh n&uacute;m chi&ecirc;ng hoặc mũi thuyền.</p>

<p>9. Nếu như m&aacute;i đ&igrave;nh miền xu&ocirc;i gắn liền với h&igrave;nh ảnh c&acirc;y đa, th&igrave; nh&agrave; r&ocirc;ng T&acirc;y Nguy&ecirc;n c&oacute; c&acirc;y n&ecirc;u. C&acirc;y n&ecirc;u được trang tr&iacute; nhiều họa tiết, đặt ở ph&iacute;a trước s&acirc;n ch&iacute;nh giữa của ng&ocirc;i nh&agrave; r&ocirc;ng để phục vụ c&aacute;c lễ hội lớn của bu&ocirc;n l&agrave;ng. Theo quan niệm, c&acirc;y n&ecirc;u l&agrave; nơi hội tụ c&aacute;c vị thần linh. Ở từng lễ hội, c&acirc;y n&ecirc;u mang một h&igrave;nh ảnh biểu tượng kh&aacute;c nhau như c&acirc;y n&ecirc;u trong lễ đ&acirc;m tr&acirc;u c&oacute; 4 nh&aacute;nh, lễ mừng l&uacute;a mới c&acirc;y n&ecirc;u chỉ c&oacute; 1 nh&aacute;nh&hellip;</p>

<p>10. Nh&agrave; r&ocirc;ng chỉ gắn với bu&ocirc;n l&agrave;ng, kh&ocirc;ng c&oacute; nh&agrave; r&ocirc;ng cấp tỉnh, cấp huyện hoặc nh&agrave; r&ocirc;ng chung nhiều l&agrave;ng. Hiện nay, nh&agrave; r&ocirc;ng truyền thống lu&ocirc;n được g&igrave;n giữ tại trung t&acirc;m l&agrave;ng. Một số địa điểm bu&ocirc;n l&agrave;ng c&oacute; nh&agrave; r&ocirc;ng hiện nay l&agrave; nh&agrave; r&ocirc;ng Kon Klor ở th&agrave;nh phố Kon Tum, l&agrave;ng Plei Phung, l&agrave;ng Kon So Lăl (huyện Chư Pah) v&agrave; l&agrave;ng Đ&ecirc; K&rsquo;tu (huyện Mang Yang) ở Gia Lai.</p>

<p>&nbsp;</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link -->', NULL, NULL, N'19052021_110520yh_2.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:05:00.000' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-13T23:10:00.000' AS DateTime), 1, NULL)
INSERT [dbo].[HotelNews] ([HotelNewsID], [NewsTypeID], [Title], [Brief], [Content], [Link], [Source], [FileAttchment], [OrderID], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID], [ProvinceID]) VALUES (N'3a76d88b-0494-4c6a-8b72-d22a86bcee7d', 2, N' Du lịch Kon Tum khám phá những điều thú vị về nhà Rông', N'Được coi là biểu tượng văn hóa của cộng đồng các dân tộc ở Tây Nguyên, nhà Rông thể hiện giá trị vật chất và tinh thần trong đời sống đồng bào nơi đây. Tuổi trẻ hãy một lần đi du lịch Kon Tum để khám phá những điều thú vị về ngôi nhà độc đáo này', N'<h2><strong>10 ĐIỀU VỀ NH&Agrave; R&Ocirc;NG T&Acirc;Y NGUY&Ecirc;N</strong></h2>

<h2><strong>&nbsp;</strong></h2>

<p>Nh&agrave; r&ocirc;ng được xem l&agrave; biểu tượng văn h&oacute;a của cộng đồng c&aacute;c d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n, thể hiện gi&aacute; trị vật chất v&agrave; tinh thần trong đời sống đồng b&agrave;o nơi đ&acirc;y.</p>

<p><img alt="Lễ hội ở nhà Rông của người dân tộc Ba Na" src="https://hfvtravel.com/wp-content/uploads/2020/10/le-hoi-nha-rong-cua-nguoi-dan-toc-ba-na.jpg" /></p>

<p>1. Kh&ocirc;ng phải d&acirc;n tộc n&agrave;o ở T&acirc;y Nguy&ecirc;n cũng c&oacute; nh&agrave; r&ocirc;ng. Nh&agrave; r&ocirc;ng xuất hiện nhiều tại c&aacute;c bu&ocirc;n l&agrave;ng d&acirc;n tộc khu vực ph&iacute;a bắc T&acirc;y Nguy&ecirc;n, đặc biệt ở hai tỉnh Gia Lai v&agrave; Kon Tum. Ph&iacute;a nam T&acirc;y Nguy&ecirc;n từ &ETH;ắk Lắk trở v&agrave;o, nh&agrave; r&ocirc;ng xuất hiện thưa thớt dần.</p>

<p>2. Nh&agrave; r&ocirc;ng l&agrave; kh&ocirc;ng gian sinh hoạt cộng đồng lớn nhất mỗi l&agrave;ng; nơi người d&acirc;n trao đổi, thảo luận về c&aacute;c lĩnh vực h&agrave;nh ch&iacute;nh, qu&acirc;n sự; nơi thực thi c&aacute;c luật tục, bảo tồn truyền thống v&agrave; diễn ra những nghi thức t&ocirc;n gi&aacute;o, t&iacute;n ngưỡng.</p>

<p>3. Nh&agrave; r&ocirc;ng kh&ocirc;ng phải d&ugrave;ng để lưu tr&uacute;, mặc d&ugrave; c&oacute; kết cấu v&agrave; vật liệu tương tự nh&agrave; s&agrave;n d&ugrave;ng để ở (được x&acirc;y dựng bằng gỗ, tre, cỏ tranh&hellip;), nh&agrave; r&ocirc;ng mang c&aacute;c n&eacute;t kiến tr&uacute;c đặc sắc v&agrave; cao, rộng hơn nhiều. Nh&agrave; r&ocirc;ng c&agrave;ng cao v&agrave; rộng th&igrave; c&agrave;ng thể hiện sự gi&agrave;u c&oacute;, thịnh vượng, sung t&uacute;c, h&ugrave;ng mạnh của l&agrave;ng.</p>

<p><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-1.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-15.jpg" /><img alt="Khám phá nét độc đáo của nhà rông Tây Nguyên " src="https://hfvtravel.com/wp-content/uploads/2020/10/kham-pha-nha-rong-Tay-Nguyen-4.jpg" />4. Đồng b&agrave;o T&acirc;y Nguy&ecirc;n quan niệm nh&agrave; r&ocirc;ng l&agrave; nơi thu h&uacute;t kh&iacute; thi&ecirc;ng đất trời để bảo trợ cho d&acirc;n l&agrave;ng. Do đ&oacute; trong mỗi nh&agrave; r&ocirc;ng đều c&oacute; một nơi trang trọng để thờ c&aacute;c vật được người d&acirc;n cho l&agrave; thần linh tr&uacute; ngụ như con dao, h&ograve;n đ&aacute;, sừng tr&acirc;u&hellip; Ngo&agrave;i ra, nơi n&agrave;y c&ograve;n như một bảo t&agrave;ng lưu giữ c&aacute;c hiện vật truyền thống gắn liền với lịch sử h&igrave;nh th&agrave;nh bu&ocirc;n l&agrave;ng như cồng chi&ecirc;ng, trống, vũ kh&iacute;, đầu c&aacute;c con vật hiến sinh trong ng&agrave;y lễ.</p>

<p>5. Nh&agrave; r&ocirc;ng l&agrave; nơi quan trọng nhất l&agrave;ng n&ecirc;n đ&agrave;n &ocirc;ng trong l&agrave;ng phải thay nhau ngủ qua đ&ecirc;m tại đ&acirc;y để tr&ocirc;ng coi. Một số l&agrave;ng l&agrave;m đến hai nh&agrave; r&ocirc;ng: &ldquo;nh&agrave; r&ocirc;ng c&aacute;i&rdquo; nhỏ v&agrave; c&oacute; m&aacute;i thấp d&agrave;nh cho phụ nữ, &ldquo;nh&agrave; r&ocirc;ng đực&rdquo; d&agrave;nh cho đ&agrave;n &ocirc;ng c&oacute; quy m&ocirc; lớn hơn v&agrave; trang tr&iacute; c&ocirc;ng phu. Ngo&agrave;i mục đ&iacute;ch g&igrave;n giữ kh&ocirc;ng gian thi&ecirc;ng, nh&agrave; r&ocirc;ng l&agrave; nơi người d&acirc;n trao đổi những c&acirc;u chuyện, kinh nghiệm trong đời sống. Nam nữ độc th&acirc;n trong l&agrave;ng c&oacute; thể qu&acirc;y quần tại nh&agrave; r&ocirc;ng để thăm hỏi, t&igrave;m bạn đời, tuy nhi&ecirc;n kh&ocirc;ng đi được ph&eacute;p đi qu&aacute; giới hạn.</p>

<p>6. Mỗi d&acirc;n tộc ở T&acirc;y Nguy&ecirc;n c&oacute; kiểu l&agrave;m nh&agrave; r&ocirc;ng kh&aacute;c nhau. K&iacute;ch thước nh&agrave; r&ocirc;ng nhỏ v&agrave; thấp nhất l&agrave; của người Giẻ Tri&ecirc;ng. Nh&agrave; r&ocirc;ng của người X&ecirc; &ETH;ăng cao v&uacute;t. Nh&agrave; r&ocirc;ng của người Gia Rai c&oacute; m&aacute;i mảnh, dẹt như lưỡi r&igrave;u. Nh&agrave; r&ocirc;ng của người Ba Na to hơn nh&agrave; Gia Rai, c&oacute; đường n&eacute;t mềm mại v&agrave; thường c&oacute; c&aacute;c nh&agrave; s&agrave;n xung quanh. Điểm chung của c&aacute;c ng&ocirc;i nh&agrave; r&ocirc;ng l&agrave; được x&acirc;y cất tr&ecirc;n một khoảng đất rộng, nằm ngay tại khu vực trung t&acirc;m của bu&ocirc;n l&agrave;ng.</p>

<p>&nbsp;</p>

<p>7. S&agrave;n nh&agrave; r&ocirc;ng được thiết kế gắn liền với văn h&oacute;a qu&acirc;y quần uống rượu cần của đồng b&agrave;o. S&agrave;n thường được l&agrave;m từ v&aacute;n gỗ hay ống tre nứa đập dập, khi gh&eacute;p kh&ocirc;ng kh&iacute;t nhau m&agrave; c&aacute;c tấm c&aacute;ch nhau khoảng 1 cm. Nhờ thế m&agrave; khi người d&acirc;n tập trung ăn uống, nước kh&ocirc;ng bị chảy l&ecirc;nh l&aacute;ng ra s&agrave;n. Mặt kh&aacute;c, kiểu s&agrave;n n&agrave;y gi&uacute;p cho việc vệ sinh nh&agrave; dễ d&agrave;ng hơn.</p>

<p>8. Cầu thang nh&agrave; r&ocirc;ng thường c&oacute; 7 đến 9 bậc, tuy nhi&ecirc;n mỗi d&acirc;n tộc lại c&oacute; trang tr&iacute; kh&aacute;c nhau. Tr&ecirc;n th&agrave;nh v&agrave; cột của cầu thang, người Gia Rai hay tạc h&igrave;nh quả bầu đựng nước, người Bana khắc h&igrave;nh ngọn c&acirc;y rau dớn, c&ograve;n người Giẻ Tri&ecirc;ng, Xơ Đăng thường đẽo h&igrave;nh n&uacute;m chi&ecirc;ng hoặc mũi thuyền.</p>

<p>9. Nếu như m&aacute;i đ&igrave;nh miền xu&ocirc;i gắn liền với h&igrave;nh ảnh c&acirc;y đa, th&igrave; nh&agrave; r&ocirc;ng T&acirc;y Nguy&ecirc;n c&oacute; c&acirc;y n&ecirc;u. C&acirc;y n&ecirc;u được trang tr&iacute; nhiều họa tiết, đặt ở ph&iacute;a trước s&acirc;n ch&iacute;nh giữa của ng&ocirc;i nh&agrave; r&ocirc;ng để phục vụ c&aacute;c lễ hội lớn của bu&ocirc;n l&agrave;ng. Theo quan niệm, c&acirc;y n&ecirc;u l&agrave; nơi hội tụ c&aacute;c vị thần linh. Ở từng lễ hội, c&acirc;y n&ecirc;u mang một h&igrave;nh ảnh biểu tượng kh&aacute;c nhau như c&acirc;y n&ecirc;u trong lễ đ&acirc;m tr&acirc;u c&oacute; 4 nh&aacute;nh, lễ mừng l&uacute;a mới c&acirc;y n&ecirc;u chỉ c&oacute; 1 nh&aacute;nh&hellip;</p>

<p>10. Nh&agrave; r&ocirc;ng chỉ gắn với bu&ocirc;n l&agrave;ng, kh&ocirc;ng c&oacute; nh&agrave; r&ocirc;ng cấp tỉnh, cấp huyện hoặc nh&agrave; r&ocirc;ng chung nhiều l&agrave;ng. Hiện nay, nh&agrave; r&ocirc;ng truyền thống lu&ocirc;n được g&igrave;n giữ tại trung t&acirc;m l&agrave;ng. Một số địa điểm bu&ocirc;n l&agrave;ng c&oacute; nh&agrave; r&ocirc;ng hiện nay l&agrave; nh&agrave; r&ocirc;ng Kon Klor ở th&agrave;nh phố Kon Tum, l&agrave;ng Plei Phung, l&agrave;ng Kon So Lăl (huyện Chư Pah) v&agrave; l&agrave;ng Đ&ecirc; K&rsquo;tu (huyện Mang Yang) ở Gia Lai.</p>

<p>&nbsp;</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link -->', NULL, NULL, N'19052021_110520yh_2.jpg', 1, 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:05:20.213' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-13T23:10:19.677' AS DateTime), 1, NULL)
GO
SET IDENTITY_INSERT [dbo].[NewsAndCategories] ON 

INSERT [dbo].[NewsAndCategories] ([Id], [CategoryId], [NewsId]) VALUES (3, 2, N'ab7c739d-6632-4002-bfe4-5c036c1dae29')
INSERT [dbo].[NewsAndCategories] ([Id], [CategoryId], [NewsId]) VALUES (4, 16, N'cd6dfbac-874f-49d6-a594-3b6cececa2fa')
SET IDENTITY_INSERT [dbo].[NewsAndCategories] OFF
GO
SET IDENTITY_INSERT [dbo].[NewsCategory] ON 

INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (1, N'Ẩm thực', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (2, N'Cảnh đẹp', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (3, N'Khám phá', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (4, N'Du lịch biển', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (5, N'Di sản văn hóa', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (6, N'Mẹo du lịch', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (7, N'Du lịch tâm linh', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (8, N'Vui chơi - Giải trí', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (9, N'Thế giới đó đây', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (10, N'Lịch trình du lịch A - Z', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (11, N'Lễ hội - Sự kiện', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (12, N'Du lịch cộng đồng', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (13, N'Nghỉ dưỡng', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (14, N'Du lịch nghỉ mát', 1)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (15, N'Trăng mật', 0)
INSERT [dbo].[NewsCategory] ([CategoryID], [CategoryName], [IsActive]) VALUES (16, N'Thông tin covid', 1)
SET IDENTITY_INSERT [dbo].[NewsCategory] OFF
GO
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (1, 1, N'Tin tức ', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NewsTypes] ([NewsTypeID], [Code], [Name], [Status], [Note], [CrtBy], [CrtDate], [UpdBy], [UpdDate], [CompID]) VALUES (2, 2, N'Cẩm nang du lịch', 1, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (0, N'Chợ Đà Lạt', 466, NULL, 0, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-13T14:34:10.457' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-13T14:34:10.457' AS DateTime))
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ Tuyền Lâm', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Ven Hồ', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Ga Xe Lửa Cũ', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Trung Tâm Thành Phố Đà Lạt', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Thác Cam Ly', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Thung Lũng Tình Yêu / Đồi Mộng Mơ', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Thung Lũng Vàng', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Núi LangBiang', 466, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Area] ([AreaId], [AreaName], [DistrictId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Khu du lịch Trúc Lâm Viên', 466, NULL, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_banner] ON 

INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (2, N'Hồ Chí Minh', N'bn_210605_hcmPopular.jpg', N'Khách sạn tại Hồ Chí Minh', 3, NULL, NULL, NULL, N'#', N'_blank', NULL, 1, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (3, N'Đà Nẵng', N'bn_210605_danangPopular.jpg', N'Khách sạn tại Đà Nẵng', 3, NULL, NULL, NULL, NULL, N'_blank', NULL, 2, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (5, N'Bà Rịa - Vũng Tàu', N'bn_210605_VungTau.jpg', N'Khách sạn tại Bà Rịa - Vũng Tàu', 3, NULL, NULL, NULL, N'#', N'_blank', NULL, NULL, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (6, N'Đà Lạt', N'bn_210605_Dalat.jpg', N'Khách sạn tại Đà Lạt', 3, NULL, NULL, NULL, NULL, N'_blank', NULL, 4, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (7, N'Sapa', N'bn_210605_Sapa.jpg', N'Khách sạn tại Sapa', 3, NULL, NULL, NULL, NULL, N'_blank', NULL, 5, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (8, N'Hạ Long', N'bn_210605_Halong.jpg', N'Điểm đến phổ biến tại Hạ Long', 3, NULL, NULL, NULL, NULL, N'_blank', NULL, 6, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (9, N'Huế', N'bn_210606_hue.jpg', N'Khách sạn tại Huế', 3, NULL, NULL, NULL, NULL, N'_blank', NULL, 4, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (10, N'Hà Nội', N'bn_210606_hanoi.jpg', N'Khách sạn tại Hà Nội', 3, NULL, NULL, NULL, NULL, N'_blank', NULL, NULL, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (11, N'Phú Quốc', N'bn_210606_PhuQuoc.jpg', N'Khách sạn tại Phú Quốc', 3, NULL, NULL, NULL, NULL, N'_blank', NULL, 8, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (12, N'Chương trình Ưu đãi VNPay', N'bn_210705_BANNER TOP 1.png', N'Chương trình Ưu đãi VNPay', 2, NULL, NULL, NULL, N'#', N'_blank', NULL, 1, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (13, N'Ưu đãi du lịch', N'bn_210606_16052021_080417yh_img-1.jpeg', N'Ưu đãi du lịch', 2, NULL, NULL, NULL, NULL, N'_blank', NULL, 1, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (14, N'Ưu đãi du lịch', N'bn_210606_16052021_091154yh_o-d1-(1).jpg', N'Ưu đãi du lịch', 2, NULL, NULL, NULL, N'http://localhost:13863/Admin/Banner/Edit/14', N'_blank', NULL, 1, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (15, N'Chương trình Ưu đãi VNPay', N'bn_210705_Khách sạn ưu đãi ĐỘC QUYỀN (1).png', N'Chương trình Ưu đãi VNPay', 4, NULL, NULL, NULL, NULL, N'_blank', NULL, 1, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (16, N'Chương trình Ưu đãi VNPay', N'bn_210705_Khách sạn ưu đãi ĐỘC QUYỀN.png', N'Chương trình Ưu đãi VNPay', 4, NULL, NULL, NULL, NULL, N'_blank', NULL, 2, 1, NULL, NULL, CAST(N'2021-06-01T00:00:00.000' AS DateTime), CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (17, N'Logo-Header', N'bn_210705_logo-combo.png', NULL, 1, NULL, NULL, NULL, NULL, N'_blank', NULL, NULL, 1, NULL, NULL, NULL, CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_banner] ([banner_id], [banner_name], [banner_src], [banner_desc], [banner_pos], [banner_width], [banner_height], [banner_dura], [banner_hyperlink], [banner_target], [isflash], [priority], [active], [language_id], [visits], [date_start], [date_end], [listpage], [img_apple]) VALUES (20, N'Why choose hotcombo', N'bn_210713_whychooseme.png', N'Why choose hotcombo', 6, NULL, NULL, NULL, N'#', N'_blank', 0, 1, 1, 2, NULL, NULL, CAST(N'2021-10-31T00:00:00.000' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_banner] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Banner_Position] ON 

INSERT [dbo].[tbl_Banner_Position] ([Id], [Name]) VALUES (1, N'Logo Công Ty(150px*61px)')
INSERT [dbo].[tbl_Banner_Position] ([Id], [Name]) VALUES (2, N'Slide Banner Trang Chủ (710px*380px)')
INSERT [dbo].[tbl_Banner_Position] ([Id], [Name]) VALUES (3, N'Điểm đến du lịch (248px*186px)')
INSERT [dbo].[tbl_Banner_Position] ([Id], [Name]) VALUES (4, N'Slide Footer Trang Chủ (523px*392px)')
INSERT [dbo].[tbl_Banner_Position] ([Id], [Name]) VALUES (5, N'Popup ngoài trang chủ (640px*424px)')
INSERT [dbo].[tbl_Banner_Position] ([Id], [Name]) VALUES (6, N'Logo Vì sao chọn HotCombo (299px*54px)')
SET IDENTITY_INSERT [dbo].[tbl_Banner_Position] OFF
GO
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'3070a538-2f53-4db8-8b0a-231faacbbcd2', N'12345v', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-14T00:23:31.970' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 2, 0, N'00000000-0000-0000-0000-000000000000', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'2590e2ed-85ca-44d4-8a4a-36acef02aff3', N'12345a', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-14T00:04:39.940' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 1, 2, 2, 124400, NULL, NULL, 1244000, 1368400, 1, 0, N'00000000-0000-0000-0000-000000000000', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'85bc2708-dfac-4f0c-9356-4085bcb04457', N'12345w', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-13T21:03:49.723' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, 0, 0, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', N'test@gmail', N'0123456789', N'Hieu Nguyen', 60, 575, N'Test123', NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-14T20:30:32.117' AS DateTime), 2, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'5b4039ac-168c-4bee-9fbc-534f8c9014f5', N'0RLOU3', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', CAST(N'2021-07-22T14:00:00.000' AS DateTime), CAST(N'2021-07-23T12:00:00.000' AS DateTime), CAST(N'2021-07-21T06:56:36.060' AS DateTime), CAST(N'2021-07-23T06:56:36.060' AS DateTime), 1, 1, 1, 59000, NULL, NULL, 590000, 649000, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 76, 731, N'Bãi Sau', N'-phòng không hút thuốc
-phòng ở tầng cao
-', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'bc0ed35f-f2c4-4061-af7a-558ca901f2f4', N'MQ0704', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-15T01:48:16.300' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-10T19:32:22.613' AS DateTime), 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'c2645888-ad73-4d0d-98e8-6512574a3fca', N'7Q5CJ1', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-07-29T14:00:00.000' AS DateTime), CAST(N'2021-07-30T12:00:00.000' AS DateTime), CAST(N'2021-07-21T07:00:39.310' AS DateTime), CAST(N'2021-07-23T07:00:39.310' AS DateTime), 1, 1, 2, 59000, NULL, NULL, 590000, 649000, 2, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 9, 57, N'Bãi Sau', N'-', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'290d68df-ec64-499a-9936-735cc15c4eba', N'ABRC21', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-07-10T19:42:07.853' AS DateTime), CAST(N'2021-07-11T14:00:00.000' AS DateTime), 1, 1, 1, 60000, NULL, NULL, 600000, 660000, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 12, 83, N'Bãi Sau', N'-phòng có 2 giường đơn
-phòng ở tầng cao
-test', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'4f06f046-75ea-4648-a755-747b38c43a1a', N'ZO152D', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-23T20:55:11.970' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 70, 685, N'Test123', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'c49fe2e1-58ce-4769-9d0c-7bcd342a04e8', N'01WEGU', N'64477619-22da-45d0-9d24-8c63124e0d82', N'7d63a5f7-513a-4a42-bf84-af8884120801', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-07-05T06:45:09.093' AS DateTime), CAST(N'2021-07-06T14:00:00.000' AS DateTime), 1, 1, 2, 60000, NULL, NULL, 600000, 660000, 1, 0, N'00000000-0000-0000-0000-000000000000', N'pquangkhanh@gmail.com', N'1246578', N'Khánh Phạm', 68, 668, N'123456', N'-', NULL, NULL, NULL, NULL, 0, NULL, 0, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-05T06:52:36.370' AS DateTime), 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'340fabe3-792e-4199-a889-881146f0dc87', N'AP6P7O', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-21T10:29:20.160' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 2, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 9, 56, N'Test123', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'0152e80c-f8a9-42bd-8491-92555e5cecfe', N'HY3PWG', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', CAST(N'2021-07-19T14:00:00.000' AS DateTime), CAST(N'2021-07-20T12:00:00.000' AS DateTime), CAST(N'2021-07-18T14:15:25.293' AS DateTime), CAST(N'2021-07-20T14:15:25.293' AS DateTime), 1, 1, 1, 59000, NULL, NULL, 590000, 649000, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 9, 56, N'Bãi Sau', N'-phòng không hút thuốc
-phòng ở tầng cao
-Dọn phòng sạch', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'1dd80086-fabd-4f33-9d7a-9633932c476a', N'56ZD37', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-07-10T19:53:05.843' AS DateTime), CAST(N'2021-07-11T14:00:00.000' AS DateTime), 1, 1, 1, 60000, NULL, NULL, 600000, 660000, 2, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 9, 56, N'Bãi Sau', N'-phòng có 1 giường đôi
-phòng ở tầng cao
-Giường nhún tốt + áo mưa + khẩu trang', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'3d88b500-39fc-49cd-8462-9a55e9e345f0', N'14EYC2', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-17T11:15:54.233' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', N'test@gmail', N'0123456789', N'Hieu Nguyen', 12, 89, N'Test123', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'73506c54-24a9-4905-8531-9d60079d5b3b', N'C5118D', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-21T14:44:00.480' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 2, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 20, 180, N'Test123', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'f8f3aa8e-cd12-451a-b2c2-a91ea3416f8d', N'06BTED', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-15T01:23:21.747' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'61b337dc-0e11-40dd-9227-ac4cfac08d23', N'5Q6G1U', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-21T11:16:09.590' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 2, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 34, 312, N'Test123', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'd183006d-4801-4b42-b14e-b42b65719a9b', N'0S63X8', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-17T11:20:53.347' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 2, 0, N'00000000-0000-0000-0000-000000000000', N'test@gmail', N'0123456789', N'Hieu Nguyen', 68, 663, N'Test123', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'fa166d3b-1068-45d5-9e7d-c5eb1bfa8717', N'1O8NTE', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-21T14:52:03.377' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 55, 524, N'Test123', NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, 1, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'bf52b1b6-3e2c-4ad4-969c-dd3ede9ca0a9', N'4R381D', N'64477619-22da-45d0-9d24-8c63124e0d82', N'7d63a5f7-513a-4a42-bf84-af8884120801', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-07-04T21:03:23.843' AS DateTime), CAST(N'2021-07-05T14:00:00.000' AS DateTime), 1, 1, 2, 59000, NULL, NULL, 590000, 649000, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 9, 59, N'Bãi Sau', NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-10T19:31:44.440' AS DateTime), 2, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'd205bf4d-9a78-4f19-99e7-e5cb88e2eee9', N'68FTKM', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-23T20:18:12.993' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 15, 117, N'Test123', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, 2, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'4d1d6612-b31e-4953-b5fa-e7d05eb816db', N'5R032O', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-17T11:24:20.370' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 2, 0, N'00000000-0000-0000-0000-000000000000', N'test@gmail', N'0123456789', N'Hieu Nguyen', 1, 4, N'Test123', NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-10T19:33:04.217' AS DateTime), 2, 0)
INSERT [dbo].[tbl_Booking] ([BookingId], [BookingCode], [HotelId], [RoomId], [CheckIn], [CheckOut], [DateCreate], [CODCancel], [CountDate], [RoomCount], [PersonCount], [Tax], [Surcharge], [Penalty], [RoomAmount], [TotalAmount], [PaymentType], [IsInvoceExport], [CustomerId], [CustomerEmail], [CustomerPhoneNumber], [CustomerName], [CustomerProvinceId], [CustomerDistrictId], [CustomerAddress], [CustomerNote], [CompanyName], [TaxCode], [CompanyAddress], [BillingAddress], [CustomerCheckPayment], [DateCheckPayment], [IsPaymentSuccess], [UserConfirm], [DateConfirm], [Status], [IsDelete]) VALUES (N'1a3a2ad7-67f8-45da-ba18-ec8e48a05b29', N'Q7071Y', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', CAST(N'2021-06-22T14:00:00.000' AS DateTime), CAST(N'2021-06-24T14:00:00.000' AS DateTime), CAST(N'2021-06-23T20:05:51.720' AS DateTime), CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 2, 2, 276400, NULL, NULL, 2764000, 3040400, 1, 0, N'00000000-0000-0000-0000-000000000000', N'hieuviet.1103@gmail.com', N'0123456789', N'Hieu Nguyen', 58, 556, N'Test123', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[tbl_BookingAllot] ON 

INSERT [dbo].[tbl_BookingAllot] ([Id], [BookingId], [UserId], [Status], [UserCreate], [DateCreate], [DeadLine], [IsDelete]) VALUES (1, N'0152e80c-f8a9-42bd-8491-92555e5cecfe', N'17fb8d13-9e2a-4dc4-93d5-7d3290ce1995', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-18T20:51:50.027' AS DateTime), CAST(N'2021-07-22T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tbl_BookingAllot] ([Id], [BookingId], [UserId], [Status], [UserCreate], [DateCreate], [DeadLine], [IsDelete]) VALUES (2, N'0152e80c-f8a9-42bd-8491-92555e5cecfe', N'17fb8d13-9e2a-4dc4-93d5-7d3290ce1995', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-18T21:20:35.983' AS DateTime), CAST(N'2021-07-19T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[tbl_BookingAllot] ([Id], [BookingId], [UserId], [Status], [UserCreate], [DateCreate], [DeadLine], [IsDelete]) VALUES (3, N'c2645888-ad73-4d0d-98e8-6512574a3fca', N'17fb8d13-9e2a-4dc4-93d5-7d3290ce1995', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:04:30.670' AS DateTime), CAST(N'2021-07-29T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[tbl_BookingAllot] ([Id], [BookingId], [UserId], [Status], [UserCreate], [DateCreate], [DeadLine], [IsDelete]) VALUES (4, N'5b4039ac-168c-4bee-9fbc-534f8c9014f5', N'17fb8d13-9e2a-4dc4-93d5-7d3290ce1995', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:06:56.133' AS DateTime), CAST(N'2021-07-22T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[tbl_BookingAllot] OFF
GO
INSERT [dbo].[tbl_BookingStatus] ([Id], [Name]) VALUES (1, N'Tạo booking thành công')
INSERT [dbo].[tbl_BookingStatus] ([Id], [Name]) VALUES (2, N'Khách hàng xác nhận thanh toán')
INSERT [dbo].[tbl_BookingStatus] ([Id], [Name]) VALUES (3, N'Admin xác nhận thanh toán')
INSERT [dbo].[tbl_BookingStatus] ([Id], [Name]) VALUES (4, N'Booking đã huỷ')
GO
SET IDENTITY_INSERT [dbo].[tbl_Content] ON 

INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (2, N'privacy-cookies', N'Chính sách và quy định chung', N'<p>&nbsp;</p>

<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>

<p>&nbsp;</p>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>
', NULL, CAST(N'2021-07-14T00:55:00.010' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (3, N'payment-condition', N'Quy định về thanh toán', N'<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>
', CAST(N'2021-07-05T05:57:31.000' AS DateTime), CAST(N'2021-07-14T01:13:21.763' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (4, N'booking-hotel-confirmation', N'Quy định về xác nhận thông tin đặt phòng', N'<html>
<head>
	<title></title>
</head>
<body data-gr-ext-installed="" data-new-gr-c-s-check-loaded="14.1019.0"></body>
</html>
', CAST(N'2021-07-05T05:58:18.150' AS DateTime), CAST(N'2021-07-05T05:58:18.150' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (5, N'cancellation-privacy', N'Chính sách về hủy đặt phòng và hoàn trả tiền', N'<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>
', CAST(N'2021-07-05T05:59:12.000' AS DateTime), CAST(N'2021-07-14T01:13:55.353' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (6, N'information-security', N'Chính sách bảo mật thông tin', N'<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>
', CAST(N'2021-07-05T05:59:48.000' AS DateTime), CAST(N'2021-07-14T01:14:06.857' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (7, N'operational-regulations', N'Quy chế hoạt động', N'<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>
', CAST(N'2021-07-05T06:00:50.000' AS DateTime), CAST(N'2021-07-14T01:14:16.417' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (8, N'privacy-policy', N'Chính sách bảo mật', N'<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>
', CAST(N'2021-07-05T06:01:40.000' AS DateTime), CAST(N'2021-07-14T01:14:27.330' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (9, N'dispute-and-complaint-settlement-process', N'Quy trình giải quyết tranh chấp, khiếu nại', N'<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2">
			<p>TP HCM sẽ thực hiện linh hoạt việc th&iacute; điểm c&aacute;ch ly F1 tại nh&agrave; theo &quot;c&ocirc;ng thức 14-14&quot; (14 ng&agrave;y c&aacute;ch ly tập trung, 14 ng&agrave;y tại nh&agrave;) theo hướng dẫn Bộ Y tế.</p>

			<p>Th&ocirc;ng tin được Chủ tịch UBND TP HCM Nguyễn Th&agrave;nh Phong n&oacute;i tại buổi l&agrave;m việc trực tuyến của Ch&iacute;nh phủ với 8 tỉnh, th&agrave;nh ph&iacute;a Nam ng&agrave;y 4/7, về phương &aacute;n ph&ograve;ng chống dịch trong thời gian tới. Trước đ&acirc;y, Việt Nam thực hiện c&aacute;ch ly F1 (tiếp x&uacute;c gần ca nhiễm) trong 14 ng&agrave;y nhưng sau đ&oacute; n&acirc;ng l&ecirc;n 21 ng&agrave;y.</p>

			<p>Hiện, TP HCM c&oacute; 14.392 người c&aacute;ch ly tập trung, 36.305 trường hợp c&aacute;ch ly tại nh&agrave;, nơi lưu tr&uacute;. Việc c&aacute;ch ly F1 tại nh&agrave; được xem gi&uacute;p giảm tải cho c&aacute;c khu c&aacute;ch ly tập trung tại TP HCM. Bởi c&ugrave;ng số ca bệnh tăng (hơn 6.000 ca t&iacute;nh đến ng&agrave;y 4/7), số ca F1 cũng tăng li&ecirc;n tục khiến c&aacute;c khu c&aacute;ch ly ở th&agrave;nh phố qu&aacute; tải.</p>

			<p><img alt="Chủ tịch UBND thành phố Nguyễn Thành Phong phát biểu tại cuộc họp. Ảnh: Trung tâm báo chí TP HCM." src="https://i1-vnexpress.vnecdn.net/2021/07/04/cachlyf114-4761-1625415889.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=A4mNIDmtFHbQayy_j2cTEw" /></p>

			<p>Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong ph&aacute;t biểu tại cuộc họp. Ảnh:&nbsp;<em>Trung t&acirc;m b&aacute;o ch&iacute; TP HCM.</em></p>

			<p>Theo hướng dẫn của Bộ Y tế gửi TP HCM ng&agrave;y 27/6, F1 ở TP HCM được c&aacute;ch ly tại nh&agrave;&nbsp;<a href="https://vnexpress.net/dieu-kien-cach-ly-f1-tai-nha-o-tp-hcm-4300628.html" rel="dofollow">28 ng&agrave;y</a>&nbsp;nếu đảm bảo ph&ograve;ng ri&ecirc;ng, kh&eacute;p k&iacute;n, t&aacute;ch biệt khu sinh hoạt chung gia đ&igrave;nh.</p>

			<p>Người c&aacute;ch ly kh&ocirc;ng ra khỏi ph&ograve;ng; kh&ocirc;ng tiếp x&uacute;c người trong gia đ&igrave;nh v&agrave; vật nu&ocirc;i. F1 lu&ocirc;n c&agrave;i đặt v&agrave; bật ứng dụng khai b&aacute;o y tế h&agrave;ng ng&agrave;y như VHD (Vietnam Health Declaration), Bluzone; tự đo th&acirc;n nhiệt, theo d&otilde;i sức khỏe để cập nhật...</p>

			<p>Tại cuộc họp, Chủ tịch UBND th&agrave;nh phố Nguyễn Th&agrave;nh Phong cho biết, từ khi &aacute;p dụng Chỉ thị 10, chủ tịch quận, huyện được giao to&agrave;n quyền quyết định c&aacute;c vấn đề tại khu vực, n&acirc;ng cao hệ thống ch&iacute;nh trị tại cơ sở, ph&aacute;t huy vai tr&ograve; của tổ Covid-19 cộng đồng.</p>

			<p>Theo &ocirc;ng Phong, việc trả kết quả x&eacute;t nghiệm chậm c&oacute; xảy ra nhưng chỉ c&aacute; biệt. Nhằm đẩy mạnh năng lực x&eacute;t nghiệm, th&agrave;nh phố triển khai th&agrave;nh lập Trung t&acirc;m điều phối v&agrave; x&eacute;t nghiệm Covid-19, chỉ đạo lập c&aacute;c tổ x&eacute;t nghiệm tại c&aacute;c quận huyện; đ&agrave;m ph&aacute;n mua 1,4 triệu test nhanh kh&aacute;ng nguy&ecirc;n; tập trung khắc phục hạn chế về tổ chức, năng lực v&agrave; đội ngũ x&eacute;t nghiệm.</p>

			<p>Để đảm bảo duy tr&igrave; sản xuất, th&agrave;nh phố tổ chức cho 43 doanh nghiệp tại khu chế xuất, khu c&ocirc;ng nghệ cao vừa sản xuất vừa c&aacute;ch ly tại chỗ theo &yacute; kiến chỉ đạo của Thủ tướng. Đồng thời, th&agrave;nh lập 100 tổ kiểm tra hướng dẫn an to&agrave;n ph&ograve;ng chống dịch tại c&aacute;c điểm tr&ecirc;n, đẩy mạnh lấy mẫu test nhanh tại doanh nghiệp.</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></p>
', CAST(N'2021-07-05T06:02:16.000' AS DateTime), CAST(N'2021-07-14T01:14:38.610' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (10, N'home-address-company', N'Menu Địa chỉ Footer', N'<h5><strong>C&Ocirc;NG TY CỔ PHẦN DU LỊCH VIỆT NAM VNTRAVEL</strong></h5>

<div class="contact-mytour">
<ul>
	<li>H&agrave; Nội: 024 7109 9999</li>
	<li>Hồ Ch&iacute; Minh: 028 7109 9998</li>
	<li>Email: admin@hotcombo.vn</li>
	<li>Văn ph&ograve;ng H&agrave; Nội: Tầng 11, T&ograve;a Peakview, 36 Ho&agrave;ng Cầu, Đống Đa</li>
	<li>Văn ph&ograve;ng HCM: Tầng 6, T&ograve;a Nh&agrave; Central Park, 117 Nguyễn Du, Q.1</li>
</ul>

<ul>
	<li>M&atilde; số doanh nghiệp: 0108886908 do Sở Kế hoạch v&agrave; Đầu tư th&agrave;nh phố H&agrave; Nội cấp lần đầu ng&agrave;y 04 th&aacute;ng 09 năm 2019.</li>
	<li>Lĩnh vực kinh doanh: Đại l&yacute; du lịch</li>
	<li>Địa chỉ t&ecirc;n miền: Hotcombo.vn</li>
</ul>
</div>
', CAST(N'2021-07-12T23:53:04.000' AS DateTime), CAST(N'2021-07-24T17:41:49.423' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (11, N'home-policy-footer', N'Menu Quy định chính sách đặt phòng khách sạn', N'<div class="footer-block">
<h5><strong>QUY ĐỊNH D&Agrave;NH CHO WEBSITE TMĐT B&Aacute;N H&Agrave;NG &ndash;<br />
DỊCH VỤ ĐẶT PH&Ograve;NG</strong></h5>

<ul>
	<li><a href="http://localhost:13863/StaticPage/Index?Type=privacy-cookies">Ch&iacute;nh s&aacute;ch v&agrave; quy định chung </a></li>
	<li><a href="http://localhost:13863/StaticPage/Index?Type=payment-condition">Quy định về thanh to&aacute;n</a></li>
	<li><a href="#">Quy định về x&aacute;c nhận th&ocirc;ng tin đặt ph&ograve;ng </a></li>
	<li><a href="http://localhost:13863/StaticPage/Index?Type=cancellation-privacy">Ch&iacute;nh s&aacute;ch về hủy đặt ph&ograve;ng v&agrave; ho&agrave;n trả tiền </a></li>
	<li><a href="http://localhost:13863/StaticPage/Index?Type=privacy-policy">Ch&iacute;nh s&aacute;ch bảo mật th&ocirc;ng tin </a></li>
</ul>
<a class="logo" href="#" target="_blank" title="CÔNG TY CỔ PHẦN DU LỊCH VIỆT NAM VNTRAVEL"><img alt="Đã thông báo Bộ Công Thương" src="https://staticproxy.mytourcdn.com/0x0,q90/themes/images/logo-dathongbao-bocongthuong-w165.png" /> </a></div>

<div class="footer-block">
<h5>QUY ĐỊNH CHO S&Agrave;N GDTMĐT - DỊCH VỤ ĐẶT PH&Ograve;NG V&Agrave; DỊCH VỤ ĐẶT TOUR DU LỊCH</h5>

<ul>
	<li><a href="http://localhost:13863/StaticPage/Index?Type=operational-regulations">Quy chế hoạt động</a></li>
	<li><a href="http://localhost:13863/StaticPage/Index?Type=privacy-policy">Ch&iacute;nh s&aacute;ch bảo mật</a></li>
	<li><a href="http://localhost:13863/StaticPage/Index?Type=dispute-and-complaint-settlement-process">Quy tr&igrave;nh giải quyết tranh chấp, khiếu nại</a></li>
</ul>
<a class="logo" href="#" target="_blank" title="CÔNG TY CỔ PHẦN DU LỊCH VIỆT NAM VNTRAVEL"><img alt="Đã đăng ký Bộ Công Thương" src="https://staticproxy.mytourcdn.com/0x0,q90/themes/images/logo-congthuong-w165.png" /> </a></div>
', CAST(N'2021-07-13T11:49:14.000' AS DateTime), CAST(N'2021-07-24T17:41:05.483' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (12, N'home-partner-info', N'Menu footer Thông tin đối tác', N'<div class="footer-block">
<p><strong>Th&ocirc;ng tin li&ecirc;n hệ</strong></p>

<ul>
	<li><a href="http://career.hotcombo.vn/" title="Tuyển dụng">Tuyển dụng</a></li>
	<li><a href="Contact/Index" title="Liên hệ">Li&ecirc;n hệ</a></li>
</ul>
</div>
', CAST(N'2021-07-13T12:02:39.000' AS DateTime), CAST(N'2021-07-25T18:40:04.643' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (13, N'header-phone-contact', N'Thông tin liên lạc header', N'<p><strong>H&agrave; Nội:</strong>&nbsp;<a href="tel:0969305043">096 9305 043</a></p>

<p><strong>TPHCM:</strong> <a href="tel:+028 7109 9998"> 028 7109 9998 </a></p>
', CAST(N'2021-07-24T16:57:44.000' AS DateTime), CAST(N'2021-07-24T17:38:50.470' AS DateTime))
INSERT [dbo].[tbl_Content] ([ID], [ContenCode], [ContentName], [ContentDetail], [DateCreate], [DateUpdate]) VALUES (14, N'header-phone-contact-flight', N'Hỗ trợ đặt vé máy bay header', N'<p>Hỗ trợ đặt v&eacute; m&aacute;y bay</p>

<p><a href="tel:+1900 2083">1900 2083 </a></p>
', CAST(N'2021-07-24T17:14:54.000' AS DateTime), CAST(N'2021-07-24T17:39:26.350' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Content] OFF
GO
INSERT [dbo].[tbl_Country] ([CountryId], [CountryName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1', N'Việt Nam', N'VIE', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Country] ([CountryId], [CountryName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'TH', N'Thái Lan', NULL, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-13T20:16:04.397' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-13T20:16:04.397' AS DateTime))
GO
INSERT [dbo].[tbl_Customer] ([CustomerId], [CustomerNo], [CustomerName], [Dob], [Gender], [Nationality], [IdCard], [DateOfIssue], [PlaceOfIssue], [Address], [CountryId], [ProvinceId], [DistrictId], [PhoneNumber], [Email], [Note], [Password], [TotalPoint], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'de1a71fb-2abf-4467-b704-2a264a793f7e', NULL, N'Thi Vu', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0932980038', N'thivu@gmail.com', NULL, N'123456', NULL, 0, NULL, CAST(N'2021-06-14T09:04:16.427' AS DateTime), NULL, CAST(N'2021-06-14T09:04:16.427' AS DateTime))
INSERT [dbo].[tbl_Customer] ([CustomerId], [CustomerNo], [CustomerName], [Dob], [Gender], [Nationality], [IdCard], [DateOfIssue], [PlaceOfIssue], [Address], [CountryId], [ProvinceId], [DistrictId], [PhoneNumber], [Email], [Note], [Password], [TotalPoint], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4cb127b0-1efd-43cc-b62a-31622f70693a', NULL, N'Dang Phuong Anh', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0932980034', N'dangphuonganh', NULL, N'123456', NULL, 0, NULL, CAST(N'2021-06-15T09:04:16.427' AS DateTime), NULL, CAST(N'2021-06-15T09:04:16.427' AS DateTime))
INSERT [dbo].[tbl_Customer] ([CustomerId], [CustomerNo], [CustomerName], [Dob], [Gender], [Nationality], [IdCard], [DateOfIssue], [PlaceOfIssue], [Address], [CountryId], [ProvinceId], [DistrictId], [PhoneNumber], [Email], [Note], [Password], [TotalPoint], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'bb785d92-8e61-494d-bffc-49b0312a8b39', NULL, N'Chi Tran', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0932980037', N'chidan@gmail.com', NULL, N'123456', NULL, 0, NULL, CAST(N'2021-06-14T09:04:16.427' AS DateTime), NULL, CAST(N'2021-06-14T09:04:16.427' AS DateTime))
INSERT [dbo].[tbl_Customer] ([CustomerId], [CustomerNo], [CustomerName], [Dob], [Gender], [Nationality], [IdCard], [DateOfIssue], [PlaceOfIssue], [Address], [CountryId], [ProvinceId], [DistrictId], [PhoneNumber], [Email], [Note], [Password], [TotalPoint], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0c5d1d8b-9aac-4fcb-b711-76feb47c6df2', NULL, N'Hoang Nguyen', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0932980036', N'hoangnguyen@gmail.cọm', NULL, N'123456', NULL, 0, NULL, CAST(N'2021-06-15T09:04:16.427' AS DateTime), NULL, CAST(N'2021-06-15T09:04:16.427' AS DateTime))
INSERT [dbo].[tbl_Customer] ([CustomerId], [CustomerNo], [CustomerName], [Dob], [Gender], [Nationality], [IdCard], [DateOfIssue], [PlaceOfIssue], [Address], [CountryId], [ProvinceId], [DistrictId], [PhoneNumber], [Email], [Note], [Password], [TotalPoint], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2f0d2eb6-f2ef-4e5f-a374-8a359364496d', NULL, N'Hoàng Duy An', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0932980033', N'hoanduyan@gmail.com', NULL, N'123456', NULL, 0, NULL, CAST(N'2021-06-15T09:04:16.427' AS DateTime), NULL, CAST(N'2021-06-15T09:04:16.427' AS DateTime))
INSERT [dbo].[tbl_Customer] ([CustomerId], [CustomerNo], [CustomerName], [Dob], [Gender], [Nationality], [IdCard], [DateOfIssue], [PlaceOfIssue], [Address], [CountryId], [ProvinceId], [DistrictId], [PhoneNumber], [Email], [Note], [Password], [TotalPoint], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7817b280-38b1-47fd-a7b1-9e2930a0f9a9', NULL, N'Trang Nguyen', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0932980035', N'trangnguyen@gmail.com', NULL, N'123456', NULL, 0, NULL, CAST(N'2021-06-20T09:04:16.427' AS DateTime), NULL, CAST(N'2021-06-20T09:04:16.427' AS DateTime))
INSERT [dbo].[tbl_Customer] ([CustomerId], [CustomerNo], [CustomerName], [Dob], [Gender], [Nationality], [IdCard], [DateOfIssue], [PlaceOfIssue], [Address], [CountryId], [ProvinceId], [DistrictId], [PhoneNumber], [Email], [Note], [Password], [TotalPoint], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'834c43e3-5835-4aef-aef8-be8fb87132ac', NULL, N'Thuong Tran', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0932980039', N'thuongtran@gmail.com', NULL, N'123456', NULL, 0, NULL, CAST(N'2021-06-14T09:04:16.427' AS DateTime), NULL, CAST(N'2021-06-14T09:04:16.427' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_Direction] ON 

INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hướng nội bộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Có cửa sổ (không hướng)', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Hướng phố', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Hướng núi', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Hướng biển', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Direction] ([Id], [DirectionName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Hướng Đông 1', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Direction] OFF
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Huyện Hoài Nhơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Huyện Vân Canh', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Huyện Hoài Ân', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Huyện Tuy Phước', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Tp.Qui Nhơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Huyện Tây Sơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Huyện Phù Cát', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'Huyện An Nhơn', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Huyện Vĩnh Thạnh', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Huyện Phù Mỹ', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Huyện An Lão', 1, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'Tam Đường', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'Tp.Lai Châu', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Sìn Hồ', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Mường Tè', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Tân Uyên', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'Than Uyên', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'Phong Thổ', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'Nậm Nhùn', 2, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'Cam Lâm', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N'Trường Sa', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'Diên Khánh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'Vạn Ninh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'Khánh Sơn', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'Thị xã Ninh Hòa', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'Tp.Cam Ranh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'Tp.Nha Trang', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'Huyện Khánh Vĩnh', 3, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'Thị xã Hà Tiên', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'Huyện Phú Quốc', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (31, N'Hòn Đất', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'An Minh', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'Tp.Rạch Giá', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N' Huyện Gò Quao', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'Tân Hiệp', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'Huyện Châu Thành', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (37, N'Huyện Vĩnh Thuận', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (38, N'Kiên Lương', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (39, N'An Biên', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (40, N'Huyện Giồng Riềng', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (41, N'Giang Thành', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (42, N'Kiên Hải', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (43, N'U Minh Thượng', 5, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (44, N'Huyện Trảng Bàng', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (45, N'Huyện Châu Thành', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (46, N'Tân Biên', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (47, N'Dương Minh Châu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (48, N'Huyện Gò Dầu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (49, N'Hòa Thành', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (50, N'Huyện Bến Cầu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (51, N'Huyện Tân Châu', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (52, N'Tp.Tây Ninh', 8, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (53, N'Huyện Tân Thành', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (54, N'Huyện Đất Đỏ', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (55, N'Huyện Châu Đức', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (56, N'Huyện Côn Đảo', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (57, N'Thị xã Bà Rịa', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (58, N'Huyện Long Đất (Xóa)', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (59, N'Tp.Vũng Tàu', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (60, N'Huyện Xuyên Mộc', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (61, N'Huyện Long Điền', 9, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (62, N'Xuân Tây', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (63, N'Hà Đông', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (64, N'Huyện Thạch Nhất', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (65, N'Quận Thanh Trì', 10, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (66, N'Tp.Hà Tĩnh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (67, N'Can Lộc', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (68, N'Nghi Xuân', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (69, N'Cẩm Xuyên', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (70, N'Đức Thọ', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (71, N'Thạch Hà', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (72, N'Kỳ Anh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (73, N'Thị xã Hồng Lĩnh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (74, N'Hương Sơn', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (75, N'Hương Khê', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (76, N'Lộc Hà', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (77, N'Thị xã Kỳ Anh', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (78, N'Vũ Quang', 11, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (79, N'Thị xã Bình Long', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (80, N'Huyện Lộc Ninh', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (81, N'Huyện Đồng Phú', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (82, N'Thị Xã Đồng Xoài', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (83, N'Huyện Phú Riềng', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (84, N'Huyện Chơn Thành', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (85, N'Huyện Hớn Quản', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (86, N'Huyện Bù Đăng', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (87, N'Thị xã Phước Long', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (88, N'Huyện Bù Gia Mập', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (89, N'Huyện Bù Đốp', 12, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (90, N'Thị xã Vĩnh Châu', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (91, N'Huyện Minh Châu', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (92, N'Long Phú', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (93, N'Huyện Kế Sách', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (94, N'Huyện Trần Đề', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (95, N'Huyện Mỹ Xuyên', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (96, N'Huyện Thạnh Trị', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (97, N'Huyện Mỹ Tú', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (98, N'Thị xã Ngã Năm', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (99, N'Huyện Châu Thành', 13, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (100, N'Tp.Sóc Trăng', 13, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (101, N'Phú Vang', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (102, N'A Lưới', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (103, N'Phong Điền', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (104, N'Phú Lộc', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (105, N'Hương Thuỷ', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (106, N'Tp.Huế', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (107, N'Quảng Điền', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (108, N'Hương Trà', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (109, N'Nam Đông', 14, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (110, N'Thái Thụy', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (111, N'Hưng Hà', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (112, N'Tp.Thái Bình', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (113, N'Đông Hưng', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (114, N'Kiến Xương', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (115, N'Vũ Thư', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (116, N'Quỳnh Phụ', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (117, N'Tiền Hải', 15, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (118, N'Tp.Yên Bái', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (119, N'Yên Bình', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (120, N'Thị xã Nghĩa Lộ', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (121, N'Văn Chấn', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (122, N'Văn Yên', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (123, N'Trạm Tấu', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (124, N'Lục Yên', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (125, N'Trấn Yên', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (126, N'Mù Căng Chải', 16, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (127, N'Đắk Glei', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (128, N'Ia H'' Drai', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (129, N'Kon Rẫy', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (130, N'Tp.Kom Tum', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (131, N'Ngọc Hồi', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (132, N'Đắk Tô', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (133, N'Tu Mơ Rông', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (134, N'Kon Plông', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (135, N'Đắk Hà', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (136, N'Sa Thầy', 17, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (137, N'Hải Hà', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (138, N'Hoành Bồ', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (139, N'Tp.Uông Bí', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (140, N'Tp.Cẩm Phả', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (141, N'Bình Liêu', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (142, N'Tp.Hạ Long', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (143, N'Tiên Yên', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (144, N'Ba Chẽ', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (145, N'Cô Tô', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (146, N'Đầm Hà', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (147, N'Thị xã Quảng Yên', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (148, N'Thị xã Đông Triều', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (149, N'Vân Đồn', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (150, N'Tp.Móng Cái', 18, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (151, N'Quận 4', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (152, N'Quận 8', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (153, N'Quận 2', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (154, N'Quận Tân Bình', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (155, N'Quận Bình Thạnh', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (156, N'Huyện Củ Chi', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (157, N'Quận 10', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (158, N'Huyện Nhà Bè', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (159, N'Quận 7', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (160, N'Huyện Hóc Môn', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (161, N'Quận 6', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (162, N'Quận Bình Tân', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (163, N'Quận 12', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (164, N'Quận Tân Phú', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (165, N'Bình Chánh', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (166, N'Quận 3', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (167, N'Quận Thủ Đức', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (168, N'Quận 9', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (169, N'Quận Phú Nhuận', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (170, N'Quận 1', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (171, N'Quận Gò Vấp', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (172, N'Quận 11', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (173, N'Huyện Cần Giờ', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (174, N'Quận 5', 19, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (175, N'Ngọc Hiển', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (176, N'Huyện Năm Căn', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (177, N'Phú Tân', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (178, N'Huyện Trần Văn Thời', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (179, N'Cái Nước', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (180, N'Huyện Thới Bình ', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (181, N'U Minh', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (182, N'Tp.Cà Mau', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (183, N'Đầm Dơi', 20, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (184, N'Con Cuông', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (185, N'Huyện Nghi Lộc', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (186, N'Yên Thành', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (187, N'Nam Đàn', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (188, N'Thanh Chương', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (189, N'Kỳ Sơn', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (190, N'Quỳnh Lưu', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (191, N'Quỳ Hợp', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (192, N'Diễn Châu', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (193, N'Thị xã Cửa Lò', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (194, N'Tương Dương', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (195, N'Tân Kỳ', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (196, N'Tp.Vinh', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (197, N'Anh Sơn', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (198, N'Thị xã Thái Hoà', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (199, N'Đô Lương', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (200, N'Nghĩa Đàn', 21, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (201, N'Hoàng Mai', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (202, N'Quế Phong', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (203, N'Hưng Nguyên', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (204, N'Quỳ Châu', 21, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (205, N'Huyện Yên Thế', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (206, N'Huyện Yên Dũng', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (207, N'Huyện Việt Yên', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (208, N'Huyện Lục Ngạn', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (209, N'Tp.Bắc Giang', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (210, N'Huyện Hiệp Hoà', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (211, N'Huyện Lạng Giang', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (212, N'Huyện Tân Yên', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (213, N'Huyện Sơn Động', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (214, N'Huyện Lục Nam', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (215, N'Huyện Gia Bình', 22, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (216, N'Huyện Giá Rai', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (217, N'Huyện Hồng Dân', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (218, N'Huyện Đông Hải', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (219, N'Huyện Phước Long', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (220, N'Huyện Vĩnh Lợi', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (221, N'Thị xã Bạc Liêu', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (222, N'Huyện Hoà Bình', 23, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (223, N'Yên Mô', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (224, N'Tp.Ninh Bình', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (225, N'Yên Khánh', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (226, N'Nho Quan', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (227, N'Hoa Lư', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (228, N'Tp.Tam Điệp', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (229, N'Kim Sơn', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (230, N'Gia Viễn', 25, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (231, N'Yên Lập', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (232, N'Cẩm Khê', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (233, N'Đoan Hùng', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (234, N'Tam Nông', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (235, N'Thanh Thuỷ', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (236, N'Thanh Ba', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (237, N'Tp.Việt Trì', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (238, N'Thanh Sơn', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (239, N'Phù Ninh', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (240, N'Hạ Hoà', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (241, N'Tân Sơn', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (242, N'Thị xã Phú Thọ', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (243, N'Lâm Thao', 26, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (244, N'Vĩnh Tường', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (245, N'Sông Lô', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (246, N'VĨNH TƯỜNG (Xóa)', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (247, N'Tam Dương', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (248, N'Bình Xuyên', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (249, N'Thị xã Phúc Yên', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (250, N'Lập Thạch', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (251, N'Yên Lạc', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (252, N'Tp.Vĩnh Yên', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (253, N'Tam Đảo', 27, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (254, N'Tân Lạc', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (255, N'Kỳ Sơn', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (256, N'Đà Bắc', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (257, N'Lạc Thủy', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (258, N'Tp.Hòa Bình', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (259, N'Mai Châu', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (260, N'Kim Bôi', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (261, N'Huyện Lương Sơn', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (262, N'Yên Thủy', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (263, N'Huyện Cao Phong', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (264, N'Lạc Sơn', 28, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (265, N'M''Đrắk', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (266, N'Ea H''leo', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (267, N'Ea Súp', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (268, N'Krông A Na', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (269, N'Ea Kar', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (270, N'Cư M''gar', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (271, N'Thị xã Gia Nghĩa (Xóa)', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (272, N'Buôn Đôn', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (273, N'Thị Xã Buôn Hồ', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (274, N'Krông Búk', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (275, N'Krông Năng', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (276, N'Lắk', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (277, N'Krông Bông', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (278, N'Tp. Buôn Mê Thuộc', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (279, N'Cư Kuin', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (280, N'Krông Pắc', 29, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (281, N'Ninh Sơn', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (282, N'Ninh Phước', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (283, N'Tp.Phan Rang-Tháp Chàm', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (284, N'Bác Ái', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (285, N'Thuận Nam', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (286, N'Ninh Hải', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (287, N'Thuận Bắc', 30, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (288, N'Hàm Thuận Bắc', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (289, N'Huyện Tuy Phong', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (290, N'Tp.Phan Thiết', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (291, N'Huyện Thánh Linh', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (292, N'Phú Quí', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (293, N'Bắc Bình', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (294, N'Đức Linh', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (295, N'Hàm Tân', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (296, N'Thị xã La Gi', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (297, N'Hàm Thuận Nam', 31, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (298, N'Giao Thủy', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (299, N'Tp.Nam Định', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (300, N'Xuân Trường', 33, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (301, N'Nghĩa Hưng', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (302, N'Nam Trực', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (303, N'Mỹ Lộc', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (304, N'Ý Yên', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (305, N'Trực Ninh', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (306, N'Vụ Bản', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (307, N'Hải Hậu', 33, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (308, N'Đắk Song', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (309, N'Đắk Mil', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (310, N'Đắk R''Lấp', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (311, N'Krông Nô', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (312, N'Đăk Glong', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (313, N'Tuy Đức', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (314, N'Cư Jút', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (315, N'Thị xã Gia Nghĩa', 34, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (316, N'Nà Hang', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (317, N'Lâm Bình', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (318, N'Yên Sơn', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (319, N'Sơn Dương', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (320, N'Hàm Yên', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (321, N'Tp.Tuyên Quang', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (322, N'Chiêm Hóa', 35, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (323, N'Cái Răng', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (324, N'Vĩnh Thạnh', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (325, N'Phong Điền', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (326, N'Vĩnh Thạnh (Xóa)', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (327, N'Bình Thủy', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (328, N'Tp.Cần Thơ', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (329, N'Thốt Nốt', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (330, N'Thới Lai', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (331, N'An Phú (Xóa)', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (332, N'Ô Môn', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (333, N'Cờ Đỏ', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (334, N'Ninh Kiều', 36, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (335, N'Tp.Bắc Ninh', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (336, N'Huyện Từ Sơn', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (337, N'Huyện Lương Tài', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (338, N'Huyện Thuận Thành', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (339, N'Huyện Tiên Du', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (340, N'Huyện Yên Phong', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (341, N'Huyện Quế Võ', 37, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (342, N'Ba Tri', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (343, N'Bình Đại', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (344, N'Thị xã Bến Tre', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (345, N'Giồng Trôm', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (346, N'Chợ Lách', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (347, N'Thạnh Phú', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (348, N'Mỏ Cày Nam', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (349, N'Mỏ Cày Bắc', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (350, N'Châu Thành', 38, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (351, N'Thiệu Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (352, N'Nông Cống', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (353, N'Thị xã Bỉm Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (354, N'Hà Trung', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (355, N'Triệu Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (356, N'Vĩnh Lộc', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (357, N'Yên Định', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (358, N'Tp Thanh Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (359, N'Thường Xuân', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (360, N'Đông Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (361, N'Quảng Xương', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (362, N'Quan Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (363, N'Hoằng Hóa', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (364, N'Thị xã Sầm Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (365, N'Thọ Xuân', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (366, N'Cẩm Thủy', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (367, N'Hậu Lộc', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (368, N'Ngọc Lặc', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (369, N'Lang Chánh', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (370, N'Như Xuân', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (371, N'Bá Thước', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (372, N'Như Thanh', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (373, N'Mường Lát', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (374, N'Nga Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (375, N'Quan Sơn', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (376, N'Tĩnh Gia', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (377, N'Thạch Thành', 39, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (378, N'Huyện Kiến Thụy', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (379, N'Huyện Tiên Lãng', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (380, N'Quận Hồng Bàng', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (381, N'Huyện Thủy Nguyên', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (382, N'Quận Đồ Sơn', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (383, N'Quận Lê Chân', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (384, N'Huyện Cát Hải', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (385, N'Huyện An Dương', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (386, N'Quận Ngô Quyền', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (387, N'Quận Dương Kinh', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (388, N'Huyện Bạch Long Vĩ', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (389, N'Quận Kiến An', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (390, N'Vĩnh Bảo', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (391, N'Quận Hải An', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (392, N'Huyện An Lão', 40, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (393, N'Cồn Cỏ', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (394, N'Cam Lộ', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (395, N'Gio Linh', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (396, N'Triệu Phong', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (397, N'Thị xã Quảng Trị', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (398, N'Hướng Hóa', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (399, N'Tp.Đồng Hà', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (400, N'Đa Krông', 41, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (401, N'Vĩnh Linh', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (402, N'Hải Lăng', 41, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (403, N'Tam Bình', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (404, N'Mang Thít', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (405, N'Huyện Vũng Liêm', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (406, N'Trà Ôn', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (407, N'Bình Minh', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (408, N'Cái Vồn (Xóa)', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (409, N'Cái Răng (Xóa)', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (410, N'Tp.Vĩnh Long', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (411, N'Huyện Long Hồ', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (412, N'Huyện Bình Tân', 42, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (413, N'Trà Cú', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (414, N'Huyện Tiểu Cần', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (415, N'Thị xã Duyện Hải', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (416, N'Càng Long', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (417, N'Duyên Hải', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (418, N'Thị xã Trà Vinh', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (419, N'Cầu Kè', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (420, N'Huyện Cầu Ngang', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (421, N'Châu Thành', 43, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (422, N'Thanh Liêm', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (423, N'Bình Lục', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (424, N'Kim Bảng', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (425, N'Duy Tiên', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (426, N'Huyện Lý Nhân', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (427, N'Tp.Phủ Lý', 46, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (428, N'Huyện Đồng Hỷ', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (429, N'Tp.Sông Công', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (430, N'Huyện Phú Bình', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (431, N'Huyện Võ Nhai', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (432, N'Huyện Đại Từ', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (433, N'Huyện Phú Lương', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (434, N'Huyện Phổ Yên', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (435, N'Tp.Thái Nguyên', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (436, N'Huyện Định Hóa', 47, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (437, N'Tp.Vị Thanh', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (438, N'Huyện Châu Thành', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (439, N'Thị xã Ngã Bảy', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (440, N'Huyện Vị Thủy', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (441, N'Thị xã Long Mỹ', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (442, N'Huyện Châu Thành A', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (443, N'Long Mỹ', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (444, N'Phụng Hiệp', 48, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (445, N'Vân Hồ', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (446, N'Sốp Cộp', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (447, N'Yên Châu', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (448, N'Mường La', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (449, N'Mai Sơn', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (450, N'Tp.Sơn La', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (451, N'Quỳnh Nhai', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (452, N'Thuận Châu', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (453, N'Sông Mã', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (454, N'Bắc Yên', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (455, N'Phù Yên', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (456, N'Mộc Châu', 49, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (457, N'Đam Rông', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (458, N'Lạc Dương', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (459, N'Đạ Tẻh', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (460, N'Huyện Bảo Lâm', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (461, N'Đơn Dương', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (462, N'Huyện Lâm Hà', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (463, N'Di Linh', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (464, N'Đức Trọng', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (465, N'Tp.Bảo Lộc', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (466, N'Tp.Đà Lạt', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (467, N'Huyện Đạ Huoai', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (468, N'Cát Tiên', 50, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (469, N'Thị xã Long Khánh', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (470, N'Huyện Nhơn Trạch', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (471, N'Xuân Lộc', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (472, N'Trảng Bom', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (473, N'Long Thành (Xóa)', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (474, N'Cẩm Mỹ', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (475, N'Tp.Biên Hòa', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (476, N'HUYỆN HỐ NAI (Xóa)', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (477, N'Định Quán', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (478, N'Long Thành', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (479, N'Huyện Vĩnh Cửu', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (480, N'Tân Phú', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (481, N'Thống Nhất', 51, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (482, N'Huyện Văn Lâm', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (483, N'Văn Giang', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (484, N'Tp.Hưng Yên', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (485, N'Khoái Châu', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (486, N'Mỹ Hào', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (487, N'Kim Động', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (488, N'Tiên Lữ', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (489, N'Yên Mỹ', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (490, N'Ân Thi', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (491, N'Huyện Phù Cừ', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (492, N'Phù Cừ (Xóa)', 52, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (493, N'Phúc Thọ', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (494, N'Hoàn Kiếm', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (495, N'Hà Đông', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (496, N'Thanh Oai', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (497, N'Phú Xuyên', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (498, N'Cầu Giấy', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (499, N'Huyện Thanh Trì', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (500, N'Huyện Gia Lâm', 53, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (501, N'Mê Linh', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (502, N'Thạch Thất', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (503, N'Huyện Sóc Sơn', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (504, N'Mỹ Đức', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (505, N'Quốc Oai', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (506, N'Sơn Tây', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (507, N'Long Biên', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (508, N'Huyện Đông Anh', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (509, N'Ứng Hòa', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (510, N'Hoài Đức', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (511, N'Hoàng Mai', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (512, N'Đan Phượng', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (513, N'Tây Hồ', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (514, N'Chương Mỹ', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (515, N'Hai Bà Trưng', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (516, N'Thanh Xuân', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (517, N'Đống Đa', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (518, N'Ba Đình', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (519, N'Thường Tín', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (520, N'Từ Liêm', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (521, N'Huyện Ba Vì', 53, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (522, N'Thị xã Bến Cát', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (523, N'Dĩ An', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (524, N'THỊ TRẤN LÁI THIÊU (Xóa)', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (525, N'Dầu Tiếng', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (526, N'Bắc Tân Uyên', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (527, N'Thuận An', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (528, N'DĨ AN (Xóa)', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (529, N'Thị xã Tân Uyên', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (530, N'Bàu Bàng', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (531, N'Huyện Dầu Tiếng (Xóa)', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (532, N'Thủ Dầu Một', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (533, N'Phú Giáo', 55, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (534, N'BMT', 56, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (535, N'Tân Hưng', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (536, N'Huyện Bến Lức', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (537, N'Đức Huệ', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (538, N'Cần Đước', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (539, N'Huyện Châu Thành', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (540, N'Tp.Tân An', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (541, N'Tân Trụ', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (542, N'Tân Thạnh', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (543, N'Thạnh Hóa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (544, N'Mộc Hóa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (545, N'Huyện Cần Giuộc', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (546, N'Huyện Đức Hòa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (547, N'Thủ Thừa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (548, N'Huyện Thủ Thừa', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (549, N'Vĩnh Hưng', 57, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (550, N'Điện Biên Đông', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (551, N'Tp.Điện Biên', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (552, N'Thị Xã Mường Lay', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (553, N'Tủa Chùa', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (554, N'Mường Chà', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (555, N'Điện Biên', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (556, N'Nậm Pồ', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (557, N'Mường Ảng', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (558, N'Mường Nhé', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (559, N'Tuần Giáo', 58, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (560, N'Kim Thành', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (561, N'Thanh Miện', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (562, N'Nam Sách', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (563, N'Thị xã Chí Linh', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (564, N'Tp.Hải Dương', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (565, N'Gia Lộc', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (566, N'Bình Giang', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (567, N'Kinh Môn', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (568, N'Cẩm Giàng', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (569, N'Tứ Kỳ', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (570, N'Ninh Giang', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (571, N'Thanh Hà', 59, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (572, N'SaPa', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (573, N'Văn Bàn', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (574, N'Bát Xát', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (575, N'Mường Khương', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (576, N'Tp.Lào Cai', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (577, N'Bảo Yên', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (578, N'Bảo Thắng', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (579, N'Si Ma Cai', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (580, N'Bắc Hà', 60, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (581, N'Thạch An', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (582, N'Trà Lĩnh', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (583, N'Bảo Lạc', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (584, N'Hà Quảng', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (585, N'Phục Hoà', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (586, N'Tp.Cao Bằng', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (587, N'Quảng Uyên', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (588, N'Nguyên Bình', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (589, N'Bảo Lâm', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (590, N'Hoà An', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (591, N'Thông Nông', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (592, N'Hạ Lang', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (593, N'Trùng Khánh', 61, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (594, N'Lộc Bình', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (595, N'Bình Gia', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (596, N'Cao Lộc', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (597, N'Hữu Lũng', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (598, N'Chi Lăng', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (599, N'Văn Lãng', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (600, N'Văn Quan', 62, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (601, N'Bắc Sơn', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (602, N'Tp.Lạng Sơn', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (603, N'Đình Lập', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (604, N'Tràng Định', 62, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (605, N'Thị xã Cai Lậy', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (606, N'Thị xã Gò Công', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (607, N'Huyện Tân Phú Đông', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (608, N'Gò Công Đông', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (609, N'Tp.Mỹ Tho', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (610, N'Châu Thành', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (611, N'Chợ Gạo', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (612, N'Cai Lậy', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (613, N'Cái Bè', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (614, N'Gò Công Tây', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (615, N'Huyện Tân Phước', 63, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (616, N'Ia Grai', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (617, N'Phú Thiện', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (618, N'Mang Yang', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (619, N'Kông Chro', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (620, N'Tp.Pleiku', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (621, N'Thị xã Ayun Pa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (622, N'KBang', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (623, N'Thị xã An Khê', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (624, N'Đăk Đoa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (625, N'Đăk Pơ', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (626, N'Chư Păh', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (627, N'Chư Pưh', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (628, N'Chư Prông', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (629, N'Ia Pa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (630, N'Đức Cơ', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (631, N'Chư Sê', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (632, N'Krông Pa', 64, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (633, N'Tp.Sa Đét', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (634, N'Hồng Ngự', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (635, N'Huyện Tam Nông', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (636, N'Cao Lãnh', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (637, N'Thị xã Hồng Ngự', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (638, N'Huyện Thanh Bình', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (639, N'Huyện Lấp Vò', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (640, N'Tp.Cao Lãnh', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (641, N'Huyện Lai Vung', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (642, N'Huyện Tân Hồng', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (643, N'Tháp Mười', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (644, N'Châu Thành', 65, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (645, N'Huyện Núi Thành', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (646, N'Bắc Trà My', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (647, N'Tp.Hội An', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (648, N'Hiệp Đức', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (649, N'Nông Sơn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (650, N'Nam Giang', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (651, N'Phú Ninh', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (652, N'Phước Sơn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (653, N'Tây Giang', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (654, N'Nam Trà My', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (655, N'Thăng Bình', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (656, N'Thị xã Điện Bàn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (657, N'Tp.Tam Kỳ', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (658, N'Quế Sơn', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (659, N'Đại Lộc', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (660, N'Đông Giang', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (661, N'Tiên Phước', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (662, N'Duy Xuyên', 66, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (663, N'Huyện Bạch Thông', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (664, N'Thị xã Bắc Cạn', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (665, N'Huyện Ngân Sơn', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (666, N'Huyện Ba Bể', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (667, N'Huyện Pác Nặm', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (668, N'Huyện Chợ Đồn', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (669, N'Huyện Chợ Mới', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (670, N'Huyện Na Rì', 68, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (671, N'Tp.Tuy Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (672, N'Phú Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (673, N'Sông Hinh', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (674, N'Đồng Xuân', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (675, N'Thị xã Sông Cầu', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (676, N'Sơn Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (677, N'Tây Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (678, N'Huyện Đông Hòa', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (679, N'Tuy An', 69, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (680, N'Quận Thanh Khê', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (681, N'Quận Liên Chiểu', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (682, N'Quận Hải Châu', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (683, N'Quận Cẩm Lệ', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (684, N'Huyện Hòa Vang', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (685, N'Huyện Hoàng Sa', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (686, N'Huyện Sơn Trà', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (687, N'Quận Ngũ Hành Sơn', 70, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (688, N'Tp.Đồng Hới', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (689, N'Quảng Ninh', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (690, N'Tuyên Hóa', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (691, N'Minh Hóa', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (692, N'Bố Trạch', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (693, N'Thị xã Ba Đồn', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (694, N'Quảng Trạch', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (695, N'Lệ Thủy', 72, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (696, N'Nghĩa Hành', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (697, N'Sơn Hà', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (698, N'Lý Sơn', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (699, N'Mộ Đức', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (700, N'Tư Nghĩa', 74, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (701, N'Bình Sơn', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (702, N'Sơn Tây', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (703, N'Sơn Tịnh', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (704, N'Minh Long', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (705, N'Tp.Quảng Ngãi', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (706, N'Tây Trà', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (707, N'Đức Phổ', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (708, N'Trà Bồng', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (709, N'Ba Tơ', 74, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (710, N'Tp.Hà Giang', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (711, N'Yên Minh', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (712, N'Quản Bạ', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (713, N'Bắc Mê', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (714, N'Huyện Mèo Vạc', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (715, N'Hoàng Su Phì', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (716, N'Vị Xuyên', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (717, N'Quang Bình', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (718, N'Bắc Quang', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (719, N'Huyện Đồng Văn', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (720, N'Xín Mần', 75, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (721, N'Huyện Phú Tân', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (722, N'Huyện Tịnh Biên', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (723, N'TP.Châu Đốc', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (724, N'Huyện Thoại Sơn', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (725, N'Huyện Tri Tôn', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (726, N'Huyện Tân Châu', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (727, N'Huyện An Phú', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (728, N'Huyện Châu Thành', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (729, N'Huyện Chợ Mới', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (730, N'Huyện Tân Châu (Xóa)', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (731, N'Huyện Châu Phú', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (732, N'TP.Long Xuyên', 76, N'', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_District] ([DistrictId], [DistrictName], [ProvinceId], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (733, N'Rạch Giá', 77, N'', 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Khách Sạn LaDaLat', N'1', 50, 466, N'11,10,7,9,4', 4, 1, N'4,7,5,15,6,10,9,18,11,16,3', N'106A  Mai Anh Đào, Phường 8, Đà Lạt, Lâm Đồng', N'54545', NULL, NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p><strong>Tại Ladalat</strong>, ch&uacute;ng t&ocirc;i ho&agrave;n to&agrave;n t&ocirc;n trọng v&agrave; bảo tồn c&aacute;c đặc điểm ri&ecirc;ng biệt đặc trưng của Đ&agrave; Lạt. H&agrave;nh lang rộng lớn đầy hoa anh đ&agrave;o &ndash; lo&agrave;i hoa được biết đến như biểu tượng của Đ&agrave; Lạt</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', N'fghdhd', NULL, N'06062021_104514yb_khach-san-5-sao-da-lat.jpg', 0, 1, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T09:04:16.427' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'd4258d99-6163-410b-a1c7-2e2fe6334a4d', N'KS3', N'1', 50, 466, NULL, 4, 1, N'34,29,14', N'200', N'200', N'200', NULL, NULL, N'200', N'200', N'200', NULL, 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T18:07:33.937' AS DateTime), NULL, NULL, NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'21985eda-87ba-4176-932e-3194d286b4b7', N'Khach san 1', N'1', 50, 466, N'1,11', 2, 1, N'4,29,8', N'119 le van khuong', N'14654321534', N'3574642314', NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p>cạnh đ&oacute;, tại khu lưu tr&uacute; n&agrave;y c&ograve;n mang đến rất nhiều tiện &iacute;ch, hứa hẹn sẽ mang đến cho du kh&aacute;ch những trải nghiệm th&uacute; vị nhất.</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', N'tranglth@saigoncenterreal.vn', N'asdfasdfasdf', N'06062021_104533yb_hotel002.jpg', NULL, 1, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-10T20:11:05.970' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'99a40b13-16e5-4a17-9f18-417566705967', N'Khách sạn A', N'1', 50, 466, N'7', 3, 1, N'4,5,6', N'20 SR', NULL, NULL, NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p>nguye thadf&aacute;df</p>
</body>
</html>
', NULL, NULL, N'06062021_104544yb_hotel001.jpg', 1, 1, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-06T10:45:44.317' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'77c99374-bd74-4378-8011-4361c2df7db5', N'KS 100', N'1', 50, 466, NULL, 3, 1, N'34,29,33', N'100', N'100', N'100', NULL, NULL, N'100', N'100', N'100', NULL, 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-18T23:52:18.853' AS DateTime), NULL, NULL, NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'9e2748c4-2909-4286-a0dc-448972927bef', N'Khách sạn Kings - Copy', N'1', 50, 465, N'1,11,9,2', 4, 1, N'34,4,33,14,8,26', N'10, Bùi Thị Xuân, Thành Phố Đà Lạt, Lâm Đồng, Việt Nam', N'0932980033', NULL, NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p>Nằm ngay tại trung t&acirc;m th&agrave;nh phố Đ&agrave; Lạt,&nbsp;<strong>kh&aacute;ch sạn Kings</strong>&nbsp;ch&iacute;nh l&agrave; điểm nghỉ ch&acirc;n được nhiều du kh&aacute;ch lựa chọn mỗi khi đến du lịch tại đ&acirc;y. Đến đ&acirc;y bạn c&oacute; thể dễ d&agrave;ng rong chơi</p>
</body>
</html>
', N'nguyenthanhdat.bc2310@gmail.com', N'https://mytour.vn/khach-san/23226-khach-san-kings.html?checkIn=11-06-2021&checkOut=13-06-2021&adults=2&rooms=1&children=0&priceKey=%2Bkhj4MYg%2F5qM8fVl2P4wdy%2BwNGdLK6%2Ff6mv1RCnej7QbvaAf7Y9zFCCe7Kmr24U1', N'10062021_081239yh_hotel003.jpg', 1, 1, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-10T20:12:39.943' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'535a5b23-6a2f-4d8e-8260-79152ff81f11', N'Khach san 2', N'1', 50, 466, N'', 4, 1, N'34,33,8,28', N'23asdf', N'23452345', N'23452345', NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p>cạnh đ&oacute;, tại khu lưu tr&uacute; n&agrave;y c&ograve;n mang đến rất nhiều tiện &iacute;ch, hứa hẹn sẽ mang đến cho du kh&aacute;ch những trải nghiệm th&uacute; vị nhất.</p>
</body>
</html>
', N'sdgsdfgsdfg', N'sdfgsdfgsdfg', NULL, 1, 1, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-10T20:11:24.397' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', N'Khách sạn Novotel', N'1', 50, 466, N'1,11,9,2', 4, 1, N'21,34,4,29,33,14,8,26,28,36', N'10, Bùi Thị Xuân, Thành Phố Đà Lạt, Lâm Đồng, Việt Nam', N'0932980033', N'0932980033', NULL, NULL, N'<p>Nằm ngay tại trung t&acirc;m th&agrave;nh phố Đ&agrave; Lạt,&nbsp;<strong>kh&aacute;ch sạn Kings</strong>&nbsp;ch&iacute;nh l&agrave; điểm nghỉ ch&acirc;n được nhiều du kh&aacute;ch lựa chọn mỗi khi đến du lịch tại đ&acirc;y. Đến đ&acirc;y bạn c&oacute; thể dễ d&agrave;ng rong chơi</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link -->', N'nguyenthanhdat.bc2310@gmail.com', N'http://hotcombo.vn', N'21072021_064937yb_img_02072021acfcd008-4dec-4d2f-adb0-7298f16bda3d.jpg', 1, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:52:55.513' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'48c71873-dfb8-4dfd-9be2-8500bc481cfb', N'ks5', N'1', 50, 466, NULL, 4, 1, N'21,4', N'500', N'500', N'500', NULL, NULL, N'sdfádf', N'vp174', N'asdfasdfasdf', NULL, 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T18:28:21.363' AS DateTime), NULL, NULL, NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'64477619-22da-45d0-9d24-8c63124e0d82', N'Khách sạn Holiday', N'1', 50, 466, N'', 1, 1, N'29,14,28', N'H59-h60, Nguyễn Thị Nghĩa, Thành Phố Đà Lạt, Lâm Đồng, Việt Nam', N'0932980033', NULL, NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p>Tọa lạc ở th&agrave;nh phố Đ&agrave; Lạt, c&aacute;ch Vườn hoa Đ&agrave; Lạt 2,3 km v&agrave; Quảng trường L&acirc;m Vi&ecirc;n 2,7 km, Dalat Holiday Hotel cung cấp chỗ nghỉ với sảnh kh&aacute;ch chung v&agrave; WiFi miễn ph&iacute; trong to&agrave;n bộ khu&ocirc;n vi&ecirc;n cũng như chỗ đỗ xe ri&ecirc;ng miễn ph&iacute; cho kh&aacute;ch l&aacute;i xe. Chỗ nghỉ n&agrave;y nằm trong b&aacute;n k&iacute;nh khoảng 2,9 km từ Hồ Xu&acirc;n Hương, 3 km từ C&ocirc;ng vi&ecirc;n Yersin v&agrave; 7 km từ Thiền viện Tr&uacute;c L&acirc;m. Chỗ nghỉ c&oacute; dịch vụ ph&ograve;ng, b&agrave;n đặt tour v&agrave; dịch vụ giữ h&agrave;nh l&yacute; cho kh&aacute;ch.</p>

<p>Tất cả ph&ograve;ng nghỉ tại kh&aacute;ch sạn đều được trang bị TV truyền h&igrave;nh c&aacute;p m&agrave;n h&igrave;nh phẳng, ấm đun nước, v&ograve;i sen, đồ vệ sinh c&aacute; nh&acirc;n miễn ph&iacute; v&agrave; b&agrave;n l&agrave;m việc. Mỗi ph&ograve;ng c&ograve;n c&oacute; tủ để quần &aacute;o v&agrave; ph&ograve;ng tắm ri&ecirc;ng.</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', N'tranglth@saigoncenterreal.vn', NULL, N'02072021_072434yh_1781388_17041317180052375369.jpg', 1, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:24:34.183' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'Khách sạn Kings', N'1', 50, 465, N'1,11,9,2', 4, 1, N'21,34,4,29,33,14,8,26,28,36', N'10, Bùi Thị Xuân, Thành Phố Đà Lạt, Lâm Đồng, Việt Nam', N'0932980033', N'0932980033', NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body>
<p>Nằm ngay tại trung t&acirc;m th&agrave;nh phố Đ&agrave; Lạt,&nbsp;<strong>kh&aacute;ch sạn Kings</strong>&nbsp;ch&iacute;nh l&agrave; điểm nghỉ ch&acirc;n được nhiều du kh&aacute;ch lựa chọn mỗi khi đến du lịch tại đ&acirc;y. Đến đ&acirc;y bạn c&oacute; thể dễ d&agrave;ng rong chơi</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', N'nguyenthanhdat.bc2310@gmail.com', N'http://hotcombo.vn', N'02072021_072454yh_hotel003.jpg', 1, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:24:54.087' AS DateTime), NULL, NULL, 48)
INSERT [dbo].[tbl_Hotel] ([Id], [HotelName], [CountryId], [ProvinceId], [DistrictId], [AreaId], [HotelRatingTypeId], [HotelTypeId], [HotelConvenientType], [Address], [Telephone], [Mobile], [Longitude], [Latitude], [Content], [Email], [Url], [Image], [IsActive], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [PriceFrom], [PricePromotion], [CancelTime]) VALUES (N'4e073663-cabe-443c-b3be-ccbc6871a544', N'Khách sạn Camelia', N'1', 50, 465, N'1,11,9,2', 4, 1, N'21,34,4,29,33,14,8,26,28,36', N'10, Bùi Thị Xuân, Thành Phố Đà Lạt, Lâm Đồng, Việt Nam', N'0932980033', N'0932980033', NULL, NULL, N'<html>
<head>
	<title></title>
</head>
<body data-gr-ext-installed="" data-new-gr-c-s-check-loaded="14.1018.0">
<p>Nằm ngay tại trung t&acirc;m th&agrave;nh phố Đ&agrave; Lạt,&nbsp;<strong>kh&aacute;ch sạn Kings</strong>&nbsp;ch&iacute;nh l&agrave; điểm nghỉ ch&acirc;n được nhiều du kh&aacute;ch lựa chọn mỗi khi đến du lịch tại đ&acirc;y. Đến đ&acirc;y bạn c&oacute; thể dễ d&agrave;ng rong chơi</p>
<!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --><!-- Visual Studio Browser Link --><!-- End Browser Link --></body>
</html>
', N'nguyenthanhdat.bc2310@gmail.com', N'http://hotcombo.vn', N'03072021_103520yb_h4.jpg', 0, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:35:20.640' AS DateTime), NULL, NULL, 48)
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] ON 

INSERT [dbo].[tbl_HotelCancellationPolicy] ([Id], [NumberDateCancel], [HotelId], [PercentAmount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 7, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'60        ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelCancellationPolicy] ([Id], [NumberDateCancel], [HotelId], [PercentAmount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, 5, N'99a40b13-16e5-4a17-9f18-417566705967', N'30        ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelCancellationPolicy] OFF
GO
INSERT [dbo].[tbl_HotelComment] ([Id], [HotelId], [CustomerId], [Content], [DateTimeComment], [PointLocation], [PointServe], [PointConvenient], [PointCost], [PointClean], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'aba1f7d6-c820-40e1-8d56-0488d292c2be', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'de1a71fb-2abf-4467-b704-2a264a793f7e', N'Khách sạn đẹp. Nhân viên nhiệt tình, dễ thương. Tuy nhiên thời gian thuê xe bất hợp lý. Nên mong khách sạn xem lại thời gian cho thuê xe.', CAST(N'2021-06-14T09:04:16.427' AS DateTime), 8, 8.4, 9.4, 10, 8, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelComment] ([Id], [HotelId], [CustomerId], [Content], [DateTimeComment], [PointLocation], [PointServe], [PointConvenient], [PointCost], [PointClean], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a703faaa-80b1-4d5b-be08-2b4b8a339ff0', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'2f0d2eb6-f2ef-4e5f-a374-8a359364496d', N'Các món ăn ở khách sạn đầy đủ nhưng về cách thức chuẩn bị và mùi vị thì bình thường. Nhân viên nhiệt tình và thân thiện. Vị trí thuận tiện, gần chợ Đà Lạt, hồ trung tâm, dinh Bảo Đại (chỉ khoảng 2km đi bộ)... Phòng ốc được dọn sạch mỗi ngày.', CAST(N'2021-06-15T09:04:16.427' AS DateTime), 7, 7, 6, 5, 7, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelComment] ([Id], [HotelId], [CustomerId], [Content], [DateTimeComment], [PointLocation], [PointServe], [PointConvenient], [PointCost], [PointClean], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f5246d2a-3f52-43fa-90b3-55b028a52adb', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'7817b280-38b1-47fd-a7b1-9e2930a0f9a9', N'Phòng ở sạch sẽ, nội thất khang trang, phù hợp giá thành. Cộng với cung cách phục vụ khách hàng nhiệt tình, thái độ khi giao tiếp với khách lịch sự.', CAST(N'2021-06-20T09:04:16.427' AS DateTime), 9, 9, 10, 10, 9, 2, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelComment] ([Id], [HotelId], [CustomerId], [Content], [DateTimeComment], [PointLocation], [PointServe], [PointConvenient], [PointCost], [PointClean], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1de4d4e8-dc37-42c8-9753-a5fc7da98213', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'bb785d92-8e61-494d-bffc-49b0312a8b39', N'Phòng ốc cung cấp nhanh chóng và sạch sẽ tiện nghi. Phòng nhìn hơi nhỏ 1 chút còn lại mọi thứ phục vụ vị trí đều cảm thấy ok.', CAST(N'2021-06-14T09:04:16.427' AS DateTime), 9, 9, 9, 8, 7, 3, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelComment] ([Id], [HotelId], [CustomerId], [Content], [DateTimeComment], [PointLocation], [PointServe], [PointConvenient], [PointCost], [PointClean], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'293c5d9a-91d6-4cfb-88dd-cad2c88ee2b9', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'4cb127b0-1efd-43cc-b62a-31622f70693a', N'Vị trí của khách sạn rất thuận tiện. Giá cả ổn, lễ tân nhiệt tình giúp đỡ. Tuy nhiên, phòng cách âm không tốt. Phòng không có khăn tắm. Bữa sáng bình thường.', CAST(N'2021-06-14T09:04:16.427' AS DateTime), 8, 9, 8, 9, 9, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelComment] ([Id], [HotelId], [CustomerId], [Content], [DateTimeComment], [PointLocation], [PointServe], [PointConvenient], [PointCost], [PointClean], [Type], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1066dba3-0056-461c-93f8-f7d24d157b1c', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'0c5d1d8b-9aac-4fcb-b711-76feb47c6df2', N'Văn hóa Việt Nam tôn trọng tuổi tác. Nhân viên nên lựa chọn danh xưng phù hợp với người cao tuổi thay vì gọi là anh hoặc chị. Khách sạn nên đào tạo cho nhân viên xưng hô với khách cho phù hợp với tuổi tác.', CAST(N'2021-06-15T09:04:16.427' AS DateTime), 8, 8, 9, 8, 9, 4, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] ON 

INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Hồ bơi 1', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Massage/Spa', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 2, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Wifi miễn phí', N'img_2906202172423fce-4335-46a9-babb-1a4fbc88a048.png', 0, 3, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-29T23:14:34.100' AS DateTime))
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Bãi đỗ xe', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Giặt là', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 10, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Lễ tân 24/24', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 13, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'Đưa đón sân bay', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 5, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'Cho thuê máy bay', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 6, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'Phòng gym', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 7, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'Nhà hàng', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 8, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'Phục vụ đồ ăn tại phòng', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 9, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'Chấp nhận thú cưng', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 11, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'Hỗ trợ đặt tour', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 12, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'Thang máy', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 14, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'Máy ATM trong khách sạn', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 15, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'Phòng họp', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 16, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'Tổ chức sự kiện', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 17, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'Vòi hoa sen', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N' Mấy sấy tóc', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'Quạt', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'Truyền hình vệ tinh/cáp', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'Két sắt trong phòng', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'Nước đóng chai miễn phí', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'Dép đi trong phòng', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'Sàn trải thảm', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'Dịch vụ báo thức', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'Buồng tắm đứng', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'Đồ vệ sinh cá nhân miễn phí', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'Quầy bar nhỏ (MiniBar)', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'Cà phê/Trà', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N'Ấm đun nước điện', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'Tivi màn hình phẳng', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'Điện thoại', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 1, 12, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelConvenientType] ([Id], [ConvenientName], [Symbol], [Type], [Order], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (41, N'1111111', N'img_290620219e87be0e-a2f8-4f06-bbaf-0bcb9ca1d72f.png', 0, 1, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelConvenientType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] ON 

INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Các mức phí trên chưa bao gồm thuế, phí g', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Giường phụ không áp dụng cho loại phòng Basement Standard Triple No Window', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'dấdfádfsadfádf', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Trẻ từ 12 tuổi trở lên được tính như người lớn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Vui lòng nhập các yêu cầu đặc biệt vào mục Yêu cầu khác bên dưới Thông tin liên hệ để được hỗ trợ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'sdfádfádfsđàádfádf', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'aaaaaaaaaaaaa', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'gggggg', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:42:41.603' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'hhhhhhh', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:44:45.910' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'lllllllllasdasd', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'vvvvvv', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:47:24.593' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'mmmmmmm', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:48:04.747' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'kkkkkk', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:51:06.397' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'21985eda-87ba-4176-932e-3194d286b4b7', N'zxczxc', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:56:04.520' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'21985eda-87ba-4176-932e-3194d286b4b7', N'rtêrẻ', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:56:19.307' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'21985eda-87ba-4176-932e-3194d286b4b7', N'tửetwgfdsg', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:56:36.810' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'21985eda-87ba-4176-932e-3194d286b4b7', N'gfdgsdfg', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:57:15.427' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'21985eda-87ba-4176-932e-3194d286b4b7', N'jkghkghkghk', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T10:58:56.517' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'21985eda-87ba-4176-932e-3194d286b4b7', N'vvvvv', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T11:27:04.320' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'21985eda-87ba-4176-932e-3194d286b4b7', N'bbbbbb', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T11:29:09.953' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N'21985eda-87ba-4176-932e-3194d286b4b7', N'nnnnn', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T11:30:29.653' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'21985eda-87ba-4176-932e-3194d286b4b7', N'rrrrr', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T11:32:38.153' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'21985eda-87ba-4176-932e-3194d286b4b7', N'uuuuu', 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T11:41:59.473' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-19T21:47:36.920' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:17:57.460' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'adfsdfsdfsdf', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:21:32.017' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asasdasdasdasd', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:36:24.200' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'adfafsdgsdfdf', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:38:45.890' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdasdasd', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:41:00.937' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdasds', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:41:45.710' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (31, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdasds', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:42:08.617' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdasdsasdasds', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:42:14.123' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'khanh pham', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:45:38.160' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdasdasd', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:50:18.137' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Khánh Phạm', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:50:30.287' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:55:08.110' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (37, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T16:55:36.160' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (38, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Phạm Quang Khánh', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T21:32:12.180' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (39, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T21:33:05.763' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (40, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Khanhpq', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T22:48:02.143' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (41, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T23:44:25.123' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (42, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-20T23:52:46.157' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (43, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'asdas sdfsdfds', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (44, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'sdasdasdvvv ', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (45, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'sdsadasdsadas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-21T17:33:49.327' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (46, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'sdsadasdsadas asdas', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (47, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Glenn Quagmire', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (48, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'sfadfgg', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (49, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'aaaaaaaaa', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-22T00:04:19.263' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (50, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'hhhhhhhhhhh', 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-22T02:09:04.480' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (51, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'nguyen', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (52, N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'vannn22', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelNotice] ([Id], [HotelId], [Content], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (53, N'64477619-22da-45d0-9d24-8c63124e0d82', N'asdas', 1, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-29T17:49:46.207' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelNotice] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] ON 

INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Ưu đãi đặc biệt 1', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Đặt sớm giá tốt', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Giá Shock giờ chót', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelPriceType] ([Id], [PriceTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Gia thuong', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelPriceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] ON 

INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, 1, N'Khách sạn 1 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, 2, N'Khách sạn 2 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, 3, N'Khách sạn 3 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, 4, N'Khách sạn 4 sao', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelRatingType] ([Id], [NumberStar], [Name], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, 5, N'Khách sạn 5 sao', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelRatingType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] ON 

INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Bữa sáng miễn phí', NULL, 0, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'3 Bữa ăn miễn phí', NULL, 0, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Thêm giường phụ', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Miễn phí hủy phòng', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelServiceType] ([Id], [ServiceName], [Symbol], [IsDelete], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Ăn trưa', NULL, 0, 3, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelServiceType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] ON 

INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Từ 0 đến 6 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 1000000, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Từ 6 đến 11 tuổi', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 1520001, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Người lớn 1', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 523000, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'dADá', N'99a40b13-16e5-4a17-9f18-417566705967', 34243, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'fádfádf', N'21985eda-87ba-4176-932e-3194d286b4b7', 4234, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T11:57:21.787' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'erêrẻ', N'21985eda-87ba-4176-932e-3194d286b4b7', 234234, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T11:57:34.810' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'6 8', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 43333, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-22T17:39:42.197' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'aaaaâ', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 222222, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-23T17:34:25.467' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'bbbbbbbb', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 333333333, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-23T17:35:21.587' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_HotelSurchargePolicy] ([Id], [Condition], [HotelId], [Amount], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'vvvvvvvv33', N'9590a723-ab0c-41a8-a992-036fd35e97b9', 444444555666, 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelSurchargePolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_HotelType] ON 

INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'Khách sạn', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'Resort', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'Homestay', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'Căn hộ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'Biệt thự', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_HotelType] ([Id], [HotelTypeName], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (6, N'Du thuyền', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_HotelType] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] ON 

INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'TTD', N'Thẻ tính dụng', 3, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'TND', N'Thẻ nội địa', 4, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'CK', N'Chuyển khoản', 2, 1, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_PaymentType] ([Id], [PayTypeCode], [PayTypeName], [Order], [Status], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'TM', N'Tiền mặt', 1, 1, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_PaymentType] OFF
GO
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'a0bef568-6251-446f-bf0f-0ef62d32f76a', N'0bc75359-d678-49ba-b033-a1209ad10e5e', NULL, N'img_170620218486bf91-3272-44be-ba3d-4672f8d3039a.jpeg', 2, 3, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:02.513' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:02.513' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'6b137a2b-e56a-412b-aeef-1212c7d46b95', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'img_04062021caa1a689-811d-4f11-8345-fe43b62cb8fd.png', 4, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:37:04.217' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:37:04.217' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'0ae7c4df-30cd-4ff4-b426-15f3b9d828da', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, N'img_02062021a215a1ce-10fa-42b3-ad48-e6a052c77d6c.jpg', 2, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:55:31.207' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:55:31.207' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'75ab53c7-d0d1-48b9-9542-208b0062aae2', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'img_290620211d567281-39d0-45bc-9c00-10254707732e.jpg', 4, 3, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-29T01:18:56.810' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-29T01:18:56.810' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'c26f764d-2781-4a8e-8fd6-23905a05788a', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'img_170620213a8c6531-be65-4a6e-82c6-dd17fdd53904.jpeg', 4, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:39:11.170' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:39:11.170' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'81b38173-5b89-46bb-a29a-2673de5d2def', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'img_170620210e287494-6752-4610-a0c8-f08b9894c4f4.jpeg', 4, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:51.957' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:51.957' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'08cf21be-be9f-468d-a72a-2f087633a4dd', N'64477619-22da-45d0-9d24-8c63124e0d82', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'img_280620212ec8ff22-3718-467a-be06-b893e2643419.jpg', 4, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-28T16:24:26.113' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:18:51.907' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'd596b385-5c6a-4c83-8e68-2f28b098b163', N'0bc75359-d678-49ba-b033-a1209ad10e5e', NULL, N'img_1706202136e34b8d-c182-4e6d-9cb1-b795d1ce4544.jpeg', 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:37:13.173' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:37:13.173' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'2657e37b-3f7e-42a4-9168-2f903d0cabba', N'64477619-22da-45d0-9d24-8c63124e0d82', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'img_0207202143f77af1-cb09-451a-89f6-e8c46d7dbb1f.jpg', 4, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:20:31.813' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:20:31.813' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'6e665f97-3ead-45e6-afa6-30de21e34331', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'img_17062021744897b5-db2c-415e-83a0-d8d81ae2c9aa.jpeg', 4, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:39:27.753' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:39:27.753' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'f73384c1-a5c2-4529-9f03-3fe5eae08c21', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, N'img_020620211303cbf2-33cc-4dc0-b0d0-2316f9a094cc.jpg', 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:56:45.413' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:57:12.263' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'b8f93976-f699-44cc-b648-45ca8b22bd33', N'0bc75359-d678-49ba-b033-a1209ad10e5e', NULL, N'img_1706202129e40ee0-9462-41c5-9bb0-3a25b547a804.jpeg', 2, 4, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:08.663' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:08.663' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'953daabe-c832-468d-a4de-497d37f971f4', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'img_2306202189546ed2-4985-4d56-acca-90d564c88c4f.PNG', 4, 2, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-23T18:06:29.680' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-23T18:06:29.680' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'3cd805d6-ddbd-43af-a201-4c5eb2bbf7c3', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'img_02062021665d2508-519a-4c4f-aca8-528775c09a2d.jpg', 4, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T13:58:10.577' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T23:45:19.643' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'e3165b1e-f4df-4e2b-a699-4d498e914c63', N'0bc75359-d678-49ba-b033-a1209ad10e5e', NULL, N'img_170620211b5e78d6-8d85-4909-92f9-656e3c3d506c.jpeg', 2, 5, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:16.817' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:38:16.817' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'fca84b1c-4a65-40bf-984a-54f20d71661e', N'64477619-22da-45d0-9d24-8c63124e0d82', NULL, N'img_020720212c758374-bb62-4229-8b25-5acfca1ddd1f.jpg', 2, 2, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:14.747' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:14.747' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'4a7ea6d4-c011-4352-91a0-577bcc97732d', N'21985eda-87ba-4176-932e-3194d286b4b7', NULL, N'img_04062021d655deb3-3a68-4297-9eb0-3a1db3539b30.png', 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:42:41.850' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:42:41.850' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'c94d2110-5a14-413a-9165-7261a6a91b05', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'img_020620210fb7e5dd-811e-498f-928e-8634eabae561.jpg', 4, 2, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T13:58:35.720' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T13:58:45.817' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'01163f5f-16e6-4679-a4a4-7a29f6be885e', N'21985eda-87ba-4176-932e-3194d286b4b7', NULL, N'img_0406202169e5e6a1-36e7-4bc2-a56e-d898e80e34ce.png', 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:35:42.013' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:36:24.280' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'b42f2320-4db5-4f83-8b8e-7dc7347e05dc', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'img_02062021d6ee83d6-eaba-45be-8b01-3beb45c2e6cd.jpg', 4, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T23:45:48.377' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T00:02:08.330' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'de099648-083c-42fc-9a85-8c9545ed3d66', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, N'img_02062021b7bff9a9-933b-4c05-bbb3-b073fb1a5f9e.jpg', 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:55:15.847' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:55:15.847' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'84737c06-29a6-49e5-ac7c-9eb15cd8ed56', N'21985eda-87ba-4176-932e-3194d286b4b7', NULL, N'img_04062021f8f22c74-6006-487c-8236-c1264683534f.png', 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:35:46.627' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:35:49.813' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'0decd8ee-0dd0-43bc-a5c1-a1fc0eefb03a', N'0bc75359-d678-49ba-b033-a1209ad10e5e', NULL, N'img_170620215ffe41f0-ea93-4742-ba04-7f7a3708e45d.jpeg', 2, 2, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:37:54.187' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:37:54.187' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'8f3ef067-557f-4982-9519-a20ca091c46d', N'64477619-22da-45d0-9d24-8c63124e0d82', NULL, N'img_02072021acfcd008-4dec-4d2f-adb0-7298f16bda3d.jpg', 2, 3, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:23.953' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:23.953' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'0403c5e1-6faf-4586-a7c8-a72ab453c7a5', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'img_2906202167060e0b-7d8d-4539-a27e-cab9c856659b.jpg', 4, 2, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-29T01:18:46.127' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-29T01:18:46.127' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'750fe319-2a4a-4583-bfdf-a78978b5c4cd', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'img_270620217cc75cd5-2a00-4fe5-8bd8-ee5c7dac5546.jpg', 4, 2, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-27T22:52:11.010' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-27T22:52:11.010' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'ea4b7b2a-1083-4bed-b5cf-ab43411bb7d7', N'64477619-22da-45d0-9d24-8c63124e0d82', NULL, N'img_020720218bc39392-b4c3-40a6-91b0-09909257ee7e.jpg', 1, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:01.167' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:01.167' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'358212ca-2689-41e4-99d4-b512b8c94d26', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', NULL, N'img_210720212e6d5e4a-e77f-4b63-9542-361e6cfd64dd.jpg', 1, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:50:26.760' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:50:26.760' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'e698e456-670f-46d5-a1b2-c6efbb416bbb', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, N'img_02062021beabf54f-277a-4eea-85b2-c1b01d682deb.jpg', 1, 3, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T23:45:28.863' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T23:45:28.863' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'd7511b3c-8d4c-4042-81d9-dda60c4c2d11', N'21985eda-87ba-4176-932e-3194d286b4b7', NULL, N'img_04062021c3ae343e-1249-4266-9ed3-9af536eeb27c.png', 2, 2, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:36:18.330' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:36:21.990' AS DateTime), 1)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'03bb86b2-49f3-431e-aebc-e38f8b2d46f8', N'64477619-22da-45d0-9d24-8c63124e0d82', NULL, N'img_02072021d3988098-68b6-467a-87c5-17f7e739a456.jpg', 2, 5, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:40.490' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:40.490' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'e63b6f0c-b64a-435e-a668-e96ba690317f', N'0bc75359-d678-49ba-b033-a1209ad10e5e', NULL, N'img_17062021ef294390-7c42-486e-94fd-6fe1157d7c2c.jpeg', 2, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:37:47.937' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T01:37:47.937' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'd0eb213b-be5c-484d-95ba-f6ce1600cc37', N'9590a723-ab0c-41a8-a992-036fd35e97b9', NULL, N'img_02062021f327052c-7b8f-4cd4-93a1-16c3450781db.jpg', 1, 2, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:55:40.840' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T02:55:40.840' AS DateTime), NULL)
INSERT [dbo].[tbl_ProductImage] ([Id], [HotelId], [RoomId], [ImageUrl], [Type], [Order], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate], [IsDelete]) VALUES (N'26970112-6aef-4e04-8e87-f7d22cd73b3c', N'64477619-22da-45d0-9d24-8c63124e0d82', NULL, N'img_0207202135cbfa3a-5af1-4c23-b471-fedfd482b508.jpg', 2, 4, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:32.080' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:19:32.080' AS DateTime), NULL)
GO
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (1, N'1', N'Bình Định', N'BDI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (2, N'1', N'Lai Châu', N'LCH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (3, N'1', N'Khánh Hòa', N'NHA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (4, N'1', N'Bình Định', N'BDI       ', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (5, N'1', N'Kiên Giang', N'KGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (7, N'1', N'Nha Trang', NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (8, N'1', N'Tây Ninh', N'TNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (9, N'1', N'Bà Rịa - Vũng Tàu', N'BRV       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (10, N'1', N'Hà Tây', N'HTA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (11, N'1', N'Hà Tĩnh', N'HTI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (12, N'1', N'Bình Phước', N'BPU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (13, N'1', N'Sóc Trăng', N'STR       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (14, N'1', N'Huế', N'HUE       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (15, N'1', N'Thái Bình', N'TBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (16, N'1', N'Yên Bái', N'YBA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (17, N'1', N'Kon Tum', N'KTU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (18, N'1', N'Quảng Ninh', N'QNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (19, N'1', N'Tp. HCM', N'SGN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (20, N'1', N'Cà Mau', N'CMA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (21, N'1', N'Nghệ An', N'NAN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (22, N'1', N'Bắc Giang', N'BGA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (23, N'1', N'Bạc Liêu', N'BLI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (24, N'1', N'Hạ Long', N'QNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (25, N'1', N'Ninh Bình', N'NBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (26, N'1', N'Phú Thọ', N'PTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (27, N'1', N'Vĩnh Phúc', N'VPH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (28, N'1', N'Hoà Bình', N'HBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (29, N'1', N'Đắk Lắk', N'DLA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (30, N'1', N'Ninh Thuận', N'NTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (31, N'1', N'Bình Thuận', N'BTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (32, N'1', N'Phan Thiết', N'BTH       ', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (33, N'1', N'Nam Định', N'NDI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (34, N'1', N'Đắk Nông', N'DKN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (35, N'1', N'Tuyên Quang', N'TQU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (36, N'1', N'Cần Thơ', N'CTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (37, N'1', N'Bắc Ninh', N'BNI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (38, N'1', N'Bến Tre', N'BTR       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (39, N'1', N'Thanh Hoá', N'THO       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (40, N'1', N'Hải Phòng', N'HPH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (41, N'1', N'Quảng Trị', N'QTR       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (42, N'1', N'Vĩnh Long', N'VLO       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (43, N'1', N'Trà Vinh', N'TVI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (44, N'1', N'Côn Đảo', NULL, 0, N'4a2ee6fc-e1e9-4643-bb36-025425223b41', NULL, N'4a2ee6fc-e1e9-4643-bb36-025425223b41', NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (45, N'1', N'Trường Sa', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (46, N'1', N'Hà Nam', N'HNA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (47, N'1', N'Thái Nguyên', N'TNG       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (48, N'1', N'Hậu Giang', N'HGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (49, N'1', N'Sơn La', N'SLA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (50, N'1', N'Lâm Đồng', N'DAT       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (51, N'1', N'Đồng Nai', N'DNA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (52, N'1', N'Hưng Yên', N'HYE       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (53, N'1', N'Hà Nội', N'HAN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (54, N'1', N'Trên tàu hỏa', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (55, N'1', N'Bình Dương', N'BDU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (56, N'1', N'Đak Lak', N'DAK       ', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (57, N'1', N'Long An', N'LAN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (58, N'1', N'Điện Biên', N'DBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (59, N'1', N'Hải Dương', N'HDU       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (60, N'1', N'Lào Cai', N'LCA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (61, N'1', N'Cao Bằng', N'CBA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (62, N'1', N'Lạng Sơn', N'LSO       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (63, N'1', N'Tiền Giang', N'TGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (64, N'1', N'Gia Lai', N'GLA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (65, N'1', N'Đồng Tháp', N'DTH       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (66, N'1', N'Quảng Nam', N'QNA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (67, N'1', N'Hà Bắc', N'HBC       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (68, N'1', N'Bắc Cạn', N'BCA       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (69, N'1', N'Phú Yên', N'PYE       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (70, N'1', N'Đà Nẵng', N'DNG       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (71, N'1', N'Hà Tiên', N'KGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (72, N'1', N'Quảng Bình', N'QBI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (73, N'1', N'Sapa', NULL, 0, N'7990014b-94be-4b4b-939b-fc1057a1047c', NULL, N'7990014b-94be-4b4b-939b-fc1057a1047c', NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (74, N'1', N'Quảng Ngãi', N'QNG       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (75, N'1', N'Hà Giang', N'HGI       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (76, N'1', N'An Giang', N'AGN       ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_Province] ([ProvinceId], [CountryId], [ProvinceName], [Symbol], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (77, N'1', N'Rạch Giá', N'KGI       ', 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tbl_Role] ON 

INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (1, N'Quản trị')
INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (2, N'Nhân viên')
INSERT [dbo].[tbl_Role] ([RoleId], [Name]) VALUES (3, N'Nhập liệu')
SET IDENTITY_INSERT [dbo].[tbl_Role] OFF
GO
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'Superior Double (Máy Lạnh)', 10, 25, N'1', NULL, 1, 2, N'21,34,29,33,26,28,36,30,24,25,22,32,27,35,23', N'17062021_111614yh_kingstype2.jpeg', NULL, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-27T15:15:27.923' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5599da0c-e1f6-4bde-877b-163b566960e5', N'21985eda-87ba-4176-932e-3194d286b4b7', N'phong4', 4, 23, N'3', 1, 3, 3, N'', NULL, NULL, 1, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-21T02:11:26.463' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', N'phong thu 3', 2, 23, N'1', 1, 2, 3, N'29,33', NULL, NULL, 1, NULL, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-21T02:06:37.987' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'Superior Twin (Máy lạnh)', 10, 27, N'2', 1, 1, 2, N'34,28,24,27,35,23', N'17062021_111537yh_kingtype3-1.jpeg', NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-17T23:15:37.667' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8a5835f8-1b8d-4ff9-8613-2c89c8421ceb', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'Phòng nhóm', 4, 50, N'2,6', 1, 2, 3, N'34,26,36', NULL, NULL, NULL, NULL, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-19T00:46:06.553' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'039a3642-7e52-4435-ab3e-52e89aabc909', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Phòng Superior', 0, 45, N'4', 0, 1, 4, N'21,34', N'25052021_124745yb_hinh-1.jpg', NULL, 1, NULL, 1, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T00:47:45.077' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'29a64c25-4c7d-48aa-911d-57a3f4b62148', N'21985eda-87ba-4176-932e-3194d286b4b7', N'aaa', 2, 23, N'4', 1, 3, 3, N'29,33', NULL, NULL, 1, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-21T01:46:10.650' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'71210465-e5d1-44be-b77f-58744efcc6cf', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', N'Phòng Superior', 1, 45, N'5,6,1,4', 0, 1, 4, N'21,34,29,33,26,28,36,30', NULL, NULL, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-22T21:57:42.540' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', N'Phòng đôi Superior', 2, 27, N'5,6', 1, 1, 2, N'34,33,26,28,36,30,25', N'17062021_111550yh_kingtype3-2.jpeg', NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-19T00:36:01.327' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'Phòng đôi', 2, 23, N'4', 1, 2, 2, N'34,29,33', N'09062021_115929yh_16052021_091513yh_advertising1617262142.jpg', NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-15T06:50:21.953' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', N'Phòng đơn', 3, 23, N'1', 1, 3, 3, N'21,34,29', N'02072021_072048yh_23226_6wldv8abbg_deluxe-twin2.jpg', NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-02T19:20:48.227' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', N'Phòng Superior 2', 5, 45, N'1', 0, 1, 4, N'21,34,29,33,26,28,36,30,24,25,22,32,27,35,23', N'03072021_103907yb_k1.jpg', NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:52:47.247' AS DateTime))
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd140945b-992f-4dc7-9c87-cbec213e22e2', N'21985eda-87ba-4176-932e-3194d286b4b7', N'aaa', 1, 23, N'2', 1, 2, 3, N'4,33', N'19052021_113021yh_cbr250rr.PNG', NULL, 1, NULL, 0, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:30:21.683' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'65e570fa-e0e3-41d2-8fc2-e2193db5e0b4', N'9590a723-ab0c-41a8-a992-036fd35e97b9', N'aaa', 4, 23, N'1', 1, 2, 3, N'34', N'25052021_022404yh_hinh3.jpg', NULL, 1, NULL, 1, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-25T14:24:04.007' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_Room] ([Id], [HotelId], [RoomName], [AvailableRoom], [RoomArea], [Direction], [SingleBed], [DoubleBed], [MaxPeople], [HotelConvenientType], [Image], [HotelServiceType], [IsCancel], [IsSurcharge], [IsDelete], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', N'Basement Standard Double No Window', 20, 25, NULL, 1, 1, 2, N'21,34,29,33,26,28,36,30,24,25,22,32,27,35,23,20', NULL, NULL, 1, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-19T23:35:17.923' AS DateTime))
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'be256480-871e-4d08-91a2-00cf5ff018c9', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-25T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.523' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:16.043' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f6ba1caa-0dd4-45a1-952b-012b207fea24', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-12T00:00:00.000' AS DateTime), 2, 4, 622000, 575000, 7.56, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-10T19:44:01.580' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-12T13:52:27.017' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6780ac35-44b8-4148-944d-0166ecde8873', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-20T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.183' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.290' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f5e646ef-8d12-4728-ba3f-021f01072edf', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-28T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.657' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:17.077' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'efd8a42b-efce-467a-aedd-0382590d8d4c', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-03T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.950' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.270' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7a5cb78b-3fb6-4d85-a4e3-053fe7c28ad8', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-26T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:26.970' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c1b67dee-48e7-4e14-9da5-06041c9d3622', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-17T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.127' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.117' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b8a488d0-89d5-461a-913a-062e803fc073', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-16T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.260' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'dc8dfbf7-7ebb-45b4-8389-0637350705b1', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-11T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.830' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.890' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1a34c9f4-e16b-4f89-80c8-07037220b0cc', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-19T00:00:00.000' AS DateTime), 3, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:05.537' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0b3d1fe2-612d-42d9-8d27-085a82d9ccf6', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-10T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.747' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd3731349-3175-4591-8406-0982be46538d', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-01T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.757' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.137' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'48b42756-c4fd-4e7a-882c-0a462d97d76e', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-17T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.303' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'111745fc-cf6d-47f4-95e6-0b1b59f7ea55', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-17T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.423' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ed19c3e6-0b45-4b6e-a049-0bc90ee084ba', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-24T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.670' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'41226ca5-dc78-4779-b8ea-0c9ae31f88ec', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-22T00:00:00.000' AS DateTime), 4, 4, 590000, 510000, 13.56, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.680' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:56:15.690' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6ec8b74a-1cd3-4fb9-bb87-0cd9a0d85ba1', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-20T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:25.007' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'30f40ea3-a60d-430f-80a6-0dcafcfec65c', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-20T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.420' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'82ea5d42-5ad7-49df-9e16-0fd856ffea0a', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-07T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:20.420' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'888c38b6-d3b0-4068-a3de-10d99d5607dc', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-25T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:05.650' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'cb3a1d09-b00c-4fca-b7dd-1211a9ec5ff0', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-25T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.707' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'98c56672-2a26-4c37-b67b-1488463e167c', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-22T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.017' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1b3381dd-ab2a-45f7-adfc-14a5b071ab37', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-27T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.673' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'236ae39c-97d1-4f37-b47c-15abdbaef0e6', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-25T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.977' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.903' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1b81694b-5745-48fa-8489-1619e48c90a9', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-20T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.337' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c6d675de-8167-4cfe-9b86-162b9963c7bb', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-12T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.827' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e44e4303-9035-46e8-a9cd-182ba8feea2e', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-11T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.047' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a38fa14c-6cae-4af7-b9cc-190be72f6516', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-05T00:00:00.000' AS DateTime), 9, 4, 590000, 500000, 15.25, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.557' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-04T19:41:48.550' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8ac4072d-852e-46a6-86cf-1a2866cc07cc', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-28T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:56.027' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a8ab5e14-ec70-4336-952f-1aa754b38592', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-29T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.773' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b4b82a8c-81c6-4488-b392-1acc2b16cb62', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-06T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.243' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.503' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6abf051c-8a01-415b-b5e9-1ae553c0d71c', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-10T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.343' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.563' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2f4de766-55d4-4e0f-9d11-1d127533c1d0', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-22T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.800' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'19a387b4-e417-4b54-be6f-1d65a35dc7fe', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-15T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.940' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd6b9b697-0b26-4061-aafa-1dd08f4547ca', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-09T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.710' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e11b8220-4ca6-44a6-a224-1eac19e1f150', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-11T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:21.890' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f67d7c65-ed9b-4de7-8552-1fba911ca2ab', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-02T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.617' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7c8426ee-6eb7-4baa-98c9-1fea4c477820', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-18T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.873' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b8a361b4-65a8-48fa-92d8-224cd5f6d651', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-17T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.073' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.137' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'32cf6c17-acb7-4c9f-97ae-22abaa673b76', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-11T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.973' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-10T19:41:37.957' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7fa0794a-9d5a-43fa-8378-23e07659bc2e', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-30T00:00:00.000' AS DateTime), 5, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.783' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-28T16:28:31.403' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'74c8ac62-8a3f-4c6e-825f-2412755a77a5', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-18T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.627' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8fcc9567-46bc-465f-b3e3-2871a28189ed', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-23T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.053' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e0ef9eff-887b-439f-89a9-28fcd32d653d', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-15T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.467' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'408e39d6-7a9b-4f3e-b672-290b2b5600a5', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-29T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.410' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a5df7b56-6740-4e2b-b3d0-294a57aaca6b', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-26T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.103' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.730' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'598a0bb2-ae03-4391-aad1-29b402abb349', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-23T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:05.347' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c0ef7ea4-d826-444c-8f89-2be5f7f24d83', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-06T00:00:00.000' AS DateTime), 5, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T17:41:57.290' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:24.110' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f6f4170f-4e53-44fc-ae75-2d8b6e5ae9d3', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-25T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.387' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.487' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4bb5d55a-d48e-4dea-817a-2d91bb8b0592', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-16T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.047' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.040' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'71d387d0-8476-4f62-90b4-2e1ca3c78e15', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-23T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.243' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'75107659-2320-486e-b1d5-2eda313a4011', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-08-02T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-04T19:42:39.020' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-04T19:42:47.887' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'cabac4d7-287d-4588-ae4a-2f60da559ec1', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-14T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.937' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.027' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a0f6611e-a7a7-4cf1-a513-308e047b536d', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-06T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:20.120' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3369cabe-b974-43cb-922a-30abb044be16', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-30T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.883' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c54d4903-0391-4c7e-9285-3203b541c4bf', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-12T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:22.200' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3c4e2966-44a4-4e23-8fc4-320571d83fca', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-15T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.107' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.210' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'227469b8-424f-4804-baef-3207569d8464', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-14T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.723' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7d448035-ffd8-461f-b940-3315f41c790a', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-12T00:00:00.000' AS DateTime), 3, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:05.427' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'77392a97-c7a6-4931-9258-336ae2d3b951', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-19T00:00:00.000' AS DateTime), 7, 4, 590000, 510000, 13.56, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.143' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-18T14:14:46.947' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'cece91c1-95c5-4399-af9a-341257c82247', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-19T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.490' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'48fb9a1b-501a-4346-8f78-3575579f9a21', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-22T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.720' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.687' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'00a571a1-c9dc-4a79-b7a5-359c7545dc88', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-26T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.163' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'41ebcd46-429c-4611-a991-359e59f8656d', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-18T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.340' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9178109e-5383-4ebd-9115-361d06f7898d', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-09T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.783' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'943d4de4-6fdf-4d36-8931-36da44f04312', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-15T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:23.277' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1e7df29a-f845-43d4-9003-3788170f39bc', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-10T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.937' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a393b0f1-a433-40bb-a186-379dd4fac262', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-16T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.527' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e14d8e9b-985c-4447-b76c-37ac4a4015ca', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-01T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.123' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.453' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'16f7d2cb-6508-4acc-9a9e-39288b4d2955', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-30T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.543' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.000' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8bb583f2-e04f-4d15-83eb-3cbc7e8efea5', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-17T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.230' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3769d6b1-a79c-449e-ad89-3d701cc89b11', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-30T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.593' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.677' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e541945b-1cd8-414c-a261-3d79324a4613', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', CAST(N'2021-06-27T00:00:00.000' AS DateTime), 6, 4, 590000, 345345, 41.47, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:45:02.710' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd21511d8-c23e-4c71-b758-3e2a6ffe3895', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-13T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.867' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'58e293b1-fb41-4ca0-b419-3e93caf8678c', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-22T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:25.707' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7b5c0d2c-ef98-4235-9fe7-3ea1145b8542', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-04T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.020' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.353' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4b0fde27-3c6d-4e84-96d5-3f63c624ff7c', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-29T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.697' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:17.497' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5b22be30-e0af-47e8-8375-3fbefd58dfab', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-08T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.603' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'36a0dc02-f40b-4a75-b9e6-3fd3359be9b7', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-06T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.373' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9fe83a4e-abd3-4b35-8b92-404279339cbc', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-12T00:00:00.000' AS DateTime), 4, 4, 215000, 181000, 15.81, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-09T02:19:51.833' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5d8f2d46-a335-465d-bf8e-4076816c820a', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-18T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.460' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9b5c5d5c-454b-434b-b7e2-41df688e682b', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-05T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:19.783' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'73aa605f-287b-4fd2-b35f-4279b901b0b3', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-30T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:28.297' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'19e38722-9c2e-4811-b1b0-427eefba22a3', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-20T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.427' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.320' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'aa414c19-d4a3-42d8-9daa-45836315e187', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-13T00:00:00.000' AS DateTime), 3, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:05.873' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ef6bab6d-9463-4e81-bed1-4643e90a8a5d', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-12T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.867' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.953' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0aaeb251-4344-4eec-aae5-464e12570003', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-26T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:07.060' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.960' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'933c76a3-5477-42dd-a2e8-468668101a79', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-25T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.973' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.663' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c433755e-d6a6-43fe-b03d-478d11ac52cb', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-24T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.087' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b63b1cc7-4def-4829-b1cd-485d44b1ef07', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-03T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.483' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'32e22ab5-1ae8-4cf6-9e94-49d5e452260a', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-12T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.820' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.993' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0a47791a-8f9f-4119-8d7d-49de227f81a6', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-29T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:07.340' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:05.180' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'834885a5-eed5-4ded-9ee8-4a9283d4f151', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-08-01T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-04T19:42:38.823' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-04T19:42:44.097' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'127bdb1a-e8cd-494b-a09d-4b3b853c6eef', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-24T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.880' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8e36b16f-d052-45f9-92a2-4b6b2c90fc6b', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-04T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.520' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5ea270f8-4d39-48a3-9572-4bbf876f082e', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-06T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.607' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.693' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5c8ce95a-78a1-48a1-991f-4bec870d47e5', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-24T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.487' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:15.700' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6cf1dc18-f288-42c8-8d02-4c91f038b098', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-20T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.523' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c76d04bc-b7e4-4372-a0ef-4ceaf6b04013', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-18T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.267' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c7dd14a0-079b-4e1b-b89c-4cfc9a874f97', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-22T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.453' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6eee01d0-ab3c-4b0b-b742-4ddc7366f704', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-29T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.747' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5891304d-f9f1-416e-8e8d-4f48b3399571', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-02T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:18.777' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6ff4fc94-324c-4541-89bc-504af5705419', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', CAST(N'2021-06-12T00:00:00.000' AS DateTime), 6, 4, 590000, 345345, 41.47, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:45:02.483' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e9759146-9360-4c94-8668-507914ba379b', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-03T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.653' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b22f220c-c402-4a15-8da5-50a64dd6ba99', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-28T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.440' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4bfe735c-690c-4602-872e-51a231641d55', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-26T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:05.773' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd5f49e6f-d8df-491c-87e1-522211ec4865', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-27T00:00:00.000' AS DateTime), 4, 4, 590000, 510000, 13.56, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.207' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:55:48.897' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'389ca536-88d3-4548-93ac-52d3f9b17c01', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-31T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.640' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.077' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8572bb9c-3e7d-46d3-9520-52e025139350', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-14T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.113' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'07f5a45e-18ef-40a8-a089-53aaba241ae8', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-09T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.203' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.453' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'09f4f320-b9ae-4590-8870-54789c81503a', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-14T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:22.997' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'66292f7d-aee0-4d00-a869-54c0f2193b7c', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-02T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.833' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.210' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b48bc602-a718-4703-8318-54cca60c795f', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-29T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:27.980' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8a5b8bcc-fe06-4882-bedf-56bb6c7329db', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-28T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.707' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'869f6759-5dc3-4d58-944c-57a71d413c48', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-26T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.957' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'df355e32-b286-464d-bc43-57c921d636b3', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-05T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.570' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.637' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c62f98fc-815f-462a-9cee-583949bde910', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-25T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.920' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'81be361b-c666-422a-9f1f-5954c04dabba', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-27T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.390' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'099311da-182f-4d71-9c6c-5991cb8bf448', N'29a64c25-4c7d-48aa-911d-57a3f4b62148', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-04T00:00:00.000' AS DateTime), 2, 4, 590023, 345345, 41.47, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-30T11:41:00.357' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:44:32.843' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'315567f8-0f42-4738-8a0d-59b1c05626e6', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-29T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:06.133' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9ebc056d-44fc-43e8-9b0c-5b30855b5055', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-14T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.013' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.133' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2b865e12-3fbb-4568-933b-5be068b3fcef', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-01T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:02.757' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'36183541-37cc-4fd7-94f9-5c6ed2ef876e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-01T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.580' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'857d5036-2588-46b1-9e1c-5cab9c5481a8', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-04T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.690' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'39550501-b754-4860-b808-5cfdf7d19024', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-20T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.130' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd45a121d-efe1-4372-a3c2-5dbe238c563a', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-19T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.787' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5999833c-3d15-4274-a8f2-5e227927ae58', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-23T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.633' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd66c11ff-1e73-43a7-a13a-5f057affad06', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-30T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.513' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'df9a5037-169c-4936-bbe0-5faaf75ed14e', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-18T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.263' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0e09ba62-4c75-4f29-83d0-6208ef9cc541', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-24T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.350' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.440' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'90252470-5ee4-4c8e-8f6b-62a4b75ee113', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-21T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.980' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd89d7e87-abcb-4c48-a3b1-6526c70b77e9', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-27T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:27.300' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f597b661-8c36-480e-a598-65f4cbcad9fe', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-16T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.980' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'48269e0f-07a4-412f-9a74-67065ca416d2', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', CAST(N'2021-06-19T00:00:00.000' AS DateTime), 6, 4, 590000, 345345, 41.47, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:45:02.523' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0fd5089f-6c1e-4f7e-b6b4-672bd58cfd8a', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-30T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.460' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e336251b-8a21-4d4a-a737-67a292abb423', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-29T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.453' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.943' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'18b188c3-5942-4192-ba86-6848ef91eee9', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-28T00:00:00.000' AS DateTime), 4, 4, 590000, 510000, 13.56, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:04.303' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:55:49.210' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'bfa8faf3-cf9c-4342-a30b-68d129bd135e', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-04T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:19.473' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0e6d0bf9-8d33-4ad3-9a45-69ea9de0e8cd', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-28T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.813' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f3e4aa94-0b79-4ebe-a789-6adb18c8d20a', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-19T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.910' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'692ecf77-1cb4-4578-a96d-6b7737e755fe', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-13T00:00:00.000' AS DateTime), 6, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.070' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-10T19:41:58.797' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8b96e9d1-dbd3-4a51-8f02-6bcc7894cdcb', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-20T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.900' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5306b10d-a439-4475-9f3d-6c719170afdb', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-24T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:05.493' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'886a92c8-9c00-4155-81c5-6cd0a8d24e2a', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-16T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.010' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.103' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6503d6bb-758a-4b22-9ae5-6d0e402bf4e4', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-03T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:01.520' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:36.147' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'294e6a4e-9f44-4937-9504-6d5763b8ae6f', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-23T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.450' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:15.410' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ee09056f-6dd9-4ef8-aa7c-6e2ab973505c', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-05T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:01.763' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:43.230' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'aa8bdb57-6ad1-4490-8c1e-6ead52909f72', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-08T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.673' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ad6b32c7-db3d-4cb3-b53f-6eb1c75fb203', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-09T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.747' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.817' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd2e38b0f-5cc4-4114-a0c8-6ef2503833b6', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-27T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:07.147' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:05.033' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c1131d48-26c6-4081-bca3-6f49e0477377', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-18T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.110' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.193' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8a169a3a-d6a6-4d01-8955-708ced55f0cc', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-21T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:25.410' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3f6a32a2-0f1d-4957-ad82-710e00b8503d', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-25T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.317' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'947750b1-177b-42c4-986c-7199847895a9', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-22T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.207' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7503ebe6-f036-4bf0-a713-71c94712524c', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-07T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.807' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3ed12765-5cc8-4791-ac1c-72790d191ece', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-18T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.220' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.183' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e5c5b1c1-d9f5-4730-9a9b-72ab882755bb', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-27T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.990' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f73c850e-97b5-4ad2-9ad6-72b0b2a08b0f', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-11T00:00:00.000' AS DateTime), 4, 4, 215000, 181000, 15.81, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-09T02:19:11.860' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-09T02:19:51.783' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0b4c5c5d-843c-4d2a-84fc-7326159a48d0', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-06T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:01.917' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:29.987' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'32eda40a-43dd-4bbe-9015-73446098174a', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-15T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.150' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4c823049-533c-4569-8474-76a08a6d757a', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-24T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:26.327' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7d6b8d24-32e3-4ef3-9d4b-7785bacb52cf', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-27T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.490' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.557' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5aa8b171-be14-404c-bfcc-797860132aa8', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-27T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:05.873' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7e055d7d-9e3d-4bee-a24f-7993493f7089', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-03T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.073' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'75bb46d6-64e1-4f3d-9426-7a579b14857d', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-20T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.527' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.557' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'50e76912-3440-411a-87f4-7a975a806d0b', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-24T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.883' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.593' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9735eabc-57c1-47ae-85c7-7adc225cc875', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-08T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.680' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.770' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fe831f5f-2fa8-4ee7-92a4-7b35326c7b17', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-08T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.103' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.363' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9025f332-6131-4b87-8c9e-7e8ae5951219', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-04T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.533' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.603' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'54557676-4322-4380-9d34-7fe4366b4b56', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-23T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.547' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fa75d5d2-35c0-4949-a1db-809bffb8af01', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-05T00:00:00.000' AS DateTime), 2, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T17:34:24.830' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:26.877' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd2622a79-d3c6-4c11-a9aa-80e23ffc7938', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-09T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.897' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7746384c-5861-44b7-8f22-81051585a38e', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-14T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.117' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fb39ac4d-40c2-486b-90aa-82b814221a2b', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-11T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.753' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.880' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'01b0ba5d-23a9-4dec-b126-835cda611067', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-22T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.260' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.370' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'703e9ee5-37c7-4ab0-ac57-843482a5dfdb', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-15T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.977' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.070' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd52e2e78-8a3d-4fae-974c-85088cb976ac', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-21T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.167' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b081f6e0-6bdd-48e2-8eb1-85e6ae1fe06a', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-21T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.373' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:14.767' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ffe8be32-462d-4a70-9311-863818b865d0', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-28T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.360' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f02d83a3-2ef0-4f6a-bba1-865ff9c3e728', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-22T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.593' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e3d73eb3-1680-47e5-b9d1-872cd60f01bf', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-23T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.843' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2643d56c-756c-4cff-8d47-87d61a04c414', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-30T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.740' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:17.817' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'02a5e64f-f687-4c78-abb1-8890f32b559e', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-21T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.573' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.390' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8fea7d6b-8e59-4f9b-9b67-89cc7dd11afe', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-06T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.593' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-04T19:40:41.623' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'44668b14-30d7-423a-91a1-8a14259fdb7f', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-19T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.323' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:02.250' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'50064b09-89f0-4045-b676-8a2dd14aa192', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-10T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.787' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.853' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3ae4e97c-70a9-419e-882e-8b80743b3778', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-20T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.337' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f8284d57-8dc5-4498-a95f-8babe2f2c97d', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-11T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.450' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.697' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'dcc9d3e1-dd11-4a92-a2d4-8be6192f1558', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-26T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.627' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd4a53ed9-eeed-440b-a8ba-8c5cbd65d92a', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-17T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.837' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'322547c5-76bd-4f49-9af2-8e917f8ac09d', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-27T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.240' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'92641790-7f12-4b7b-bd8d-8f4b72f04c26', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-19T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:24.667' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd2c55f8a-f382-425f-9a07-8f4bb3cdfdd6', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-21T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.457' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'63099940-4501-4d3d-b003-8fdaf8f1c684', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-13T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.217' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9a255b93-eb6c-4cef-82c3-8fedd975af0a', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-29T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.473' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4826c01e-4a49-48cf-8d1c-90782c40f0e6', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-15T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.153' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3b195c04-c725-45a9-9f8b-90c69e0a9fee', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-28T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:07.243' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:05.093' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'31552db3-1c82-4d13-8fd6-92fc5ccdde35', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-25T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.617' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9255de0c-ca71-4f22-912d-941cc11ad4ef', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-11T00:00:00.000' AS DateTime), 2, 4, 595000, 532000, 10.59, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-10T19:43:14.840' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c21b195c-c979-4236-b169-9471468e27c6', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-21T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.753' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'623b928d-3e86-40fd-bcb9-969da390f5d3', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-24T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.583' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2e66cf52-5c9c-46aa-9fcb-96dc90e1c7f0', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-29T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.557' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.640' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8eb571a9-6fb7-496e-97bc-96f32ef1a2f5', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-03T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:19.130' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fe6caae9-20a0-45b8-b6fe-971a21d11e05', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-11T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.790' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7866771c-5182-4e36-a87b-99274b5b7b27', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-20T00:00:00.000' AS DateTime), 3, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:05.980' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b53ce15a-742c-4a0c-838b-9982b7d1ea37', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-26T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.560' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:16.377' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'aecf425b-ff1b-421b-b4fb-999dc03db21b', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-10T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.947' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c9827955-047f-47c7-83b6-9a8fc325cc6b', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-02T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.447' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'679592bf-6d97-4be5-bbcf-9deb08f725b7', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-13T00:00:00.000' AS DateTime), 2, 4, 760000, 600000, 21.05, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-10T19:49:33.027' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-12T13:57:17.553' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'24049ad0-8f30-4771-9b75-9e2b02ec2423', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-05T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.730' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ff6fc199-4051-4ad5-a30e-9eee5c38a26d', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-25T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:26.643' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'06b21981-a9cd-4309-be1e-9f25386c31c6', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-27T00:00:00.000' AS DateTime), 3, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:06.077' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7f030434-96e7-4722-a8de-a06dccd9ce35', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-02T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:02.953' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c145f530-1a82-4006-aafb-a1d9f642da84', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-26T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.353' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b2b2bb7f-91d8-4fc3-8884-a214af355097', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-18T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:24.327' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5879d33c-95b6-45bb-986b-a51c2122e9a8', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-14T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.903' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2fba3e49-faff-4742-9f4b-a7a9bf13b300', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-07T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.643' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.730' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0f981249-1d77-4221-a23b-a8d86ea7ea2e', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', CAST(N'2021-06-20T00:00:00.000' AS DateTime), 6, 4, 590000, 345345, 41.47, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:45:02.670' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4c6b57d2-c5e9-48ff-8182-a9cc54dab0de', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-24T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.543' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e46e819c-d63e-4e1c-803e-aa59c9e3f172', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-27T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.703' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3636375f-2089-492c-af2b-ab2aee77248d', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-28T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.523' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.593' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'cccfcb45-51d3-4eca-9e5c-ab36a784c9c0', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-23T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.313' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.407' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6a3689d9-7f5b-461d-9279-ab814f159bbc', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-27T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.620' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:16.717' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e07b79a8-2a2f-4b40-be93-abf146f1cf41', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-31T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:28.577' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'cdf06a49-29e5-4a5e-905a-aceb38daab48', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-19T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.093' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e338b508-0df0-4bb5-aba8-ad67d7880015', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-08T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.843' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'24dd1347-ba72-4897-beeb-afa32c83fa33', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-15T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.760' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ef2c5a49-1609-44ad-8f84-afc906e520f4', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-14T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.297' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'167c1425-012e-4bc7-9abd-afe37260d7f3', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-28T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:27.627' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6516740c-53f9-4538-8604-afecd03058d4', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-12T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.603' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.757' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'16da795d-de9a-4e57-9366-b08611563e2e', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-16T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.487' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5749d4d5-b2db-4b1b-9c3a-b182a0a4c147', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-19T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.670' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd8c8fc07-13bb-42aa-ad1f-b3a112e08468', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-21T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.220' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.330' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b2bf2354-65ac-4452-b3da-b3d1e3f05307', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-18T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.057' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'86485552-3f23-4e62-b9bb-b5d040c9e9fa', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-17T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.580' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1ee18781-ada2-4eab-a992-b68f7533274a', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-31T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.633' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.713' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4c00f7ad-1cc0-4bb5-a317-b6c95f5bb395', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-30T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.810' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'887998b2-4d59-44b9-8823-b7541d02bc39', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-19T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.300' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'194ecec4-8ac1-4dce-84eb-b7d021b1f178', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-08T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:20.780' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5979418e-adf1-4a55-a951-b979d87ecbf2', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-25T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:28.127' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0a1700e8-136a-42aa-bb6e-b9ecca65df00', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-14T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.120' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'cdda7b5b-9712-44fc-abdd-b9f3b20cc41a', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-21T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:05.043' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b384c72e-f412-480f-81be-baaa77574363', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-04T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:01.653' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:40.883' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ae993821-2f9a-42a1-ad40-bd960ca87d21', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-03T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.497' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.550' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3abd5ca2-6476-4408-9609-bd98e84161bf', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-26T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.740' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ad32d4ac-1582-40f6-8184-becd0b5a0cf5', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-05T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.150' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.413' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a8b0b969-25f7-48ef-8899-bee0bb17eb5f', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-08T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.430' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.643' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8fb2745e-bb71-4840-b551-bf40345d5dcf', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-01T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.180' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'7cfee4c2-4018-4fc7-b0e9-bf46a2c5f711', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-20T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.943' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'862758ba-f3dc-4837-9d3e-bf47c81f78bc', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-17T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.293' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.350' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'93bd4677-2ee4-4d87-b9d7-c013f7212793', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-26T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:59.420' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:08.520' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3fe67492-bb9d-4b5f-9061-c0b481300ad3', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-13T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.907' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.067' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'22edf05b-b228-4e5b-a68c-c2060b642295', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-04T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T17:41:17.520' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T17:41:37.720' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5720d07b-9fc8-4888-bcac-c297557dcd52', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-07T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:29.640' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-04T19:41:17.140' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e1fe73a7-fec3-4700-a71d-c2bc3d3d6e2c', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-21T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.377' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4f55cdd9-1916-4e16-9363-c35775ca9f3c', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-17T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.227' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6bc74174-143d-4838-83aa-c3f9ae430ab7', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-17T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:24.000' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd3ffedbc-49ae-4cf7-b288-c3ff8ef385fb', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-20T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:55.710' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f419bdd3-5375-4f5b-8037-c4095858e651', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-29T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.850' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4630ecb7-99eb-4e8a-884a-c565decad4bc', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-04T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.193' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5ab73483-a65e-4438-9497-c7dd2e6ffc38', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-02T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.460' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.503' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fdfb0f48-aecc-4ca0-9153-c80b0b78763c', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-15T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.940' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.983' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'74b3342d-a989-4ef6-acdf-c8dcd1dce9fd', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-16T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.183' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.270' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3d906a27-7c63-4d06-ae1c-c940a719af34', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-29T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:56.063' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5f0da668-7b7a-4478-a648-c99ac2560f14', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', CAST(N'2021-06-26T00:00:00.000' AS DateTime), 6, 4, 590000, 345345, 41.47, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:45:02.560' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'ace20053-e327-4b5b-bf87-ca10af1ad73b', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-17T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.020' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'04f0f7e0-a5e1-41e0-8505-cc4447d054bb', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-23T00:00:00.000' AS DateTime), 4, 4, 590000, 510000, 13.56, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:03.777' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T06:56:16.050' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c4d19546-d12d-4d65-8ec8-ccd37e1255d2', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-14T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.800' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.913' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e495c656-0d36-4ba6-a714-cdf52f1c9364', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-10T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.663' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.773' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a673063d-4e4b-4ccc-a05a-ce4ee9a72ebb', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-17T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.587' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e549f9c6-2003-4cb9-8d46-ce6abe8625f5', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-23T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:26.017' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2faef7b2-4167-45c8-a959-ced0ad607e5b', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-16T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.190' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5e1cc892-d22e-4e15-96f7-cf853a9fd2b0', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-16T00:00:00.000' AS DateTime), 4, 4, 700000, 588000, 16, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:25:27.793' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'889e1d14-4b5b-4bdf-9a20-cfba5e97814d', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-30T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:06.223' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5bc448fa-f56e-40ab-acee-cfc51fdc6d40', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-22T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.507' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a384740b-f19e-4b5e-91b4-d14a2cc4a860', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', CAST(N'2021-06-29T00:00:00.000' AS DateTime), 5, 4, 550000, 345345, 37.21, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-30T11:24:40.433' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-30T11:29:05.953' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f9d1305f-ad54-444b-a25b-d46f5f4b6675', N'68660afe-9e67-4ec8-839a-8855be7a1f62', N'9590a723-ab0c-41a8-a992-036fd35e97b9', CAST(N'2021-06-13T00:00:00.000' AS DateTime), 4, 4, 340000, 297000, 12.65, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-09T02:20:37.767' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8a6f690b-2388-4a8a-a06f-d4e32a0ffa64', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-03T00:00:00.000' AS DateTime), 2, 4, 590000, 500000, 15.25, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T17:36:14.240' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-02T17:37:00.087' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'2c449c62-3633-472e-ad75-d4fd60bf4daf', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-22T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:05.200' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'76914f85-0a10-4b08-9fd3-d6a2b5e5733e', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-23T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.800' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.760' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a2cc01a6-7f47-4bc5-888d-d89f49a0d75c', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-19T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.303' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'00d58d21-7601-46e0-af4d-d98af7dfdbfe', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-18T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.377' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.410' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9fbb8b84-2840-4d52-ac48-db3a66d890f8', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-22T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.413' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:15.077' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'fac75a63-2c51-4429-8f87-db52c0d6865d', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-19T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.377' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1fadcf8c-4bd0-404a-ba28-dbc36e2f4066', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-05T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.290' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f231bd6c-b9e7-4cf9-8182-dc310f9ef183', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-26T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.667' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b6a944c0-4d9a-4f95-aa60-dce4332a5df3', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-07T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.333' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.570' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b3b930d3-beec-4e06-b2db-dcfbfde52276', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-31T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.777' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:18.110' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'40279b1c-3583-4cdd-9cc0-dd4c8a06d7fe', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-25T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.583' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e981d4f1-5ff2-45c3-85d3-dd6911121fe7', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-12T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.020' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-10T19:41:40.437' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'd9c69dae-8dd2-43d8-8b98-e005a8b7f483', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-06-23T00:00:00.000' AS DateTime), 5, 4, 600000, 508000, 15.33, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:26:03.503' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'86c0ac53-d8d3-40bf-b4fd-e073c5355c8c', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-28T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:06.013' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b6244436-dea3-4e3e-a2cd-e0ec4c4da433', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-21T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.653' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.630' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5ca6d8ba-2435-4982-acc3-e17b7657e096', N'c7459dbe-b613-48e2-8c4a-e9ac7173cba3', N'535a5b23-6a2f-4d8e-8260-79152ff81f11', CAST(N'2021-06-30T00:00:00.000' AS DateTime), 5, 4, 550000, 345345, 37.21, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-30T11:24:41.093' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:51:40.203' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'986497f1-ea0a-4369-8d12-e38eda979b56', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-07T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:03.490' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'e57ac02f-fa6b-4ab9-b9dd-e45451f75a82', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-30T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:07.450' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:05.243' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'bf00aab3-97df-4116-9748-e4817516017b', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-21T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.560' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'cab7816e-e8e8-484f-95db-e4c8fea24228', N'29a64c25-4c7d-48aa-911d-57a3f4b62148', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-05T00:00:00.000' AS DateTime), 2, 4, 590023, 345345, 41.47, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-05-30T11:41:00.453' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-03T01:44:38.937' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b41a982c-9429-43f5-a10e-e4fc75de012f', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-12T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.143' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c0b91f32-f7c2-4a7d-9ec0-e54ecce4cc72', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-13T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.717' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.840' AS DateTime))
GO
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6ad870ec-cf2d-48c4-896d-e5ece067682b', N'386dd306-a7a8-4e88-8543-2668a981eb73', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-28T00:00:00.000' AS DateTime), 6, 4, 380000, 340000, 10.53, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:53:47.740' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'a95feba8-1e6a-456d-9939-e7877f048225', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-27T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.773' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'f449f8b2-d750-4a4f-a757-e819485eea1b', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-13T00:00:00.000' AS DateTime), 7, 4, 700000, 590000, 15.71, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T00:59:58.903' AS DateTime), N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:01:07.993' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'6a96cf27-283c-4333-870f-e89aee315788', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-09T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:05.507' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:03.703' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8e57b11b-3124-41d6-b4f7-e90c84841509', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-10T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:21.490' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9dcc55e8-286f-4379-9801-ee0d0c487f2c', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-24T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.280' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'715d0feb-4b5c-4712-b205-ee5640b6f43f', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-13T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:22.660' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8bc383ff-ed39-4344-b260-f07f8fbc7ee0', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-01T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:18.427' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'df7daaae-3808-4887-988d-f1eb0a6d409a', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-18T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.667' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'de2ae7a6-c3a4-48fc-91f6-f272c210c8f2', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-07-07T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:02.017' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:01.297' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'78f3b87e-c4d6-4df0-b9d2-f441336808d9', N'7d63a5f7-513a-4a42-bf84-af8884120801', N'64477619-22da-45d0-9d24-8c63124e0d82', CAST(N'2021-07-31T00:00:00.000' AS DateTime), 9, 4, 600000, 575000, 4.17, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:03:30.547' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b7acd36e-f49c-40d4-a9a3-f4bcf4132986', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-24T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.873' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.827' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'5a0b3ca7-3715-463e-8974-f85e74c32763', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-31T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:07.527' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:27.050' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'1f99541e-c43a-4eef-9aae-f919bf18aa1c', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-09T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:21.147' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'445c381e-48fe-4ae5-8d20-f955399d44e9', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-16T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:33.193' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'8f1a4a16-9058-4b5c-a84b-f9ce6ca47db1', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-09-15T00:00:00.000' AS DateTime), 6, 4, 5000000, 4000000, 20, 1, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T11:50:04.393' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'3ea471f7-654d-4c60-b29e-fca339c652c3', N'd8fd6c14-9581-4d4d-a11f-121b3b5f2f74', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-16T00:00:00.000' AS DateTime), 7, 4, 750000, 700000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-16T17:54:17.383' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'729618c8-d075-49ce-b487-fdd0f75f80fd', N'dc7d3400-ecf8-4ea2-bf45-c37bf6cdb892', N'b1f0d4e4-94cb-40d4-a5de-8122fe02e1cb', CAST(N'2021-08-19T00:00:00.000' AS DateTime), 0, 4, 5000000, 4000000, 20, 0, NULL, NULL, 0, NULL, NULL, N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:40:06.463' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-03T10:43:04.487' AS DateTime))
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'4a0599ec-f1fa-4d45-b9c0-fe4c4517867e', N'fa7001d1-db4c-4ea7-86ed-18f3679109b0', N'21985eda-87ba-4176-932e-3194d286b4b7', CAST(N'2021-06-26T00:00:00.000' AS DateTime), 3, 4, 590000, 510000, 13.56, NULL, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-04T21:34:05.640' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'c65932ff-ddbe-48a4-9b7c-fecefbee401b', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-07-06T00:00:00.000' AS DateTime), 6, 4, 600000, 560000, 6.67, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-30T01:00:32.770' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'0a700c59-628a-47d6-ae8b-ff3dfbecf2c0', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-06-30T00:00:00.000' AS DateTime), 7, 4, 500000, 480000, 4, 0, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-06-14T00:24:56.100' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_RoomPrice] ([Id], [RoomId], [HotelId], [RoomDate], [AvailableRoom], [HotelPriceTypeId], [PriceContract], [PricePromotion], [PercentPromotion], [IsActive], [IsPromotion], [HotelConvenientType], [IsDelete], [Tax], [TransactionCosts], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'9baa06f0-e9e3-4d2a-9492-ff5c1c1b9f8a', N'446cc8ef-4871-48d5-910f-690932dfe1e4', N'0bc75359-d678-49ba-b033-a1209ad10e5e', CAST(N'2021-08-16T00:00:00.000' AS DateTime), 5, 4, 590000, 500000, 15.25, 1, NULL, NULL, 0, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-21T07:00:23.717' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'31626bac-ff21-47a3-8166-27ba5aabd1f5', N'hieunv', N'M/tTWwIES8wxgueZZcLelw==', N'Nguyễn Việt Hiếu', NULL, 1, N'hieuviet.1103@gmail.com', NULL, NULL, NULL, N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-18T14:48:18.817' AS DateTime))
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'17fb8d13-9e2a-4dc4-93d5-7d3290ce1995', N'hieu', N'M/tTWwIES8wxgueZZcLelw==', N'Hieu', N'', 1, N'hieu@gmail.com', N'', N'31626bac-ff21-47a3-8166-27ba5aabd1f5', CAST(N'2021-07-18T14:25:36.050' AS DateTime), NULL, NULL)
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', N'khanhpq', N'Z13nyDTQaQAst65Ic/lBUA==', N'a khánh', N'', 1, N'khanh@gmail.com', N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_User] ([UserId], [LoginName], [Password], [FullName], [CodeStaff], [Active], [Email], [PhoneNumber], [UserCreate], [DateCreate], [UserUpdate], [DateUpdate]) VALUES (N'16dce4e1-8464-4b2b-b613-8384c374033a', N'khanhpham', N'4QrcOUm6Wau+VuBX8g+IPg==', N'Phạm Quang Khánh', N'asdsasd', 1, N'pquangkhanh@gmail.com', N'zsczxczc', N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-06-05T15:56:18.000' AS DateTime), N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', CAST(N'2021-07-05T05:07:57.340' AS DateTime))
GO
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'5fa93e1f-dfff-46f2-9eab-0f09edb30326', N'31626bac-ff21-47a3-8166-27ba5aabd1f5', 1, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'41dd2c59-1465-4b11-99d6-44128992a8a9', N'17fb8d13-9e2a-4dc4-93d5-7d3290ce1995', 2, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'5827c8f5-d56d-40dd-bb2f-5292b3047f58', N'16dce4e1-8464-4b2b-b613-8384c374033a', 1, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'abff8394-9817-4a67-a129-593a61b16f5b', N'31626bac-ff21-47a3-8166-27ba5aabd1f5', 2, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', N'b9b07f9e-1312-49b0-a37e-8189150bfb5e', 1, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'18c7e212-adc1-4cfc-b38c-9c3e3fab2fab', N'16dce4e1-8464-4b2b-b613-8384c374033a', 3, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'614e97c1-cfc3-4e19-adf3-ca14ae9dc576', N'31626bac-ff21-47a3-8166-27ba5aabd1f5', 3, 1)
INSERT [dbo].[tbl_User_Role] ([Id], [UserId], [RoleId], [IsActive]) VALUES (N'e88ce058-a753-4c05-82ec-d49445082770', N'16dce4e1-8464-4b2b-b613-8384c374033a', 2, 1)
GO
ALTER TABLE [dbo].[tbl_Area] ADD  CONSTRAINT [DF_tbl_Area_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_isflash]  DEFAULT ((0)) FOR [isflash]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_priority]  DEFAULT ((1)) FOR [priority]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_active]  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[tbl_banner] ADD  CONSTRAINT [DF_tbl_banner_language_id]  DEFAULT ((2)) FOR [language_id]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking1_IsInvoceExport]  DEFAULT ((0)) FOR [IsInvoceExport]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking_CustomerCheckPayment]  DEFAULT ((0)) FOR [CustomerCheckPayment]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking_IsPaymentSuccess]  DEFAULT ((0)) FOR [IsPaymentSuccess]
GO
ALTER TABLE [dbo].[tbl_Booking] ADD  CONSTRAINT [DF_tbl_Booking1_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Booking2] ADD  CONSTRAINT [DF_tbl_Booking_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Country] ADD  CONSTRAINT [DF_tbl_Country_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Coupon] ADD  CONSTRAINT [DF_tbl_Coupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CouponType] ADD  CONSTRAINT [DF_tbl_CouponType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] ADD  CONSTRAINT [DF_tbl_CustomerCoupon_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Direction] ADD  CONSTRAINT [DF_tbl_Direction_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_District] ADD  CONSTRAINT [DF_tbl_District_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Hotel] ADD  CONSTRAINT [DF_tbl_Hotel_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] ADD  CONSTRAINT [DF_tbl_HotelCancellationPolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelComment] ADD  CONSTRAINT [DF_tbl_HotelComment_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelConvenientType] ADD  CONSTRAINT [DF_tbl_HotelConvenientType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelNotice] ADD  CONSTRAINT [DF_tbl_HotelNotice_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelPriceType] ADD  CONSTRAINT [DF_tbl_HotelPriceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelRatingType] ADD  CONSTRAINT [DF_tbl_HotelRatingType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelServiceType] ADD  CONSTRAINT [DF_tbl_HotelServiceType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] ADD  CONSTRAINT [DF_tbl_HotelSurchargePolicy_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_HotelType] ADD  CONSTRAINT [DF_tbl_HotelType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_PaymentType] ADD  CONSTRAINT [DF_tbl_PaymentType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_HotelImage_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[tbl_ProductImage] ADD  CONSTRAINT [DF_tbl_ProductImage_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Province] ADD  CONSTRAINT [DF_tbl_Province_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_Room] ADD  CONSTRAINT [DF_tbl_RoomType_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_RoomPrice] ADD  CONSTRAINT [DF_tbl_Room_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[tbl_User_Role] ADD  CONSTRAINT [DF_tbl_User_Role_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[HotelNews]  WITH CHECK ADD  CONSTRAINT [FK_HotelNews_NewsTypes] FOREIGN KEY([NewsTypeID])
REFERENCES [dbo].[NewsTypes] ([NewsTypeID])
GO
ALTER TABLE [dbo].[HotelNews] CHECK CONSTRAINT [FK_HotelNews_NewsTypes]
GO
ALTER TABLE [dbo].[tbl_Area]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Area_tbl_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[tbl_District] ([DistrictId])
GO
ALTER TABLE [dbo].[tbl_Area] CHECK CONSTRAINT [FK_tbl_Area_tbl_District]
GO
ALTER TABLE [dbo].[tbl_Coupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Coupon_tbl_CouponType] FOREIGN KEY([Type])
REFERENCES [dbo].[tbl_CouponType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Coupon] CHECK CONSTRAINT [FK_tbl_Coupon_tbl_CouponType]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon] FOREIGN KEY([CouponId])
REFERENCES [dbo].[tbl_Coupon] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Coupon]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[tbl_Receipt] ([Id])
GO
ALTER TABLE [dbo].[tbl_CustomerCoupon] CHECK CONSTRAINT [FK_tbl_CustomerCoupon_tbl_Receipt]
GO
ALTER TABLE [dbo].[tbl_District]  WITH CHECK ADD  CONSTRAINT [FK_tbl_District_tbl_Province] FOREIGN KEY([ProvinceId])
REFERENCES [dbo].[tbl_Province] ([ProvinceId])
GO
ALTER TABLE [dbo].[tbl_District] CHECK CONSTRAINT [FK_tbl_District_tbl_Province]
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelCancellationPolicy] CHECK CONSTRAINT [FK_tbl_HotelCancellationPolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Customer]
GO
ALTER TABLE [dbo].[tbl_HotelComment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelComment] CHECK CONSTRAINT [FK_tbl_HotelComment_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelNotice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelNotice] CHECK CONSTRAINT [FK_tbl_HotelNotice_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy]  WITH CHECK ADD  CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_HotelSurchargePolicy] CHECK CONSTRAINT [FK_tbl_HotelSurchargePolicy_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_Province]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Province_tbl_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[tbl_Country] ([CountryId])
GO
ALTER TABLE [dbo].[tbl_Province] CHECK CONSTRAINT [FK_tbl_Province_tbl_Country]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_Booking] FOREIGN KEY([BookingId])
REFERENCES [dbo].[tbl_Booking2] ([BookingId])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_Booking]
GO
ALTER TABLE [dbo].[tbl_Receipt]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType] FOREIGN KEY([PaymentType])
REFERENCES [dbo].[tbl_PaymentType] ([Id])
GO
ALTER TABLE [dbo].[tbl_Receipt] CHECK CONSTRAINT [FK_tbl_Receipt_tbl_PaymentType]
GO
ALTER TABLE [dbo].[tbl_Room]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Room_tbl_Hotel] FOREIGN KEY([HotelId])
REFERENCES [dbo].[tbl_Hotel] ([Id])
GO
ALTER TABLE [dbo].[tbl_Room] CHECK CONSTRAINT [FK_tbl_Room_tbl_Hotel]
GO
ALTER TABLE [dbo].[tbl_RoomPrice]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RoomPrice_tbl_Room] FOREIGN KEY([RoomId])
REFERENCES [dbo].[tbl_Room] ([Id])
GO
ALTER TABLE [dbo].[tbl_RoomPrice] CHECK CONSTRAINT [FK_tbl_RoomPrice_tbl_Room]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_Role]
GO
ALTER TABLE [dbo].[tbl_User_Role]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_Role_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO
ALTER TABLE [dbo].[tbl_User_Role] CHECK CONSTRAINT [FK_tbl_User_Role_tbl_User]
GO
/****** Object:  StoredProcedure [dbo].[sp_hotel_list]    Script Date: 1/8/2021 6:05:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Datnt>
-- Create date: <5/6/2021>
-- Description:	<Lấy danh sách hotel ở trang Danh sách hoặc trang Detail (check biến @typeInput chuyền vào)>
-- =============================================
CREATE PROCEDURE [dbo].[sp_hotel_list]
	-- Add the parameters for the stored procedure here
	@typeInput INT = NUll, -- 0: Hotel; 1: Province; 
	@itemId nvarchar(255) = NULL, 
	@startDate datetime = NULL,
	@endDate datetime = NULL,
	@numberRoom int = NULL,
	@numberAdul INT = NULL,
	@numberChildren INT = NULL,
	@priceFrom float = NULL,
	@priceTo float = NULL,
	@hotelPriceType NVARCHAR(100) = NULL,
	@hotelRatingType NVARCHAR(100) = NULL,
	@hotelType INT = NULL,
	@hotelServiceType nvarchar(255) = NULL,
	@hotelConvenientType nvarchar(255) = NULL,
	@hotelArea INT = NULL,
	@orderPrice INT = NULL, -- 0: Tang : giam
	@orderRating INT = NULL -- 0: Tang : giam
AS
BEGIN
	SET NOCOUNT ON;

	

	SELECT ROW_NUMBER () OVER (ORDER BY vrp.DateCreate DESC) STT
		  ,vrp.Id
		  ,vrp.RoomId
		  ,vrp.HotelId
		  ,vrp.RoomDate
		  ,vrp.AvailableRoom
		  ,vrp.HotelPriceTypeId
		  ,vrp.PriceContract
		  ,vrp.PricePromotion
		  ,vrp.PercentPromotion
		  ,vrp.HotelServiceType
		  ,vrp.IsCancel
		  ,vrp.IsSurcharge
		  ,vrp.IsPromotion
		  --,vrp.HotelConvenientType
		  ,vrp.IsDelete
		  ,vrp.Tax
		  ,vrp.TransactionCosts
		  ,vrp.UserCreate
		  ,vrp.DateCreate
		  ,vrp.UserUpdate
		  ,vrp.DateUpdate
		  ,vrp.RoomName
		  ,vrp.HotelName
		  ,vrp.ProvinceId
		  ,vrp.DistrictId
		  ,vrp.AreaId
		  ,vrp.Address
		  ,vrp.Latitude
		  ,vrp.Longitude
		  ,vrp.Content
		  ,vrp.Image
		  ,vrp.ImageRoom
		  ,vrp.RoomArea
		  ,vrp.HotelConvenientTypeHotel
		  ,vrp.HotelRatingTypeId
		  ,vrp.NumberStar
		  ,vrp.HotelTypeId
		  ,vrp.Direction
		  --,vrp.DirectionName
		  ,vrp.SingleBed
		  ,vrp.DoubleBed
		  ,vrp.MaxPeople
		  ,vrp.HotelConvenientTypeRoom
		INTO #KhachSanGia
		FROM vw_roomprice_profile vrp 
		WHERE vrp.IsDelete <> 1

	IF(@typeInput = 0)
	BEGIN
		SELECT
			temp.RoomId
			,temp.HotelId
			,temp.AvailableRoom
			,temp.IsCancel
			,temp.HotelServiceType
			,temp.HotelConvenientTypeHotel
			,temp.RoomName
			,temp.HotelName
			,temp.Address
			,temp.Latitude
			,temp.Longitude
			,temp.Content
			,temp.Image
			,temp.ImageRoom
			,temp.Direction
			--,temp.DirectionName
			,temp.SingleBed
			,temp.DoubleBed
			,temp.MaxPeople
			,temp.RoomArea
			,(SUM(temp.PriceContract)/COUNT(temp.Id)) AS PriceContract
			,(SUM(temp.PricePromotion)/COUNT(temp.Id)) AS PricePromotion
		FROM #KhachSanGia temp
		WHERE temp.HotelId = @itemId
		AND temp.RoomDate >= @startDate AND temp.RoomDate < @endDate
		GROUP BY RoomId, HotelId, HotelName, RoomName, temp.AvailableRoom, temp.IsCancel, temp.HotelServiceType, temp.HotelConvenientTypeHotel, temp.Address, temp.Latitude, temp.Longitude, temp.Content, temp.Image, temp.Direction, temp.SingleBed, temp.DoubleBed, temp.MaxPeople, temp.ImageRoom, temp.RoomArea
	END
	ELSE IF(@typeInput = 1)
	BEGIN
		SELECT
			temp.RoomId
			,temp.HotelId
			,temp.HotelRatingTypeId
			,temp.NumberStar
			,temp.HotelTypeId
			,temp.AvailableRoom
			,temp.IsCancel
			,temp.HotelServiceType
			,temp.HotelConvenientTypeHotel
			,temp.RoomName
			,temp.HotelName
			,temp.Address
			,temp.Latitude
			,temp.Longitude
			,temp.Content
			,temp.Image
			,temp.Direction
			--,temp.DirectionName
			,temp.SingleBed
			,temp.DoubleBed
			,temp.MaxPeople
			,(SUM(temp.PriceContract)/COUNT(temp.Id)) AS PriceContract
			,(SUM(temp.PricePromotion)/COUNT(temp.Id)) AS PricePromotion
		INTO #tableRusult
		FROM #KhachSanGia temp 
		WHERE (@itemId IS NULL OR temp.ProvinceId = @itemId) 
		AND temp.RoomDate >= @startDate AND temp.RoomDate < @endDate
		AND (@hotelPriceType IS NULL OR temp.HotelPriceTypeId IN (SELECT value FROM STRING_SPLIT(@hotelPriceType, ',')))
		AND (@hotelRatingType IS NULL OR temp.HotelRatingTypeId IN (SELECT value FROM STRING_SPLIT(@hotelRatingType, ',')))
		AND (@hotelType IS NULL OR temp.HotelTypeId = @hotelType)
		AND (@hotelServiceType IS NULL OR temp.HotelServiceType IN (SELECT value FROM STRING_SPLIT(@hotelServiceType, ',')))
		AND (@hotelConvenientType IS NULL OR temp.HotelConvenientTypeHotel IN (SELECT value FROM STRING_SPLIT(@hotelConvenientType, ',')))
		AND (@hotelArea IS NULL OR temp.AreaId = CAST(@hotelArea as nvarchar(255)))
		AND (@priceFrom IS NULL OR temp.PriceContract >= @priceFrom)
		AND (@priceTo IS NULL OR temp.PriceContract <= @priceTo)
		GROUP BY RoomId, HotelId, temp.HotelRatingTypeId, temp.HotelTypeId, HotelName, RoomName, temp.AvailableRoom, temp.IsCancel, temp.HotelServiceType, temp.HotelConvenientTypeHotel, temp.Address, temp.Latitude, temp.Longitude, temp.Content, temp.Image, temp.Direction, temp.SingleBed, temp.DoubleBed, temp.MaxPeople, temp.HotelRatingTypeId, temp.NumberStar--, temp.DirectionName

		--Lay ra record co gia thap nhat cua moi 1 khach san
		SELECT * FROM #tableRusult t1
		INNER JOIN (
			SELECT HotelId, MIN(PriceContract) AS MinV 
			FROM #tableRusult 
			GROUP BY HotelId
		) t2 ON t1.HotelId = t2.HotelId AND t1.PriceContract = t2.MinV
		DROP TABLE #tableRusult
	END

	DROP TABLE #KhachSanGia
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_hotel_review]    Script Date: 1/8/2021 6:05:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Datnt>
-- Create date: <19/06/2021>
-- Description:	<Lấy ra đánh giá chung bình của một khách sạn>
-- Tra ra 6 record trong do lan luoc la: TatCa; CongTac; CapDoi; GiaDinh; BanBe; CaNhan
-- =============================================
CREATE PROCEDURE [dbo].[sp_hotel_review]
	@id nvarchar(255) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	SELECT COUNT(*) AS NumAll
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 0) AS NumCongTac 
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 1) AS NumCapDoi 
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 2) AS NumGiaDinh 
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 3) AS NumBanBe
	, (SELECT COUNT(*) FROM tbl_HotelComment WHERE HotelId = @id AND Type = 4) AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM [Combo].[dbo].[tbl_HotelComment] WHERE HotelId = @id
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM [Combo].[dbo].[tbl_HotelComment] WHERE HotelId = @id AND Type = 0
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM [Combo].[dbo].[tbl_HotelComment] WHERE HotelId = @id AND Type = 1
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM [Combo].[dbo].[tbl_HotelComment] WHERE HotelId = @id AND Type = 2
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM [Combo].[dbo].[tbl_HotelComment] WHERE HotelId = @id AND Type = 3
  UNION ALL
  SELECT COUNT(*) AS NumAll
	, 0 AS NumCongTac 
	, 0 AS NumCapDoi 
	, 0 AS NumGiaDinh 
	, 0 AS NumBanBe
	, 0 AS NumCaNhan
	, AVG(PointLocation) AS PointLocation
	, AVG(PointServe) AS PointServe
	, AVG(PointConvenient) AS PointConvenient
	, AVG(PointCost) AS PointCost
	, AVG(PointClean) AS PointClean
  FROM [Combo].[dbo].[tbl_HotelComment] WHERE HotelId = @id AND Type = 4
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Thời gian huỷ sau khi đặt booking' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Hotel', @level2type=N'COLUMN',@level2name=N'CancelTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Công tác; 1: Cặp đôi; 2: Gia Đình; 3: Ban Be; 4: Cá nhân' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_HotelComment', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Sảnh lễ tân; 2: Ảnh ngoại cảnh; 3: Ảnh khác; 4: Ảnh Phòng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ProductImage', @level2type=N'COLUMN',@level2name=N'Type'
GO
USE [master]
GO
ALTER DATABASE [Combo] SET  READ_WRITE 
GO
