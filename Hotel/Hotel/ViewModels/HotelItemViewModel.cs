﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.ViewModels
{
    public class HotelItemViewModel
    {
        public Guid Id { get; set; }
        public string HotelName { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string AreaId { get; set; }
        public int? HotelRatingTypeId { get; set; }
        public int? HotelTypeId { get; set; }
        public string HotelConvenientType { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Content { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public int? IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public Guid? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public double? PriceFrom { get; set; }
        public double? PricePromotion { get; set; }
        public int? CancelTime { get; set; }
        public List<ConvenientTypeViewModel> ConvenientTypeList { get; set; }
    }
}