﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.ViewModels
{
    public class HotelCommentViewModel
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Content { get; set; }
        public DateTime DateTimeComment { get; set; }
        public double? PointLocation { get; set; }
        public double? PointServe { get; set; }
        public double? PointConvenient { get; set; }
        public double? PointCost { get; set; }
        public double? PointClean { get; set; }
        public string PointLocationText { get; set; }
        public string PointServeText { get; set; }
        public string PointConvenientText { get; set; }
        public string PointCostText { get; set; }
        public string PointCleanText { get; set; }
        public double? PointAvg { get; set; }
        public string PointAvgText { get; set; }
        public string RankText { get; set; }
        public int Type { get; set; }
        public string TypeIcon { get; set; }
        public bool IsDelete { get; set; }
        public Guid? UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public Guid? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
    }
}