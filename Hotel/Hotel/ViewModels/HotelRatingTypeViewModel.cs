﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.ViewModels
{
    public class HotelRatingTypeViewModel
    {
        public int Id { get; set; }
        public int NumberStar { get; set; }
        public string Name { get; set; }
        public string IsCheck { get; set; }
        public bool IsDelete { get; set; }
        public Guid? UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public Guid? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
    }
}