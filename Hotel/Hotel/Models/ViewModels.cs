﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Hotel.EntityFramework.Models;
using Hotel.Utilities;

namespace Hotel.Models
{
    public class ViewModels
    {
    }
    public class NewsListViewModel
    {
        public NewsListViewModel()
        {
            DateTime date = DateTime.Now;
            DateTime fDate = new DateTime(date.Year, date.Month, 1).AddMonths(1);
            DateTime tDate = fDate.AddMonths(-6);

            CrtDateFrom = new DateTime(date.Year, date.Month, 1);
            CrtDateTo = tDate;
        }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CrtDateFrom { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CrtDateTo { get; set; }
        public int NewsTypeID { get; set; }
        public String Title { get; set; }
        public String Brief { get; set; }
        public String Source { get; set; }

        public List<HotelNews> News { get; set; }
    }

    public class HotelViewModel
    {
        public Guid Id { get; set; }
        public string HotelName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string AreaId { get; set; }
        public int? HotelRatingTypeId { get; set; }
        public int? HotelTypeId { get; set; }
        public string HotelConvenientType { get; set; }
        public double? PriceFrom { get; set; }
        public double? PricePromotion { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Content { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public int? IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public Guid? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
        //Thời Gian huỷ sau khi book tour (Tính bằng giờ)
        public int? CancelTime { get; set; }

        public List<RoomViewModel> Rooms { get; set; }
        public List<HotelNoticeViewModel> HotelNotices { get; set; }
        public List<HotelSurchargePolicyViewModel> HotelSurchargePolicys { get; set; }

        public string ContentNotice { get; set; }
        public string ConditionSurcharge { get; set; }
        public double? AmountSurcharge { get; set; }
    }

    public class RoomViewModel
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public string HotelName { get; set; }
        public string RoomName { get; set; }
        public int AvailableRoom { get; set; }
        public double? RoomArea { get; set; }
        public string Direction { get; set; }
        public string DirectionName { get; set; }
        public int? SingleBed { get; set; }
        public int? DoubleBed { get; set; }
        public int? MaxPeople { get; set; }
        public string HotelConvenientType { get; set; }
        public string Image { get; set; }
        public string HotelServiceType { get; set; }
        public Nullable<bool> IsCancel { get; set; }
        public Nullable<bool> IsSurcharge { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public Guid? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
    }

    public class HotelNoticeViewModel
    {
        public int Id { get; set; }
        public Guid HotelId { get; set; }
        public string HotelName { get; set; }
        public string Content { get; set; }
        public bool IsDelete { get; set; }
        public Guid? UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public Guid? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
    }

    public class HotelSurchargePolicyViewModel
    {
        public int Id { get; set; }
        public string Condition { get; set; }
        public Guid HotelId { get; set; }
        public string HotelName { get; set; }
        public double? Amount { get; set; }
        public bool IsDelete { get; set; }
        public Guid? UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public Guid? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
    }

    public class RoomPrice
    {
        public RoomPrice()
        {

        }

        public System.Guid Id { get; set; }
        public System.Guid RoomId { get; set; }
        public System.Guid HotelId { get; set; }

        public string DateType { get; set; }
        public string DateList { get; set; }
        public System.DateTime? DateFrom { get; set; }

        public System.DateTime? DateTo { get; set; }
        public System.DateTime RoomDate { get; set; }
        public Nullable<int> AvailableRoom { get; set; }
        public Nullable<int> HotelPriceTypeId { get; set; }
        public double PriceContract { get; set; }
        public Nullable<double> PricePromotion { get; set; }
        public Nullable<double> PercentPromotion { get; set; }
        public string HotelServiceType { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsSurcharge { get; set; }
        public Nullable<bool> IsPromotion { get; set; }
        public string HotelConvenientType { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<double> Tax { get; set; }
        public Nullable<double> TransactionCosts { get; set; }
        public Nullable<System.Guid> UserCreate { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.Guid> UserUpdate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
    }
    public partial class CalendarDataView
    {
        public string title { get; set; }
        public string description { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
    }

    public class SearchViewModel
    {
        public SearchViewModel()
        {
            StartDate = DateTime.Today.AddDays(1);
            EndDate = DateTime.Today.AddDays(2);
            NumberRoom = 1;
            NumberAdul = 2;
            NumberChildren = 0;
        }
        public int TypeInput { get; set; }
        public string TypeInputText { get; set; }
        //public string Id { get; set; } //KeyWork
        public string ItemId { get; set; }
        public string Keyword { get; set; } //KeyWork
        //public string Area { get; set; } //khu vuc
        public DateTime? StartDate { get; set; } //Ngay den
        public DateTime? EndDate { get; set; } //Ngay ve
        public int? NumberRoom { get; set; } //So luong phong
        public int? NumberAdul { get; set; } //So luong nguoi lon
        public int? NumberChildren { get; set; } //So luong tre em
        public double? PriceFrom { get; set; }//Gia tu 
        public double? PriceTo { get; set; }//Gia đến
        public string HotelPriceType { get; set; } //HotelPriceType: Loại giá ưu đãi
        public string HotelRatingType { get; set; } //HotelRatingType: Hang Khach San(sao)
        public int? HotelType { get; set; }//HotelType: Loại hình khách sạn
        public string HotelServiceType { get; set; } //HotelServiceType: dịch vụ đính kèm
        public string HotelConvenientType { get; set; } //HotelConvenientType: Tiện nghi khách sạn
        public int? HotelArea { get; set; } //HotelArea: Khu vuc
        public int? OrderPrice { get; set; } //OrderPrice: 0: null; 1: Giam dan; 2: Tang dan
        public int? OrderRating { get; set; } //OrderRating: 0: null; 1: Giam dan; 2: Tang dan

        public string GetSEOFriendlyURL()
        {
            string phrase = "";
            // string.Format("&startdate={0}&enddate={1}&numberroom={2}&numberadul={3}&numberchildren={4}", StartDate, EndDate, NumberRoom, NumberAdul, NumberChildren);

            if (StartDate != null)
            {
                phrase += string.Format("&startdate={0}", StartDate.Value.ToString("dd-MM-yyyy"));
            }
            if (EndDate != null)
            {
                phrase += string.Format("&enddate={0}", EndDate.Value.ToString("dd-MM-yyyy"));
            }
            if (NumberRoom != null)
            {
                phrase += string.Format("&numberroom={0}", NumberRoom);
            }
            if (NumberAdul != null)
            {
                phrase += string.Format("&numberadul={0}", NumberAdul);
            }
            if (NumberChildren != null)
            {
                phrase += string.Format("&numberchildren={0}", NumberChildren);
            }
            if (!string.IsNullOrEmpty(phrase))
            {
                if (phrase[0] == '&')
                {
                    return phrase.Remove(0, 1);
                }
            }

            return Utility.CleanUrlParameters(phrase);
        }

    }

    public class CustomerViewModel
    {
        public CustomerViewModel()
        {
            CustomerId = Guid.Empty;
        }
        public Guid? CustomerId { get; set; }
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Vui lòng nhập đúng định dạng Email")]
        [StringLength(250, MinimumLength = 1, ErrorMessage = "Vui lòng nhập Email")]
        public string CustomerEmail { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Vui lòng nhập đúng định dạng")]
        [StringLength(15, MinimumLength = 1, ErrorMessage = "Vui lòng nhập Số điện thoại")]
        public string CustomerPhoneNumber { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Vui lòng nhập Tên khách hàng")]
        public string CustomerName { get; set; }
        public int? CustomerProvinceId { get; set; }
        public string CustomerProvinceName { get; set; }
        public int? CustomerDistrictId { get; set; }
        public string CustomerDistrictName { get; set; }
        public string CustomerAddress { get; set; }
        public double? TotalPoint { get; set; }
    }

    public class InvoceExport
    {
        public string CompanyName { get; set; }
        public string TaxCode { get; set; }
        public string CompanyAddress { get; set; }
        public string BillingAddress { get; set; }
    }

    #region Booking
    public class BookingViewModel
    {
        public BookingViewModel()
        {
            Hotel = new HotelViewModel();
            Room = new RoomViewModel();
            RoomPrices = new List<RoomPrice>();
            IsInvoceExport = false;
            InvoceInfo = new InvoceExport();
            CustomerInfo = new CustomerViewModel();
        }

        public Guid BookingId { get; set; }
        public string BookingCode { get; set; }
        public HotelViewModel Hotel { get; set; }
        public RoomViewModel Room { get; set; }
        public List<RoomPrice> RoomPrices { get; set; }
        public DateTime DateCheckIn { get; set; }
        public DateTime DateCheckOut { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime CODCancel { get; set; }
        public int CountDate { get; set; }
        public int RoomCount { get; set; }
        public int PersonCount { get; set; }
        public double Tax { get; set; }
        public double? Surcharge { get; set; }
        public double? Penalty { get; set; }
        public double RoomAmount { get; set; }
        public double Discount { get; set; }
        public double TotalAmount { get; set; }
        public double? NetPrice { get; set; }
        public int Status { get; set; }
        public int PaymentType { get; set; }
        public bool IsInvoceExport { get; set; }
        public InvoceExport InvoceInfo { get; set; }
        public CustomerViewModel CustomerInfo { get; set; }
        public string CustomerNote { get; set; }
        public bool CustomerCheckPayment { get; set; }
        public bool IsPaymentSuccess { get; set; }
        public bool IsDelete { get; set; }
        public double? TotalPointUse { get; set; }
        public bool IsTotalPointUse { get; set; }
        public void ConvertDtoToViewModel(tbl_Hotel hotelDb, tbl_Room roomDb)
        {
            var _db = new HotelEntities();
            Hotel = new HotelViewModel()
            {
                Id = hotelDb.Id,
                HotelName = hotelDb.HotelName,
                CountryId = hotelDb.CountryId,
                CountryName = _db.tbl_Country.FirstOrDefault(d => d.CountryId == hotelDb.CountryId).CountryName,
                ProvinceId = hotelDb.ProvinceId,
                ProvinceName = _db.tbl_Province.FirstOrDefault(d => d.ProvinceId == hotelDb.ProvinceId).ProvinceName,
                DistrictId = hotelDb.DistrictId,
                DistrictName = _db.tbl_District.FirstOrDefault(d => d.DistrictId == hotelDb.DistrictId).DistrictName,
                Rooms = hotelDb.tbl_Room.Select(d => new ConvertDbToModel().ToRoom(d)).ToList(),
                Address = hotelDb.Address,
                AreaId = hotelDb.AreaId,
                Content = hotelDb.Content,
                Email = hotelDb.Email,
                HotelConvenientType = hotelDb.HotelConvenientType,
                HotelRatingTypeId = hotelDb.HotelRatingTypeId,
                HotelTypeId = hotelDb.HotelTypeId,
                Image = hotelDb.Image,
                IsActive = hotelDb.IsActive,
                IsDelete = hotelDb.IsDelete,
                Latitude = hotelDb.Latitude,
                Longitude = hotelDb.Longitude,
                Mobile = Hotel.Mobile,
                PriceFrom = hotelDb.PriceFrom,
                PricePromotion = hotelDb.PricePromotion,
                Telephone = hotelDb.Telephone,
                Url = hotelDb.Url,
                DateCreate = hotelDb.DateCreate,
                DateUpdate = hotelDb.DateUpdate,
                UserCreate = hotelDb.UserCreate,
                UserUpdate = hotelDb.UserUpdate
            };
        }

    }

    public class BookingReportViewModel
    {
        public Guid BookingId { get; set; }
        public string BookingCode { get; set; }
        public Guid HotelId { get; set; }
        public string HotelName { get; set; }
        public Guid RoomId { get; set; }
        public string RoomName { get; set; }
        public DateTime DateCheckIn { get; set; }
        public DateTime DateCheckOut { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime CODCancel { get; set; }
        public int CountDate { get; set; }
        public int RoomCount { get; set; }
        public int PersonCount { get; set; }
        public double? Tax { get; set; }
        public double? Surcharge { get; set; }
        public double? Penalty { get; set; }
        public double RoomAmount { get; set; }
        public double Discount { get; set; }
        public double TotalAmount { get; set; }
        public int Status { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public bool CustomerCheckPayment { get; set; }
        public bool IsPaymentSuccess { get; set; }
        public bool IsDelete { get; set; }
        public Guid UserAlloted { get; set; }
        public string UserAllotedName { get; set; }
        public Guid UserConfirm { get; set; }
        public string UserConfirmName { get; set; }

        public void Init(tbl_Booking bk)
        {
            BookingId = bk.BookingId;
            BookingCode = bk.BookingCode;
            HotelId = bk.HotelId;
            RoomId = bk.RoomId;
            DateCheckIn = bk.CheckIn;
            DateCheckOut = bk.CheckOut;
            DateCreate = bk.DateCreate;
            CODCancel = bk.CODCancel;
            RoomCount = bk.RoomCount;
            PersonCount = bk.PersonCount;
            Tax = bk.Tax;
            Surcharge = bk.Surcharge;
            Penalty = bk.Penalty;
            RoomAmount = bk.RoomAmount;
            Discount = bk.Discount.GetValueOrDefault(0);
            TotalAmount = bk.TotalAmount;
            Status = bk.Status;
            CustomerId = bk.CustomerId.GetValueOrDefault(Guid.NewGuid());
            CustomerName = bk.CustomerName;
            CustomerPhone = bk.CustomerPhoneNumber;
            CustomerEmail = bk.CustomerEmail;
            CustomerCheckPayment = bk.CustomerCheckPayment;
            IsPaymentSuccess = bk.IsPaymentSuccess;
            IsDelete = bk.IsDelete;
            Guid temp;
            Guid.TryParse(bk.UserConfirm, out temp);
            UserConfirm = temp;

            //manual
            //HotelName
            //RoomName
            //UserAlloted
            //UserAllotedName
            //UserConfirmName           
        }
    }
    public class SearchBookingViewModel
    {
        public SearchBookingViewModel()
        {
            BookingCode = string.Empty;
            HotelName = string.Empty;
            CustomerName = string.Empty;
            Status = -1;
        }
        public string BookingCode { get; set; }
        public string HotelName { get; set; }
        public int HotelProvince { get; set; }
        public string CustomerName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? CODPayment { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public Guid UserAlloted { get; set; }
        public int Status { get; set; } 
    }
    #endregion

    public class ConvertDbToModel
    {
        private readonly HotelEntities _db = new HotelEntities();
        public HotelViewModel ToHotel(tbl_Hotel hotelDb, List<tbl_Room> listRoomDb)
        {
            HotelViewModel hotel = new HotelViewModel()
            {
                Id = hotelDb.Id,
                HotelName = hotelDb.HotelName,
                CountryId = hotelDb.CountryId,
                CountryName = _db.tbl_Country.FirstOrDefault(d => d.CountryId == hotelDb.CountryId).CountryName,
                ProvinceId = hotelDb.ProvinceId,
                ProvinceName = _db.tbl_Province.FirstOrDefault(d => d.ProvinceId == hotelDb.ProvinceId).ProvinceName,
                DistrictId = hotelDb.DistrictId,
                DistrictName = _db.tbl_District.FirstOrDefault(d => d.DistrictId == hotelDb.DistrictId).DistrictName,
                Address = hotelDb.Address,
                AreaId = hotelDb.AreaId,
                CancelTime = hotelDb.CancelTime,
                Content = hotelDb.Content,
                Email = hotelDb.Email,
                HotelConvenientType = hotelDb.HotelConvenientType,
                HotelRatingTypeId = hotelDb.HotelRatingTypeId,
                HotelTypeId = hotelDb.HotelTypeId,
                Image = hotelDb.Image,
                IsActive = hotelDb.IsActive,
                IsDelete = hotelDb.IsDelete,
                Latitude = hotelDb.Latitude,
                Longitude = hotelDb.Longitude,
                Mobile = hotelDb.Mobile,
                PriceFrom = hotelDb.PriceFrom,
                PricePromotion = hotelDb.PricePromotion,
                Telephone = hotelDb.Telephone,
                Url = hotelDb.Url,
                DateCreate = hotelDb.DateCreate,
                DateUpdate = hotelDb.DateUpdate,
                UserCreate = hotelDb.UserCreate,
                UserUpdate = hotelDb.UserUpdate
            };
            hotel.Rooms = listRoomDb.Select(d => new ConvertDbToModel().ToRoom(d)).ToList();
            return hotel;
        }

        public RoomViewModel ToRoom (tbl_Room roomDb)
        {
            RoomViewModel room = new RoomViewModel()
            {
                Id = roomDb.Id,
                HotelId = roomDb.HotelId,
                HotelName = _db.tbl_Hotel.FirstOrDefault(d => d.Id == roomDb.HotelId).HotelName,
                RoomName = roomDb.RoomName,
                AvailableRoom = roomDb.AvailableRoom,
                RoomArea = roomDb.RoomArea,
                Direction = roomDb.Direction,
                //DirectionName = _db.tbl_Direction.FirstOrDefault(d => d.Id == roomDb.Direction).DirectionName,
                SingleBed = roomDb.SingleBed,
                DoubleBed = roomDb.DoubleBed,
                MaxPeople = roomDb.MaxPeople,
                HotelConvenientType = roomDb.HotelConvenientType,
                Image = roomDb.Image,
                HotelServiceType = roomDb.HotelServiceType,
                IsCancel = roomDb.IsCancel,
                IsSurcharge = roomDb.IsSurcharge,
                IsDelete = roomDb.IsDelete,
                UserCreate = roomDb.UserCreate,
                DateCreate = roomDb.DateCreate,
                UserUpdate = roomDb.UserUpdate,
                DateUpdate = roomDb.DateUpdate,
            };
            return room;
        }

        public RoomPrice ToRoomPrice (tbl_RoomPrice roomPriceDb)
        {
            RoomPrice price = new RoomPrice()
            {
                Id = roomPriceDb.Id,
                RoomId = roomPriceDb.RoomId,
                HotelId = roomPriceDb.HotelId,
                RoomDate = roomPriceDb.RoomDate,
                AvailableRoom = roomPriceDb.AvailableRoom,
                HotelPriceTypeId = roomPriceDb.HotelPriceTypeId,
                PriceContract = roomPriceDb.PriceContract,
                PricePromotion = roomPriceDb.PricePromotion,
                PercentPromotion = roomPriceDb.PercentPromotion,
                IsActive = roomPriceDb.IsActive,
                IsPromotion = roomPriceDb.IsPromotion,
                HotelConvenientType = roomPriceDb.HotelConvenientType,
                IsDelete = roomPriceDb.IsDelete,
                Tax = roomPriceDb.Tax,
                TransactionCosts = roomPriceDb.TransactionCosts,
                UserCreate = roomPriceDb.UserCreate,
                DateCreate = roomPriceDb.DateCreate,
                UserUpdate = roomPriceDb.UserUpdate,
                DateUpdate = roomPriceDb.DateUpdate,
            };
            return price;
        }

        public BookingViewModel ToBooking (tbl_Booking bookingDb, tbl_Hotel hotelDb)
        {
            BookingViewModel booking = new BookingViewModel() {
                BookingId = bookingDb.BookingId,
                BookingCode = bookingDb.BookingCode,
                DateCheckIn = bookingDb.CheckIn,
                DateCheckOut = bookingDb.CheckOut,
                DateCreate = bookingDb.DateCreate,
                CODCancel = bookingDb.CODCancel,
                CountDate = bookingDb.CountDate,
                RoomCount = bookingDb.RoomCount,
                PersonCount = bookingDb.PersonCount,
                Tax = bookingDb.Tax.Value,
                Surcharge = bookingDb.Surcharge,
                Penalty = bookingDb.Penalty,
                RoomAmount = bookingDb.RoomAmount,
                TotalAmount = bookingDb.TotalAmount,
                NetPrice = bookingDb.NetPrice,
                PaymentType = bookingDb.PaymentType,
                CustomerCheckPayment = bookingDb.CustomerCheckPayment,
                IsPaymentSuccess = bookingDb.IsPaymentSuccess,
                IsInvoceExport = bookingDb.IsInvoceExport,
                Discount = (double)bookingDb.Discount.GetValueOrDefault(0),

                Hotel = new ConvertDbToModel().ToHotel(hotelDb, hotelDb.tbl_Room.ToList()),
                Room = new ConvertDbToModel().ToRoom(hotelDb.tbl_Room.FirstOrDefault(d => d.Id == bookingDb.RoomId)),
                RoomPrices = hotelDb.tbl_Room.FirstOrDefault(d => d.Id == bookingDb.RoomId).tbl_RoomPrice
                                .Where(d => d.RoomDate >= bookingDb.CheckIn && d.RoomDate < bookingDb.CheckOut).OrderBy(d=>d.RoomDate)
                                .Select(s => new ConvertDbToModel().ToRoomPrice(s)).ToList(),
                CustomerInfo = new CustomerViewModel()
                {
                    CustomerId = bookingDb.CustomerId,
                    CustomerEmail = bookingDb.CustomerEmail,
                    CustomerPhoneNumber = bookingDb.CustomerPhoneNumber,
                    CustomerName = bookingDb.CustomerName,
                    CustomerProvinceId = bookingDb.CustomerProvinceId,
                    CustomerProvinceName = _db.tbl_Province.Where(d=>d.ProvinceId == bookingDb.CustomerProvinceId).Select(d=>d.ProvinceName).FirstOrDefault(),
                    CustomerDistrictId = bookingDb.CustomerDistrictId,
                    CustomerDistrictName = _db.tbl_District.Where(d => d.DistrictId == bookingDb.CustomerDistrictId).Select(d => d.DistrictName).FirstOrDefault(),
                    CustomerAddress = bookingDb.CustomerAddress,
                },
                
                InvoceInfo = new InvoceExport()
                {
                    CompanyName = bookingDb.CompanyName,
                    TaxCode = bookingDb.TaxCode,
                    CompanyAddress = bookingDb.CompanyAddress,
                    BillingAddress = bookingDb.BillingAddress,
                },
                CustomerNote = bookingDb.CustomerNote,
                Status = bookingDb.Status,
                IsDelete = bookingDb.IsDelete,
            };
            return booking;
        }
    }
    public class ConvertModelToDb
    {
        public tbl_Booking ToBookingDb(BookingViewModel booking)
        {
            tbl_Booking bookingDb = new tbl_Booking()
            {
                BookingId = booking.BookingId,
                BookingCode = booking.BookingCode,
                HotelId = booking.Hotel.Id,
                RoomId = booking.Room.Id,
                CheckIn = booking.DateCheckIn,
                CheckOut = booking.DateCheckOut,
                DateCreate = booking.DateCreate,
                CODCancel = booking.CODCancel,
                CountDate = booking.CountDate,
                RoomCount = booking.RoomCount,
                PersonCount = booking.PersonCount,
                CustomerNote = booking.CustomerNote,
                Tax = booking.Tax,
                Surcharge = booking.Surcharge,
                Penalty = booking.Penalty,
                RoomAmount = booking.RoomAmount,
                TotalAmount = booking.TotalAmount,
                PaymentType = booking.PaymentType,
                CustomerCheckPayment = booking.CustomerCheckPayment,
                IsPaymentSuccess = booking.IsPaymentSuccess,
                IsInvoceExport = booking.IsInvoceExport,
                CustomerId = booking.CustomerInfo.CustomerId,
                CustomerEmail = booking.CustomerInfo.CustomerEmail,
                CustomerPhoneNumber = booking.CustomerInfo.CustomerPhoneNumber,
                CustomerName = booking.CustomerInfo.CustomerName,
                CustomerProvinceId = booking.CustomerInfo.CustomerProvinceId,
                CustomerDistrictId = booking.CustomerInfo.CustomerDistrictId,
                CustomerAddress =booking.CustomerInfo.CustomerAddress,
                CompanyName = booking.InvoceInfo.CompanyName,
                TaxCode = booking.InvoceInfo.TaxCode,
                CompanyAddress = booking.InvoceInfo.CompanyAddress,
                BillingAddress = booking.InvoceInfo.BillingAddress,
                Status = booking.Status,
                IsDelete = booking.IsDelete,        
                Discount = booking.Discount,
            };


            return bookingDb;
        }
    }
    public class SearchHotelSuggestModel
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}