﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class HotelCommentModel
    {
        public System.Guid Id { get; set; }
        public System.Guid HotelId { get; set; }
        public System.Guid CustomerId { get; set; }
        public string Content { get; set; }
        public System.DateTime DateTimeComment { get; set; }
        public Nullable<double> PointLocation { get; set; }
        public Nullable<double> PointServe { get; set; }
        public Nullable<double> PointConvenient { get; set; }
        public Nullable<double> PointCost { get; set; }
        public Nullable<double> PointClean { get; set; }
        public int Type { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<System.Guid> UserCreate { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.Guid> UserUpdate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public string HotelName { get; set; }
        public string CustomerName { get; set; }
    }
}