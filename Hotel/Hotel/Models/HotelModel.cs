﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    
    public class HotelModel
    {
        public Guid RoomId { get; set; }
        public Guid HotelId { get; set; }
        public int? AvailableRoom { get; set; }
        public bool IsCancel { get; set; }
        public string HotelServiceType { get; set; }
        public string HotelConvenientTypeHotel { get; set; }
        public string RoomName { get; set; }
        public string HotelName { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
        public string ImageRoom { get; set; }
        public string Direction { get; set; }
        public string DirectionName { get; set; }
        public int? SingleBed { get; set; }
        public int? DoubleBed { get; set; }
        public int? MaxPeople { get; set; }
        public int? HotelRatingTypeId { get; set; }
        public string NumberStar { get; set; }
        public double PriceContract { get; set; }
        public double PricePromotion { get; set; }
        public double RoomArea { get; set; }
    }
}