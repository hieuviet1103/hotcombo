﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class CommentModel
    {
        public int NumAll { get; set; }
        public int NumCongTac { get; set; }
        public int NumCapDoi { get; set; }
        public int NumGiaDinh { get; set; }
        public int NumBanBe { get; set; }
        public int NumCaNhan { get; set; }
        public double PointLocation { get; set; }
        public double PointServe { get; set; }
        public double PointConvenient { get; set; }
        public double PointCost { get; set; }
        public double PointClean { get; set; }
    }
}