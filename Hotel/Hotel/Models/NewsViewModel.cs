﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class NewsViewModel
    {
    }

    public class TravelGuideByProvince
    {
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public Guid NewsId { get; set; }
        public int NewsTypeId { get; set; }
        public string Title { get; set; }
        public string Brief { get; set; }

    }
}