﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hotel.EntityFramework.Models;

namespace Hotel.Models
{
    public class CustomerAccountViewModel
    {
        public System.Guid CustomerId { get; set; }
        public string CustomerNo { get; set; }
        public bool IsActive { get; set; }
        public string CustomerName { get; set; }
        public Nullable<System.DateTime> Dob { get; set; }
        public byte Gender { get; set; }
        public string Nationality { get; set; }
        public string IdCard { get; set; } //CMND
        public Nullable<System.DateTime> DateOfIssue { get; set; } //Ngày phát hành
        public string PlaceOfIssue { get; set; } //Nơi phát hành
        public string Address { get; set; }
        public Nullable<int> CountryId { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string ImageUrl { get; set; }
        public string Note { get; set; }
        public Nullable<double> TotalPoint { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }

        public void InitData(tbl_Customer customer)
        {
            CustomerId = customer.CustomerId;
            CustomerNo = customer.CustomerNo;
            CustomerName = customer.CustomerName;
            Email = customer.Email;
            PhoneNumber = customer.PhoneNumber;
            Dob = customer.Dob;
            Gender = customer.Gender;
            Address = customer.Address;
            CountryId = customer.CountryId;
            ProvinceId = customer.ProvinceId;
            DistrictId = customer.DistrictId;
            Address = customer.Address;
            Nationality = customer.Nationality;
            IdCard = customer.IdCard; 
            DateOfIssue = customer.DateOfIssue;
            PlaceOfIssue = customer.PlaceOfIssue;
            ImageUrl = customer.Image;
            DateCreate = customer.DateCreate;
            DateUpdate = customer.DateUpdate;
            IsActive = customer.IsActive;
            IsDelete = customer.IsDelete;
            TotalPoint = customer.TotalPoint;
        }
    }

    public class SocialMediaAccount
    {
        public SocialMediaAccount()
        {
            Gender = 0;
        }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string ImageUrl { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime BirthDay { get; set; }
        public int Age { get; set; }
        public byte Gender { get; set; }
        public string DataFrom { get; set; }
        public int TypeLogin { get; set; }
    }

    public class PhoneNumberAuthenticate 
    {
        public PhoneNumberAuthenticate()
        {

        }
        public string OTP { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? ExpDate { get; set; }
    }
}

