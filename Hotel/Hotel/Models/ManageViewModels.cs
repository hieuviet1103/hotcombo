﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using Hotel.EntityFramework.Models;

namespace Hotel.Models
{
    public class AccountUpdateViewModel
    {
        public AccountUpdateViewModel MapFrom(tbl_User itemObj)
        {
            AccountUpdateViewModel model = new Models.AccountUpdateViewModel();

            Utilities.Utility.CopyObject(itemObj, model);
            model.ConfirmPassword = model.Password;

            return model;
        }

        public tbl_User MapTo(AccountUpdateViewModel itemObj)
        {
            tbl_User model = new tbl_User();

            Utilities.Utility.CopyObject(itemObj, model);

            return model;
        }

        public System.Guid UserId { get; set; }

        //[Required(ErrorMessage = "Vui lòng chọn nhóm phân hệ")]
        //public Nullable<int> GroupApplicationId { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên đăng nhập")]
        public string LoginName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác nhận lại mật khẩu")]
        [Compare("Password", ErrorMessage = "Mật khẩu và xác nhận mật khẩu không trùng khớp")]
        public string ConfirmPassword { get; set; }

        public string FullName { get; set; }
        public byte Active { get; set; }
        public byte chpwd_allowed { get; set; }
        public byte chpwd_required { get; set; }
        public byte never_expired { get; set; }
        public System.DateTime expired_date { get; set; }
        public System.DateTime created_date { get; set; }
        public Nullable<byte> TourType { get; set; }
        public Nullable<int> NhanVienID { get; set; }
        public string Nhom { get; set; }
        public Nullable<byte> AccountType { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Địa chỉ email không hợp lệ")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Skype { get; set; }
        public string Ext { get; set; }
        public string OTP { get; set; }
        public Nullable<System.DateTime> OTP_date { get; set; }
        public string PasswordEmail { get; set; }
        public string UserEmail { get; set; }
        public string CodeStaff { get; set; }
    }

    public class ChangePassViewModel
    {
        public ChangePassViewModel MapFrom(tbl_User itemObj)
        {
            ChangePassViewModel model = new Models.ChangePassViewModel();
            model.UserId = itemObj.UserId;

            model.UserName = itemObj.LoginName;
            model.FullName = itemObj.FullName;
            model.Email = itemObj.Email;
            //model.UserEmail = itemObj.Email;
            //model.PasswordEmail = itemObj.PasswordEmail;
            //model.Phone1 = itemObj.Phone1;
            //model.Phone2 = itemObj.Phone2;
            //model.Ext = itemObj.Ext;
            //model.Skype = itemObj.Skype;
            //model.CodeStaff = itemObj.CodeStaff;
            //model.ThongTinNhanHoaDon = itemObj.ThongTinNhanHoaDon;
            return model;
        }

        public System.Guid UserId { get; set; }

        [Display(Name = "Tên đăng nhập")]
        [StringLength(50, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        public string UserName { get; set; }

        [Display(Name = "Mật khẩu cũ")]
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu để có thể thay đổi thông tin tài khoản")]
        public string OldPassword { get; set; }

        [StringLength(127, ErrorMessage = "{0} phải được dài tối thiểu {2} kí tự", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác nhận lại mật khẩu")]
        [Compare("password", ErrorMessage = "Mật khẩu mới và xác nhận mật khẩu mới không trùng khớp")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Họ và tên")]
        [StringLength(50, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        public string FullName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Địa chỉ email không hợp lệ")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(100, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        [Display(Name = "Email")]
        public string UserEmail { get; set; }

        [StringLength(50, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu email")]
        public string PasswordEmail { get; set; }

        [Display(Name = "Điện thoại 1")]
        [StringLength(20, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        public string Phone1 { get; set; }

        [Display(Name = "Điện thoại 2")]
        [StringLength(20, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        public string Phone2 { get; set; }

        [Display(Name = "Extension")]
        [StringLength(20, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        public string Ext { get; set; }

        [Display(Name = "Skype")]
        [StringLength(20, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]

        public string Skype { get; set; }
        [Display(Name = "Mã Nhân Viên")]
        [StringLength(20, ErrorMessage = "{0} chỉ được dài tối đa {1} kí tự", MinimumLength = 0)]
        public string CodeStaff { get; set; }
        [Display(Name = "Thông tin nhận hóa đơn")]
        public string ThongTinNhanHoaDon { get; set; }
    }
}