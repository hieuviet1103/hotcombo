﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Hotel.Utilities;

namespace Hotel.Utilities
{
    public class SocialService
    {
        public readonly string facebookClientId = ConfigurationManager.AppSettings["AppFacebookId"];
        public readonly string facebookClientSecretId = ConfigurationManager.AppSettings["AppSecretFacebook"];
        public readonly string googleClientId = ConfigurationManager.AppSettings["AppGoogleId"];
        public readonly string googleClientSecretId = ConfigurationManager.AppSettings["AppSecretGoogle"];

        public static async Task<GoogleProfile> GetTokenAndProfile(string code, string clientid, string clientsecret, string redirection_url)
        {
            string url = "https://accounts.google.com/o/oauth2/token";
            string poststring = "grant_type=authorization_code&code=" + code + "&client_id=" + clientid + "&client_secret=" + clientsecret + "&redirect_uri=" + redirection_url + "";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            UTF8Encoding utfenc = new UTF8Encoding();
            byte[] bytes = utfenc.GetBytes(poststring);
            Stream outputstream = null;
            try
            {
                request.ContentLength = bytes.Length;
                outputstream = request.GetRequestStream();
                outputstream.Write(bytes, 0, bytes.Length);
            }
            catch { }
            var response = (HttpWebResponse)request.GetResponse();
            var streamReader = new StreamReader(response.GetResponseStream());
            string responseFromServer = streamReader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Tokenclass obj = js.Deserialize<Tokenclass>(responseFromServer);
            return await GetuserProfile(obj.access_token);
        }
        private static async Task<GoogleProfile> GetuserProfile(string accesstoken)
        {
            try
            {
                HttpClient client = new HttpClient();
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + accesstoken;

                client.CancelPendingRequests();
                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleProfile serStatus = JsonConvert.DeserializeObject<GoogleProfile>(outputData);
                    return serStatus;


                }
                return null;
            }
            catch (Exception ex)
            {
                //catching the exception
            }
            return null;
        }
    }
}