﻿using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace System.Web
{
    public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData;
            string area = routeValues.DataTokens["area"] as string;
            if (!string.IsNullOrEmpty(area))
            {
                //Kiểm tra có check IP hay ko
                string isCheckLoginAdmin = ConfigurationManager.AppSettings["isCheckLoginAdmin"];
                if (isCheckLoginAdmin == "1")
                {

                    bool isAlow = false;
                    //Kiem tra IPAddress
                    string ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                    string IpAlowLogin = ConfigurationManager.AppSettings["IpAlowLogin"];
                    string[] lIPAlow = IpAlowLogin.Split(';');
                    foreach (string item in lIPAlow)
                    {
                        if (ipAddress == item)
                        {
                            isAlow = true;
                            break;
                        }
                    }
                    if (isAlow == false)//Không có quyền truy cập.Trở về trang chủ
                    {
                        filterContext.Result = new RedirectResult("~/");
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }

    }
}