﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Custom.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class WebMasterAttribute : ActionFilterAttribute
    {
        public String Role { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var ctx = filterContext.HttpContext;
            if (ctx.Request.IsAuthenticated)
            {
                //var db = new eOfficeEntities();
                var CurrentUser = (CustomPrincipal)HttpContext.Current.User;
                return;
                //var RoleName = db.Roles.Single(m => m.RoleId == CurrentUser).Name;

                //if ((Role == null && RoleName.Length > 0) || (Role != null && Role.Contains(RoleName)))
                //{
                //    return; // cho phép làm việc tiếp
                //}
            }
            ctx.Response.Redirect("/InternalAccount/Index");
        }
    }
}