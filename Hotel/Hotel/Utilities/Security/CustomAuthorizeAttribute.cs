﻿using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Custom.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string UsersConfigKey { get; set; }
        public string RolesConfigKey { get; set; }

        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                var authorizedUsers = ConfigurationManager.AppSettings[UsersConfigKey];
                var authorizedRoles = ConfigurationManager.AppSettings[RolesConfigKey];

                Users = String.IsNullOrEmpty(Users) ? authorizedUsers : Users;

                Roles = String.IsNullOrEmpty(Roles) ? authorizedRoles : Roles;

                bool isAccessDenied = true;

                if (!String.IsNullOrEmpty(Roles))
                {
                    List<int> listRole = CurrentUser.Roles;

                    foreach (var item in listRole)
                    {
                        var _roleId = item.ToString();
                        isAccessDenied = false;
                        var RoleName = Parameters.RoleNames().FirstOrDefault(d => d.Value == _roleId).Text;
                        if (Roles.Contains(RoleName)) //!CurrentUser.IsInRole(Roles)
                        {
                            isAccessDenied = false;
                            break;
                        }
                    }                    
                }

                if (!String.IsNullOrEmpty(Users))
                {
                    if (Users.Contains(CurrentUser.UserId.ToString()))
                    {
                        isAccessDenied = false;
                    }
                }

                if (isAccessDenied)
                {
                    filterContext.Result = new RedirectToRouteResult(new
                            RouteValueDictionary(new { controller = "Errors", action = "AccessDenied", area = "" }));
                }
            }
            else
            {
                var url = filterContext.HttpContext.Request.RawUrl;
                if (!string.IsNullOrEmpty(url))
                    filterContext.HttpContext.Response.Redirect("/InternalAccount/Index?returnUrl=" + url);
                else
                    filterContext.HttpContext.Response.Redirect("/InternalAccount/Index");
            }

        }
    }
}