﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using Hotel.Models;

namespace Custom.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            //if (roles.Any(r => role.Contains(r)))
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return true;
        }

        public CustomPrincipal(string UserName)
        {
            this.Identity = new GenericIdentity(UserName);
            UserId = Guid.Empty;
            RoleId = -1;
            UserName = "User";
            Email = "";
            ApplicationId = -1;
            DepartureId = 1;
            Roles = new List<int>();


        }

        public Guid UserId { get; set; }
        public int RoleId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int ApplicationId { get; set; }
        public int DepartureId { get; set; }
        public string CodeStaff { get; set; }
        public List<int> Applications { get; set; }
        public List<int> Roles { get; set; }


    }
    public class CustomPrincipalSerializeModel
    {
        public CustomPrincipalSerializeModel()
        {
            Applications = new List<int>();
            Roles = new List<int>();
        }

        public Guid UserId { get; set; }
        public int RoleId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int ApplicationId { get; set; }
        public int DepartureId { get; set; }
        public string CodeStaff { get; set; }
        public List<int> Applications { get; set; }
        public List<int> Roles { get; set; }
    }
}