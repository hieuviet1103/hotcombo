﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Hotel.EntityFramework.Models;

namespace Hotel.Utilities
{
    public static class Utility
    {
        public static string[] AllowedExtensions = new[] { ".png", ".jpg", ".gif", ".jpeg", ".PNG", ".JPG", ".GIF", ".JPEG", ".pdf", ".doc", ".docx", ".xls", ".xlsx" };

        #region Extention Functions

        #endregion

        #region Utilities Functions
        public static string GetConditionFromToDate(DateTime? From, DateTime? To, string Field)
        {
            return ((From != null && To != null) ? (" and (CONVERT(VARCHAR(10), " + Field + ", 111) between '" + From.Value.ToString("yyyy/MM/dd") + "' and '" +
                    To.Value.ToString("yyyy/MM/dd") + "') ") : "")
                + ((From != null && To == null) ? " and (CONVERT(VARCHAR(10), " + Field + ", 111)>='" + From.Value.ToString("yyyy/MM/dd") + "')" : "")
                + ((From == null && To != null) ? " and (CONVERT(VARCHAR(10), " + Field + ", 111)<='" + To.Value.ToString("yyyy/MM/dd") + "')" : "");
        }
        public static String Remove_ParameterURL(this String strTemp)
        {
            return strTemp.Split('?')[0];
        }

        public static String GenCustomerCode()
        {
            String Code = "";
            String Year = "";
            String Month = "";
            String Day = "";

            var CurrDate = DateTime.Now.Date;
            Year = (CurrDate.Year - 2000).ToString();
            Month = CurrDate.Month.ToString();
            Day = CurrDate.Day.ToString();


            if (Month.Length == 1) { Month = ("0" + Month); }
            if (Day.Length == 1) { Day = ("0" + Day); }

            Code = "KH" + Year + Month + Day;

            return Code;
        }

        public static string GenCustomerNo(int number)
        {
            string CustomerNo = "";

            CustomerNo = number.ToString("000000");

            return CustomerNo; 
        }

        public static string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);
            var _random = new Random();
            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length=26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        //Random char and number
        public static string RandomCode(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);
            var _random = new Random();
            
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length=26  

            for (var i = 0; i < size; i++)
            {
                switch (_random.Next(0,2))
                {
                    case 0:
                        var @number = _random.Next(0, 9);
                        builder.Append(@number);
                        break;
                    case 1:
                        var @char = (char)_random.Next(offset, offset + lettersOffset);
                        builder.Append(@char);
                        break;
                    default:
                        break;
                }                
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        public static int GetCurrentWeek(DateTime date)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            //DateTime date1 = new DateTime(year, month, DateTime.Now.Day);
            Calendar cal = dfi.Calendar;
            //Console.WriteLine("{0:d}: Week {1} ({2})", date1,
            //                  cal.GetWeekOfYear(date1, dfi.CalendarWeekRule, dfi.FirstDayOfWeek),
            //                  cal.ToString().Substring(cal.ToString().LastIndexOf(".") + 1));


            return cal.GetWeekOfYear(date, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }

        public static DateTime GetFirstDateOfWeek(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        public static List<DateTime> GenDaysOfWeek(DateTime date)
        {
            int daynow = date.Date.Day;

            int crrDate = 0;
            string dayofweek = date.Date.DayOfWeek.ToString();
            switch (dayofweek)
            {
                case "Monday":
                    crrDate = 0;
                    break;
                case "Tuesday":
                    crrDate = -1;
                    break;
                case "Wednesday":
                    crrDate = -2;
                    break;
                case "Thursday":
                    crrDate = -3;
                    break;
                case "Friday":
                    crrDate = -4;
                    break;
                case "Saturday":
                    crrDate = -5;
                    break;
                case "Sunday":
                    crrDate = -6;
                    break;
                default:
                    break;
            }

            List<DateTime> dayofWeek = new List<DateTime>();
            DateTime item0 = date.AddDays(crrDate).Date;
            DateTime item1 = date.AddDays(crrDate + 1).Date;
            DateTime item2 = date.AddDays(crrDate + 2).Date;
            DateTime item3 = date.AddDays(crrDate + 3).Date;
            DateTime item4 = date.AddDays(crrDate + 4).Date;
            DateTime item5 = date.AddDays(crrDate + 5).Date;
            DateTime item6 = date.AddDays(crrDate + 6).Date;

            dayofWeek.Add(item0);
            dayofWeek.Add(item1);
            dayofWeek.Add(item2);
            dayofWeek.Add(item3);
            dayofWeek.Add(item4);
            dayofWeek.Add(item5);
            dayofWeek.Add(item6);

            return dayofWeek;
        }

        public static string GetDayOfWeek(int date)
        {
            switch (date)
            {
                case (int)DayOfWeek.Sunday:
                    return "Chủ nhật";
                case (int)DayOfWeek.Monday:
                    return "Thứ hai";
                    
                case (int)DayOfWeek.Tuesday:
                    return "Thứ ba";
                    
                case (int)DayOfWeek.Wednesday:
                    return "Thứ tư";
                    
                case (int)DayOfWeek.Thursday:
                    return "Thứ năm";
                    
                case (int)DayOfWeek.Friday:
                    return "Thứ sáu";
                    
                case (int)DayOfWeek.Saturday:
                    return "Thứ bảy";
                    
                default:
                    break;
            }
            return string.Empty;
        }

        // the original MD5 method
        public static string GetMD5(string input)
        {
            MD5 x = new MD5CryptoServiceProvider();
            byte[] lam = Encoding.UTF8.GetBytes(input);
            lam = x.ComputeHash(lam);
            StringBuilder s = new StringBuilder();
            foreach (byte b in lam)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }

        public static string init_DublinCore(string title, string keyword, string desc, string url, string logo)  /*HttpContext context*/
        {
            string liOG = "";
            liOG += "<link rel=\"canonical\" href=\"" + url + "\" />";
            liOG += "<meta property=\"og:title\" content=\"" + title + "\"/>";
            liOG += "<meta property=\"og:description\" content=\"" + desc + "\"/>";
            liOG += "<meta property=\"og:url\" content=\"" + url + "\"/>";
            liOG += "<meta property=\"og:image\" content=\"" + logo + "\"/>";
            liOG += "<meta property=\"og:type\" content=\"article\"/>";
            liOG += "<link rel=\"schema.DC\" href=\"http://purl.org/dc/elements/1.1/\" />";
            liOG += "<meta name=\"DC.title\" content=\"" + title + "\" />";
            liOG += "<meta name=\"DC.identifier\" content=\"" + url + "\" />";
            liOG += "<meta name=\"DC.description\" content=\"" + desc + "\" />";
            liOG += "<meta name=\"DC.subject\" content=\"" + keyword + "\" />";
            liOG += "<meta name=\"DC.language\" scheme=\"UTF-8\" content=\"vi\" />";

            return liOG;
        }

        public static string CleanUrlParameters(string phrase)
        {
            byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(phrase);
            phrase = Encoding.ASCII.GetString(bytes);

            string str = phrase.ToLower();
            str = Regex.Replace(str, @"\s+", "-").Trim(); // convert multiple spaces into one hyphens   
            str = Regex.Replace(str, @"\s", "-"); // Replaces spaces with hyphens     
            //str = Regex.Replace(str, @"[^a-z0-9\s-]", "");// Remove invalid characters for param  
            //str = str.Substring(0, str.Length <= 30 ? str.Length : 30).Trim(); //Trim to max 30 char  
            return str;
        }

        public static string initBreadcrumbs(string keyWord, string url)
        {
            string strBreadCrumbs = string.Empty;
            strBreadCrumbs += "<a href=" + url + " itemprop=\"url\">";
            strBreadCrumbs += "<span itemprop=\"title\">" + keyWord + "</span></a>";
            return strBreadCrumbs;
        }

        public static bool checkFileValid(string fileExtension)
        {

            if (fileExtension.ToLower() != ".jpg"
               && fileExtension.ToLower() != ".png"
               && fileExtension.ToLower() != ".gif"
               && fileExtension.ToLower() != ".jpeg"
                && fileExtension.ToLower() != ".pdf"
                && fileExtension.ToLower() != ".doc"
                && fileExtension.ToLower() != ".docx"
                && fileExtension.ToLower() != ".xls"
                && fileExtension.ToLower() != ".xlsx"
                && fileExtension.ToLower() != ".ppt"
                && fileExtension.ToLower() != ".pptx"
                && fileExtension.ToLower() != ".pps"
                && fileExtension.ToLower() != ".ppsx")
            {
                return false;
            }

            return true;
        }

        public static string ConvertToYMD(string date)
        {
            try
            {
                date = date.Trim();
                return date.Substring(6, 4) + "/" + date.Substring(3, 2) + "/" + date.Substring(0, 2);
            }
            catch
            {
                return "";
            }
        }

        public static string ConvertStringToVND(string strSTR)
        {
            try
            {
                bool check = false;
                if (int.Parse(strSTR) < 0)
                {
                    check = true;
                    strSTR = strSTR.Replace("-", "");
                }
                string strTemp = "";
                string reserved = "";
                if (strSTR == "" || strSTR.Length <= 0)
                {
                    return "";
                }
                int count = 0;
                try
                {
                    // 12.000.000,00
                    for (int i = strSTR.Length - 1; i >= 0; i--)
                    {
                        count++;
                        if (count == 3 && i != 0)
                        {
                            reserved += strSTR[i] + ",";
                            count = 0;
                        }
                        else
                            reserved += strSTR[i];

                    }
                    for (int j = reserved.Length - 1; j >= 0; j--)
                    {
                        strTemp += reserved[j];
                    }
                }
                catch (Exception ex)
                {
                    return strSTR;

                }
                if (check)
                    strTemp = "- " + strTemp;
                return strTemp;
            }
            catch { return "0"; };
        }
        public static string XoaDau(string str)
        {
            string[] VietnameseSigns = new string[]
                  {
                   "aAeEoOuUiIdDyY",
                   "áàạảãâấầậẩẫăắằặẳẵ",
                   "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                   "éèẹẻẽêếềệểễ",
                   "ÉÈẸẺẼÊẾỀỆỂỄ",
                   "óòọỏõôốồộổỗơớờợởỡ",
                   "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                   "úùụủũưứừựửữ",
                   "ÚÙỤỦŨƯỨỪỰỬỮ",
                   "íìịỉĩ",
                   "ÍÌỊỈĨ",
                   "đ",
                   "Đ",
                   "ýỳỵỷỹ",
                   "ÝỲỴỶỸ"
                  };
            //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
            str = str.Replace("*", "sao");
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {

                for (int j = 0; j < VietnameseSigns[i].Length; j++)

                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

            }
            str = Regex.Replace(str, "[^a-zA-Z0-9_.]+", " ", RegexOptions.Singleline);
            return str;
        }

        public static string XoaDau1(string str)
        {
            string[] VietnameseSigns = new string[]
                  {
                   "aAeEoOuUiIdDyY",
                   "áàạảãâấầậẩẫăắằặẳẵ",
                   "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                   "éèẹẻẽêếềệểễ",
                   "ÉÈẸẺẼÊẾỀỆỂỄ",
                   "óòọỏõôốồộổỗơớờợởỡ",
                   "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                   "úùụủũưứừựửữ",
                   "ÚÙỤỦŨƯỨỪỰỬỮ",
                   "íìịỉĩ",
                   "ÍÌỊỈĨ",
                   "đ",
                   "Đ",
                   "ýỳỵỷỹ",
                   "ÝỲỴỶỸ"
                  };
            //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
            str = str.Replace("*", "sao");
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {

                for (int j = 0; j < VietnameseSigns[i].Length; j++)

                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

            }
            return str;
        }
        #region Biến
        private static string[] VietnameseSigns = new string[]
        {
            "aAeEoOuUiIdDyY-",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ",
            "?/:()+*&%^$#@!,.\\|:;'\""
        };
        #endregion
        public static string fRemoveSign4VietnameseString(string Str)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    Str = Str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return Str.Replace("-", "").Replace(" ", "-").ToLower();
        }

        #region Đổi từ số sang chữ

        private const string constError = "";
        private static string[] KySo_vn = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
        private static string[] Hang_vn = { "nghìn", "triệu", "tỷ" };
        private static string[] KySo_en = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
        private static string[] HaiKySo1 = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        private static string[] HaiKySo2 = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
        private static string[] Hang_en = { "thousand", "million", "billion", "trillion", "quadrillion", "quintillion" };

        public static string SoToChu(long prmSo, byte prmLanguage)
        {
            string temp = string.Empty;
            switch (prmLanguage)
            {
                case 1:
                    // Đọc theo tiếng anh
                    temp = (prmSo < 0 ? "(minus) " + So2Chu_en(Math.Abs(prmSo)) : So2Chu_en(prmSo));
                    break;
                case 2:
                    // Đọc theo tiếng việt
                    temp = (prmSo < 0 ? "(Trừ) " + So2Chu_vn(Math.Abs(prmSo)) : So2Chu_vn(prmSo));
                    temp = temp.Trim() + " đồng chẵn";
                    break;
            }
            if ((temp.Length > 0))
            {
                temp = temp.Substring(0, 1).ToUpper() + temp.Substring(1);
            }
            return temp;
        }
        public static string SoToChu(double prmSoDouble, byte prmLanguage)
        {
            long prmSo = (long)prmSoDouble;
            string temp = string.Empty;
            switch (prmLanguage)
            {
                case 1:
                    // Đọc theo tiếng anh
                    temp = (prmSo < 0 ? "(minus) " + So2Chu_en(Math.Abs(prmSo)) : So2Chu_en(prmSo));
                    break;
                case 2:
                    // Đọc theo tiếng việt
                    temp = (prmSo < 0 ? "(Trừ) " + So2Chu_vn(Math.Abs(prmSo)) : So2Chu_vn(prmSo));
                    temp = temp.Trim() + " đồng chẵn";
                    break;
            }
            if ((temp.Length > 0))
            {
                temp = temp.Substring(0, 1).ToUpper() + temp.Substring(1);
            }
            return temp;
        }
        public static string So2Chu_vn(long prmSo)
        {
            string functionReturnValue = null;
            try
            {
                long lctemp = prmSo / 1000000000;
                functionReturnValue = (lctemp > 0 ? ChinSo(lctemp) + " tỷ " : "") + ChinSo(prmSo % 1000000000, 0, lctemp > 0);
            }
            catch
            {
                functionReturnValue = constError;
            }
            return functionReturnValue;
        }

        private static string ChinSo(long prmSo, int prmInc, bool prmConTiep)
        {
            string strReturn = null;
            try
            {
                if ((prmSo / 1000) > 0)
                {
                    strReturn += ChinSo(prmSo / 1000, prmInc + 1) + ((prmSo / 1000) % 1000 > 0 ? " " + Hang_vn[prmInc] + " " : "") + BaSo(prmSo % 1000, true);
                }
                else
                {
                    strReturn += BaSo(prmSo % 1000, prmConTiep);
                }
            }
            catch
            {
                strReturn = constError;
            }
            return strReturn;
        }

        private static string ChinSo(long prmSo, int prmInc)
        {
            return ChinSo(prmSo, prmInc, false);
        }

        private static string ChinSo(long prmSo)
        {
            return ChinSo(prmSo, 0, false);
        }

        private static string BaSo(long prmSo, bool prmConTiep)
        {
            long lcDonvi = 0;
            long lcChuc = 0;
            long lcTram = 0;
            string strReturn = "";
            if ((prmSo == 0))
            {
                return (prmConTiep ? "" : KySo_vn[0]);
            }

            lcDonvi = prmSo % 10;
            // Hàng đơn vị
            lcChuc = (prmSo / 10) % 10;
            // Hàng chục
            lcTram = prmSo / 100;
            // Hàng trăm

            // Xét trường hợp hàng trăm
            switch (lcTram)
            {
                case 0:
                    if (prmConTiep)
                        strReturn = KySo_vn[0] + " trăm ";
                    break;
                default:
                    strReturn = KySo_vn[lcTram] + " trăm ";
                    break;
            }
            // Xét trường hợp hàng chục
            switch (lcChuc)
            {
                case 0:
                    if ((lcDonvi > 0 & ((lcTram > 0) | (lcTram == 0 & prmConTiep))))
                        strReturn += "linh ";
                    break;
                case 1:
                    strReturn += "mười ";
                    break;
                default:
                    strReturn += KySo_vn[lcChuc] + " mươi ";
                    break;
            }
            // Xét trường hợp hàng đơn vị
            switch (lcDonvi)
            {
                case 0:
                    strReturn += "";
                    break;
                case 1:
                    // 1, 11: một  -  21, 31: .... mốt
                    strReturn += (lcChuc <= 1 ? KySo_vn[1] : "mốt");
                    break;
                case 5:
                    // 5: năm   -  15, 25: .... lăm 
                    strReturn += (lcChuc == 0 ? KySo_vn[5] : "lăm");
                    break;
                default:
                    strReturn += KySo_vn[lcDonvi];
                    break;
            }
            return strReturn;
        }

        private static string BaSo(int prmSo)
        {
            return BaSo(prmSo, false);
        }

        public static string So2Chu_en(long prmSo, int prmInc, bool prmConTiep)
        {
            string strReturn = null;
            try
            {
                if ((prmSo / 1000) > 0)
                {
                    strReturn += So2Chu_en(prmSo / 1000, prmInc + 1) + ((prmSo / 1000) % 1000 > 0 ? " " + Hang_en[prmInc] + "  " : "") + Baso_en(prmSo % 1000, true);
                }
                else
                {
                    strReturn += Baso_en(prmSo % 1000, prmConTiep);
                }
            }
            catch
            {
                strReturn = constError;
            }
            return strReturn;
        }

        public static string So2Chu_en(long prmSo, int prmInc)
        {
            return So2Chu_en(prmSo, prmInc, false);
        }

        public static string So2Chu_en(long prmSo)
        {
            return So2Chu_en(prmSo, 0, false);
        }

        public static string So2Chu_en(long prmSo, bool prmConTiep)
        {
            return So2Chu_en(prmSo, 0, prmConTiep);
        }

        private static string Baso_en(long prmSo, bool prmConTiep)
        {
            long lcDonvi = 0;
            long lcChuc = 0;
            long lcTram = 0;
            string strReturn = "";
            if ((prmSo == 0))
            {
                return (prmConTiep ? "" : KySo_en[0]);
            }

            lcDonvi = prmSo % 10;
            // Hàng đơn vị
            lcChuc = (prmSo / 10) % 10;
            // Hàng chục
            lcTram = prmSo / 100;
            // Hàng trăm        

            // Xét trường hợp hàng trăm
            strReturn = (lcTram == 0 ? "" : KySo_en[lcTram] + " hundred ");

            // Xét trường hợp hàng chục và đơn vị
            switch (lcChuc)
            {
                case 0:
                    strReturn += (lcDonvi == 0 ? "" : KySo_en[lcDonvi]);
                    break;
                case 1:
                    strReturn += HaiKySo1[lcDonvi];
                    break;
                default:
                    strReturn += (lcDonvi == 0 ? HaiKySo2[lcChuc - 2] : HaiKySo2[lcChuc - 2] + " " + KySo_en[lcDonvi]);
                    break;
            }
            return strReturn;
        }

        private static string Baso_en(int prmSo)
        {
            return Baso_en(prmSo, false);
        }

        #endregion

        public static string ReadTextFile(string PathFile)
        {
            string strOut;
            StreamReader sr = new StreamReader(PathFile);
            strOut = sr.ReadToEnd();
            sr.Close();
            return strOut;
        }

        public static DateTime ParseDateTime(string value)
        {
            DateTime result;
            DateTimeFormatInfo info;
            info = (DateTimeFormatInfo)CultureInfo.CurrentUICulture.DateTimeFormat.Clone();
            info.ShortDatePattern = "dd/MM/yyyy";
            info.FullDateTimePattern = "dd/MM/yyyy HH:mm";
            try { result = DateTime.Parse(value, info); }
            catch { result = DateTime.MinValue; }
            return result;
        }

        public static object ParseDateTime_New(string strvalue)
        {
            if (strvalue == "")
                return DBNull.Value;
            else
            {
                try
                {
                    DateTimeFormatInfo info;
                    info = (DateTimeFormatInfo)CultureInfo.CurrentUICulture.DateTimeFormat.Clone();
                    info.ShortDatePattern = "dd/MM/yyyy";
                    info.FullDateTimePattern = "dd/MM/yyyy HH:mm:ss";
                    return DateTime.Parse(strvalue, info);
                }
                catch (Exception ex)
                {
                    string strerror = ex.Message.ToString();
                    return DBNull.Value;
                }
            }
        }
        public static void CopyObject(object source, object destination)
        {
            Type typeB = destination.GetType();
            foreach (PropertyInfo property in source.GetType().GetProperties())
            {
                try
                {
                    if (!property.CanRead || (property.GetIndexParameters().Length > 0))
                        continue;

                    PropertyInfo other = typeB.GetProperty(property.Name);
                    if ((other != null) && (other.CanWrite))
                        other.SetValue(destination, property.GetValue(source, null), null);
                }
                catch (Exception e)
                {
                    string message = e.ToString();
                }
            }


        }
        public static string HttpWebRequestGet_Url(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            //request.Timeout = 120000;
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }

        public static String GetContentValue(string Key)
        {
            using (var db = new HotelEntities())
            {
                var model = db.tbl_Content.FirstOrDefault(p => p.ContenCode == Key);
                if (model != null)
                {
                    return model.ContentDetail;
                }
                return "";
            }

        }

        public static bool CheckPhoneNumber(string phone)
        {
            bool isPhone = true;
            try
            {
                if (phone.Length != 10)
                {
                    isPhone = false;
                }
                if (phone[0] != '0')
                {
                    isPhone = false;
                }
                if (!phone.All(Char.IsDigit))
                {
                    isPhone = false;
                }
                if (phone[1] == '0')
                {
                    isPhone = false;
                }
            }
            catch (Exception)
            {
            }

            return isPhone;
        }

        public static string initBreadcrumbs(string keyWord, string url, int position)
        {
            url = removeHtmlTag(url);

            string strBreadCrumbs = string.Empty;
            if (position > 1)
            {
                strBreadCrumbs += "<li><i class=\"icon icon--chevron - right px-2 p-2\"></i></li>";
            }
            strBreadCrumbs += "<li itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\">";
            strBreadCrumbs += "<a href=\"" + url + "\" itemprop=\"item\">";
            strBreadCrumbs += "<span itemprop=\"name\">" + keyWord + "</span>";
            strBreadCrumbs += "</a>";
            strBreadCrumbs += "<meta itemprop=\"position\" content=\"" + position + "\" />";
            strBreadCrumbs += "</li>";

            return strBreadCrumbs;
        }

        public static string removeHtmlTag(string text)
        {
            StringBuilder b = new StringBuilder(text.Length);
            bool inside = false;
            for (int i = 0; i < text.Length; i++)
            {
                char let = text[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (inside == false)
                {
                    b.Append(let);
                }
            }
            return b.ToString();
        }

        #endregion

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

    }
    public class DataItemModel
    {
        public DataItemModel()
        { }

        public string text
        {
            get;
            set;
        }
        public string value
        {
            get;
            set;
        }
    }
    public class XMLUtils
    {
        public static List<DataItemModel> BindData(string pGroupNodeName)
        {
            return BindData(pGroupNodeName, false);
        }
        public static List<DataItemModel> BindData(string pGroupNodeName, bool pEmptyAsDefault)
        {
            List<DataItemModel> types = new List<DataItemModel>();
            DataItemModel itemInfo;
            if (pEmptyAsDefault)
            {
                itemInfo = new DataItemModel();
                itemInfo.text = "--Choose--";
                itemInfo.value = string.Empty;
                types.Add(itemInfo);
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath("~/App_Data/XMLData.xml"));
            if (doc != null)
            {
                XmlNodeList nodes = doc.SelectNodes("/autobind/" + pGroupNodeName + "/item");
                if (nodes != null)
                {
                    int nodeCount = nodes.Count;
                    for (int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
                    {
                        itemInfo = new DataItemModel();
                        itemInfo.text = Protect.ToString(nodes[nodeIndex].Attributes["text"].Value);
                        itemInfo.value = Protect.ToString(nodes[nodeIndex].Attributes["value"].Value);
                        types.Add(itemInfo);
                    }
                }
            }
            return types;
        }
    }
}
