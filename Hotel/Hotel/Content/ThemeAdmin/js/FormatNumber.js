﻿function FormatAmount(what) {
    var strAmt = what.value;
    if (strAmt.length > 0) {
        strAmt = clearFormatNumber(strAmt);
        strAmt = Number(strAmt).format(0, 3);
    }
    what.value = strAmt;
}

function clearFormatNumber(_value) {
    var temp = '';
    if (isEmpty(_value)) {
        return temp;
    }
    return replaceAll(_value, ',', '');
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function replaceAll(varb, replaceThis, replaceBy) {
    newvarbarray = varb.split(replaceThis);
    newvarb = newvarbarray.join(replaceBy);
    return newvarb;
}

Number.prototype.format = function (d, w, s, c) {
    var re = '\\d(?=(\\d{' + (w || 3) + '})+' + (d > 0 ? '\\b' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~d));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

Number.prototype.format = function (n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};