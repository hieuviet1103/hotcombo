﻿//Facebook SDK login
// Load the SDK asynchronously
(function (d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

window.fbAsyncInit = function () {
    FB.init({
        //appId: '418074079581319', // App ID
        appId: '4695465440482781', // App ID
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true,  // parse XFBML
        version: 'v11.0'
    });
};

$(document).ready(function () {
    'use strict';

    $('.lbtSignInFacebook').click(function () {
        FacebookLogin();
    })

    $('.lbtLogOutFacebook').click(function () {
        FacebookLogout();
        var token = $('input[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: "/AccountCustomer/LogOut",
            headers: { "__RequestVerificationToken": token },
            type: "POST",
            success: function (data) {
                if (data.success === "True") {
                    location.reload();
                }
            },
            error: function (data) {
                console.log(data);
            }
        })
    })
});

function FacebookLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            getFacebookUserInfo();
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    }, { scope: 'email' });
}

function getFacebookUserInfo() {
    FB.api('/me?fields=email,name', function (response) {
        var token = $('input[name="__RequestVerificationToken"]').val();
        var imageUrl = 'https://graph.facebook.com/' + response.id + '/picture?type=normal&width=500&height=500';
        $.ajax({
            url: "/AccountCustomer/SocialMediaLogin",
            type: "POST",
            data: {
                "__RequestVerificationToken": token,
                'Id': response.id,
                'FullName': response.name,
                'Email': response.email,
                'ImageUrl': imageUrl,
                'DataFrom': "Facebook",
                'TypeLogin': 1
            },
            success: function (data) {
                if (data.success === "True") {
                    alert("Thành công");
                    location.reload();
                }
                else {
                    alert("Vui lòng thử lại sau");
                }
            },
            error: function (data) {
                alert("Vui lòng thử lại sau");
                console.log(data);
            }
        })
    });
}

function FacebookLogout() {
    FB.logout(function () { document.location.reload(); });
}

//End Facebook SDK login

//Google Login
//Add <meta name="google-signin-client_id" content="234733481047-gap4nigm8dtb4dd6i9njaapeedr8bbh4.apps.googleusercontent.com">
//And <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
//Add <script src="https://apis.google.com/js/api:client.js"></script>


var googleUser = {};
var startApp = function () {
    gapi.load('auth2', function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '234733481047-gap4nigm8dtb4dd6i9njaapeedr8bbh4.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        attachSignin(document.getElementById('customBtn'));
    });
};

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function (googleUser) {
            //var profile = auth2.currentUser.get().getBasicProfile();
            var profile = googleUser.getBasicProfile();
            //console.log('ID: ' + profile.getId());
            //console.log('Full Name: ' + profile.getName());
            //console.log('Given Name: ' + profile.getGivenName());
            //console.log('Family Name: ' + profile.getFamilyName());
            //console.log('Image URL: ' + profile.getImageUrl());
            //console.log('Email: ' + profile.getEmail());

            var token = $('input[name="__RequestVerificationToken"]').val();
            $.ajax({
                url: "/AccountCustomer/SocialMediaLogin",
                type: "POST",
                data: {
                    "__RequestVerificationToken": token,
                    'Id': profile.getId(),
                    'FirstName': profile.getGivenName(),
                    'LastName': profile.getFamilyName(),
                    'FullName': profile.getName(),
                    'Email': profile.getEmail(),
                    'ImageUrl': profile.getImageUrl(),
                    'DataFrom': "Google",
                    'TypeLogin': 2
                },
                success: function (data) {
                    if (data.success === "True") {
                        alert("Thành công");
                        location.reload();
                    }
                    else {
                        alert("Vui lòng thử lại sau");
                    }
                },
                error: function (data) {
                    alert("Vui lòng thử lại sau");
                    console.log(data);
                }
            })
        }, function (error) {
            alert(JSON.stringify(error, undefined, 2));
        });
}
function googleSignOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}
//End Google Login