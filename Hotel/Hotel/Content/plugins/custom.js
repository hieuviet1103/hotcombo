﻿function ShowNotify(_message, _type, _postion) {
    setTimeout(function () {
        $.notify({
            message: _message
        },
        {
            type: _type,
            placement: {
                from: _postion
            },
            animate: {
                enter: "animated fadeInRight",
                exit: "animated fadeOutRight"
            },
            newest_on_top: false
        });
    }, 500);
}

$(function () {
    if ($.validator != null) {
        $.validator.addMethod('date',
        function (value, element) {
            if (this.optional(element)) {
                return true;
            }
            var ok = true;
            try {
                $.datepicker.parseDate('dd/mm/yy', value);
            }
            catch (err) {
                ok = false;
            }
            return ok;
        });
        $(".datefield").datepicker({ dateFormat: 'dd/mm/yy', changeYear: true });
    }
});