﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Hotel.EntityFramework.Models;
using Hotel.Models;
using Hotel.Utilities;
using PagedList;

namespace Hotel.Controllers
{
    public class TravelGuideController : Controller
    {
        private readonly HotelEntities db = new HotelEntities();
        private const int pageSize = 9;

        private string strHost
        {
            get
            {
                string strH = WebConfigurationManager.AppSettings["host"].ToString() + "/";
                return strH;
            }
        }

        // GET: TravelGuide
        public ActionResult Index(int page = 1, int category = 0)
        {
            List<HotelNews> models = new List<HotelNews>();
            string categoryName = "";
            if (category != 0)
            {
                models = db.NewsAndCategories.Where(d => d.CategoryId == category)
                    .Select(d=> db.HotelNews.FirstOrDefault(m=>m.HotelNewsID == d.NewsId)).ToList();

                categoryName = db.NewsCategories.Where(d => d.CategoryID == category).Select(d => d.CategoryName).FirstOrDefault();
            }
            else
            {
                models = db.HotelNews.ToList();
            }

            string Title = "";
            string Keyword = "";
            string Introduction = "";
            string Image_src = "";
            string link = strHost + Request.RawUrl.ToString();

            string breadcrumbs = string.Empty;
            breadcrumbs += Utility.initBreadcrumbs("Trang chủ", strHost, 1) + " » ";
            Title = "Cẩm nang du lịch";
            breadcrumbs += Utility.initBreadcrumbs(Title, link, 2);
            if (!string.IsNullOrEmpty(categoryName))
            {
                breadcrumbs += " » " + Utility.initBreadcrumbs(categoryName, "/cam-nang-du-lich/" + categoryName.convertToUnSign2() + "-c" + category, 3);
            }

            initBreadCrumbs(Title, Keyword, Introduction, Request.Url.ToString(), Image_src, breadcrumbs);

            ViewBag.NewsCategories = db.NewsCategories.Where(d => d.IsActive).ToList();
            ViewBag.LatestNews = db.HotelNews.OrderByDescending(d => d.CrtDate).Take(5).ToList();

            return View(models.ToPagedList(page, pageSize));
        }

        public ActionResult Details(Guid id)
        {
            var models = db.HotelNews.FirstOrDefault(d => d.HotelNewsID == id);

            //== init breadCrumbs ==
            //GetBreadCrumbs(tour, destination);\

            string breadcrumbs = string.Empty;
            breadcrumbs += Utility.initBreadcrumbs("Trang chủ", strHost, 1);
            //page2
            string Title2 = "";
            string link2 = strHost + "cam-nang-du-lich";
            Title2 = "Cẩm nang du lịch";
            breadcrumbs += " » " + Utility.initBreadcrumbs(Title2, link2, 2);
            //page3
            string Title3 = "";
            string Keyword3 = "";
            string Introduction3 = "";
            string Image_src3 = "";
            string link3 = strHost + Request.RawUrl.ToString();
            Title3 = models.Title;
            breadcrumbs += " » " + Utility.initBreadcrumbs(Title3, link3, 3);


            initBreadCrumbs(Title3, Keyword3, Introduction3, Request.Url.ToString(), Image_src3, breadcrumbs);

            ViewBag.NewsCategories = db.NewsCategories.Where(d => d.IsActive).ToList();

            return View(models);
        }

        public ActionResult _SideBar()
        {
            try
            {
                ViewBag.NewsCategories = db.NewsCategories.Where(d => d.IsActive).ToList();
                ViewBag.LatestNews = db.HotelNews.OrderByDescending(d => d.CrtDate).Take(5).ToList();
            }
            catch (Exception)
            {

            }
            return PartialView();
        }

        public void initBreadCrumbs(string title, string keyword, string desc, string url, string logo, string breadcrumbs)
        {
            //title = title + " - VIETRAVEL";
            if (title == null)
            {
                title = "";
            }
            if (keyword == null)
            {
                keyword = "";
            }
            if (desc == null)
            {
                desc = "";
            }
            if (url == null)
            {
                url = "";
            }
            if (logo.Length == 0)
            {
                logo = strHost + "Images/vtv-logo.png";
            }
            if (breadcrumbs == null)
            {
                breadcrumbs = "";
            }
            ViewBag.liOG = Utility.init_DublinCore(title, keyword, desc, url, logo);
            ViewBag.Title = title;
            ViewBag.Breadcrumbs = breadcrumbs;
        }
    }
}