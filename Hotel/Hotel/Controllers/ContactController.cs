﻿using Admin.Helpers;
//using Hotel.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hotel.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendEmail(string txtHoTen, string txtTieuDe, string txtNoiDung, string txtEmail, string txtDienThoai)
        {

            MailControl mailcontrol = new MailControl();
            string emailFrom = "nguyenthanhdat.bc2310@gmail.com";

            Email email = new Email();

            //string emailTo = WebConfigurationManager.AppSettings["emailTo_ContactCustomer"].ToString();
            //string emailTo = "thanhdatmyhanh@gmail.com";

            //email.From = txtEmail;
            //email.FromName = txtHoTen;
            //email.To = emailTo;
            string subjectCSKH = "HotCombo - Thông tin liên hệ của khách hàng";
            email.Subject = subjectCSKH;
            email.Body = buildContentEmail(txtHoTen, txtDienThoai, txtEmail, txtTieuDe, txtNoiDung);
            bool isSendCSKH = mailcontrol.SendCSKH(email);
            if (!isSendCSKH)
            {
                return Json(new { Status = 0, Message = "Có lỗi xảy ra trong quá trình gửi email" });
            }

            StreamReader rd = new StreamReader(Server.MapPath(@"~/FileUploads/Template/Mail/ConfirmContact.html"));
            string template = rd.ReadToEnd();
            rd.Close();
            string subject = "HotCombo - Xác nhận gửi thông tin liên hệ thành công";
            
            string bodyMail = template;

            Email emailCus = new Email();
            emailCus.From = emailFrom;
            emailCus.To = txtEmail;
            emailCus.Subject = subject;
            emailCus.Body = bodyMail;
            bool isSend = mailcontrol.Send(emailCus);

            

            //string strEmail = "Cảm ơn quý khách đã liên lạc. Chúng tôi sẽ liên lạc với quý khách trong thời gian sớm nhất";

            //return Content(strEmail);
            if (isSend)
            {
                return Json(new { Status = 1, Message = "Cảm ơn quý khách đã liên lạc. Chúng tôi sẽ liên lạc với quý khách trong thời gian sớm nhất" });
            }
            else
            {
                return Json(new { Status = 0, Message = "Có lỗi xảy ra trong quá trình gửi email" });
            }
        }

        private string buildContentEmail(string txtHoTen, string txtDienThoai, string txtEmail, string txtTieuDe, string txtNoiDung)
        {

            StreamReader rd = new StreamReader(Server.MapPath(@"~/FileUploads/Template/Mail/ContactToCSKH.html"));
            string template = rd.ReadToEnd();
            rd.Close();

            string subject = "HotCombo - Thông tin liên hệ của khách hàng";
            template = template.Replace("{HoTen}", txtHoTen);
            template = template.Replace("{DienThoai}", txtDienThoai);
            template = template.Replace("{Email}", txtEmail);
            template = template.Replace("{ChuDe}", txtTieuDe);
            template = template.Replace("{NoiDung}", txtNoiDung);
            string bodyMail = template;

            return bodyMail;
        }
    }
}