﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.Models;
using Hotel.EntityFramework.Models;

namespace Hotel.Controllers
{
    public class NewsController : Controller
    {
        private readonly HotelEntities db = new HotelEntities();
        // GET: News
        public ActionResult Index()
        {
            List<HotelNews> models = new List<HotelNews>();
            models = db.HotelNews.Where(d => d.NewsTypeID == 2).ToList();
            return View(models);
        }
        public ActionResult Details(Guid id)
        {
            var models = db.HotelNews.FirstOrDefault(d => d.HotelNewsID == id && d.NewsTypeID == 2);
            return View(models);
        }
        public ActionResult TravelGuide(Guid id)
        {
            var models = db.HotelNews.FirstOrDefault(d => d.HotelNewsID == id && d.NewsTypeID == 2);
            return View(models);
        }

        public ActionResult _SideBar()
        {
            return PartialView();
        }
    }
}