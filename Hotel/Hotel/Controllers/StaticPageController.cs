﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hotel.Controllers
{
    public class StaticPageController : Controller
    {
        // GET: StaticPage
        public ActionResult Index(string type = "home-policy-footer")
        {
            return View("~/Views/StaticPage/Index.cshtml" /* view name*/,
            null /* master name */,
            type /* model */);
        }
    }
}