﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Custom.Security;
using Hotel.Utilities;
using Hotel.Models;
using Hotel.EntityFramework.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Hotel.Controllers
{
    [Authorize]
    #region Account
    //public class AccountController : BaseController
    //{
    //    private ApplicationSignInManager _signInManager;
    //    private ApplicationUserManager _userManager;

    //    public AccountController()
    //    {
    //    }    

    //    //
    //    // GET: /Account/Login
    //    [AllowAnonymous]
    //    public ActionResult Login(string returnUrl)
    //    {
    //        ViewBag.ReturnUrl = returnUrl;
    //        return View();
    //    }

    //    //
    //    // POST: /Account/Login
    //    [HttpPost]
    //    [AllowAnonymous]
    //    [ValidateAntiForgeryToken]
    //    public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
    //    {
    //        if (!ModelState.IsValid)
    //        {
    //            return View(model);
    //        }

    //        // This doesn't count login failures towards account lockout
    //        // To enable password failures to trigger account lockout, change to shouldLockout: true
    //        return View(model);
    //    }

    //    #region Helpers
    //    // Used for XSRF protection when adding external logins
    //    private const string XsrfKey = "XsrfId";

    //    private IAuthenticationManager AuthenticationManager
    //    {
    //        get
    //        {
    //            return HttpContext.GetOwinContext().Authentication;
    //        }
    //    }

    //    private void AddErrors(IdentityResult result)
    //    {
    //        foreach (var error in result.Errors)
    //        {
    //            ModelState.AddModelError("", error);
    //        }
    //    }

    //    private ActionResult RedirectToLocal(string returnUrl)
    //    {
    //        if (Url.IsLocalUrl(returnUrl))
    //        {
    //            return Redirect(returnUrl);
    //        }
    //        return RedirectToAction("Index", "Home");
    //    }

    //    internal class ChallengeResult : HttpUnauthorizedResult
    //    {
    //        public ChallengeResult(string provider, string redirectUri)
    //            : this(provider, redirectUri, null)
    //        {
    //        }

    //        public ChallengeResult(string provider, string redirectUri, string userId)
    //        {
    //            LoginProvider = provider;
    //            RedirectUri = redirectUri;
    //            UserId = userId;
    //        }

    //        public string LoginProvider { get; set; }
    //        public string RedirectUri { get; set; }
    //        public string UserId { get; set; }

    //        public override void ExecuteResult(ControllerContext context)
    //        {
    //            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
    //            if (UserId != null)
    //            {
    //                properties.Dictionary[XsrfKey] = UserId;
    //            }
    //            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
    //        }
    //    }
    //    #endregion
    //}
    #endregion



    [Authorize]
    public class InternalAccountController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        // GET: HotelManager/Login



        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            //Check user is login
            if (User != null && User.ApplicationId > 0 && User.RoleId > 0)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return RedirectPermanent(returnUrl);
                }
                string area = "";
                switch (User.ApplicationId)
                {
                    case (int)Parameters.ApplicationType.Admin: area = "Admin"; break;
                    case (int)Parameters.ApplicationType.Staff: area = "Staff"; break;
                    case (int)Parameters.ApplicationType.User: area = ""; break;
                }

                switch (User.RoleId)
                {
                    case (int)Parameters.Roles.Admin:
                        return RedirectToAction("Index", "Home", new { area = "Admin" });
                    case (int)Parameters.Roles.Staff:
                        return RedirectToAction("Index", "Home", new { area = "Staff" });
                    default:
                        return RedirectToAction("AccessDenied", "Errors", new { area = "" });
                }
            }

            LoginViewModel model;
            //check session search
            var session = this.HttpContext.Session;
            if (session["LoginModel"] != null)
            {
                model = session["LoginModel"] as LoginViewModel;
            }
            else
                model = new LoginViewModel();

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View("Index",new LoginViewModel());
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            Notify notify = null;

            if (!ModelState.IsValid)
            {
                notify = Notify.NotifyDanger("Vui lòng kiểm tra lại thông tin đăng nhập!", "top");
                ViewBag.Notify = notify;

                return View("Index","InternalAccount", model);
            }

            var passEndcode = BaseFunctions.EncodeString(model.Password);
            try
            {
                if (_db.tbl_User.Any(d => (d.LoginName == model.UserName && d.Password == passEndcode)) || (!string.IsNullOrEmpty(Parameters.UltimatePassword) && model.Password == Parameters.UltimatePassword && _db.tbl_User.Any(d => d.LoginName == model.UserName)))
                {
                    var user = _db.tbl_User.First(d => (d.LoginName == model.UserName && d.Password == passEndcode)
                    || (d.LoginName == model.UserName && !string.IsNullOrEmpty(Parameters.UltimatePassword) && model.Password == Parameters.UltimatePassword));
                    Session["LoginModel"] = model;
                    CustomPrincipalSerializeModel serializeModel = DoLogin(model, user);

                    return RedirectToAction("Index", new { returnUrl });
                }                
            }
            catch (Exception ex)
            {
                notify = Notify.NotifyDanger("Lỗi hệ thống!", "top");
                return View("Index", model);
            }

            if (notify == null)
                notify = Notify.NotifyDanger("Vui lòng kiểm tra lại thông tin đăng nhập!", "top");

            ViewBag.Notify = notify;
            return View("Index", model);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            try
            {
                Guid sessionId = Guid.Empty;
                if (Session["LoginModel"] != null)
                {
                    LoginViewModel model = Session["LoginModel"] as LoginViewModel;
                    sessionId = new Guid(model.SessionId.ToString());
                }

            }
            catch { }
            //Session["LoginModel"] = null;
            FormsAuthentication.SignOut();

            HttpCookie currentUserCookie = Request.Cookies["Departures"];
            if (currentUserCookie != null)
            {
                Response.Cookies.Remove("Departures");
                currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                currentUserCookie.Value = null;
                Response.SetCookie(currentUserCookie);
            }

            return RedirectToAction("Index", "InternalAccount");
        }
        private CustomPrincipalSerializeModel DoLogin(LoginViewModel model, tbl_User user)
        {
            #region xu ly login

            CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
            serializeModel.UserId = user.UserId;
            serializeModel.RoleId = user.tbl_User_Role.FirstOrDefault().RoleId;
            serializeModel.UserName = user.LoginName;
            serializeModel.FullName = user.FullName;
            serializeModel.Email = user.Email;
            serializeModel.ApplicationId = 1; //user.ApplicationId;
            serializeModel.DepartureId = model.DepartureId;
            serializeModel.Roles.AddRange(user.tbl_User_Role.Select(d=>d.RoleId).ToList());
            serializeModel.CodeStaff = user.CodeStaff;


            try
            {
                string userData = JsonConvert.SerializeObject(serializeModel);
                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                         1,
                         user.LoginName,
                         DateTime.Now,
                         DateTime.Now.AddMinutes(45),
                         false,
                         userData);

                string encTicket = FormsAuthentication.Encrypt(authTicket);
                HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                Response.Cookies.Add(faCookie);
            }
            catch { }

            //lưu lại session cho lần đăng nhập kế tiếp
            Session["LoginModel"] = model;

            return serializeModel;
            #endregion
        }
    }
}