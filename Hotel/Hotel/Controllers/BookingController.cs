﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel;
using Hotel.Models;
using Hotel.EntityFramework.Models;
//using Hotel.Utilities;
using Admin.Helpers;
using AutoMapper;
using System.IO;
using System.Data.Entity;
using System.Web.Configuration;
using Hotel.Common.Utilities;

namespace Hotel.Controllers
{
    public class BookingController : Controller
    {
        private readonly HotelEntities _db = new HotelEntities();
        private readonly string isSendSMS = WebConfigurationManager.AppSettings["IsSendSMS"];
        private readonly string adminPhoneNumber = WebConfigurationManager.AppSettings["AdminPhoneNumber"];

        // GET: Booking
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateBooking(Guid RoomId, DateTime DateCheckIn, DateTime DateCheckOut, int RoomCount, int PersonCount)
        {
            try
            {
                ConvertDbToModel convertDb = new ConvertDbToModel();
                BookingViewModel model = new BookingViewModel()
                {
                    BookingId = Guid.NewGuid(),
                    DateCheckIn = DateCheckIn.Date.AddHours(14),
                    DateCheckOut = DateCheckOut.Date.AddHours(12),
                    CountDate = Convert.ToInt32((DateCheckOut - DateCheckIn).TotalDays),
                    RoomCount = RoomCount,
                    PersonCount = PersonCount,   
                    CustomerCheckPayment = false,
                    IsPaymentSuccess = false,
                    IsDelete = false,
                    DateCreate = DateTime.Now
                };
                var room = _db.tbl_Room.FirstOrDefault(d => d.Id == RoomId && d.IsDelete.Value != true);
                int maxPeople = room.MaxPeople.Value * RoomCount;
                if (maxPeople < PersonCount)
                {
                    return Json(new { Status = 0, Message = string.Format("Số khách tối đa cho {0} phòng là {1}",RoomCount,maxPeople) });
                }

                if (RoomCount > room.AvailableRoom)
                {
                    return Json(new { Status = 0, Message = string.Format("Số phòng còn lại không đủ {0}", RoomCount) });
                }

                model.Room = convertDb.ToRoom(room);
                model.Hotel = convertDb.ToHotel(room.tbl_Hotel, room.tbl_Hotel.tbl_Room.ToList());
                model.CODCancel = model.Hotel.CancelTime != null ? model.DateCreate.AddHours(model.Hotel.CancelTime.Value) : model.DateCheckIn;
                if (model.CODCancel > model.DateCheckIn)
                {
                    model.CODCancel = model.DateCheckIn.AddHours(-0.5);
                }
                double roomAmount = 0;
                for (DateTime day = DateCheckIn.Date; day < DateCheckOut.Date; day = day.AddDays(1))
                {
                    if (room.tbl_RoomPrice.Any(d=>d.RoomDate == day && d.IsActive.Value && d.IsDelete != true))
                    {
                        var roomPrice = room.tbl_RoomPrice.FirstOrDefault(d => d.RoomDate == day && d.IsActive.Value && d.IsDelete != true);
                        roomAmount += roomPrice.PricePromotion.Value * RoomCount;
                        model.RoomPrices.Add(convertDb.ToRoomPrice(roomPrice));
                    }
                    else
                    {
                        return Json(new { Status = 0, Message = "Không có phòng cho ngày "+ day.ToString("dd/MM/yyyy") });
                    }
                }
                model.RoomAmount = roomAmount;
                model.Tax = roomAmount * 0.1; // Thuế 10%
                model.TotalAmount = model.RoomAmount + model.Tax;
                Session["Booking"] = model;
                return Json(new { Status = 1, BookingId = model.BookingId, Message = ""});
            }
            catch (Exception ex)
            {
                return Json(new { Status = 0, Message = "Không thực hiện được " });
                throw;
            }
        }

        public ActionResult ConfirmBooking()
        {
            BookingViewModel model = Session["Booking"] as BookingViewModel;
            if (model != null)
            {
                CustomerViewModel customerInfo = new CustomerViewModel();
                if (Session["CustomerLogin"] != null)
                {
                    var loginInfo = (CustomerAccountViewModel)Session["CustomerLogin"];
                    customerInfo.CustomerId = loginInfo.CustomerId;
                    customerInfo.CustomerName = loginInfo.CustomerName;
                    customerInfo.CustomerEmail = loginInfo.Email;
                    customerInfo.CustomerPhoneNumber = loginInfo.PhoneNumber;
                    customerInfo.CustomerProvinceId = loginInfo.ProvinceId;
                    customerInfo.CustomerDistrictId = loginInfo.DistrictId;
                    customerInfo.CustomerAddress = loginInfo.Address;
                    customerInfo.TotalPoint = loginInfo.TotalPoint;
                }
                model.CustomerInfo = customerInfo;
                return View(model);
            }
            return RedirectToAction("Index", "Home");

        }

        [HttpPost]
        public ActionResult ConfirmBooking(BookingViewModel model)
        {
            if (ModelState.IsValid)
            {
                BookingViewModel booking = Session["Booking"] as BookingViewModel;
                if(model.IsTotalPointUse == true)
                {
                    //cap nhat trong booking
                    booking.TotalAmount = booking.TotalAmount - ((double)model.TotalPointUse * 1000);
                    booking.Discount = (double)model.TotalPointUse * 1000;
                    //cap nhat trong tbl_customer
                    var customer = _db.tbl_Customer.Where(x => x.Email == model.CustomerInfo.CustomerEmail).FirstOrDefault();
                    customer.TotalPoint -= model.TotalPointUse;
                    _db.Entry(customer).State = EntityState.Modified;
                    _db.SaveChanges();
                    //Cap nhat tbl_DiscountHistory
                    var itemDiscountHistory = new tbl_DiscountHistory()
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = (Guid)model.CustomerInfo.CustomerId,
                        BookingId = booking.BookingId,
                        Discount = (double)model.TotalPointUse * 1000,
                        CustomerName = model.CustomerInfo.CustomerName,
                        UserCreate = model.CustomerInfo.CustomerId,
                        DateCreate = DateTime.Now,
                    };
                    _db.tbl_DiscountHistory.Add(itemDiscountHistory);
                    _db.SaveChanges();
                }
                booking.CustomerInfo = model.CustomerInfo;
                booking.CustomerNote = model.CustomerNote;
                booking.BookingCode = Utilities.Utility.RandomCode(6, false);
                //Kiểm tra trùng bookingCode
                while (_db.tbl_Booking.Any(d=>d.BookingCode == booking.BookingCode))
                {
                    booking.BookingCode = Utilities.Utility.RandomCode(6, false);
                }
                booking.Status = (int)Parameters.Booking.BookingStatus.TaoThanhCong;
                booking.CustomerCheckPayment = false;
                booking.IsPaymentSuccess = false;

                if (model.IsInvoceExport)
                {
                    booking.InvoceInfo = model.InvoceInfo;
                }
                booking.PaymentType = model.PaymentType;

                if (booking.PaymentType == (int)Parameters.Booking.PaymentTypes.TienMat)
                {
                    booking.CODCancel = booking.DateCheckOut;
                }

                if (booking.CustomerInfo.CustomerId == Guid.Empty)  //Kh chưa có tài khoản/ Chưa đăng nhập
                {     

                }
                else
                {
                    try
                    {
                        var customer = _db.tbl_Customer.FirstOrDefault(d => d.CustomerId == booking.CustomerInfo.CustomerId);
                        if (string.IsNullOrEmpty(customer.CustomerName))
                        {
                            customer.CustomerName = booking.CustomerInfo.CustomerName;
                        }
                        if (string.IsNullOrEmpty(customer.PhoneNumber))
                        {
                            customer.PhoneNumber = booking.CustomerInfo.CustomerPhoneNumber;
                        }
                        if (customer.ProvinceId == null)
                        {
                            customer.ProvinceId = booking.CustomerInfo.CustomerProvinceId;
                        }
                        if (customer.DistrictId == null)
                        {
                            customer.DistrictId = booking.CustomerInfo.CustomerDistrictId;
                        }
                        if (string.IsNullOrEmpty(customer.Address))
                        {
                            customer.Address = booking.CustomerInfo.CustomerAddress;
                        }
                        _db.Entry(customer).State = System.Data.Entity.EntityState.Modified;
                        _db.SaveChanges();
                    }
                    catch (Exception)
                    {
                    }
                }

                Session["Booking"] = booking;
                #region Send SMS Admin

                string message = "";

                message = "KH {0} booking thanh cong. \r\n";
                message += "Dien thoai: {1} \r\n";
                message += "Email: {2} \r\n";
                message += "Ma booking: {3}";

                message = string.Format(message, Utilities.Utility.fRemoveSign4VietnameseString(booking.CustomerInfo.CustomerName),
                    booking.CustomerInfo.CustomerPhoneNumber ?? "",
                    booking.CustomerInfo.CustomerEmail ?? "",
                    booking.BookingCode);

                SendAdminSMS(isSendSMS, adminPhoneNumber, message); // func send sms

                #endregion

                #region Sent Mail
                try
                {
                    MailControl mailControl = new MailControl();

                    StreamReader rd = new StreamReader(Server.MapPath(@"~/FileUploads/Template/Mail/ConfirmSuccessMail.html"));
                    string template = rd.ReadToEnd();
                    rd.Close();

                    string subject = "HotCombo - Xác nhận yêu cầu đặt phòng thành công";
                    template = template.Replace("{BookingCode}", booking.BookingCode);
                    template = template.Replace("{HotelName}", booking.Hotel.HotelName);
                    template = template.Replace("{NumberStar}", booking.Hotel.HotelRatingTypeId.ToString());
                    template = template.Replace("{HotelAddress}", booking.Hotel.Address);
                    string nhanPhong = string.Format("{0}, {1}, {2}", "14:00", Hotel.Utilities.Utility.GetDayOfWeek((int)booking.DateCheckIn.DayOfWeek), booking.DateCheckIn.ToString("dd/MM/yyyy"));
                    string traPhong = string.Format("{0}, {1}, {2}", "12:00", Hotel.Utilities.Utility.GetDayOfWeek((int)booking.DateCheckOut.DayOfWeek), booking.DateCheckOut.ToString("dd/MM/yyyy"));
                    template = template.Replace("{CheckIn}", nhanPhong);
                    template = template.Replace("{CheckOut}", traPhong);
                    template = template.Replace("{RoomCount}", booking.RoomCount.ToString());

                    template = template.Replace("{CountDay}", booking.CountDate.ToString());
                    template = template.Replace("{CustomerName}", booking.CustomerInfo.CustomerName);
                    template = template.Replace("{CustomerPhone}", booking.CustomerInfo.CustomerPhoneNumber);
                    template = template.Replace("{CustomerEmail}", booking.CustomerInfo.CustomerEmail);
                    template = template.Replace("{PersonCount}", booking.PersonCount.ToString());
                    template = template.Replace("{RoomName}", booking.Room.RoomName);
                    template = template.Replace("{Tax}", booking.Tax.ToString("#,###"));
                    template = template.Replace("{RoomAmount}", booking.RoomAmount.ToString("#,###"));
                    template = template.Replace("{TotalAmount}", booking.TotalAmount.ToString("#,###"));
                    template = template.Replace("{ThongTinLienHe}", Hotel.Utilities.Utility.GetContentValue("header-phone-contact"));
                    template = template.Replace("{HuongDanThanhToanChuyenKhoan}", Hotel.Utilities.Utility.GetContentValue("payment-guide-banking"));
                    template = template.Replace("{LinkBooking}", "https://hotcombo.vn/Booking/ConfirmSuccess?Id="+ booking.BookingId);
                    string bodyMail = template;

                    Email mail = new Email()
                    {
                        To = booking.CustomerInfo.CustomerEmail,
                        ToName = booking.CustomerInfo.CustomerName,
                        Subject = subject,
                        Body = bodyMail,

                    };
                    bool isSent = mailControl.Send(mail);
                    if (isSent)
                    {
                        _db.tbl_Booking.Add(new ConvertModelToDb().ToBookingDb(booking));
                        _db.SaveChanges();
                        Session["Booking"] = null;
                        return Json(new { Status = 1, BookingId = booking.BookingId, Message = "Tạo yêu cầu booking thành công" });
                    }
                    else
                    {
                        return Json(new { Status = 0, Message = "Không gửi được email xác nhận. Vui lòng liên hệ hotline để được hỗ trợ" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { Status = 0, Message = "Tạo yêu cầu booking thất bại. Vui lòng thử lại" });
                }
                #endregion

               
            }
            return Json(new { Status = 0, Message = "Kiểm tra thông tin booking" });
        }

        public ActionResult ConfirmSuccess(Guid id)
        {
            var bookingDb = _db.tbl_Booking.FirstOrDefault(d => d.BookingId == id);
            var hotelDb = _db.tbl_Hotel.FirstOrDefault(d => d.Id == bookingDb.HotelId);
            BookingViewModel booking = new ConvertDbToModel().ToBooking(bookingDb, hotelDb);
            ViewBag.PaymentGuide = GetPaymentGuide(booking.PaymentType);
            ViewBag.Contact = _db.tbl_Content.Where(d => d.ContenCode == "header-phone-contact").Select(d=>d.ContentDetail).FirstOrDefault();
            return View(booking);
        }
        private string GetPaymentGuide(int paymentType)
        {
            string guide = "";
            try
            {                 
                if (paymentType == (int)Parameters.Booking.PaymentTypes.ChuyenKhoan)
                {
                    guide = _db.tbl_Content.FirstOrDefault(d => d.ContenCode == "payment-guide-banking").ContentDetail;
                }
                else
                {
                    guide = _db.tbl_Content.FirstOrDefault(d => d.ContenCode == "payment-guide-money").ContentDetail;
                }
            }
            catch (Exception)
            {
            }
            return guide;
        }
        [HttpPost]
        public ActionResult CustomerPaymentChange(Guid bookingId, int status = 0)
        {
            try
            {
                var booking = _db.tbl_Booking.FirstOrDefault(d => d.BookingId == bookingId);
                if (!booking.CustomerCheckPayment)
                {
                    booking.CustomerCheckPayment = true;
                    booking.Status = (int)Parameters.Booking.BookingStatus.KHDaThanhToan;
                    _db.Entry(booking).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();

                    #region Send SMS Admin
                    string message = "";

                    message = "KH {0} xac nhan thanh toan. \r\n";
                    message += "Dien thoai: {1} \r\n";
                    message += "Email: {2} \r\n";
                    message += "Ma booking: {3}";

                    message = string.Format(message, Utilities.Utility.fRemoveSign4VietnameseString(booking.CustomerName),
                        booking.CustomerPhoneNumber ?? "",
                        booking.CustomerEmail ?? "",
                        booking.BookingCode);

                    SendAdminSMS(isSendSMS, adminPhoneNumber, message); // func send sms

                    #endregion
                }

                return Json(new { Id = 1, Message = "Cập nhật thành công" });
            }
            catch (Exception)
            {

                return Json(new { Id = 0, Message = "" });
            }
        }

        private bool SaveBooking(BookingViewModel model)
        {            
            try
            {
                tbl_Booking booking = new tbl_Booking()
                {
                    BookingId = Guid.NewGuid(),

                };


            }
            catch (Exception)
            {

                throw;
            }
            return false;
        }

        private void SendAdminSMS(string isSendSMS, string adminPhoneNumber, string message)
        {
            SMSControl smsControl = new SMSControl();
            if (isSendSMS == "1" && !string.IsNullOrEmpty(adminPhoneNumber))
            {
                foreach (var phone in adminPhoneNumber.Split(','))
                {
                    if (Utilities.Utility.CheckPhoneNumber(phone))
                    {
                        smsControl.SendJson(phone, message);
                    }
                }
            }
        }
    }
}