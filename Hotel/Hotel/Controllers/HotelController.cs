﻿using Hotel.DataAccess;
using Hotel.EntityFramework.Models;
using Hotel.IDataAccess;
using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.ViewModels;
using PagedList;
using Hotel.Utilities;
using Hotel.Common.Utilities;
using System.Web.Configuration;
using System.Web.Mvc.Html;

namespace Hotel.Controllers
{
    public class HotelController : Controller
    {
        private readonly HotelEntities _db = new HotelEntities();
        private IHotelDataAccess _hotelDA;
        private IHotelDataAccess HotelDA => _hotelDA ?? (_hotelDA = new HotelDataAccess());

        private string strHost
        {
            get
            {
                string strH = WebConfigurationManager.AppSettings["host"].ToString() + "/";
                return strH;
            }
        }

        // GET: Hotel

        //public ActionResult detail()
        //{
        //    return View();
        //}

        [HttpPost]
        public ActionResult Index(SearchViewModel searchViewModel, int? page = 1)
        {
            if (searchViewModel.TypeInput == 0)
            {
                searchViewModel.Keyword = _db.tbl_Hotel.Where(d => d.Id.ToString() == searchViewModel.ItemId)
                        .Select(d => d.HotelName).FirstOrDefault().convertToUnSign2();
                Session["SearchHotel"] = searchViewModel;
                string rewriteUrl = ("/khach-san/" + (searchViewModel.Keyword ?? "-") + "-v"
                    + searchViewModel.ItemId + ".aspx?" + searchViewModel.GetSEOFriendlyURL());
                return RedirectPermanent(rewriteUrl);
            }
            if (searchViewModel.TypeInput == 1 && !string.IsNullOrEmpty(searchViewModel.ItemId) && searchViewModel.ItemId != "Refresh")
            {
                searchViewModel.Keyword = _db.tbl_Province.Where(d => d.ProvinceId.ToString() == searchViewModel.ItemId)
                        .Select(d => d.ProvinceName).FirstOrDefault().convertToUnSign2();
                Session["SearchHotel"] = searchViewModel;
                string rewriteUrl = ("/khach-san/tp"+ searchViewModel.ItemId +"/khach-san-tai-" + (searchViewModel.Keyword ?? "-")
                    +".aspx?" + searchViewModel.GetSEOFriendlyURL());
                return RedirectPermanent(rewriteUrl);
            }

            return RedirectPermanent("/khach-san");
        }
        public ActionResult Index(SearchViewModel searchViewModel, string dateCheckIn, string dateCheckOut, int? page = 1)
        {
            try
            {
                int pageSize = 4;
                int pageNumber = (page ?? 1);

                if (searchViewModel.TypeInput == 0)
                {
                    searchViewModel.Keyword = _db.tbl_Hotel.Where(d => d.Id.ToString() == searchViewModel.ItemId)
                        .Select(d => d.HotelName).FirstOrDefault().convertToUnSign2();
                    Session["SearchHotel"] = searchViewModel;
                    string rewriteUrl = ("/khach-san/" + (searchViewModel.Keyword ?? "-") + "-v" 
                        + searchViewModel.ItemId + ".aspx?" + searchViewModel.GetSEOFriendlyURL());
                    return RedirectPermanent(rewriteUrl);
                    //Session["SearchHotel"] = searchViewModel;
                    //return RedirectToAction("Detail", searchViewModel);
                }


                var listDistrictForProvince = new int[100];

                ViewBag.ArrListRatingTypeStr = "";
                string[] arrListRatingTypeStr = { };

                ViewBag.ArrListPriceTypeStr = "";
                string[] arrListPriceTypeStr = { };

                ViewBag.ArrListServiceTypeStr = "";
                string[] arrListServiceTypeStr = { };

                ViewBag.ArrListConvenientTypeStr = "";
                string[] arrListConvenientTypeStr = { };

                SearchViewModel search = Session["SearchHotel"] as SearchViewModel;

                if (search == null)
                    search = new SearchViewModel();

                if (searchViewModel.TypeInputText != null)
                    search.TypeInputText = searchViewModel.TypeInputText;
                if (searchViewModel.Keyword != null)
                    search.Keyword = searchViewModel.Keyword;
                if (searchViewModel.StartDate != null)
                    search.StartDate = searchViewModel.StartDate;
                if (searchViewModel.EndDate != null)
                    search.EndDate = searchViewModel.EndDate;
                if (searchViewModel.NumberRoom != null)
                    search.NumberRoom = searchViewModel.NumberRoom;
                if (searchViewModel.NumberAdul != null)
                    search.NumberAdul = searchViewModel.NumberAdul;
                if (searchViewModel.NumberChildren != null)
                    search.NumberChildren = searchViewModel.NumberChildren;
                if (searchViewModel.PriceFrom != null)
                    search.PriceFrom = searchViewModel.PriceFrom;
                else
                    search.PriceFrom = searchViewModel.PriceFrom;
                if (searchViewModel.PriceTo != null)
                    search.PriceTo = searchViewModel.PriceTo;
                else
                    search.PriceTo = searchViewModel.PriceTo;
                if (searchViewModel.HotelPriceType != null)
                {
                    search.HotelPriceType = searchViewModel.HotelPriceType;
                    arrListPriceTypeStr = search.HotelPriceType.Split(',');
                    ViewBag.ArrListPriceTypeStr = arrListPriceTypeStr;
                }
                else
                {
                    search.HotelPriceType = searchViewModel.HotelPriceType;
                    ViewBag.ArrListPriceTypeStr = arrListPriceTypeStr;
                }


                if (searchViewModel.HotelRatingType != null)
                {
                    search.HotelRatingType = searchViewModel.HotelRatingType;
                    arrListRatingTypeStr = search.HotelRatingType.Split(',');
                    ViewBag.ArrListRatingTypeStr = arrListRatingTypeStr;
                }
                else
                {
                    search.HotelRatingType = searchViewModel.HotelRatingType;
                    //arrListRatingTypeStr = search.HotelRatingType.Split(',');
                    ViewBag.ArrListRatingTypeStr = arrListRatingTypeStr;
                }

                if (searchViewModel.HotelType != null)
                    search.HotelType = searchViewModel.HotelType;
                else
                    search.HotelType = searchViewModel.HotelType;
                if (searchViewModel.HotelServiceType != null)
                {
                    search.HotelServiceType = searchViewModel.HotelServiceType;
                    arrListServiceTypeStr = search.HotelServiceType.Split(',');
                    search.HotelServiceType = searchViewModel.HotelServiceType;
                }
                else
                {
                    search.HotelServiceType = searchViewModel.HotelServiceType;
                    search.HotelServiceType = searchViewModel.HotelServiceType;
                }

                if (searchViewModel.HotelConvenientType != null)
                {
                    search.HotelConvenientType = searchViewModel.HotelConvenientType;
                    arrListConvenientTypeStr = search.HotelConvenientType.Split(',');
                    search.HotelConvenientType = searchViewModel.HotelConvenientType;
                }
                else
                {
                    search.HotelConvenientType = searchViewModel.HotelConvenientType;
                    search.HotelConvenientType = searchViewModel.HotelConvenientType;
                }

                if (searchViewModel.HotelArea != null)
                    search.HotelArea = searchViewModel.HotelArea;
                else
                    search.HotelArea = searchViewModel.HotelArea;
                if (searchViewModel.OrderPrice != null)
                    search.OrderPrice = searchViewModel.OrderPrice;
                else
                    search.OrderPrice = searchViewModel.OrderPrice;
                if (searchViewModel.OrderRating != null)
                    search.OrderRating = searchViewModel.OrderRating;
                else
                    search.OrderRating = searchViewModel.OrderRating;



                if (searchViewModel.ItemId != null)
                {
                    if(searchViewModel.ItemId == "Resfresh")
                    {
                        search.TypeInput = searchViewModel.TypeInput;
                        search.ItemId = null;
                    }
                    else
                    {
                        search.TypeInput = searchViewModel.TypeInput;
                        search.ItemId = searchViewModel.ItemId;
                    }
                    
                }
                //else
                //{
                //    search.TypeInput = 1;
                //    search.ItemId = "50";
                //}

                if (search.TypeInput == 1 && search.ItemId != null)
                {
                    var idProvince = Int32.Parse(search.ItemId);
                    var nameHotelsSearch = _db.tbl_Province.Find(idProvince);
                    listDistrictForProvince = _db.tbl_District.Where(x => x.ProvinceId == nameHotelsSearch.ProvinceId).Select(x => x.DistrictId).ToArray();
                    ViewBag.NameHotelsSearch = nameHotelsSearch;
                }

                //HotelPriceType: Loại giá
                var hotelPriceType = _db.tbl_HotelPriceType.Where(x => x.IsDelete == false).ToList();
                var hotelPriceTypeViewModel = hotelPriceType.Select(s => new ViewModels.HotelPriceTypeViewModel()
                {
                    Id = s.Id,
                    PriceTypeName = s.PriceTypeName,
                    IsCheck = ""
                }).ToList();
                foreach (var item in hotelPriceTypeViewModel)
                {
                    for (int i = 0; i < arrListPriceTypeStr.Length; i++)
                    {
                        if (item.Id == Int32.Parse(arrListPriceTypeStr[i]))
                        {
                            item.IsCheck = "checked";
                            break;
                        }
                        else
                        {
                            item.IsCheck = "";
                        }
                    }
                }

                ViewBag.HotelPriceType = hotelPriceTypeViewModel;

                //HotelRatingType: Hạng sao
                var hotelRatingType = _db.tbl_HotelRatingType.Where(x => x.IsDelete == false).OrderBy(x => x.NumberStar).ToList();
                var hotelRatingTypeViewModel = hotelRatingType.Select(s => new ViewModels.HotelRatingTypeViewModel()
                {
                    Id = s.Id,
                    NumberStar = s.NumberStar,
                    Name = s.Name,
                    IsCheck = ""
                }).ToList();

                foreach (var item in hotelRatingTypeViewModel)
                {
                    for (int i = 0; i < arrListRatingTypeStr.Length; i++)
                    {
                        if (item.NumberStar == Int32.Parse(arrListRatingTypeStr[i]))
                        {
                            item.IsCheck = "checked";
                            break;
                        }
                        else
                        {
                            item.IsCheck = "";
                        }
                    }
                }

                ViewBag.HotelRatingType = hotelRatingTypeViewModel;

                //HotelType: Loai khach san
                var hotelType = _db.tbl_HotelType.Where(x => x.IsDelete == false).OrderBy(x => x.HotelTypeName).ToList();
                ViewBag.HotelType = hotelType;

                //HotelServiceType: dich vu dinh kem
                var hotelServiceType = _db.tbl_HotelServiceType.Where(x => x.IsDelete == false).OrderBy(x => x.ServiceName).ToList();
                var hotelServiceTypeViewModel = hotelServiceType.Select(s => new ViewModels.HotelServiceTypeViewModel()
                {
                    Id = s.Id,
                    ServiceName = s.ServiceName,
                    IsCheck = ""
                }).ToList();
                foreach (var item in hotelServiceTypeViewModel)
                {
                    for (int i = 0; i < arrListServiceTypeStr.Length; i++)
                    {
                        if (item.Id == Int32.Parse(arrListServiceTypeStr[i]))
                        {
                            item.IsCheck = "checked";
                            break;
                        }
                        else
                        {
                            item.IsCheck = "";
                        }
                    }
                }
                ViewBag.HotelServiceType = hotelServiceTypeViewModel;

                //HotelConvenientType: tien nghi khach san
                var hotelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false && x.Type == 0).ToList();
                var hotelConvenientTypeViewModel = hotelConvenientType.Select(s => new ViewModels.HotelConvenientTypeViewModel()
                {
                    Id = s.Id,
                    ConvenientName = s.ConvenientName,
                    Type = s.Type,
                    IsCheck = ""
                }).ToList();
                foreach (var item in hotelConvenientTypeViewModel)
                {
                    for (int i = 0; i < arrListConvenientTypeStr.Length; i++)
                    {
                        if (item.Id == Int32.Parse(arrListConvenientTypeStr[i]))
                        {
                            item.IsCheck = "checked";
                            break;
                        }
                        else
                        {
                            item.IsCheck = "";
                        }
                    }
                }
                ViewBag.HotelConvenientType = hotelConvenientTypeViewModel;

                var province = _db.tbl_Province.Where(d => d.CountryId == 1 && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
                ViewBag.Province = province;

                //HotelArea: Khu vuc
                var area = _db.tbl_Area.Where(x => x.IsDelete == false && listDistrictForProvince.Contains(x.DistrictId)).OrderBy(x => x.AreaName).ToList();
                ViewBag.Area = area;

                var viewModel = new List<Hotel.ViewModels.HotelViewModel>();
                ViewBag.Search = search;
                Session["SearchHotel"] = search;

                string Title = "";
                string Keyword = "";
                string Introduction = "";
                string Image_src = "";
                string link = "/khach-san";

                string breadcrumbs = string.Empty;
                breadcrumbs += Utility.initBreadcrumbs("Trang chủ", strHost, 1) + " » ";
                Title = "Khách sạn";
                breadcrumbs += Utility.initBreadcrumbs(Title, link, 2);

                initBreadCrumbs(Title, Keyword, Introduction, Request.Url.ToString(), Image_src, breadcrumbs);


                return View(viewModel.ToPagedList(pageNumber, pageSize));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult LayDanhSachKhachSan(SearchViewModel searchViewModel, int? page = 1)
        {
            try
            {
                int pageSize = 4;
                int pageNumber = (page ?? 1);

                //SearchModel session hien tai
                SearchViewModel search = Session["SearchHotel"] as SearchViewModel;
                if (search == null)
                    search = new SearchViewModel();

                #region Cập nhật giá trị sesstion
                //if (searchViewModel.ItemId == null)
                //{
                //    search.TypeInput = 1;
                //    //search.ItemId = searchViewModel.ItemId;
                //}
                if (searchViewModel.TypeInputText != null)
                    search.TypeInputText = searchViewModel.TypeInputText;
                if (searchViewModel.Keyword != null)
                    search.Keyword = searchViewModel.Keyword;
                //if (searchViewModel.StartDate != null)
                //    search.StartDate = searchViewModel.StartDate;
                //if (searchViewModel.EndDate != null)
                //    search.EndDate = searchViewModel.EndDate;
                if (searchViewModel.NumberRoom != null)
                    search.NumberRoom = searchViewModel.NumberRoom;
                if (searchViewModel.NumberAdul != null)
                    search.NumberAdul = searchViewModel.NumberAdul;
                if (searchViewModel.NumberChildren != null)
                    search.NumberChildren = searchViewModel.NumberChildren;
                if (searchViewModel.PriceFrom != null)
                    search.PriceFrom = searchViewModel.PriceFrom;
                else
                    search.PriceFrom = searchViewModel.PriceFrom;
                if (searchViewModel.PriceTo != null)
                    search.PriceTo = searchViewModel.PriceTo;
                else
                    search.PriceTo = searchViewModel.PriceTo;
                if (searchViewModel.HotelPriceType != null)
                {
                    search.HotelPriceType = searchViewModel.HotelPriceType;
                }
                else
                {
                    search.HotelPriceType = searchViewModel.HotelPriceType;
                }


                if (searchViewModel.HotelRatingType != null)
                {
                    search.HotelRatingType = searchViewModel.HotelRatingType;
                }
                else
                {
                    search.HotelRatingType = searchViewModel.HotelRatingType;
                }

                if (searchViewModel.HotelType != null)
                    search.HotelType = searchViewModel.HotelType;
                else
                    search.HotelType = searchViewModel.HotelType;
                if (searchViewModel.HotelServiceType != null)
                {
                    search.HotelServiceType = searchViewModel.HotelServiceType;
                }
                else
                {
                    search.HotelServiceType = searchViewModel.HotelServiceType;
                    search.HotelServiceType = searchViewModel.HotelServiceType;
                }

                if (searchViewModel.HotelConvenientType != null)
                {
                    search.HotelConvenientType = searchViewModel.HotelConvenientType;
                }
                else
                {
                    search.HotelConvenientType = searchViewModel.HotelConvenientType;
                }

                if (searchViewModel.HotelArea != null)
                    search.HotelArea = searchViewModel.HotelArea;
                else
                    search.HotelArea = searchViewModel.HotelArea;
                if (searchViewModel.OrderPrice != null)
                    search.OrderPrice = searchViewModel.OrderPrice;
                else
                    search.OrderPrice = searchViewModel.OrderPrice;
                if (searchViewModel.OrderRating != null)
                    search.OrderRating = searchViewModel.OrderRating;
                else
                    search.OrderRating = searchViewModel.OrderRating;
                #endregion

                #region Lấy danh sách khác sạn
                var model = HotelDA.HotelDanhSach(search).OrderBy(x => x.PriceContract);
                if (search.OrderPrice == 0)
                {
                    model = HotelDA.HotelDanhSach(search).OrderBy(x => x.PriceContract);
                }
                else if (search.OrderPrice == 1)
                {
                    model = HotelDA.HotelDanhSach(search).OrderByDescending(x => x.PriceContract);
                }

                if (search.OrderRating == 0)
                {
                    model = HotelDA.HotelDanhSach(search).OrderBy(x => x.NumberStar);
                }
                else if (search.OrderRating == 1)
                {
                    model = HotelDA.HotelDanhSach(search).OrderByDescending(x => x.NumberStar);
                }

                var viewModel = model.Select(s => new ViewModels.HotelViewModel()
                {
                    RoomId = s.RoomId,
                    HotelId = s.HotelId,
                    AvailableRoom = s.AvailableRoom,
                    IsCancel = s.IsCancel,
                    HotelServiceType = s.HotelServiceType,
                    HotelConvenientTypeHotel = s.HotelConvenientTypeHotel,
                    RoomName = s.RoomName,
                    HotelName = s.HotelName,
                    Address = s.Address,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    Content = s.Content,
                    Image = s.Image,
                    ImageRoom = s.ImageRoom,
                    Direction = s.Direction,
                    //DirectionName = s.DirectionName,
                    SingleBed = s.SingleBed,
                    DoubleBed = s.DoubleBed,
                    MaxPeople = s.MaxPeople,
                    HotelRatingTypeId = s.HotelRatingTypeId,
                    NumberStar = s.NumberStar,
                    PriceContract = s.PriceContract,
                    PricePromotion = s.PricePromotion
                }).ToList();
                foreach (var item in viewModel)
                {
                    #region DANH SACH BINH LUAN

                    //Thong tin binh luan tong hop
                    var modelCommentAVG = HotelDA.HotelCommentAvg(item.HotelId.ToString());
                    var viewModelCommentAVG = modelCommentAVG.Select(s => new CommentViewModel()
                    {
                        NumAll = s.NumAll,
                        NumCongTac = s.NumCongTac,
                        NumCapDoi = s.NumCapDoi,
                        NumGiaDinh = s.NumGiaDinh,
                        NumBanBe = s.NumBanBe,
                        NumCaNhan = s.NumCaNhan,
                        PointLocation = (double)Math.Round(s.PointLocation, 2),
                        PointConvenient = s.PointConvenient,
                        PointCost = s.PointCost,
                        PointClean = s.PointClean,
                        PointServe = s.PointServe,
                        PointLocationText = String.Format("{0:0.0}", s.PointLocation),
                        PointConvenientText = String.Format("{0:0.0}", s.PointConvenient),
                        PointCostText = String.Format("{0:0.0}", s.PointCost),
                        PointCleanText = String.Format("{0:0.0}", s.PointClean),
                        PointServeText = String.Format("{0:0.0}", s.PointServe)
                    }).ToList();

                    foreach (var itemAVG in viewModelCommentAVG)
                    {
                        itemAVG.PointAvg = (itemAVG.PointClean + itemAVG.PointConvenient + itemAVG.PointCost + itemAVG.PointLocation + itemAVG.PointServe) / 5;
                        itemAVG.PointAvgText = String.Format("{0:0.0}", itemAVG.PointAvg);
                        if (itemAVG.PointAvg >= 0 && itemAVG.PointAvg < 5)
                        {
                            itemAVG.RankText = "Kém";
                        }
                        else if (itemAVG.PointAvg >= 5 && itemAVG.PointAvg < 7)
                        {
                            itemAVG.RankText = "Trung Bình";
                        }
                        else if (itemAVG.PointAvg >= 7 && itemAVG.PointAvg < 8)
                        {
                            itemAVG.RankText = "Tốt";
                        }
                        else if (itemAVG.PointAvg >= 8 && itemAVG.PointAvg < 9)
                        {
                            itemAVG.RankText = "Rẩt tốt";
                        }
                        else if (itemAVG.PointAvg >= 9 && itemAVG.PointAvg <= 10)
                        {
                            itemAVG.RankText = "Tuyệt vời";
                        }
                        else
                        {
                            itemAVG.RankText = "Đánh giá";
                        }
                    }
                    item.PointReview = viewModelCommentAVG[0].PointAvgText;
                    item.PointReviewText = viewModelCommentAVG[0].RankText;
                    item.CountReview = viewModelCommentAVG[0].NumAll;
                    #endregion

                    #region Tien ich

                    item.ConvenientTypeList = new List<ConvenientTypeViewModel>();
                    string[] convenientTypeIds = { };
                    if (item.HotelConvenientTypeHotel != "" && item.HotelConvenientTypeHotel != null)
                    {
                        convenientTypeIds = item.HotelConvenientTypeHotel.Split(new char[] { ',' });
                    }
                    var modelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

                    var viewModelConvenientType = modelConvenientType.Select(s => new ConvenientTypeViewModel()
                    {
                        Id = s.Id,
                        ConvenientName = s.ConvenientName,
                        Symbol = s.Symbol,
                        Type = s.Type,
                        Order = s.Order,
                        IsDelete = s.IsDelete,
                        UserCreate = s.UserCreate,
                        DateCreate = s.DateCreate,
                        UserUpdate = s.UserUpdate,
                        DateUpdate = s.DateUpdate,

                    }).ToList();

                    foreach (var itemConvenientType in viewModelConvenientType)
                    {
                        foreach (var check in convenientTypeIds)
                        {
                            if (itemConvenientType.Id == int.Parse(check))
                            {
                                item.ConvenientTypeList.Add(itemConvenientType);
                            }
                        }
                    }

                    #endregion
                }
                #endregion

                //Lay gia tri danh gia

                ViewBag.CountKhachSanSearch = viewModel.Count;
                Session["SearchHotel"] = search;

                return PartialView("_DanhSachKhachSan", viewModel.ToPagedList(pageNumber, pageSize));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult _SearchHotels()
        {
            SearchViewModel search = Session["SearchHotel"] as SearchViewModel;
            //var province = _db.tbl_Province.Where(d => d.CountryId == "1" && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
            //ViewBag.Province = province;
            ViewBag.Search = search;
            List<SearchHotelSuggestModel> result = new List<SearchHotelSuggestModel>();
            List<tbl_Hotel> listHotel = _db.tbl_Hotel.Where(d => d.IsActive == 1 && d.IsDelete != true).OrderBy(d => d.HotelName).ToList();
            foreach (var item in listHotel)
            {
                result.Add(new SearchHotelSuggestModel() { Id = item.Id.ToString(), Name = item.HotelName, Type = "0" });
            }
            List<tbl_Province> listProvince = _db.tbl_Province.Where(d => d.CountryId == 1 && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
            foreach (var item in listProvince)
            {
                result.Add(new SearchHotelSuggestModel() { Id = item.ProvinceId.ToString(), Name = item.ProvinceName, Type = "1" });
            }
            ViewBag.hotelSuggest = result;
            return PartialView();
        }
        public ActionResult Detail(SearchViewModel searchViewModel)
        {
            int? provinceId = null;
            SearchViewModel searchKhachSanTuongTu = new SearchViewModel();

            //Lấy ra id khách sạn
            SearchViewModel search = (SearchViewModel)Session["SearchHotel"] ?? new SearchViewModel();
            search.ItemId = searchViewModel.ItemId;
            search.TypeInput = 0;
            
            var idHotel = Guid.Parse(search.ItemId);
           
            #region THÔNG TIN KHÁCH SẠN

            //lấy thông tin khách sạn
            var modelHotel = _db.tbl_Hotel.Where(x => x.Id == idHotel).ToList();
            provinceId = modelHotel[0].ProvinceId;
            var viewModelHotel = modelHotel.Select(s => new HotelItemViewModel()
            {
                Id = s.Id,
                HotelName = s.HotelName,
                CountryId = s.CountryId,
                ProvinceId = s.ProvinceId,
                DistrictId = s.DistrictId,
                AreaId = s.AreaId,
                HotelRatingTypeId = s.HotelRatingTypeId,
                HotelTypeId = s.HotelTypeId,
                HotelConvenientType = s.HotelConvenientType,
                Address = s.Address,
                Telephone = s.Telephone,
                Mobile = s.Mobile,
                Longitude = s.Longitude,
                Latitude = s.Latitude,
                Content = s.Content,
                Email = s.Email,
                Url = s.Url,
                Image = s.Image,
                IsActive = s.IsActive,
                IsDelete = s.IsDelete,
                UserCreate = s.UserCreate,
                DateCreate = s.DateCreate,
                UserUpdate = s.UserUpdate,
                DateUpdate = s.DateUpdate,
                PriceFrom = s.PriceFrom,
                PricePromotion = s.PricePromotion,
                CancelTime = s.CancelTime,
            }).ToList();
            foreach (var item in viewModelHotel)
            {
                item.ConvenientTypeList = new List<ConvenientTypeViewModel>();
                string[] convenientTypeIds = { };
                if (item.HotelConvenientType != "" && item.HotelConvenientType != null)
                {
                    convenientTypeIds = item.HotelConvenientType.Split(new char[] { ',' });
                }
                var modelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

                var viewModelConvenientType = modelConvenientType.Select(s => new ConvenientTypeViewModel()
                {
                    Id = s.Id,
                    ConvenientName = s.ConvenientName,
                    Symbol = s.Symbol,
                    Type = s.Type,
                    Order = s.Order,
                    IsDelete = s.IsDelete,
                    UserCreate = s.UserCreate,
                    DateCreate = s.DateCreate,
                    UserUpdate = s.UserUpdate,
                    DateUpdate = s.DateUpdate,

                }).ToList();

                foreach (var itemConvenientType in viewModelConvenientType)
                {
                    foreach (var check in convenientTypeIds)
                    {
                        if (itemConvenientType.Id == int.Parse(check))
                        {
                            item.ConvenientTypeList.Add(itemConvenientType);
                        }
                    }
                }

            }
            ViewBag.HotelItem = viewModelHotel;

            //Lay thông tin tien nghi khách san


            //Lay hinh anh khách sạn
            var modelProductImages = _db.tbl_ProductImage.Where(x => x.HotelId == idHotel && x.Type != 4).OrderBy(x => x.Type).ToList();
            ViewBag.ProductImages = modelProductImages;
            ViewBag.ProductImageFirst = new tbl_ProductImage();
            if (modelProductImages.Count == 0)
            {
                ViewBag.ProductImageFirst = new tbl_ProductImage()
                { ImageUrl = "default-image.jpg" };
                
            }
            else
            {
                ViewBag.ProductImageFirst = modelProductImages?.First();
            }

            #endregion

            #region LẤY THÔNG TIN DANH SÁCH PHÒNG THEO NGÀY
            var model = HotelDA.HotelDanhSach(search).OrderBy(x => x.PriceContract);
            var viewModel = model.Select(s => new ViewModels.HotelViewModel()
            {
                RoomId = s.RoomId,
                HotelId = s.HotelId,
                AvailableRoom = s.AvailableRoom,
                IsCancel = s.IsCancel,
                HotelServiceType = s.HotelServiceType,
                HotelConvenientTypeHotel = s.HotelConvenientTypeHotel,
                RoomName = s.RoomName,
                HotelName = s.HotelName,
                Address = s.Address,
                Latitude = s.Latitude,
                Longitude = s.Longitude,
                Content = s.Content,
                Image = s.Image,
                ImageRoom = s.ImageRoom,
                Direction = s.Direction,
                //DirectionName = s.DirectionName,
                SingleBed = s.SingleBed,
                DoubleBed = s.DoubleBed,
                MaxPeople = s.MaxPeople,
                HotelRatingTypeId = s.HotelRatingTypeId,
                NumberStar = s.NumberStar,
                PriceContract = s.PriceContract,
                PricePromotion = s.PricePromotion,
                RoomArea = s.RoomArea
            }).ToList();
            foreach (var item in viewModel)
            {
                //Lay danh sách huong phòng
                string[] directionIds = { };
                directionIds = item.Direction.Split(new char[] { ',' });
                var directionModel = _db.tbl_Direction.Where(d => d.IsDelete == false).ToList();
                foreach (var itemDirection in directionModel)
                {
                    foreach (var check in directionIds)
                    {
                        if (itemDirection.Id == int.Parse(check))
                        {
                            item.DirectionName += itemDirection.DirectionName + ", ";
                        }
                    }
                }
                item.DirectionName = item.DirectionName.Remove(item.DirectionName.Length - 2);

                item.ConvenientTypeList = new List<ConvenientTypeViewModel>();
                string[] convenientTypeIds = { };
                if (item.HotelConvenientTypeHotel != "" && item.HotelConvenientTypeHotel != null)
                {
                    convenientTypeIds = item.HotelConvenientTypeHotel.Split(new char[] { ',' });
                }
                var modelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

                var viewModelConvenientType = modelConvenientType.Select(s => new ConvenientTypeViewModel()
                {
                    Id = s.Id,
                    ConvenientName = s.ConvenientName,
                    Symbol = s.Symbol,
                    Type = s.Type,
                    Order = s.Order,
                    IsDelete = s.IsDelete,
                    UserCreate = s.UserCreate,
                    DateCreate = s.DateCreate,
                    UserUpdate = s.UserUpdate,
                    DateUpdate = s.DateUpdate,

                }).ToList();

                foreach (var itemConvenientType in viewModelConvenientType)
                {
                    foreach (var check in convenientTypeIds)
                    {
                        if (itemConvenientType.Id == int.Parse(check))
                        {
                            item.ConvenientTypeList.Add(itemConvenientType);
                        }
                    }
                }

            }
            #endregion

            #region DANH SÁCH KHÁCH SẠN TƯƠNG TỰ KHÁC
            if(provinceId != null)
            {
                searchKhachSanTuongTu = search;
                searchKhachSanTuongTu.TypeInput = 1;
                searchKhachSanTuongTu.ItemId = provinceId.ToString();

                var modelKhachSanTuongTu = HotelDA.HotelDanhSach(searchKhachSanTuongTu).OrderBy(x => x.PriceContract);
                var viewModelKhachSanTuongTu = modelKhachSanTuongTu.Select(s => new ViewModels.HotelViewModel()
                {
                    RoomId = s.RoomId,
                    HotelId = s.HotelId,
                    AvailableRoom = s.AvailableRoom,
                    IsCancel = s.IsCancel,
                    HotelServiceType = s.HotelServiceType,
                    HotelConvenientTypeHotel = s.HotelConvenientTypeHotel,
                    RoomName = s.RoomName,
                    HotelName = s.HotelName,
                    Address = s.Address,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    Content = s.Content,
                    Image = s.Image,
                    ImageRoom = s.ImageRoom,
                    Direction = s.Direction,
                    //DirectionName = s.DirectionName,
                    SingleBed = s.SingleBed,
                    DoubleBed = s.DoubleBed,
                    MaxPeople = s.MaxPeople,
                    HotelRatingTypeId = s.HotelRatingTypeId,
                    NumberStar = s.NumberStar,
                    PriceContract = s.PriceContract,
                    PricePromotion = s.PricePromotion
                }).ToList();
                foreach (var item in viewModelKhachSanTuongTu)
                {
                    item.ConvenientTypeList = new List<ConvenientTypeViewModel>();
                    string[] convenientTypeIds = { };
                    if (item.HotelConvenientTypeHotel != "" && item.HotelConvenientTypeHotel != null)
                    {
                        convenientTypeIds = item.HotelConvenientTypeHotel.Split(new char[] { ',' });
                    }
                    var modelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

                    var viewModelConvenientType = modelConvenientType.Select(s => new ConvenientTypeViewModel()
                    {
                        Id = s.Id,
                        ConvenientName = s.ConvenientName,
                        Symbol = s.Symbol,
                        Type = s.Type,
                        Order = s.Order,
                        IsDelete = s.IsDelete,
                        UserCreate = s.UserCreate,
                        DateCreate = s.DateCreate,
                        UserUpdate = s.UserUpdate,
                        DateUpdate = s.DateUpdate,

                    }).ToList();

                    foreach (var itemConvenientType in viewModelConvenientType)
                    {
                        foreach (var check in convenientTypeIds)
                        {
                            if (itemConvenientType.Id == int.Parse(check))
                            {
                                item.ConvenientTypeList.Add(itemConvenientType);
                            }
                        }
                    }

                }
                ViewBag.KhachSanTuongTu = viewModelKhachSanTuongTu;
            }
            #endregion

            #region DANH SACH BINH LUAN

            //Thong tin binh luan tong hop
            var modelCommentAVG = HotelDA.HotelCommentAvg(idHotel.ToString());
            var viewModelCommentAVG = modelCommentAVG.Select(s => new CommentViewModel() {
                NumAll = s.NumAll,
                NumCongTac = s.NumCongTac,
                NumCapDoi = s.NumCapDoi,
                NumGiaDinh = s.NumGiaDinh,
                NumBanBe = s.NumBanBe,
                NumCaNhan = s.NumCaNhan,
                PointLocation = (double)Math.Round(s.PointLocation, 2),
                PointConvenient = s.PointConvenient,
                PointCost = s.PointCost,
                PointClean = s.PointClean,
                PointServe = s.PointServe,
                PointLocationText = String.Format("{0:0.0}", s.PointLocation),
                PointConvenientText = String.Format("{0:0.0}", s.PointConvenient),
                PointCostText = String.Format("{0:0.0}", s.PointCost),
                PointCleanText = String.Format("{0:0.0}", s.PointClean),
                PointServeText = String.Format("{0:0.0}", s.PointServe)
            }).ToList(); 

            foreach(var item in viewModelCommentAVG)
            {
                item.PointAvg = (item.PointClean + item.PointConvenient + item.PointCost + item.PointLocation + item.PointServe) / 5;
                item.PointAvgText = String.Format("{0:0.0}", item.PointAvg);
                if (item.PointAvg >= 0 && item.PointAvg < 5)
                {
                    item.RankText = "Kém";
                }
                else if(item.PointAvg >= 5 && item.PointAvg < 7)
                {
                    item.RankText = "Trung Bình";
                }
                else if (item.PointAvg >= 7 && item.PointAvg < 8)
                {
                    item.RankText = "Tốt";
                }
                else if (item.PointAvg >= 8 && item.PointAvg < 9)
                {
                    item.RankText = "Rẩt tốt";
                }
                else if (item.PointAvg >= 9 && item.PointAvg <= 10)
                {
                    item.RankText = "Tuyệt vời";
                }
                else
                {
                    item.RankText = "Đánh giá";
                }
            }
            ViewBag.CommentAVG = viewModelCommentAVG;
            #endregion
            #region dong code cmt lại
            //var modelHotelComment = _db.tbl_HotelComment.Where(x => x.HotelId == idHotel && x.IsDelete == false).OrderByDescending(x => x.DateTimeComment).ToList();

            //var viewModelHotelComment = modelHotelComment.Select(s => new HotelCommentViewModel()
            //{
            //    Id = s.Id,
            //    HotelId = s.HotelId,
            //    CustomerId = s.CustomerId,
            //    Content = s.Content,
            //    DateTimeComment = s.DateTimeComment,
            //    PointLocation = s.PointLocation,
            //    PointServe = s.PointServe,
            //    PointConvenient = s.PointConvenient,
            //    PointCost = s.PointCost,
            //    PointClean = s.PointClean,
            //    PointLocationText = String.Format("{0:0.0}", s.PointLocation),
            //    PointConvenientText = String.Format("{0:0.0}", s.PointConvenient),
            //    PointCostText = String.Format("{0:0.0}", s.PointCost),
            //    PointCleanText = String.Format("{0:0.0}", s.PointClean),
            //    PointServeText = String.Format("{0:0.0}", s.PointServe),
            //    PointAvg = (s.PointClean + s.PointConvenient + s.PointCost + s.PointLocation + s.PointServe) / 5,
            //    PointAvgText = String.Format("{0:0.0}", (s.PointClean + s.PointConvenient + s.PointCost + s.PointLocation + s.PointServe) / 5),
            //    RankText = GetRankText((s.PointClean + s.PointConvenient + s.PointCost + s.PointLocation + s.PointServe) / 5),
            //    Type = s.Type,
            //    TypeIcon = GetTypeCommentIcon(s.Type),
            //    IsDelete = s.IsDelete,
            //    UserCreate = s.UserCreate,
            //    DateCreate = s.DateCreate,
            //    UserUpdate = s.UserUpdate,
            //    DateUpdate = s.DateUpdate
            //}).ToList();

            //foreach (var item in viewModelHotelComment)
            //{
            //    var modelCustommer = _db.tbl_Customer.Where(x => x.IsDelete == false).ToList();
            //    foreach (var cus in modelCustommer)
            //    {
            //        if (item.CustomerId == cus.CustomerId)
            //        {
            //            item.CustomerName = cus.CustomerName;
            //            break;
            //        }
            //    }
            //}

            //ViewBag.viewModelHotelComment = viewModelHotelComment;
            #endregion


            search.ItemId = searchViewModel.ItemId;
            search.TypeInput = 0;
            ViewBag.Search = search;
            Session["SearchHotel"] = search;


            //== init breadCrumbs ==
            //GetBreadCrumbs(tour, destination);\
            
            string breadcrumbs = string.Empty;
            breadcrumbs += Utility.initBreadcrumbs("Trang chủ", strHost, 1);
            //page2
            string Title2 = "";
            string Keyword2 = "";
            string Introduction2 = "";
            string Image_src2 = "";
            string link2 = "/khach-san";
            Title2 = "Khách sạn";
            breadcrumbs += " » " + Utility.initBreadcrumbs(Title2, link2, 2);
            //page3
            string Title3 = "";
            string Keyword3 = "";
            string Introduction3 = "";
            string Image_src3 = "";
            string link3 = string.Format("/khach-san/{0}-v{1}.aspx?{2}", viewModelHotel[0].HotelName.convertToUnSign2(), viewModelHotel[0].Id,search.GetSEOFriendlyURL());
            Title3 = viewModelHotel[0].HotelName;
            breadcrumbs += " » " + Utility.initBreadcrumbs(Title3, link3, 3);


            initBreadCrumbs(Title3, Keyword3, Introduction3, Request.Url.ToString(), Image_src3, breadcrumbs);

            return View(viewModel);
        }
        [HttpGet]
        public ActionResult _LoadImageRoom(Guid roomId)
        {
            // Lấy danh sách hình anh
            var modelProductImages = _db.tbl_ProductImage.Where(x => x.RoomId == roomId && x.Type == 4).OrderBy(x => x.Type).ToList();
            ViewBag.ProductImages = modelProductImages;

            //Lay danh sách tiện nghi
            var modelRoom = _db.tbl_Room.Where(x => x.Id == roomId).FirstOrDefault();


            var ConvenientTypeList = new List<ConvenientTypeViewModel>();
            string[] convenientTypeIds = { };
            if (modelRoom.HotelConvenientType != "" && modelRoom.HotelConvenientType != null)
            {
                convenientTypeIds = modelRoom.HotelConvenientType.Split(new char[] { ',' });
            }
            var modelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();
            var viewModelConvenientType = modelConvenientType.Select(s => new ConvenientTypeViewModel()
            {
                Id = s.Id,
                ConvenientName = s.ConvenientName,
                Symbol = s.Symbol,
                Type = s.Type,
                Order = s.Order,
                IsDelete = s.IsDelete,
                UserCreate = s.UserCreate,
                DateCreate = s.DateCreate,
                UserUpdate = s.UserUpdate,
                DateUpdate = s.DateUpdate,
            }).ToList();

            foreach (var itemConvenientType in viewModelConvenientType)
            {
                foreach (var check in convenientTypeIds)
                {
                    if (itemConvenientType.Id == int.Parse(check))
                    {
                        ConvenientTypeList.Add(itemConvenientType);
                    }
                }
            }

            ViewBag.ConvenientTypeList = ConvenientTypeList;

            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult _TravelGuideByProvince(tbl_Province province)
        {
            List<TravelGuideByProvince> model = new List<TravelGuideByProvince>();
            model = _db.HotelNews.Where(d => d.ProvinceID == province.ProvinceId).Select(d => new TravelGuideByProvince
            {
                ProvinceId = province.ProvinceId,
                ProvinceName = province.ProvinceName,
                NewsId = d.HotelNewsID,
                NewsTypeId = d.NewsTypeID,
                Title = d.Title,
                Brief = d.Brief
            }).Take(10).ToList();            

            return PartialView(model);
        }
        public string GetTypeCommentIcon(int type)
        {
            string result = "";
            if(type == 0)
            {
                result = "fa fa-suitcase";
            }
            else if(type == 1)
            {
                result = "fa fa-heart";
            }
            else if (type == 2)
            {
                result = "fa fa-home";
            }
            else if (type == 3)
            {
                result = "fa fa-group";
            }
            else if (type == 4)
            {
                result = "fa fa-male";
            }
            return result;
        }

        public string GetRankText(double? point)
        {
            string result = "";
            if (point >= 0 && point < 5)
            {
                result = "Kém";
            }
            else if (point >= 5 && point < 7)
            {
                result = "Trung Bình";
            }
            else if (point >= 7 && point < 8)
            {
                result = "Tốt";
            }
            else if (point >= 8 && point < 9)
            {
                result = "Rẩt tốt";
            }
            else if (point >= 9 && point <= 10)
            {
                result = "Tuyệt vời";
            }
            else
            {
                result = "Đánh giá";
            }
            return result;
        }

        public ActionResult LoadComment(Guid? hotelId, int? sortComment = 1, int? typeComment = -1, int extend = 2)
        {

            //var modelHotelComment = _db.tbl_HotelComment.Where(x => x.HotelId == hotelId && x.Type == typeComment && x.IsDelete == false).OrderByDescending(x => x.DateTimeComment).ToList();
            var modelHotelComment = new List<tbl_HotelComment>();
            var countAllCmt = 0;
            var countCurrentCmt = 0;

            if (typeComment == -1)
            {
                modelHotelComment = _db.tbl_HotelComment.Where(x => x.HotelId == hotelId && x.IsDelete == false).OrderByDescending(x => x.DateTimeComment).ToList();
                countAllCmt = modelHotelComment.Count();
                modelHotelComment = _db.tbl_HotelComment.Where(x => x.HotelId == hotelId && x.IsDelete == false).OrderByDescending(x => x.DateTimeComment).Take(extend).ToList();
                countCurrentCmt = modelHotelComment.Count();
            }
            else
            {
                modelHotelComment = _db.tbl_HotelComment.Where(x => x.HotelId == hotelId && x.Type == typeComment && x.IsDelete == false).OrderByDescending(x => x.DateTimeComment).ToList();
                countAllCmt = modelHotelComment.Count();
                modelHotelComment = _db.tbl_HotelComment.Where(x => x.HotelId == hotelId && x.Type == typeComment && x.IsDelete == false).OrderByDescending(x => x.DateTimeComment).Take(extend).ToList();
                countCurrentCmt = modelHotelComment.Count();
            }

            ViewBag.CountAllComment = countAllCmt;
            ViewBag.CountCurrentComment = countCurrentCmt;

            var viewModelHotelComment = modelHotelComment.Select(s => new HotelCommentViewModel()
            {
                Id = s.Id,
                HotelId = s.HotelId,
                CustomerId = s.CustomerId,
                Content = s.Content,
                DateTimeComment = s.DateTimeComment,
                PointLocation = s.PointLocation,
                PointServe = s.PointServe,
                PointConvenient = s.PointConvenient,
                PointCost = s.PointCost,
                PointClean = s.PointClean,
                PointLocationText = String.Format("{0:0.0}", s.PointLocation),
                PointConvenientText = String.Format("{0:0.0}", s.PointConvenient),
                PointCostText = String.Format("{0:0.0}", s.PointCost),
                PointCleanText = String.Format("{0:0.0}", s.PointClean),
                PointServeText = String.Format("{0:0.0}", s.PointServe),
                PointAvg = (s.PointClean + s.PointConvenient + s.PointCost + s.PointLocation + s.PointServe) / 5,
                PointAvgText = String.Format("{0:0.0}", (s.PointClean + s.PointConvenient + s.PointCost + s.PointLocation + s.PointServe) / 5),
                RankText = GetRankText((s.PointClean + s.PointConvenient + s.PointCost + s.PointLocation + s.PointServe) / 5),
                Type = s.Type,
                TypeIcon = GetTypeCommentIcon(s.Type),
                IsDelete = s.IsDelete,
                UserCreate = s.UserCreate,
                DateCreate = s.DateCreate,
                UserUpdate = s.UserUpdate,
                DateUpdate = s.DateUpdate
            }).ToList();

            foreach (var item in viewModelHotelComment)
            {
                var modelCustommer = _db.tbl_Customer.Where(x => x.IsDelete == false).ToList();
                foreach (var cus in modelCustommer)
                {
                    if (item.CustomerId == cus.CustomerId)
                    {
                        item.CustomerName = cus.CustomerName;
                        break;
                    }
                }
            }

            if (sortComment == 1)
            {
                viewModelHotelComment = viewModelHotelComment.OrderByDescending(x => x.DateTimeComment).ToList();
            }
            else if (sortComment == 2)
            {
                viewModelHotelComment = viewModelHotelComment.OrderBy(x => x.DateTimeComment).ToList();
            }
            else if (sortComment == 3)
            {
                viewModelHotelComment = viewModelHotelComment.OrderBy(x => x.PointAvg).ToList();
            }
            else if (sortComment == 4)
            {
                viewModelHotelComment = viewModelHotelComment.OrderByDescending(x => x.PointAvg).ToList();
            }

            //ViewBag.viewModelHotelComment = viewModelHotelComment;
            return PartialView("~/Views/Hotel/_DanhSachComment.cshtml", viewModelHotelComment);
        }

        public void initBreadCrumbs(string title, string keyword, string desc, string url, string logo, string breadcrumbs)
        {
            //title = title + " - VIETRAVEL";
            if (title == null)
            {
                title = "";
            }
            if (keyword == null)
            {
                keyword = "";
            }
            if (desc == null)
            {
                desc = "";
            }
            if (url == null)
            {
                url = "";
            }
            if (logo.Length == 0)
            {
                logo = strHost + "Images/vtv-logo.png";
            }
            if (breadcrumbs == null)
            {
                breadcrumbs = "";
            }
            ViewBag.liOG = Utility.init_DublinCore(title, keyword, desc, url, logo);
            ViewBag.Title = title;
            ViewBag.Breadcrumbs = breadcrumbs;
        }

        //private void GetBreadCrumbs(v_TourDetails tour, string destination)
        //{

        //    var groupPageCode = _db.tbl_GroupPageCodes.Where(m => m.page_id == tour.page_id).FirstOrDefault();
        //    if (groupPageCode != null)
        //    {
        //        var tourRes = new TourRepository(_db);
        //        DestinationGroupViewModels group = tourRes.GetGroupByLanguage(groupPageCode.GroupId.GetValueOrDefault(), LanguageID);
        //        DestinationGroupViewModels region = tourRes.GetGroupByLanguage(group.parent.GetValueOrDefault(), LanguageID);

        //        var breadcrumbs = Utility.initBreadcrumbs(Language.strTraveling, strHost, 1) + " » ";
        //        if (tour.tourtypeID == 1)
        //        {
        //            breadcrumbs += Utility.initBreadcrumbs(Language.strDomesticTour, strHost + "du-lich-viet-nam.aspx", 2) + " » ";
        //            breadcrumbs += Utility.initBreadcrumbs("Tour " + region.group_name, strHost + "du-lich-viet-nam/tour-" + region.Code + ".aspx", 3) + " » ";
        //        }
        //        else
        //        {
        //            breadcrumbs += Utility.initBreadcrumbs(Language.strInternationalTour, strHost + "du-lich-nuoc-ngoai.aspx", 2) + " » ";
        //            breadcrumbs += Utility.initBreadcrumbs("Tour " + region.group_name, strHost + "du-lich-nuoc-ngoai/tour-" + region.Code + ".aspx", 3) + " » ";
        //        }

        //        breadcrumbs += Utility.initBreadcrumbs(Language.strTraveling + " " + group.group_name, strHost + "du-lich-" + group.Code + ".aspx", 4) + " » ";
        //        breadcrumbs += Utility.initBreadcrumbs(destination, strHost + "tour" + tour.tour_code + "/" + tour.destination.ToAscii() + ".aspx", 5);

        //        string strLogo = "";
        //        if (tour.Image_small != null && tour.Image_small != "")
        //        {
        //            strLogo = strHost + "images/destination/" + tour.Image_small;

        //            FileInfo info = new FileInfo(Server.MapPath("~/images/destination/Large/" + tour.Image_small));
        //            if (info.Exists)
        //            {
        //                strLogo = strHost + "images/destination/Large/" + tour.Image_small;
        //            }
        //        }
        //        string sKeyword = tour.Keywords;
        //        if (sKeyword == null || sKeyword.Trim() == "")
        //        {
        //            sKeyword = group.Keyword;
        //            if (sKeyword == null || sKeyword.Trim() == "")
        //            {
        //                sKeyword = region.Keyword;
        //            }
        //        }

        //        string Desc = tour.Description;
        //        if (Desc == null || Desc.Trim() == "")
        //        {
        //            Desc = group.Destination;
        //            if (Desc == null || Desc.Trim() == "")
        //            {
        //                Desc = region.Destination;
        //            }
        //        }

        //        initBreadCrumbs(destination + ". " + Language.strTourCode + " " + tour.tour_code, sKeyword, Utility.CutText(Desc, 300) + ". " + Language.strTourCode + " " + tour.tour_code, strHost + "tour" + tour.tour_code + "/" + tour.destination.ToAscii() + ".aspx", strLogo, breadcrumbs);
        //        showSocial(strHost + "tour" + tour.tour_code + "/" + tour.destination.ToAscii() + ".aspx", strLogo, Utility.CutText(Desc, 340));
        //    }
        //}
    }
}