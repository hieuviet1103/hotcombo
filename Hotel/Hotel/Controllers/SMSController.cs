﻿using Hotel.Common.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Hotel.Controllers
{
    public class SMSController : Controller
    {
        // GET: Test
        private readonly string APIKey = System.Configuration.ConfigurationManager.AppSettings["SMSApiKey"];//Login to eSMS.vn to get this";//Dang ky tai khoan tai esms.vn de lay key//Register account at esms.vn to get key
        private readonly string SecretKey = System.Configuration.ConfigurationManager.AppSettings["SMSSecretKey"];//Login to eSMS.vn to get this";
        private readonly string BranchName = System.Configuration.ConfigurationManager.AppSettings["SMSBranchName"];
        private readonly string SMSSandbox = System.Configuration.ConfigurationManager.AppSettings["SMSSandbox"];
        public ActionResult Index(string result)
        {
            ViewBag.Result = result;
            return View();
        }

        //Send SMS with Sender is a number
        public ActionResult SendJson(string phone = "0969305043", string message = "[^.]{0,20} la ma dat lai mat khau Baotrixemay cua ban")
        {
            phone = "0969305043";
            message = "[^.]{0,20} la ma dat lai mat khau Baotrixemay cua ban";

            SMSControl sms = new SMSControl();
            string result = sms.SendJson(phone, message) ;  //SendGetRequest(URL);
            JObject ojb = JObject.Parse(result);
            int CodeResult = (int)ojb["CodeResult"];//100 is successfull

            string SMSID = (string)ojb["SMSID"];//id of SMS
            return RedirectToAction("Index", "SMS", new { result = result });
        }

        private string SendGetRequest(string RequestUrl)
        {
            Uri address = new Uri(RequestUrl);
            HttpWebRequest request;
            HttpWebResponse response = null;
            StreamReader reader;
            if (address == null) { throw new ArgumentNullException("address"); }
            try
            {
                request = WebRequest.Create(address) as HttpWebRequest;
                request.UserAgent = ".NET Sample";
                request.KeepAlive = false;
                request.Timeout = 15 * 1000;
                response = request.GetResponse() as HttpWebResponse;
                if (request.HaveResponse == true && response != null)
                {
                    reader = new StreamReader(response.GetResponseStream());
                    string result = reader.ReadToEnd();
                    result = result.Replace("</string>", "");
                    return result;
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        Console.WriteLine(
                            "The server returned '{0}' with the status code {1} ({2:d}).",
                            errorResponse.StatusDescription, errorResponse.StatusCode,
                            errorResponse.StatusCode);
                    }
                }
            }
            finally
            {
                if (response != null) { response.Close(); }
            }
            return null;
        }

        //Send SMS with Alpha Sender
        public ActionResult SendBrandnameJson(string phone, string message, string brandname)
        {
            brandname = "Baotrixemay";
            phone = "0969305043";
            message = "[^.]{0,20} la ma dat lai mat khau Baotrixemay cua ban";

            SMSControl sms = new SMSControl();
            string result = sms.SendBrandnameJson(phone,message,brandname);
            JObject ojb = JObject.Parse(result);
            int CodeResult = (int)ojb["CodeResult"];//trả về 100 là thành công
            int CountRegenerate = (int)ojb["CountRegenerate"];
            string SMSID = (string)ojb["SMSID"];//id của tin nhắn
            return RedirectToAction("Index", "SMS", new { result = result });
        }

        //Get Account Balance - Lay so du tai khoan
        public ActionResult GetBalance()
        {
            SMSControl sms = new SMSControl();
            string result = sms.GetBalance();
            JObject ojb = JObject.Parse(result);
            int CodeResult = (int)ojb["CodeResponse"];//trả về 100 là thành công
            int UserID = (int)ojb["UserID"];//id tài khoản
            long Balance = (long)ojb["Balance"];//tiền trong tài khoản
            return RedirectToAction("Index", "SMS");
        }
    }
}