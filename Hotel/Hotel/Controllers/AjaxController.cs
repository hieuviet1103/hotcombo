﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.EntityFramework.Models;
using Hotel.Utilities;

namespace Hotel.Controllers
{  
    public class AjaxController : Controller
    {
        private readonly HotelEntities _db = new HotelEntities();
        // GET: Ajax
        public ActionResult Index()
        {
            return View();
        }
        #region Search
        public ActionResult _DestinationSuggest()
        {
            var province = _db.tbl_Province.Where(d => d.CountryId == 1 && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
            ViewBag.Province = province;
            return PartialView();
        }

        public ActionResult _HotelSuggest(string keyword, int type)
        {
            if (!string.IsNullOrEmpty(keyword) && type == 1)
            {
                List<tbl_Hotel> listHotel = new List<tbl_Hotel>();
                try
                {
                    
                    listHotel = _db.tbl_Hotel.Where(delegate (tbl_Hotel d)
                    {
                        if (d.IsActive == 1 && d.IsDelete != true
                        && Utilities.Utility.XoaDau(d.HotelName).ToLower()
                        .IndexOf(Utilities.Utility.XoaDau(keyword.ToLower()), StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }).Take(30).ToList();

                    ViewBag.Hotels = listHotel;
                    return PartialView("_HotelSuggest");
                }
                catch (Exception ex)
                {
                }

                
            }

            return RedirectToAction("_DestinationSuggest");
        }
        #endregion

        #region Select-Option
        [HttpGet]
        public JsonResult _GetProvince(int countryid)
        {
            List<SelectListItem> sllProvince = new List<SelectListItem>();
            sllProvince.Add(new SelectListItem { Value = "", Text = "---   Chọn tỉnh thành   ---" });
            var provinces = _db.tbl_Province.Where(d => d.CountryId == countryid && d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList();
            foreach (var item in provinces)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.ProvinceId.ToString();
                sllItem.Text = item.ProvinceName;

                sllProvince.Add(sllItem);
            }
            return Json(sllProvince, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult _GetDistrict(int provinceid)
        {
            List<SelectListItem> sllDistrict = new List<SelectListItem>();
            sllDistrict.Add(new SelectListItem { Value = "", Text = "---   Chọn quận / huyện   ---" });
            var districts = _db.tbl_District.Where(d => d.ProvinceId == provinceid).OrderBy(d => d.DistrictName).ToList();
            foreach (var item in districts)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.DistrictId.ToString();
                sllItem.Text = item.DistrictName;
                sllDistrict.Add(sllItem);
            }
            return Json(sllDistrict, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}