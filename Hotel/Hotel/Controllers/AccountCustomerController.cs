﻿using Admin.Helpers;
using Hotel.EntityFramework.Models;
using Hotel.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Facebook;
using Hotel.Common.Utilities;
using Newtonsoft.Json.Linq;

namespace Hotel.Controllers
{
    public class AccountCustomerController : Controller
    {
        private readonly HotelEntities _db = new HotelEntities();
        private readonly string isSendSMS = WebConfigurationManager.AppSettings["IsSendSMS"];

        private ApplicationUserManager _userManager;

        public string strHost
        {
            get
            {
                string strH = WebConfigurationManager.AppSettings["host"].ToString() + "/";
                return strH;
            }
        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //public AccountCustomerController(ApplicationUserManager userManager)
        //{
        //    UserManager = userManager;
        //}

        //public AccountCustomerController()
        //{
        //}

        // GET: AccountCustomer
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]  
        [ValidateAntiForgeryToken]
        public JsonResult Login(string emailOrPhoneNumber, string password)
        {
            try
            {
                if (Utilities.Utility.CheckPhoneNumber(emailOrPhoneNumber)) // dang nhap bang SDT
                {
                    string phoneNumber = emailOrPhoneNumber;

                    if (_db.tbl_Customer.Any(d => d.PhoneNumber == phoneNumber && d.Password == password && !d.IsDelete))
                    {
                        var customer = _db.tbl_Customer.FirstOrDefault(d => d.PhoneNumber == phoneNumber && d.Password == password && !d.IsDelete);

                        CustomerAccountViewModel customerLogin = new CustomerAccountViewModel();
                        customerLogin.InitData(customer);
                        Session["CustomerLogin"] = customerLogin;
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Số điện thoại hoặc mật khẩu không chính xác" });
                    }

                    return Json(new { status = 1, message = "Đăng nhập thành công" });
                }
                else //Dang nhap bang Email
                {
                    string email = emailOrPhoneNumber;

                    if (_db.tbl_Customer.Any(d => d.Email.ToLower() == email.ToLower() && d.Password == password && !d.IsDelete))
                    {
                        var customer = _db.tbl_Customer.FirstOrDefault(d => d.Email.ToLower() == email.ToLower() && d.Password == password && !d.IsDelete);
                        if (!customer.IsActive)
                        {
                            return Json(new { status = 0, message = "Email chưa được kích hoạt thành công." });
                        }

                        CustomerAccountViewModel customerLogin = new CustomerAccountViewModel();
                        customerLogin.InitData(customer);
                        Session["CustomerLogin"] = customerLogin;
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Email hoặc mật khẩu không chính xác" });
                    }
                    return Json(new { status = 1, message = "Đăng nhập thành công" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, message = "Đăng nhập thất bại", error = ex });
            }
        }

        //Dang ky tai khoan
        public ActionResult Register()
        {
            return View();
        }
        public string RegisterAccount(string CustomerName, string Email, string Password, string RePassword)
        {
            MailControl mailcontrol = new MailControl();
            tbl_Customer customer = null;
            string strResult = "0";
            try
            {
                //Kien tra email
                var checkEmail = _db.tbl_Customer.FirstOrDefault(d => d.Email == Email);
                if (checkEmail != null && checkEmail.IsActive == true)
                {
                    strResult = "-1"; // thong bao tai khoan da ton tai
                }
                else if (checkEmail != null && checkEmail.IsActive == false)
                {
                    //thuc hien xac nhan lai eamil
                    Email email = new Email();

                    var code = checkEmail.CustomerId;
                    var callbackUrl = Url.Action(
                        "ConfirmEmail", "AccountCustomer",
                        new { customerId = checkEmail.CustomerId },
                        protocol: Request.Url.Scheme);

                    email.To = Email;
                    email.Subject = "[hotcombo.vn]Xác thực tài khoản";
                    email.Body = "Để xác thực tài khoản tại hotcombo.vn vui lòng click : <a href=\"" + callbackUrl + "\">vào đây</a>";

                    mailcontrol.Send(email);

                    strResult = "2";
                }
                else
                {
                    //tao moi tai khoan
                    customer = new tbl_Customer();
                    customer.CustomerId = Guid.NewGuid();
                    customer.Password = Password;
                    customer.Email = Email;
                    customer.IsActive = false;
                    customer.IsDelete = false;
                    customer.CustomerName = CustomerName;
                    customer.Gender = 0;
                    customer.DateCreate = DateTime.Now;
                    customer.DateUpdate = DateTime.Now;
                    _db.tbl_Customer.Add(customer);
                    _db.SaveChanges();

                    Email email = new Email();

                    var code = customer.CustomerId;
                    var callbackUrl = Url.Action(
                        "ConfirmEmail", "AccountCustomer",
                        new { customerId = customer.CustomerId },
                        protocol: Request.Url.Scheme);

                    email.To = Email;
                    email.Subject = "[hotcombo.vn]Xác thực tài khoản";
                    email.Body = "Để xác thực tài khoản tại hotcombo.vn vui lòng click : <a href=\"" + callbackUrl + "\">vào đây</a>";
                    mailcontrol.Send(email);


                    strResult = "1";


                }

            }
            catch (Exception ex)
            {
                return strResult = "0";
            }
            return strResult;
            //return Json(strResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RegisterByPhoneNumber(string CustomerName, string PhoneNumber, string OTP, string Password, string RePassword)
        {
            try
            {
                if (!Utilities.Utility.CheckPhoneNumber(PhoneNumber))
                {
                    return Json(new { status = 0, message = "Định dạng số điện thoại không đúng" });
                }

                if (_db.tbl_Customer.Any(d => d.PhoneNumber == PhoneNumber))
                {
                    return Json(new { status = 0, message = "Số điện thoại đã được đăng ký." });
                }

                List<PhoneNumberAuthenticate> listAuth = (List<PhoneNumberAuthenticate>)Session["PhoneNumberAuthenticate"];

                if (listAuth == null || !listAuth.Any(d => d.PhoneNumber == PhoneNumber && d.OTP == OTP && d.ExpDate >= DateTime.Now))
                {
                    return Json(new { status = 0, message = "Mã OTP không đúng hoặc đã hết hạn. Vui lòng kiểm tra lại hoặc nhận mã OTP mới" });
                }

                tbl_Customer customer = new tbl_Customer();

                //tao moi tai khoan
                customer.CustomerId = Guid.NewGuid();
                customer.Password = Password;
                customer.PhoneNumber = PhoneNumber;
                customer.IsActive = true;
                customer.IsDelete = false;
                customer.CustomerName = CustomerName;
                customer.Gender = 0;
                customer.CustomerNo = String.Format("{0:D6}", int.Parse(_db.tbl_Customer.Where(d => !string.IsNullOrEmpty(d.CustomerNo))
                        .Select(d => d.CustomerNo).DefaultIfEmpty("000000").Max()) + 1);
                customer.DateCreate = DateTime.Now;
                _db.tbl_Customer.Add(customer);
                _db.SaveChanges();

                CustomerAccountViewModel customerLogin = new CustomerAccountViewModel();
                customerLogin.InitData(customer);
                Session["CustomerLogin"] = customerLogin;

                return Json(new { status = 1, message = "Đăng ký tài khoản thành công" });
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, message = "Xảy ra lỗi", error = ex });
            }
        }


        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public ActionResult ConfirmEmail(string customerId)
        {
            if (customerId == null)
            {
                return View("Error");
            }
            try
            {
                var id = Guid.Parse(customerId);
                var itemAccount = _db.tbl_Customer.FirstOrDefault(d => d.CustomerId == id);

                if (itemAccount != null)
                {
                    itemAccount.CustomerNo = String.Format("{0:D6}", int.Parse(_db.tbl_Customer.Where(d => !string.IsNullOrEmpty(d.CustomerNo))
                        .Select(d => d.CustomerNo).DefaultIfEmpty("000000").Max()) + 1);
                    
                    itemAccount.IsActive = true;
                    itemAccount.DateUpdate = DateTime.Now;
                    _db.Entry(itemAccount).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index","Home");
                }
                else
                {
                    ViewBag.errorMessage = "Xác thực không thành công";
                    return View();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult SendOTPMessage(string phoneNumber)
        {
            try
            {
                if (!Utilities.Utility.CheckPhoneNumber(phoneNumber))
                {
                    return Json(new { status = 0, message = "Định dạng số điện thoại không đúng" });
                }

                if (Session["TimePhoneRegister"] != null)
                {
                    DateTime last = (DateTime)Session["TimePhoneRegister"];

                    if ((DateTime.Now - last).TotalMinutes < 5)
                    {
                        return Json(new { status = 0, message = "Vui lòng chờ" });
                    }
                }

                if (_db.tbl_Customer.Any(d=>d.PhoneNumber == phoneNumber))
                {
                    return Json(new { status = 0, message = "Số điện thoại đã được đăng ký." });
                }

                string OTP = Utilities.Utility.RandomCode(6, false);

                string message = "";

                message = "Ma OTP dang ky tai khoan tai HotCombo la " + OTP;

                if (isSendSMS == "1")
                {
                    SMSControl smsControl = new SMSControl();

                    string result = smsControl.SendJson(phoneNumber, message); //Gửi tin nhắn OTP

                    if (result != null)
                    {
                        JObject ojb = JObject.Parse(result);
                        if ((int)ojb["CodeResult"] == 100) //Gui tin thanh cong
                        {
                            Session["TimePhoneRegister"] = DateTime.Now;

                            List<PhoneNumberAuthenticate> listAuth = (List<PhoneNumberAuthenticate>)Session["PhoneNumberAuthenticate"];
                            if (listAuth == null)
                            {
                                listAuth = new List<PhoneNumberAuthenticate>();
                            }

                            var auth = new PhoneNumberAuthenticate();

                            auth.OTP = OTP;
                            auth.PhoneNumber = phoneNumber;
                            auth.DateCreate = DateTime.Now;
                            auth.ExpDate = DateTime.Now.Date.AddHours(23.99);


                            if (listAuth.Any(d => d.PhoneNumber == phoneNumber))
                            {
                                var oldAuth = listAuth.FirstOrDefault(d => d.PhoneNumber == phoneNumber);
                                listAuth.Remove(oldAuth);
                            }

                            listAuth.Add(auth);

                            Session["PhoneNumberAuthenticate"] = listAuth;

                            return Json(new { status = 1, message = "Mã xác nhận đã được gửi đi, vui lòng kiểm tra tin nhắn. \r\n" });
                        }
                    }
                }
                else
                {
                    Session["TimePhoneRegister"] = null;

                    List<PhoneNumberAuthenticate> listAuth = (List<PhoneNumberAuthenticate>)Session["PhoneNumberAuthenticate"];
                    if (listAuth == null)
                    {
                        listAuth = new List<PhoneNumberAuthenticate>();
                    }

                    var auth = new PhoneNumberAuthenticate();

                    auth.OTP = OTP;
                    auth.PhoneNumber = phoneNumber;
                    auth.DateCreate = DateTime.Now;
                    auth.ExpDate = DateTime.Now.Date.AddHours(23.99);


                    if (listAuth.Any(d => d.PhoneNumber == phoneNumber))
                    {
                        var oldAuth = listAuth.FirstOrDefault(d => d.PhoneNumber == phoneNumber);
                        listAuth.Remove(oldAuth);
                    }

                    listAuth.Add(auth);

                    Session["PhoneNumberAuthenticate"] = listAuth;

                    return Json(new { status = 0, message = message });
                }               

            }
            catch (Exception ex)
            {
                return Json(new { status = -1, message = "Xảy ra lỗi. Vui lòng thử lại", error = ex });
            }

            return Json(new { status = 0, message = "Vui lòng thử lại sau" });
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult SocialMediaLogin(SocialMediaAccount info)
        {
            try
            {
                if (!string.IsNullOrEmpty(info.Email))
                {
                    bool status = true;
                    if (!_db.tbl_Customer.Any(d => d.Email.ToLower() == info.Email.ToLower() && d.IsActive && !d.IsDelete))
                    {
                        status = SocialMediaFirstLogin(info);
                    }

                    if (status)
                    {
                        var userDb = _db.tbl_Customer.FirstOrDefault(d => d.Email.ToLower() == info.Email.ToLower() && d.IsActive && !d.IsDelete);
                        if (!string.IsNullOrEmpty(info.ImageUrl))
                        {
                            userDb.Image = info.ImageUrl;
                            _db.Entry(userDb).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                        CustomerAccountViewModel user = new CustomerAccountViewModel();
                        user.InitData(userDb);
                        Session["CustomerLogin"] = user;
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = "False" });
            }
            
            return Json(new { success = "True" });
        }

        private bool SocialMediaFirstLogin(SocialMediaAccount info)
        {
            bool status = false;
            try
            {
                tbl_Customer customerAccount = new tbl_Customer()
                {
                    CustomerId = Guid.NewGuid(),
                    Email = info.Email.ToLower(),
                    CustomerName = !string.IsNullOrEmpty(info.FullName) ? info.FullName : "Khách hàng",
                    Gender = info.Gender,
                    Password = Utilities.Utility.RandomCode(6, true),
                    CustomerNo = String.Format("{0:D6}", int.Parse(_db.tbl_Customer.Where(d => !string.IsNullOrEmpty(d.CustomerNo)).Select(d => d.CustomerNo).DefaultIfEmpty("000000").Max()) + 1),
                    Image = info.ImageUrl,
                    IsActive = true,
                    IsDelete = false,
                    DateCreate = DateTime.Now,
                    TotalPoint = 0
                };
                _db.tbl_Customer.Add(customerAccount);
                status = _db.SaveChanges() != 0 ? true : false;
            }
            catch
            {
                
            }

            return status;
        }

        public ActionResult LogOut()
        {
            Session.Abandon();

            return RedirectToAction("Index", "Home");
        }

        public string ForgotPasswordAccount(string Email)
        {
            string strResult = "false";

            //Kiểm tra thông tin Email có trong hệ thống chưa mới gửi mail
            var customer = _db.tbl_Customer.FirstOrDefault(x => x.Email == Email);
            if(customer != null)
            {
                //Gui email tới khách hàng nhập mật khẩu mới
                strResult = SendEmailForgotPassword(Email).ToString();
            }

            return strResult;
        }

        [HttpGet]
        public object SendEmailForgotPassword(string Email)
        {
            bool isCheck = false;
            try
            {
                string link = strHost + "AccountCustomer/CustomerEnterPassword?tk=" + Utilities.Utility.Base64Encode(Email);
                MailControl mailcontrol = new MailControl();
                Email email = new Email();

                email.To = Email;
                email.Subject = "[hotcombo.vn]Quên mật khẩu";
                email.Body = getContentEmailForgetPassword(link);

                mailcontrol.Send(email);
                isCheck = true;
            }
            catch
            {
                isCheck = false;
            }
            return JsonConvert.SerializeObject(isCheck);
        }

        private string getContentEmailForgetPassword(string link)
        {
            //string path = Server.MapPath("~/HtmlFile/ForgotPassword.html");
            //string path = System.Web.HttpContext.Current.Server.MapPath("~/HtmlFile/ForgotPassword.html");
            //string content = System.IO.File.ReadAllText(path);
            //content = content.Replace("{link}", link);

            string content = "Để cập nhật lại mật khẩu tài khoản tại hotcombo.vn vui lòng click : <a href=\"" + link + "\">vào đây</a>";
            

            return content;
        }

        //Nhap mật khẩu
        public ActionResult CustomerEnterPassword()
        {
            string tk = Request["tk"];

            //string breadcrumbs = string.Empty;
            //breadcrumbs += Utilities.initBreadcrumbs(Language.strTraveling, strHost, 1) + " » ";
            //breadcrumbs += Utilities.initBreadcrumbs(Language.strForgotPassword, strHost, 2);

            //initBreadCrumbs(Language.strForgotPassword, "", "", Request.Url.ToString(), "", breadcrumbs);
            //showSocial(strHostBlank + Request.RawUrl.ToString(), "", "");

            ViewBag.tk = tk;

            return View();
        }

        public string CustomerEnterPasswordAccount(string tk, string Password, string RePassword)
        {
            string strResult = "";
            try
            {
                //Goi lại Api bên Care quên mật khẩu
                string dEmail = Utilities.Utility.Base64Decode(tk);
               
                if (string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(RePassword) || Password != RePassword)
                {
                    //strResult = "Nhập mật khẩu không đúng";
                    strResult = "0";
                    Session["AccountLogin"] = null;
                }
                else
                {
                    //Goi lại Api bên Care Update mat khau tài khoản
                    tbl_Customer Account = _db.tbl_Customer.FirstOrDefault(x => x.Email == dEmail);
                    Account.Password = Password;//Utilities.Utility.EncodeString(Password);
                    Account.DateUpdate = DateTime.Now;
                    _db.Entry(Account).State = EntityState.Modified;
                    _db.SaveChanges();
                    Session["AccountLogin"] = Account;

                    //strResult = "Đổi mật khẩu thành công";
                    strResult = "1";
                }

                return strResult;
            }
            catch
            {
                
                Session["AccountLogin"] = null;
                return strResult = "-1";
                //return strResult = "Đổi mật khẩu thất bại";
            }
           
        }

        //Doi mat khau
        public ActionResult ChangePassword()
        {
            return View();
        }
        public JsonResult ChangePasswordAccount(string Password, string PasswordNew, string RePasswordNew)
        {
            string strResult = "";
            if (PasswordNew != RePasswordNew)
            {
                strResult = "Nhập mật khẩu không đúng";
            }
            else
            {
                if (Session["CustomerLogin"] != null)
                {
                    CustomerAccountViewModel AccountSession = (CustomerAccountViewModel)Session["CustomerLogin"];

                    tbl_Customer Account = _db.tbl_Customer.FirstOrDefault(x => x.Email == AccountSession.Email);

                    if (Account.Password != Password)
                    {
                        strResult = "Mật khẩu cũ không đúng";
                    }
                    else
                    {
                        Account.Password = PasswordNew;
                        //AccountReturn result = InputAccountToCare("Travel", Account, "1");

                        Account.Password = PasswordNew;
                        _db.Entry(Account).State = EntityState.Modified;
                        _db.SaveChanges();

                        strResult = "Đổi mật khẩu thành công";
                    }
                }
                else
                {
                    strResult = "Không thể đổi mật khẩu";
                }
            }

            return Json(strResult, JsonRequestBehavior.AllowGet);
        }
        #region Facebook

        private readonly Utilities.SocialService socialService = new Utilities.SocialService();
        private Uri FacebookRedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallback");
                return uriBuilder.Uri;
            }
        }
        private Uri GoogleRedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("GoogleCallBack");
                return uriBuilder.Uri;
            }
        }
        [AllowAnonymous]
        public ActionResult Facebook(string returnUrl = "")
        {
            //Utilities.SocialService socialService = new Utilities.SocialService();
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = socialService.facebookClientId,
                client_secret = socialService.facebookClientSecretId,
                redirect_uri = FacebookRedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email" // Add other permissions as needed
            });

            TempData["ReturnUrl"] = returnUrl;

            return Redirect(loginUrl.AbsoluteUri);
        }
        [AllowAnonymous]
        public ActionResult FacebookCallback(string code)
        {
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = socialService.facebookClientId,
                client_secret = socialService.facebookClientSecretId,
                redirect_uri = FacebookRedirectUri.AbsoluteUri,
                code = code
            });

            var accessToken = result.access_token;

            // Store the access token in the session
            Session["AccessToken"] = accessToken;

            // update the facebook client with the access token so
            // we can make requests on behalf of the user
            fb.AccessToken = accessToken;

            // Get the user's information
            dynamic me = fb.Get("me?fields=first_name,last_name,id,email");
            string email = me.email;
            string id = me.id;
            string fullname = me.first_name + " " + me.last_name;

            SocialMediaAccount socialMediaAccount = new SocialMediaAccount()
            {
                Id = id,
                Email = email,
                FirstName = me.first_name,
                LastName = me.last_name,
                FullName = fullname,
                ImageUrl = "https://graph.facebook.com/" + id + "/picture?type=normal&width=500&height=500",
                TypeLogin = 1 //Facebook
            };

            string returnUrl = (string)TempData["ReturnUrl"] ?? Request.Url.Authority;
            if (SocialMediaLoginResult(socialMediaAccount))
            {
                TempData["Alert"] = "Đăng nhập thành công";
            }
            else
            {
                TempData["Alert"] = "Đăng nhập thất bại";
            }
            return RedirectPermanent(returnUrl);
        }
        [AllowAnonymous]
        public ActionResult Google(string returnUrl = "")
        {
            string redirection_url = GoogleRedirectUri.AbsoluteUri;
            string url = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + redirection_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + socialService.googleClientId;
            TempData["ReturnUrl"] = returnUrl;
            return Redirect(url);
        }
        [AllowAnonymous]
        public async Task<ActionResult> GoogleCallBack(string code)
        {
            string returnUrl = (string)TempData["ReturnUrl"] ?? Request.Url.Authority;
            string redirection_url = GoogleRedirectUri.AbsoluteUri;
            if (!string.IsNullOrEmpty(code))
            {
                var info = await Utilities.SocialService.GetTokenAndProfile(code, socialService.googleClientId, socialService.googleClientSecretId, redirection_url);
                SocialMediaAccount socialMediaAccount = new SocialMediaAccount()
                {
                    Id = info.id,
                    Email = info.email,
                    FirstName = info.given_name,
                    LastName = info.family_name,
                    FullName = info.name,
                    ImageUrl = info.picture,
                    TypeLogin = 2 //Google
                };
                
                if (SocialMediaLoginResult(socialMediaAccount))
                {
                    TempData["Alert"] = "Đăng nhập thành công";
                }
                else
                {
                    TempData["Alert"] = "Đăng nhập thất bại";
                }
                return RedirectPermanent(returnUrl);
            }

            TempData["Alert"] = "Đăng nhập thất bại";
            return RedirectPermanent(returnUrl);
        }

        private bool SocialMediaLoginResult(SocialMediaAccount info)
        {
            try
            {
                if (!string.IsNullOrEmpty(info.Email))
                {
                    bool status = true;
                    if (!_db.tbl_Customer.Any(d => d.Email.ToLower() == info.Email.ToLower() && d.IsActive && !d.IsDelete))
                    {
                        status = SocialMediaFirstLogin(info);
                    }

                    if (status)
                    {
                        var userDb = _db.tbl_Customer.FirstOrDefault(d => d.Email.ToLower() == info.Email.ToLower() && d.IsActive && !d.IsDelete);
                        if (!string.IsNullOrEmpty(info.ImageUrl))
                        {
                            userDb.Image = info.ImageUrl;
                            _db.Entry(userDb).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                        CustomerAccountViewModel user = new CustomerAccountViewModel();
                        user.InitData(userDb);
                        Session["CustomerLogin"] = user;
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion


    }
}