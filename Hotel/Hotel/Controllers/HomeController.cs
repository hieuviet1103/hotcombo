﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.EntityFramework.Models;
using Admin.Helpers;
using System.Net.Mail;
using Hotel.Models;

namespace Hotel.Controllers
{
    public class HomeController : Controller
    {
        private readonly HotelEntities _db = new HotelEntities();
        public ActionResult Index()
        {
            ViewBag.WhyChooseMe = _db.tbl_banner.FirstOrDefault(t => t.banner_pos == (int)Parameters.Banner.Position.LogoWhyChoose);
            return View();
        }
        
        public ActionResult _SearchHotel1()
        {
            ViewBag.BannerTop = _db.HotelNews.Where(d => d.NewsTypeID == (int)Parameters.NewsType.BannerTop && d.Status).OrderBy(d=>d.OrderID).Take(5).ToList();
            return PartialView();
        }


        public ActionResult _SearchHome()
        {
            //var province = _db.tbl_Province.Where(d => d.CountryId == "1" && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
            //ViewBag.Province = province;
            List<SearchHotelSuggestModel> result = new List<SearchHotelSuggestModel>();
            List<tbl_Hotel> listHotel = _db.tbl_Hotel.Where(d => d.IsActive == 1 && d.IsDelete != true).OrderBy(d => d.HotelName).ToList();
            foreach (var item in listHotel)
            {
                result.Add(new SearchHotelSuggestModel() { Id = item.Id.ToString(), Name = item.HotelName, Type = "0" });
            }
            List<tbl_Province> listProvince = _db.tbl_Province.Where(d => d.CountryId == 1 && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
            foreach (var item in listProvince)
            {
                result.Add(new SearchHotelSuggestModel() { Id = item.ProvinceId.ToString(), Name = item.ProvinceName, Type = "1" });
            }
            ViewBag.hotelSuggest = result;
            return PartialView();
        }
        public ActionResult _HotelByProvince()
        {
            var province = _db.tbl_Province.Where(d => d.CountryId == 1 && d.IsDelete != true).OrderBy(d=>d.ProvinceName).ToList();
            return PartialView(province);
        }
        public ActionResult _SlideBannerTop()
        {
            var banner = _db.tbl_banner.Where(d => d.banner_pos == (int)Parameters.Banner.Position.SlideBannerTrangChu && d.active == 1
            && ((d.date_start <= DateTime.Now && d.date_end >= DateTime.Now) || d.date_end == null)).OrderBy(d => d.priority).Take(20).ToList();
            return PartialView(banner);
        }
        public ActionResult _SlideBannerBottom()
        {
            var banner = _db.tbl_banner.Where(d => d.banner_pos == (int)Parameters.Banner.Position.SlideFooterTrangChu && d.active == 1
            && ((d.date_start <= DateTime.Now && d.date_end >= DateTime.Now) || d.date_end == null)).OrderBy(d => d.priority).Take(20).ToList();
            return PartialView(banner);
        }

        public ActionResult _PopularDestination()
        {
            var banner = _db.tbl_banner.Where(d => d.banner_pos == (int)Parameters.Banner.Position.DiemDenDuLich && d.active == 1
            && ((d.date_start <= DateTime.Now && d.date_end >= DateTime.Now) || d.date_end == null)).OrderBy(d => d.priority).Take(20).ToList();
            return PartialView(banner);
        }

        public ActionResult _HeaderMobile()
        {
            //var province = _db.tbl_Province.Where(d => d.CountryId == "1" && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
            //ViewBag.Province = province;
            List<SearchHotelSuggestModel> result = new List<SearchHotelSuggestModel>();
            List<tbl_Hotel> listHotel = _db.tbl_Hotel.Where(d => d.IsActive == 1 && d.IsDelete != true).OrderBy(d => d.HotelName).ToList();
            foreach (var item in listHotel)
            {
                result.Add(new SearchHotelSuggestModel() { Id = item.Id.ToString(), Name = item.HotelName, Type = "0" });
            }
            List<tbl_Province> listProvince = _db.tbl_Province.Where(d => d.CountryId == 1 && d.IsDelete != true).OrderBy(d => d.ProvinceName).ToList();
            foreach (var item in listProvince)
            {
                result.Add(new SearchHotelSuggestModel() { Id = item.ProvinceId.ToString(), Name = item.ProvinceName, Type = "1" });
            }
            ViewBag.hotelSuggest = result;
            ViewBag.Search = (SearchViewModel)Session["SearchHotel"];
            return PartialView("~/Views/Shared/_HeaderMobile.cshtml");
        }
    }
}