﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    //public class ViewModels
    //{
    //}

    public class AllotBooking
    {
        public int Id { get; set; }
        public Guid BookingId { get; set; }
        public string BookingCode { get; set; }
        public Guid UserId { get; set; }
        public Guid UserCreate { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime DeadLine { get; set; }
        public int? Status { get; set; }
        public bool IsDelete { get; set; }
    }

    public class CustomerAdminViewModel
    {
        public System.Guid CustomerId { get; set; }
        public string CustomerNo { get; set; }
        public bool IsActive { get; set; }
        public int IsActiveInt { get; set; }
        public string CustomerName { get; set; }
        public Nullable<System.DateTime> Dob { get; set; }
        public byte Gender { get; set; }
        public string Nationality { get; set; }
        public string IdCard { get; set; }
        public Nullable<System.DateTime> DateOfIssue { get; set; }
        public string PlaceOfIssue { get; set; }
        public string Address { get; set; }
        public Nullable<int> CountryId { get; set; }
        public string CountryName { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public string Password { get; set; }
        public Nullable<double> TotalPoint { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<System.Guid> UserCreate { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.Guid> UserUpdate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
    }

    public class HotelCommentAreaViewModel
    {
        public Guid HotelId { get; set; }
        public string HotelName { get; set; }
        public int NumAll { get; set; }
        public int NumCongTac { get; set; }
        public int NumCapDoi { get; set; }
        public int NumGiaDinh { get; set; }
        public int NumBanBe { get; set; }
        public int NumCaNhan { get; set; }
        public double PointLocation { get; set; }
        public double PointServe { get; set; }
        public double PointConvenient { get; set; }
        public double PointCost { get; set; }
        public double PointClean { get; set; }
        public double PointAvg { get; set; }
        public string RankText { get; set; }
    }

    public class HotelCommentAdminViewModel
    {
        public System.Guid Id { get; set; }
        public System.Guid HotelId { get; set; }
        public System.Guid CustomerId { get; set; }
        public string Content { get; set; }
        public System.DateTime DateTimeComment { get; set; }
        public Nullable<double> PointLocation { get; set; }
        public Nullable<double> PointServe { get; set; }
        public Nullable<double> PointConvenient { get; set; }
        public Nullable<double> PointCost { get; set; }
        public Nullable<double> PointClean { get; set; }
        public int Type { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<System.Guid> UserCreate { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.Guid> UserUpdate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public string HotelName { get; set; }
        public string CustomerName { get; set; }
        public double? PointAvg { get; set; }
    }
}