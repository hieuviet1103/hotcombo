﻿using Custom.Security;
using Hotel.Common.Utilities;
using Hotel.EntityFramework.Models;
using Hotel.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class BookingController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        private readonly string isSendSMS = WebConfigurationManager.AppSettings["IsSendSMS"];
        private const int RecordsPerPage = 25;
        // GET: Admin/Booking
        public ActionResult Index(SearchBookingViewModel search, int page = 1)
        {
            if (Session["SearchBooking"] != null && Request.HttpMethod == "GET")
            {
                search = (SearchBookingViewModel)Session["SearchBooking"];
            }
            else
            {
                Session["SearchBooking"] = search;
            }
            ViewBag.Search = search;
            List<BookingViewModel> models = new List<BookingViewModel>();
            var query = _db.tbl_Booking.Select(d => d);
            query = GetQueryBooking(query, search);

            //lấy ds nhân viên
            ViewBag.ListStaff = _db.tbl_User_Role.Where(d => d.RoleId == 2 && d.IsActive)
                .Select(d => new SelectListItem()
                {
                    Value = d.UserId.ToString(),
                    Text = d.tbl_User.FullName + " - " + d.tbl_User.LoginName,
                    Selected = d.UserId == search.UserAlloted
                }).ToList();

            //Init Pager
            Pager pager = new Pager();
            pager.PageUrlFormat = "";
            pager.TotalRecords = query.Count();
            pager.TotalRecordsPerPage = RecordsPerPage;
            pager.TotalSlots = 10;
            pager.CurrentPage = page;
            pager.CssClass = "pager_primary";
            pager.CssClassCurrentPage = "active";
            pager.FirstPageDisplayText = "‹";
            pager.LastPageDisplayText = "›";

            ViewBag.Pager = pager.GetHtml();
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"];
                TempData["Status"] = null;
            }
            var booking = query.OrderBy(d => d.CheckIn).ThenBy(d => d.DateCreate).Skip((page - 1) * RecordsPerPage).Take(RecordsPerPage).ToList();

            //Get bookingViewModel
            foreach (var item in booking)
            {
                models.Add(new ConvertDbToModel().ToBooking(item, _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId)));
            }
            return View(models);
        }

        public ActionResult Detail(Guid Id)
        {
            var booking = _db.tbl_Booking.FirstOrDefault(d => d.BookingId == Id);

            var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == booking.HotelId);

            BookingViewModel model = new BookingViewModel();
            model = new ConvertDbToModel().ToBooking(booking, hotel);


            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentSuccess(Guid bookingId, bool IsPaymentSuccess = false)
        {
            var booking = _db.tbl_Booking.Find(bookingId);

            if (booking.NetPrice == null)
            {
                return Json(new { status = 0, msg = "Vui lòng cập nhật giá Net", check = !IsPaymentSuccess });
            }
            booking.IsPaymentSuccess = IsPaymentSuccess;
            booking.UserConfirm = User.UserId.ToString();
            booking.DateConfirm = DateTime.Now;
            double point = 0;
            if (IsPaymentSuccess)
            {
                if (!_db.tbl_BookingPoints.Any(d => d.BookingId == bookingId) && booking.CustomerId != null && booking.CustomerId != Guid.Empty) //kiểm tra booking có KH và chưa được cộng point
                {
                    point = (booking.TotalAmount - booking.NetPrice.Value) * 0.0001; //point = (giá bán - giá net)*10% /1000
                    if (point > 0)
                    {
                        tbl_BookingPoints bkPoint = new tbl_BookingPoints()
                        {
                            BookingId = booking.BookingId,
                            CustomerId = booking.CustomerId.Value,
                            Point = point,
                            DateCreate = DateTime.Now,
                            UserCreate = User.UserId
                        };
                        _db.tbl_BookingPoints.Add(bkPoint);

                        //Update customer Point

                        var customer = _db.tbl_Customer.FirstOrDefault(d => d.CustomerId == bkPoint.CustomerId);
                        customer.TotalPoint = customer.TotalPoint.GetValueOrDefault(0) + point;
                        _db.Entry(customer).State = EntityState.Modified;
                    }
                }
            }
            _db.Entry(booking).State = EntityState.Modified;
            _db.SaveChanges();

            #region Send SMS KH

            try
            {
                string message = "";

                message = "HotCombo da xac nhan thanh toan thanh cong booking {0}. \r\n";
                if (point > 0)
                {
                    message += "Quy khach duoc cong {1} diem cho booking nay. \r\n";
                }
                message += "Cam on Quy khach da su dung dich vu tai https://hotcombo.vn";

                message = string.Format(message, booking.BookingCode, point);

                SMSControl smsControl = new SMSControl();
                if (isSendSMS == "1" && Utilities.Utility.CheckPhoneNumber(booking.CustomerPhoneNumber))
                {
                    string result = smsControl.SendJson(booking.CustomerPhoneNumber, message);
                    if (result != null)
                    {
                        JObject ojb = JObject.Parse(result);
                        if ((int)ojb["CodeResult"] != 100) //Gui tin that bai
                        {
                            return Json(new { status = 1, msg = "Xác nhận thành công, gửi tin nhắn cho KH thất bại. Mã lỗi: " + ojb["CodeResult"]
                                , point = point, check = IsPaymentSuccess });
                        }
                    }
                    else
                    {
                        return Json(new { status = 1, msg = "Xác nhận thành công, gửi tin nhắn cho KH thất bại", point = point, check = IsPaymentSuccess });
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { status = 1, msg = "Xác nhận thành công, gửi tin nhắn cho KH thất bại", point = point, check = IsPaymentSuccess });
            }            

            #endregion

            return Json(new { status = 1, msg = "Xác nhận thành công", point = point, check = IsPaymentSuccess });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateNetPrice(Guid bookingId, double NetPrice = 0)
        {
            var booking = _db.tbl_Booking.FirstOrDefault(d => d.BookingId == bookingId);
            if (booking.IsPaymentSuccess)
            {
                return Json(new { status = 0, msg = "Booking đã thanh toán, không thể thay đổi giá Net" });
            }
            booking.NetPrice = NetPrice;
            _db.Entry(booking).State = EntityState.Modified;
            _db.SaveChanges();

            return Json(new { status = 1, msg = "Giá Net đã được cập nhật thành công" });
        }

        public ActionResult AllotBooking(Guid bookingId)
        {
            var booking = _db.tbl_Booking.FirstOrDefault(d => d.BookingId == bookingId);
            AllotBooking models = _db.tbl_BookingAllot.Where(d => d.BookingId == bookingId && !d.IsDelete).Select(d => new AllotBooking()
            {
                Id = d.Id,
                BookingId = d.BookingId,
                UserId = d.UserId,
                DateCreate = d.DateCreate,
                DeadLine = d.DeadLine,
                UserCreate = d.UserCreate,
                Status = d.Status,
                IsDelete = d.IsDelete
            }).FirstOrDefault();

            if (models != null)
            {
                models.BookingCode = booking.BookingCode;
            }
            else
            {
                models = new AllotBooking()
                {
                    BookingId = booking.BookingId,
                    BookingCode = booking.BookingCode,
                    DeadLine = booking.CheckIn.Date
                };
            }
            ViewBag.ListStaff = _db.tbl_User_Role.Where(d => d.RoleId == 2 && d.IsActive)
                .Select(d => new SelectListItem() { Value = d.UserId.ToString(), Text = d.tbl_User.FullName + " - " + d.tbl_User.LoginName }).ToList();
            return PartialView(models);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AllotBooking(AllotBooking model)
        {
            tbl_BookingAllot allot = new tbl_BookingAllot()
            {
                BookingId = model.BookingId,
                UserId = model.UserId,
                UserCreate = User.UserId,
                DateCreate = DateTime.Now,
                DeadLine = model.DeadLine,
                IsDelete = false,
                Status = 0
            };
            _db.tbl_BookingAllot.Add(allot);
            _db.SaveChanges();
            return Json(new { id = 1, message = "Thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AllotCancel(AllotBooking model)
        {
            tbl_BookingAllot allot = _db.tbl_BookingAllot.FirstOrDefault(d => d.Id == model.Id);
            allot.IsDelete = true;
            _db.SaveChanges();
            return Json(new { id = 1, message = "Thành công" });
        }


        #region Private
        private IQueryable<tbl_Booking> GetQueryBooking(IQueryable<tbl_Booking> query, SearchBookingViewModel search)
        {
            DateTime now = DateTime.Now.Date;
            //IQueryable<tbl_Booking> query = Enumerable.Empty<tbl_Booking>().AsQueryable();
            if (!string.IsNullOrEmpty(search.BookingCode))
            {
                query = query.Where(m => m.BookingCode == search.BookingCode.ToUpper());
            }

            if (!string.IsNullOrEmpty(search.HotelName))
            {
                var hotelId = _db.tbl_Hotel.Where(d => d.HotelName.ToLower().Contains(search.HotelName.ToLower())).Select(d => d.Id).FirstOrDefault();
                if (hotelId != null)
                {
                    query = query.Where(m => m.HotelId == hotelId);
                }
            }
            if (search.UserAlloted != null && search.UserAlloted != Guid.Empty)
            {
                query = query.Join(_db.tbl_BookingAllot.Where(d => d.UserId == search.UserAlloted),
                    b => b.BookingId,
                    a => a.BookingId,
                     (b, a) => b);
            }
            if (!string.IsNullOrEmpty(search.CustomerName))
            {
                query = query.Where(m => m.CustomerName.ToLower().Contains(search.CustomerName.ToLower()));
            }
            if (!string.IsNullOrEmpty(search.CustomerEmail))
            {
                query = query.Where(m => m.CustomerEmail.ToLower().Contains(search.CustomerEmail.ToLower()));
            }
            if (!string.IsNullOrEmpty(search.CustomerPhone))
            {
                query = query.Where(m => m.CustomerPhoneNumber.Contains(search.CustomerPhone));
            }
            if (search.FromDate != null)
            {
                query = query.Where(m => m.CheckIn >= search.FromDate.Value);
            }
            if (search.ToDate != null)
            {
                query = query.Where(m => m.CheckIn <= search.ToDate.Value);
            }
            if (search.FromDate == null && search.ToDate == null)
            {
                query = query.Where(m => m.CheckOut >= now && !m.IsDelete);
            }
            if (search.Status != -1)
            {
                switch (search.Status)
                {
                    case (int)Parameters.Booking.BookingStatus.ChuaPhanBo:
                        var bookingList = query.Select(d => d.BookingId).ToList();
                        var exceptionList = _db.tbl_BookingAllot.Where(d => bookingList.Contains(d.BookingId) && !d.IsDelete)
                            .Select(d => d.BookingId).ToList();
                        query = query.Where(d => !exceptionList.Contains(d.BookingId));
                        break;
                    case (int)Parameters.Booking.BookingStatus.KHDaThanhToan:
                        query = query.Where(m => m.CustomerCheckPayment == true);
                        break;
                    case (int)Parameters.Booking.BookingStatus.AdminXNThanhToan:
                        query = query.Where(m => m.IsPaymentSuccess == true);
                        break;
                    case (int)Parameters.Booking.BookingStatus.DaHuy:
                        query = query.Where(m => m.CustomerCheckPayment == true);
                        break;

                    default:
                        break;
                }
            }
            return query;
        }
        #endregion

    }
}