﻿using Hotel.Models;
using Hotel.EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.Utilities;
using System.Data.Entity;
using Custom.Security;
using System.IO;
using System.Web.Mvc.Html;
using System.Net;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.Web.Helpers;
using Hotel.IDataAccess;
using Hotel.DataAccess;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class CategoryController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        private IHotelDataAccess _hotelDA;
        private IHotelDataAccess HotelDA => _hotelDA ?? (_hotelDA = new HotelDataAccess());
        private const string UploadFolderFile = "~/FileUploads/Image";
        int RecordsPerPage = 10;
        // GET: Admin/Categories
        public ActionResult Index()
        {
            return View();
        }

        #region Địa điểm
        #region quoc gia

        public ActionResult Countries()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var countries = _db.tbl_Country.Where(p => p.IsDelete == null || p.IsDelete == false).OrderBy(d => d.CountryId).ToList();
            ViewBag.Countries = countries;

            List<SelectListItem> sllType = new List<SelectListItem>{
                new SelectListItem { Text = "Không", Value = "0" },
                new SelectListItem { Text = "Có", Value = "1" }
            };
            ViewBag.phuthuvisatainhapvn = new SelectList(sllType, "Value", "Text");
            return View(new tbl_Country());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CountriesCreate(tbl_Country model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {

                    model.UserCreate = User.UserId;
                    model.DateCreate = DateTime.Now;
                    model.UserUpdate = User.UserId;
                    model.DateUpdate = DateTime.Now;
                    model.IsDelete = false;

                    _db.tbl_Country.Add(model);
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("Countries");
                }
                catch (Exception e)
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("Countries");
                }
            }

            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("Countries");
        }

        public ActionResult _CountriesEdit(int id)
        {
            var country = _db.tbl_Country.Where(d => d.CountryId == id).FirstOrDefault();
            List<SelectListItem> sllType = new List<SelectListItem>{
                new SelectListItem { Text = "Không", Value = "0" },
                new SelectListItem { Text = "Có", Value = "1" }
            };
            ViewBag.phuthuvisatainhapvn = new SelectList(sllType, "Value", "Text");

            return PartialView(country);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CountriesEdit(tbl_Country model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    model.UserUpdate = User.UserId;
                    model.DateUpdate = DateTime.Now;
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("Countries");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("Countries");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("Countries");
        }

        [HttpPost]
        public JsonResult CountriesDelete(int id)
        {
            bool check = false;
            try
            {
                var model = _db.tbl_Country.Where(d => d.CountryId == id).FirstOrDefault();
                model.IsDelete = true;
                model.UserUpdate = User.UserId;
                model.DateUpdate = DateTime.Now;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        #endregion

        #region tinh thanh

        public ActionResult Provinces()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            int contryIdInt = 1;
            string countryId = "1", provinceName = "";
            if (Request.HttpMethod == "POST")
            {
                countryId = Request["CountryId"];

                provinceName = Request["ProvinceName"];
            }

            var session = this.HttpContext.Session;
            if (session["SearchProvince"] != null && Request.HttpMethod == "GET")
            {
                countryId = session["SearchProvince"] as string;
            }

            contryIdInt = Int32.Parse(countryId);
            var provinces = _db.tbl_Province.Where(d => d.CountryId == contryIdInt && d.IsDelete == false).OrderByDescending(d => d.ProvinceName).ToList();
            if (!string.IsNullOrEmpty(provinceName))
                provinces = provinces.Where(d => d.ProvinceName.Trim().ToLower().Contains(provinceName.Trim().ToLower())).ToList();

            ViewBag.Provinces = provinces;
            ViewBag.CountryId = new SelectList(_db.tbl_Country.OrderByDescending(d => d.CountryName), "CountryId", "CountryName", null);
            ViewBag.ProvinceName = provinceName;
            session["SearchProvince"] = countryId;
            return View(new tbl_Province());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProvincesCreate(tbl_Province model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    // model.ProvinceId = Guid.NewGuid();
                    model.DateCreate = DateTime.Now;
                    model.UserCreate = User.UserId;
                    model.UserUpdate = User.UserId;
                    model.DateUpdate = DateTime.Now;
                    model.IsDelete = false;
                    _db.tbl_Province.Add(model);
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("Provinces");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("Provinces");
                }
            }

            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("Provinces");
        }

        public ActionResult _ProvincesEdit(int id)
        {
            var model = _db.tbl_Province.First(d => d.ProvinceId == id);
            ViewBag.CountryId = new SelectList(_db.tbl_Country.OrderByDescending(d => d.CountryName), "country_id", "country_name", model.CountryId);

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProvincesEdit(tbl_Province model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    model.UserUpdate = User.UserId;
                    model.DateUpdate = DateTime.Now;
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("Provinces");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("Provinces");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("Provinces");
        }

        [HttpPost]
        public JsonResult ProvincesDelete(int id)
        {
            bool check = false;
            try
            {
                var model = _db.tbl_Province.First(d => d.ProvinceId == id);
                model.IsDelete = true;
                model.UserUpdate = User.UserId;
                model.DateUpdate = DateTime.Now;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        [HttpGet]
        public JsonResult _GetProvince(int countryid)
        {
            List<SelectListItem> sllProvince = new List<SelectListItem>();
            sllProvince.Add(new SelectListItem { Value = "0", Text = "---   Chọn tỉnh thành   ---" });
            var provinces = _db.tbl_Province.Where(d => d.CountryId == countryid && d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList();
            foreach (var item in provinces)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.ProvinceId.ToString();
                sllItem.Text = item.ProvinceName;

                sllProvince.Add(sllItem);
            }
            return Json(sllProvince, JsonRequestBehavior.AllowGet);
        }



        #endregion

        #region quan huyen

        public ActionResult Districts()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var districts = _db.tbl_District.OrderBy(d => d.DistrictId).ToList();
            ViewBag.Districts = districts;

            ViewBag.ProvinceId = new SelectList(_db.tbl_Province.ToList(), "ProvinceId", "ProvinceName");
            var countries = _db.tbl_Country.ToList();
            ViewBag.country_id = new SelectList(countries, "CountryId", "CountryName", "VN");
            return View(new tbl_District());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DistrictsCreate(tbl_District model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //model.DistrictId = Guid.NewGuid();
                    _db.tbl_District.Add(model);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    return RedirectToAction("Districts");
                }
                catch
                {
                    TempData["Notify"] = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");

                    return RedirectToAction("Districts");
                }
            }

            TempData["Notify"] = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            return RedirectToAction("Districts");
        }

        public ActionResult _DistrictsEdit(int id)
        {
            var district = _db.tbl_District.FirstOrDefault(d => d.DistrictId == id);

            var province = _db.tbl_Province.FirstOrDefault(d => d.ProvinceId == district.ProvinceId);

            if (province != null)
            {
                var countries = _db.tbl_Country.ToList();
                ViewBag.country_id = new SelectList(countries, "CountryId", "CountryName", province.CountryId);
                ViewBag.ProvinceId = new SelectList(
                    _db.tbl_Province.Where(d => d.CountryId == province.CountryId && d.IsDelete == false).ToList(), "ProvinceId", "ProvinceName",
                    province.ProvinceId);
            }
            else
            {
                ViewBag.country_id = new SelectList(_db.tbl_Country.ToList(), "CountryId", "CountryName", "VN");
                ViewBag.ProvinceId = new SelectList(
                    _db.tbl_Province.Where(d => d.CountryId == province.CountryId && d.IsDelete == false).ToList(), "ProvinceId", "ProvinceName");
            }

            return PartialView(district);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DistrictsEdit(tbl_District model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("Districts");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("Districts");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("Districts");
        }

        [HttpPost]
        public JsonResult DistrictsDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_District.FirstOrDefault(d => d.DistrictId == id);
                _db.tbl_District.Remove(model);
                //_db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        [HttpGet]
        public JsonResult _GetDistrict(int provinceid)
        {
            List<SelectListItem> sllDistrict = new List<SelectListItem>();
            sllDistrict.Add(new SelectListItem { Value = "0", Text = "---   Chọn quận / huyện   ---" });
            var districts = _db.tbl_District.Where(d => d.ProvinceId == provinceid).OrderBy(d => d.DistrictName).ToList();
            foreach (var item in districts)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.DistrictId.ToString();
                sllItem.Text = item.DistrictName;
                sllDistrict.Add(sllItem);
            }
            return Json(sllDistrict, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region khu vực

        public ActionResult Areas()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var areas = _db.tbl_Area.OrderBy(d => d.AreaId).ToList();
            ViewBag.Areas = areas;
            ViewBag.DistrictId = new SelectList(_db.tbl_District.ToList(), "DistrictId", "DistrictName");
            ViewBag.ProvinceId = new SelectList(_db.tbl_Province.ToList(), "ProvinceId", "ProvinceName");
            var countries = _db.tbl_Country.ToList();
            ViewBag.country_id = new SelectList(countries, "CountryId", "CountryName", "VN");
            return View(new tbl_Area());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AreasCreate(tbl_Area model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!_db.tbl_Area.Any(d => d.AreaName.ToLower() == model.AreaName.ToLower() && d.DistrictId == model.DistrictId))
                    {
                        //model.DistrictId = Guid.NewGuid();
                        model.DateCreate = DateTime.Now;
                        model.UserCreate = User.UserId;
                        model.DateUpdate = DateTime.Now;
                        model.UserUpdate = User.UserId;
                        model.IsDelete = false;
                        _db.tbl_Area.Add(model);
                        _db.SaveChanges();

                        return Json(Notify.NotifySuccess("Thêm mới dữ liệu thành công"));
                    }
                    else 
                    {
                        return Json(Notify.NotifyWarning("Đã tồn tại thông tin khu vực " + model.AreaName));
                    }
                }
                catch (Exception ex)
                {
                    return Json(Notify.NotifyDanger(ex.Message));
                }
            }

            return Json(Notify.NotifyWarning("Thông tin nhập không hợp lệ"));
        }

    public ActionResult _AreasEdit(int id)
    {
        var area = _db.tbl_Area.FirstOrDefault(d => d.AreaId == id);
        
        var district = _db.tbl_District.FirstOrDefault(d => d.DistrictId == area.DistrictId);
        var province = district.tbl_Province;
        var country = province.tbl_Country;

        if (country != null)
        {
                var countries = _db.tbl_Country.Where(d=>d.IsDelete == false).ToList();
                ViewBag.CountryId = new SelectList(countries, "CountryId", "CountryName", country.CountryId);
                ViewBag.ProvinceId = new SelectList(country.tbl_Province.Where(d => d.IsDelete == false).ToList(), "ProvinceId", "ProvinceName", province.ProvinceId);
                ViewBag.DistrictId = new SelectList(province.tbl_District.Where(d => d.IsDelete == false && d.ProvinceId == province.ProvinceId).ToList(), "DistrictId", "DistrictName", district.DistrictId);
        }
        else
        {
            ViewBag.country_id = new SelectList(_db.tbl_Country.ToList(), "CountryId", "CountryName", "VN");
        }

        return PartialView(area);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult AreasEdit(tbl_Area model)
    {
        Notify notify;
        if (ModelState.IsValid)
        {
            try
            {
                    model.DateUpdate = DateTime.Now;
                    model.UserUpdate = User.UserId;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                TempData["Notify"] = notify;

                return RedirectToAction("Areas");
            }
            catch
            {
                notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                TempData["Notify"] = notify;
                return RedirectToAction("Areas");
            }
        }
        notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
        TempData["Notify"] = notify;

        return RedirectToAction("Areas");
    }

    [HttpPost]
    public JsonResult AreasDelete(int id)
    {
        bool check;
        try
        {
            var model = _db.tbl_District.FirstOrDefault(d => d.DistrictId == id);
            _db.tbl_District.Remove(model);
            //_db.SaveChanges();

            check = true;
        }
        catch
        {
            check = false;
        }
        return Json(check);
    }
    #endregion
    #endregion

        #region Khách sạn
        
        #region Loại khách sạn

    public ActionResult HotelTypes()
    {
        if (TempData["Notify"] != null)
        {
            Notify notify = (Notify)TempData["Notify"];
            ViewBag.Notify = notify;
        }

        var applications = _db.tbl_HotelType.ToList();
        ViewBag.Types = applications;

        return View(new tbl_HotelType());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult HotelTypesCreate(tbl_HotelType model)
    {
        Notify notify;
        if (ModelState.IsValid)
        {
            try
            {
                int count = 1;
                var types = _db.tbl_HotelType.ToList();
                if (types != null && types.Count() > 0)
                    count = types.Max(d => d.Id) + 1;

                model.Id = byte.Parse(count.ToString());
                model.IsDelete = false;
                _db.tbl_HotelType.Add(model);
                _db.SaveChanges();

                notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                TempData["Notify"] = notify;

                return RedirectToAction("HotelTypes");
            }
            catch (Exception e)
            {
                notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                TempData["Notify"] = notify;
                return RedirectToAction("HotelTypes");
            }
        }

        notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
        TempData["Notify"] = notify;

        return RedirectToAction("HotelTypes");
    }

    public ActionResult _HotelTypesEdit(int id = 0)
    {
        tbl_HotelType hotelType = _db.tbl_HotelType.FirstOrDefault(d => d.Id == id);
        if (hotelType == null)
        {
            return HttpNotFound();
        }
        return PartialView(hotelType);
    }

    [HttpPost]
    public ActionResult HotelTypesEdit(tbl_HotelType model)
    {
        Notify notify;
        if (ModelState.IsValid)
        {
            try
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                TempData["Notify"] = notify;

                return RedirectToAction("HotelTypes");
            }
            catch
            {
                notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                TempData["Notify"] = notify;
                return RedirectToAction("HotelTypes");
            }
        }
        notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
        TempData["Notify"] = notify;

        return RedirectToAction("HotelTypes");
    }

    [HttpPost]
    public JsonResult HotelTypesDelete(int id)
    {
        bool check;
        try
        {
            var model = _db.tbl_HotelType.FirstOrDefault(d => d.Id == id);
            _db.tbl_HotelType.Remove(model);
            _db.SaveChanges();

            check = true;
        }
        catch
        {
            check = false;
        }
        return Json(check);
    }

    #endregion

        #region cap khach san

    public ActionResult HotelLevels()
    {
        if (TempData["Notify"] != null)
        {
            Notify notify = (Notify)TempData["Notify"];
            ViewBag.Notify = notify;
        }

        var applications = _db.tbl_HotelRatingType.ToList();
        ViewBag.Levels = applications;

        return View(new tbl_HotelRatingType());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult HotelLevelsCreate(tbl_HotelRatingType model)
    {
        Notify notify;
        if (ModelState.IsValid)
        {
            try
            {
                int count = 1;
                var levels = _db.tbl_HotelRatingType.ToList();
                if (levels != null && levels.Count() > 0)
                    count = levels.Max(d => d.Id) + 1;

                model.Id = byte.Parse(count.ToString());
                _db.tbl_HotelRatingType.Add(model);
                _db.SaveChanges();

                notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                TempData["Notify"] = notify;

                return RedirectToAction("HotelLevels");
            }
            catch (Exception e)
            {
                notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                TempData["Notify"] = notify;
                return RedirectToAction("HotelLevels");
            }
        }

        notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
        TempData["Notify"] = notify;

        return RedirectToAction("HotelLevels");
    }

    public ActionResult _HotelLevelsEdit(int id = 0)
    {
        tbl_HotelRatingType tbl_HotelRatingType = _db.tbl_HotelRatingType.FirstOrDefault(d => d.Id == id);
        if (tbl_HotelRatingType == null)
        {
            return HttpNotFound();
        }
        return PartialView(tbl_HotelRatingType);
    }

    [HttpPost]
    public ActionResult HotelLevelsEdit(tbl_HotelRatingType model)
    {
        Notify notify;
        if (ModelState.IsValid)
        {
            try
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                TempData["Notify"] = notify;

                return RedirectToAction("HotelLevels");
            }
            catch
            {
                notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                TempData["Notify"] = notify;
                return RedirectToAction("HotelLevels");
            }
        }
        notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
        TempData["Notify"] = notify;

        return RedirectToAction("HotelLevels");
    }

    [HttpPost]
    public JsonResult HotelLevelsDelete(int id)
    {
        bool check;
        try
        {
            var model = _db.tbl_HotelRatingType.FirstOrDefault(d => d.Id == id);
            _db.tbl_HotelRatingType.Remove(model);
            _db.SaveChanges();

            check = true;
        }
        catch
        {
            check = false;
        }
        return Json(check);
    }

        #endregion
   
        #region Tien ich

        public ActionResult ConvenientTypes()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var applications = _db.tbl_HotelConvenientType.OrderBy(t => t.Type).OrderBy(t => t.Order).ToList();
            ViewBag.ConvenientTypes = applications;

            return View(new tbl_HotelConvenientType());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConvenientTypesCreate(tbl_HotelConvenientType model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    int count = 1;
                    var types = _db.tbl_HotelConvenientType.ToList();
                    if (types != null && types.Count() > 0)
                        count = types.Max(d => d.Id) + 1;

                    model.Id = byte.Parse(count.ToString());
                    model.IsDelete = false;
                    model.Symbol = model.Symbol;
                    #region Upload hình
                    string fileName = "";

                    foreach (string file in Request.Files)
                    {
                        var postedFile = Request.Files[file];

                        string fileExtension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf("."));

                        string newName = Guid.NewGuid().ToString();

                        fileName = "img_" + DateTime.Now.ToString("ddMMyyyy") + newName + fileExtension;


                        postedFile.SaveAs(Server.MapPath(string.Format("~/{0}/", "FileUploads/Convenient/")) + fileName);
                        model.Symbol = fileName;


                    }
                    #endregion

                    _db.tbl_HotelConvenientType.Add(model);
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("ConvenientTypes");
                }
                catch (Exception e)
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("ConvenientTypes");
                }
            }

            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("ConvenientTypes");
        }

        public ActionResult _ConvenientTypesEdit(int id = 0)
        {
            tbl_HotelConvenientType convenientType = _db.tbl_HotelConvenientType.FirstOrDefault(d => d.Id == id);
            if (convenientType == null)
            {
                return HttpNotFound();
            }
            return PartialView(convenientType);
        }

        [HttpPost]
        public ActionResult ConvenientTypesEdit(tbl_HotelConvenientType model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    tbl_HotelConvenientType oldItem = _db.tbl_HotelConvenientType.FirstOrDefault(t => t.Id == model.Id);
                    oldItem.ConvenientName = model.ConvenientName;
                    oldItem.Order = model.Order;
                    oldItem.DateUpdate = DateTime.Now;
                    oldItem.UserUpdate = User.UserId;
                    #region Upload hình
                    string fileName = "";
                    foreach (string file in Request.Files)
                    {
                        var postedFile = Request.Files[file];
                        if (postedFile.HasFile())
                        {
                            string fileExtension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf("."));
                            string newName = Guid.NewGuid().ToString();
                            fileName = "img_" + DateTime.Now.ToString("ddMMyyyy") + newName + fileExtension;
                            postedFile.SaveAs(Server.MapPath(string.Format("~/{0}/", "FileUploads/Convenient/")) + fileName);
                            oldItem.Symbol = fileName;
                        }


                    }
                    #endregion
                    _db.Entry(oldItem).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("ConvenientTypes");
                }
                catch(Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("ConvenientTypes");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("ConvenientTypes");
        }

        [HttpPost]
        public JsonResult ConvenientTypesDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelConvenientType.FirstOrDefault(d => d.Id == id);
                _db.tbl_HotelConvenientType.Remove(model);
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }


        #endregion

        #region Loại giá

        public ActionResult PriceTypes()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var applications = _db.tbl_HotelPriceType.ToList();
            ViewBag.PriceTypes = applications;

            return View(new tbl_HotelPriceType());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PriceTypesCreate(tbl_HotelPriceType model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    int count = 1;
                    var types = _db.tbl_HotelPriceType.ToList();
                    if (types != null && types.Count() > 0)
                        count = types.Max(d => d.Id) + 1;

                    model.Id = byte.Parse(count.ToString());
                    model.IsDelete = false;
                    _db.tbl_HotelPriceType.Add(model);
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("PriceTypes");
                }
                catch (Exception e)
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("PriceTypes");
                }
            }

            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("PriceTypes");
        }

        public ActionResult _PriceTypesEdit(int id = 0)
        {
            tbl_HotelPriceType paymentType = _db.tbl_HotelPriceType.FirstOrDefault(d => d.Id == id);
            if (paymentType == null)
            {
                return HttpNotFound();
            }
            return PartialView(paymentType);
        }

        [HttpPost]
        public ActionResult PriceTypesEdit(tbl_HotelPriceType model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("PriceTypes");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("PriceTypes");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("PriceTypes");
        }

        [HttpPost]
        public JsonResult PriceTypesDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelPriceType.FirstOrDefault(d => d.Id == id);
                _db.tbl_HotelPriceType.Remove(model);
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        #endregion

        #region Chính sách hủy

        public ActionResult CancellationPolicys()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var cancellationPolicys = _db.tbl_HotelCancellationPolicy.OrderBy(d => d.Id).ToList();
            ViewBag.CancellationPolicys = cancellationPolicys;

            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.ToList(), "Id", "HotelName");
            var hotels = _db.tbl_Hotel.ToList();
            ViewBag.hotel_id = new SelectList(hotels, "Id", "HotelName");
            return View(new tbl_HotelCancellationPolicy());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancellationPolicysCreate(tbl_HotelCancellationPolicy model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //model.DistrictId = Guid.NewGuid();
                    _db.tbl_HotelCancellationPolicy.Add(model);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    return RedirectToAction("CancellationPolicys");
                }
                catch(Exception ex)
                {
                    TempData["Notify"] = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");

                    return RedirectToAction("CancellationPolicys");
                }
            }

            TempData["Notify"] = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            return RedirectToAction("CancellationPolicys");
        }

        public ActionResult _CancellationPolicysEdit(int id)
        {
            var cancellationPolicy = _db.tbl_HotelCancellationPolicy.FirstOrDefault(d => d.Id == id);

            var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == cancellationPolicy.HotelId);

            if (hotel != null)
            {
                ViewBag.HotelId = new SelectList(
                    _db.tbl_Hotel.Where(d => d.IsDelete == false).ToList(), "Id", "HotelName",
                    hotel.Id);
            }
            else
            {
                ViewBag.ProvinceId = new SelectList(
                    _db.tbl_Province.Where(d => d.IsDelete == false).ToList(), "Id", "HotelName");
            }

            return PartialView(cancellationPolicy);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancellationPolicysEdit(tbl_HotelCancellationPolicy model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("CancellationPolicys");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("CancellationPolicys");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("CancellationPolicys");
        }

        [HttpPost]
        public JsonResult CancellationPolicysDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelCancellationPolicy.FirstOrDefault(d => d.Id == id);
                _db.tbl_HotelCancellationPolicy.Remove(model);
                //_db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        [HttpGet]
        public JsonResult _GetHotel(Guid hotelid)
        {
            List<SelectListItem> sllHotel = new List<SelectListItem>();
            sllHotel.Add(new SelectListItem { Value = "0", Text = "---   Chọn khách sạn   ---" });
            var hotels = _db.tbl_Hotel.Where(d => d.Id == hotelid).OrderBy(d => d.HotelName).ToList();
            foreach (var item in hotels)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.Id.ToString();
                sllItem.Text = item.HotelName;
                sllHotel.Add(sllItem);
            }
            return Json(sllHotel, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lưu ý khách sạn

        public ActionResult HotelNotices()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var hotelNotices = _db.tbl_HotelNotice.OrderBy(d => d.Id).ToList();
            ViewBag.HotelNotices = hotelNotices;

            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.ToList(), "Id", "HotelName");
            var hotels = _db.tbl_Hotel.ToList();
            ViewBag.hotel_id = new SelectList(hotels, "Id", "HotelName");
            return View(new tbl_HotelNotice());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HotelNoticesCreate(tbl_HotelNotice model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //model.DistrictId = Guid.NewGuid();
                    _db.tbl_HotelNotice.Add(model);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    return RedirectToAction("HotelNotices");
                }
                catch (Exception ex)
                {
                    TempData["Notify"] = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");

                    return RedirectToAction("HotelNotices");
                }
            }

            TempData["Notify"] = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            return RedirectToAction("HotelNotices");
        }

        public ActionResult _HotelNoticesEdit(int id)
        {
            var hotelNotice = _db.tbl_HotelNotice.FirstOrDefault(d => d.Id == id);

            var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == hotelNotice.HotelId);


            ViewBag.HotelId = new SelectList(
                _db.tbl_Hotel.Where(d => d.IsDelete == false).ToList(), "Id", "HotelName",
                hotel.Id);


            return PartialView(hotelNotice);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HotelNoticesEdit(tbl_HotelNotice model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("HotelNotices");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("HotelNotices");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("HotelNotices");
        }

        [HttpPost]
        public JsonResult HotelNoticesDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelNotice.FirstOrDefault(d => d.Id == id);
                model.IsDelete = true;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
                //_db.tbl_HotelNotice.Remove(model);
                //_db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }


        #endregion

        #region dịch vụ đính kèm

        public ActionResult ServiceTypes()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var applications = _db.tbl_HotelServiceType.OrderBy(x => x.Order).ToList();
            ViewBag.ServiceTypes = applications;

            return View(new tbl_HotelServiceType());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ServiceTypesCreate(tbl_HotelServiceType model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    int count = 1;
                    var types = _db.tbl_HotelServiceType.ToList();
                    if (types != null && types.Count() > 0)
                        count = types.Max(d => d.Id) + 1;

                    model.Id = byte.Parse(count.ToString());
                    model.IsDelete = false;
                    _db.tbl_HotelServiceType.Add(model);
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("ServiceTypes");
                }
                catch (Exception e)
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("ServiceTypes");
                }
            }

            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("ServiceTypes");
        }

        public ActionResult _ServiceTypesEdit(int id = 0)
        {
            tbl_HotelServiceType serviceType = _db.tbl_HotelServiceType.FirstOrDefault(d => d.Id == id);
            if (serviceType == null)
            {
                return HttpNotFound();
            }
            return PartialView(serviceType);
        }

        [HttpPost]
        public ActionResult ServiceTypesEdit(tbl_HotelServiceType model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("ServiceTypes");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("ServiceTypes");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("ServiceTypes");
        }

        [HttpPost]
        public JsonResult ServiceTypesDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelServiceType.FirstOrDefault(d => d.Id == id);
                model.IsDelete = true;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }


        #endregion

        #region Chính sách hủy

        public ActionResult SurchargePolicys()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var surchargePolicys = _db.tbl_HotelSurchargePolicy.OrderBy(d => d.Id).ToList();
            ViewBag.SurchargePolicys = surchargePolicys;

            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.ToList(), "Id", "HotelName");
            var hotels = _db.tbl_Hotel.ToList();
            ViewBag.hotel_id = new SelectList(hotels, "Id", "HotelName");
            return View(new tbl_HotelSurchargePolicy());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SurchargePolicysCreate(tbl_HotelSurchargePolicy model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //model.DistrictId = Guid.NewGuid();
                    _db.tbl_HotelSurchargePolicy.Add(model);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    return RedirectToAction("SurchargePolicys");
                }
                catch (Exception ex)
                {
                    TempData["Notify"] = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");

                    return RedirectToAction("SurchargePolicys");
                }
            }

            TempData["Notify"] = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            return RedirectToAction("SurchargePolicys");
        }

        public ActionResult _SurchargePolicysEdit(int id)
        {
            var surchargePolicy = _db.tbl_HotelSurchargePolicy.FirstOrDefault(d => d.Id == id);

            var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == surchargePolicy.HotelId);

          
            ViewBag.HotelId = new SelectList(
                _db.tbl_Hotel.Where(d => d.IsDelete == false).ToList(), "Id", "HotelName",
                hotel.Id);

            return PartialView(surchargePolicy);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SurchargePolicysEdit(tbl_HotelSurchargePolicy model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("SurchargePolicys");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("SurchargePolicys");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("SurchargePolicys");
        }

        [HttpPost]
        public JsonResult SurchargePolicysDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelCancellationPolicy.FirstOrDefault(d => d.Id == id);
                model.IsDelete = true;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        #endregion

        #region Khach san

        public ActionResult Hotels()
        {
            try
            {
                if (TempData["Notify"] != null)
                {
                    Notify notify = (Notify)TempData["Notify"];
                    ViewBag.Notify = notify;
                }

                ViewBag.CountryId = new SelectList(_db.tbl_Country.ToList(), "CountryId", "CountryName", "1");
                
                ViewBag.HotelType = new SelectList(_db.tbl_HotelType.OrderBy(d => d.HotelTypeName).ToList(), "Id", "HotelTypeName", null);

                ViewBag.Status = new SelectList(XMLUtils.BindData("status"), "value", "text", null);

                var viewModel = new List<HotelViewModel>();
                return View(viewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetHotelsList(string HotelName = "", string Address = "", int? HotelType = null, string Phone = "", int? CountryId = null, int? ProvinceId = null, int? DistrictId = null, int? Status = null)
        {
            //var hotels = _db.tbl_Hotel.Where(x => x.HotelName.ToLower().Contains(HotelName.ToLower()) 
            //&& Address == "" || x.Address.ToLower().Contains(Address.ToLower())
            //&& x.HotelTypeId == HotelType).ToList();
            var hotels = _db.tbl_Hotel.Where(x => x.IsDelete == false
            && (HotelName == "" || x.HotelName.ToLower().Contains(HotelName.ToLower()))
            && (Address == "" || x.Address.ToLower().Contains(Address.ToLower()))
            && (HotelType == null || x.HotelTypeId == HotelType)
            && (Phone == "" || x.Telephone == Phone)
            && (CountryId == 0 || x.CountryId == CountryId)
            && (ProvinceId == 0 || x.ProvinceId == ProvinceId)
            && (DistrictId == 0 || x.DistrictId == DistrictId)
            && (Status == null || x.IsActive == Status)
            ).ToList();
            var viewModel = hotels.Select(x => new HotelViewModel()
            {
                Id = x.Id,
                HotelName = x.HotelName,
                CountryId = x.CountryId,
                ProvinceId = x.ProvinceId,
                DistrictId = x.DistrictId,
                AreaId = x.AreaId,
                HotelRatingTypeId = x.HotelRatingTypeId,
                HotelTypeId = x.HotelTypeId,
                HotelConvenientType = x.HotelConvenientType,
                Address = x.Address,
                Telephone = x.Telephone,
                Mobile = x.Mobile,
                Longitude = x.Longitude,
                Latitude = x.Latitude,
                Content = x.Content,
                Email = x.Email,
                Url = x.Url,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                var country = _db.tbl_Country.FirstOrDefault(d => d.CountryId == item.CountryId);
                var province = _db.tbl_Province.FirstOrDefault(d => d.ProvinceId == item.ProvinceId);
                var district = _db.tbl_District.FirstOrDefault(d => d.DistrictId == item.DistrictId);

                if (country != null)
                    item.CountryName = country.CountryName;

                if (province != null)
                    item.ProvinceName = province.ProvinceName;

                if (district != null)
                    item.ProvinceName = district.DistrictName;
            }

            return PartialView("_HotelsList", viewModel);
        }

        public ActionResult GetRoomsListInEditHotel(Guid HotelId)
        {
            var rooms = _db.tbl_Room.Where(x => x.IsDelete == false 
            && x.HotelId == HotelId
            ).ToList();
            var viewModel = rooms.Select(x => new RoomViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                RoomName = x.RoomName,
                AvailableRoom = x.AvailableRoom,
                RoomArea = x.RoomArea,
                Direction = x.Direction,
                SingleBed = x.SingleBed,
                DoubleBed = x.DoubleBed,
                MaxPeople = x.MaxPeople,
                HotelConvenientType = x.HotelConvenientType,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            //foreach (var item in viewModel)
            //{
            //    var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId);
            //    var direction = _db.tbl_Direction.FirstOrDefault(d => d.Id == item.Direction);
   

            //    if (hotel != null)
            //        item.HotelName = hotel.HotelName;

            //    if (direction != null)
            //        item.DirectionName = direction.DirectionName;
            //}

            return PartialView("_RoomList", viewModel);
        }

        //public ActionResult GetHotelNoticesListInEditHotel(Guid HotelId)
        //{
        //    var rooms = _db.tbl_HotelNotice.Where(x => x.IsDelete == false
        //    && x.HotelId == HotelId
        //    ).ToList();
        //    var viewModel = rooms.Select(x => new HotelNoticeViewModel()
        //    {
        //        Id = x.Id,
        //        HotelId = x.HotelId,
        //        Content = x.Content,
        //        IsDelete = x.IsDelete,
        //        UserCreate = x.UserCreate,
        //        DateCreate = x.DateCreate,
        //        UserUpdate = x.UserUpdate,
        //        DateUpdate = x.DateUpdate
        //    }).ToList();

        //    foreach (var item in viewModel)
        //    {
        //        var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId);

        //        if (hotel != null)
        //            item.HotelName = hotel.HotelName;

        //    }

        //    return PartialView("_HotelNoticeList", viewModel);
        //}

        public ActionResult EditRoomPopup(Guid id)
        {
            var model = _db.tbl_Room.Where(x => x.Id == id).ToList();
            var viewModel = model.Select(s => new RoomViewModel()
            {
                Id = s.Id,
                HotelId = s.HotelId,
                RoomName = s.RoomName,
                AvailableRoom = s.AvailableRoom,
                RoomArea = s.RoomArea,
                Direction = s.Direction,
                SingleBed = s.SingleBed,
                DoubleBed = s.DoubleBed,
                MaxPeople = s.MaxPeople,
                HotelConvenientType = s.HotelConvenientType,
                Image = s.Image,
                IsDelete = s.IsDelete,
                UserCreate = s.UserCreate,
                DateCreate = s.DateCreate,
                UserUpdate = s.UserUpdate,
                DateUpdate = s.DateUpdate
            }).FirstOrDefault();

            //dll Hotel
            ViewBag.HotelId = new SelectList(
                    _db.tbl_Hotel.Where(d => d.IsDelete == false).OrderBy(d => d.HotelName).ToList(), "Id", "HotelName",
                    viewModel.HotelId);

            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", viewModel.HotelId); ;

            //dll Hướng
            ViewBag.DirectionType = new SelectList(_db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList(), "Id", "DirectionName", viewModel.Direction);

            //dll Tien ich
            string[] convenientTypeIds = { };
            if (viewModel.HotelConvenientType != "" && viewModel.HotelConvenientType != null)
            {
                convenientTypeIds = viewModel.HotelConvenientType.Split(new char[] { ',' });
            }

            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false && x.Type == 1).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                foreach (var check in convenientTypeIds)
                {
                    if (item.Id == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            //Seesion imageOld
            if (viewModel.Image != null)
            {
                TempData["ImageRoomOld"] = viewModel.Image.ToString();
            }
            else
            {
                TempData["ImageRoomOld"] = null;
            }

            return PartialView("_EditRoom", viewModel);
        }

        public ActionResult HotelsCreate()
        {
            var modelView = new HotelViewModel();

            //dll Loai khach san
            ViewBag.HotelType = new SelectList(_db.tbl_HotelType.Where(x => x.IsDelete == false).OrderBy(x => x.HotelTypeName).ToList(), "Id", "HotelTypeName");
            //dll Hang sao
            ViewBag.HotelRatingType = new SelectList(_db.tbl_HotelRatingType.Where(x => x.IsDelete == false).OrderBy(x => x.NumberStar).ToList(), "Id", "Name");

            //dll Quoc gia
            ViewBag.CountryId = new SelectList(_db.tbl_Country.OrderBy(d => d.CountryName).ToList(), "CountryId", "CountryName", null);

            //dll Tinh Thanh
            ViewBag.ProvinceId = new SelectList(_db.tbl_Province.Where(d => d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList(), "ProvinceId", "ProvinceName", null);

            //dll Quan Huyen
            ViewBag.DistrictId = new SelectList(_db.tbl_District.Where(d => d.IsDelete == false).OrderBy(d => d.DistrictName).ToList(), "DistrictId", "DistrictName", null);

            //dll Khu vuc
            var sllAreaType = new List<SelectListItem>();
            var dbModelAreaType = _db.tbl_Area.Where(x => x.IsDelete == false).OrderBy(x => x.AreaName).ToList();

            foreach (var item in dbModelAreaType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.AreaId.ToString();
                selectItem.Text = item.AreaName;

                sllAreaType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlAreaType = sllAreaType;

            //dll Tien ich
            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            ViewBag.UrlImage = "~/FileUploads/Image/default-image.jpg";

            ViewBag.ddlStatus = new SelectList(XMLUtils.BindData("status"), "value", "text", null);

            return View(modelView);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HotelsCreate(HotelViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null, int?[] sllAreaTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {
                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    string sllAreaTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllAreaTypeID"]))
                    {
                        sllAreaTypeId = Request["sllAreaTypeID"];
                        string[] areaTypeIds = sllAreaTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Hotel() { 
                        Id = Guid.NewGuid(),
                        HotelName = viewModel.HotelName,
                        Address = viewModel.Address,
                        CountryId = viewModel.CountryId,
                        ProvinceId = viewModel.ProvinceId,
                        DistrictId = viewModel.DistrictId,
                        AreaId = sllAreaTypeId,
                        HotelTypeId = viewModel.HotelTypeId,
                        HotelRatingTypeId = viewModel.HotelRatingTypeId,
                        Telephone = viewModel.Telephone,
                        Mobile = viewModel.Mobile,
                        Email = viewModel.Email,
                        Url = viewModel.Url,
                        Content = viewModel.Content,
                        Image = viewModel.Image,
                        HotelConvenientType = sllConvenientTypeId,
                        IsActive = viewModel.IsActive,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemFile in files)
                    {
                        try
                        {
                            if (files != null)
                            {
                                //upload file
                                string fileName = itemFile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemFile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch(Exception ex) {
                            throw ex;
                        }
                    }
                    
                       

                    _db.tbl_Hotel.Add(item);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    return RedirectToAction("Hotels");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            //ViewBag.CountryId = new SelectList(_db.tbl_countries.OrderBy(d => d.country_name).ToList(), "country_id", "country_name", model.CountryId);
            //ViewBag.ProvinceId = new SelectList(_db.tbl_provinces.Where(d => d.IsDelete == false).OrderBy(d => d.province_name).ToList(), "province_id", "province_name", model.ProvinceId);
            //ViewBag.Type = new SelectList(_db.tbl_hotel_level, "hotel_type", "hotel_type_name", model.Type);

            //ViewBag.Notify = notify;
            return View(viewModel);
            //return RedirectToAction("Hotels");
        }

        public ActionResult HotelsEdit(Guid id)
        {
            List<tbl_Hotel> model = new List<tbl_Hotel>();
            List<tbl_Room> modelRoom = new List<tbl_Room>();
            model = _db.tbl_Hotel.Where(x => x.Id == id).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }

            modelRoom = _db.tbl_Room.Where(x => x.HotelId == id).ToList();

            //Hotel viewModel = Mapper.MapFrom(tbl_hotel_restaurant);
            var viewModel = model.Select(x => new HotelViewModel()
            {
                Id = x.Id,
                HotelName = x.HotelName,
                CountryId = x.CountryId,
                ProvinceId = x.ProvinceId,
                DistrictId = x.DistrictId,
                AreaId = x.AreaId,
                HotelRatingTypeId = x.HotelRatingTypeId,
                HotelTypeId = x.HotelTypeId,
                HotelConvenientType = x.HotelConvenientType,
                Address = x.Address,
                Telephone = x.Telephone,
                Mobile = x.Mobile,
                Longitude = x.Longitude,
                Latitude = x.Latitude,
                Content = x.Content,
                Email = x.Email,
                Url = x.Url,
                Image = x.Image,
                IsActive = x.IsActive,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).FirstOrDefault();

            //dll Loai khach san
            ViewBag.HotelType = new SelectList(_db.tbl_HotelType.Where(x => x.IsDelete == false).OrderBy(x => x.HotelTypeName).ToList(), "Id", "HotelTypeName", viewModel.HotelTypeId);
            //dll Hang sao
            ViewBag.HotelRatingType = new SelectList(_db.tbl_HotelRatingType.Where(x => x.IsDelete == false).OrderBy(x => x.NumberStar).ToList(), "Id", "Name", viewModel.HotelRatingTypeId);

            //dll Quoc gia
            ViewBag.CountryId = new SelectList(_db.tbl_Country.OrderBy(d => d.CountryName).ToList(), "CountryId", "CountryName", viewModel.CountryId);

            //dll Tinh Thanh
            ViewBag.ProvinceId = new SelectList(_db.tbl_Province.Where(d => d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList(), "ProvinceId", "ProvinceName", viewModel.ProvinceId);

            //dll Quan Huyen
            ViewBag.DistrictId = new SelectList(_db.tbl_District.Where(d => d.IsDelete == false).OrderBy(d => d.DistrictName).ToList(), "DistrictId", "DistrictName", viewModel.DistrictId);

            //dll Khu vuc
            string[] areaTypeIds = { };
            if (viewModel.AreaId != "" && viewModel.AreaId != null)
            {
                areaTypeIds = viewModel.AreaId.Split(new char[] { ',' });
            }

            var sllAreaType = new List<SelectListItem>();
            var dbModelAreaType = _db.tbl_Area.Where(x => x.IsDelete == false).OrderBy(x => x.AreaName).ToList();

            foreach (var item in dbModelAreaType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.AreaId.ToString();
                selectItem.Text = item.AreaName;

                foreach (var check in areaTypeIds)
                {
                    if (item.AreaId == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllAreaType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlAreaType = sllAreaType;

            //dll Tien ich
            string[] convenientTypeIds = { };
            if(viewModel.HotelConvenientType != "" && viewModel.HotelConvenientType != null)
            {
                convenientTypeIds = viewModel.HotelConvenientType.Split(new char[] { ',' });
            }

            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                foreach(var check in convenientTypeIds)
                {
                    if(item.Id == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            ViewBag.ddlStatus = new SelectList(XMLUtils.BindData("status"), "value", "text", null);

            //Seesion imageOld
            if (viewModel.Image != null)
            {
                TempData["ImageHotelOld"] = viewModel.Image.ToString();
            }
            else
            {
                TempData["ImageHotelOld"] = null;
            }


            viewModel.Rooms = new List<RoomViewModel>();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HotelsEdit(HotelViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null, int?[] sllAreaTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {
                    
                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    string sllAreaTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllAreaTypeID"]))
                    {
                        sllAreaTypeId = Request["sllAreaTypeID"];
                        string[] areaTypeIds = sllAreaTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Hotel()
                    {
                        Id = viewModel.Id,
                        HotelName = viewModel.HotelName,
                        Address = viewModel.Address,
                        CountryId = viewModel.CountryId,
                        ProvinceId = viewModel.ProvinceId,
                        DistrictId = viewModel.DistrictId,
                        AreaId = sllAreaTypeId,
                        HotelTypeId = viewModel.HotelTypeId,
                        HotelRatingTypeId = viewModel.HotelRatingTypeId,
                        Telephone = viewModel.Telephone,
                        Mobile = viewModel.Mobile,
                        Email = viewModel.Email,
                        Url = viewModel.Url,
                        IsActive = viewModel.IsActive,
                        Content = viewModel.Content,
                        Image = viewModel.Image,
                        HotelConvenientType = sllConvenientTypeId,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemfile in files)
                    {
                        try
                        {
                            if (itemfile != null)
                            {
                                //upload file
                                string fileName = itemfile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemfile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch(Exception ex) {
                            throw ex;
                        }
                    }

                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    if(files[0] != null)
                    {
                        //Xoa di anh cu trong Folder
                        string imgPath = UploadFolderFile + "\\" + (string)TempData["ImageHotelOld"];
                        //FileInfo fi = new FileInfo(imgPath);
                        FileInfo fi = new FileInfo(Server.MapPath(imgPath));
                        if (fi.Exists)
                        {
                            fi.Delete();
                        }
                    }

                    TempData["Notify"] = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    return RedirectToAction("Hotels");
                    
                }
                catch(Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            ViewBag.Notify = notify;
            return RedirectToAction("Hotels"); ;
        }

        public ActionResult DeleteHotel(Guid id)
        {
            try
            {
                tbl_Hotel hotel = _db.tbl_Hotel.Find(id);

                hotel.IsDelete = true;
                _db.Entry(hotel).State = EntityState.Modified;
                _db.SaveChanges();

                TempData["Notify"] = Notify.NotifySuccess("Xóa thành công.");
                return RedirectToAction("Hotels");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region SaveImages Hotel
        public ActionResult _AddImages(Guid Id)
        {
            ViewBag.HotelId = Id;
            ViewBag.Types = new SelectList(XMLUtils.BindData("imagetypehotel"), "value", "text", null);

            return PartialView();
        }

        public ActionResult _GetListImage(Guid HotelId)
        {
            ViewBag.List = _db.tbl_ProductImage.Where(p => p.HotelId == HotelId && p.IsDelete == null || p.IsDelete == false).OrderBy(x => x.Type).ThenBy(x => x.Order).ThenBy(x => x.DateCreate);

            return PartialView();
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult _FunSaveImage(Guid HotelId, int? Types = null, int? Order = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = new tbl_ProductImage()
                    {
                        Id = Guid.NewGuid(),
                        HotelId = HotelId,
                        Type = Types,
                        Order = Order,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                    };

                    #region Upload hình
                    string fileName = "";

                    foreach (string file in Request.Files)
                    {
                        var postedFile = Request.Files[file];

                        string fileExtension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf("."));

                        string newName = Guid.NewGuid().ToString();

                        fileName = "img_" + DateTime.Now.ToString("ddMMyyyy") + newName + fileExtension;

                        #region Check file chính hay file phụ
                        if (file.Contains("ImageSlide_"))
                        {
                            postedFile.SaveAs(Server.MapPath(string.Format("~/{0}/", "FileUploads/Image/Hotels")) + fileName);
                            item.ImageUrl = fileName;

                            var image = WebImage.GetImageFromRequest(file);
                            if (image != null)
                            {
                                //resize ảnh small
                                image.Resize(1171, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathSmall = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "hotel_" + fileName); //small
                                image.Save(pathSmall); //Lưu ảnh trên server

                                //ảnh đại diện
                                image.Resize(191, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathThumb = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "t_" + fileName); //thumbnail
                                image.Save(pathThumb); //Lưu ảnh trên server
                            }
                        }
                        #endregion
                    }
                    #endregion

                    _db.tbl_ProductImage.Add(item);

                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult _FunXoaImage(Guid Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = _db.tbl_ProductImage.FirstOrDefault(p => p.Id == Id);

                    item.DateUpdate = DateTime.Now;
                    item.UserUpdate = User.UserId;
                    item.IsDelete = true;
                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        #region Phòng

        public ActionResult Rooms()
        {
            try
            {
                if (TempData["Notify"] != null)
                {
                    Notify notify = (Notify)TempData["Notify"];
                    ViewBag.Notify = notify;
                }

                ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", null);


                var viewModel = new List<RoomViewModel>();
                return View(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetRoomsList(Guid? HotelId)
        {
            //var hotel = _db.tbl_Hotel.Find(HotelId);

            var rooms = _db.tbl_Room.Where(x => x.IsDelete == false
            ).ToList();
            var viewModel = rooms.Select(x => new RoomViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                RoomName = x.RoomName,
                AvailableRoom = x.AvailableRoom,
                RoomArea = x.RoomArea,
                Direction = x.Direction,
                SingleBed = x.SingleBed,
                DoubleBed = x.DoubleBed,
                MaxPeople = x.MaxPeople,
                HotelConvenientType = x.HotelConvenientType,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId);

                if (hotel != null)
                    item.HotelName = hotel.HotelName;

            }

            return PartialView("_RoomsList", viewModel);
        }

        public ActionResult RoomsCreate(Guid hotelId)
        {
            var modelView = new RoomViewModel();



            ////dll Khach san
            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", null);

            //dll Hướng
            ViewBag.DirectionType = new SelectList(_db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList(), "Id", "DirectionName");

            //dll Tien ich
            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false && x.Type == 1).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            //dll Dich vu dinh kem

            var sllServiceType = new List<SelectListItem>();
            var dbModelServiceType = _db.tbl_HotelServiceType.Where(x => x.IsDelete == false).OrderBy(x => x.ServiceName).ToList();
            foreach (var item in dbModelServiceType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ServiceName;

                sllServiceType.Add(selectItem);
            }
            ViewBag.ddlServiceType = sllServiceType;

            ViewBag.UrlImage = "~/FileUploads/Image/default-image.jpg";

            modelView.HotelId = hotelId;

            return View(modelView);
        }

        [HttpPost]
        public ActionResult RoomsCreate(RoomViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null, int?[] sllServiceTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {
                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Room()
                    {
                        Id = Guid.NewGuid(),
                        HotelId = viewModel.HotelId,
                        RoomName = viewModel.RoomName,
                        AvailableRoom = viewModel.AvailableRoom,
                        RoomArea = viewModel.RoomArea,
                        Direction = viewModel.Direction,
                        SingleBed = viewModel.SingleBed,
                        DoubleBed = viewModel.DoubleBed,
                        MaxPeople = viewModel.MaxPeople,
                        Image = viewModel.Image,
                        IsDelete = false,
                        HotelConvenientType = sllConvenientTypeId,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemFile in files)
                    {
                        try
                        {
                            if (itemFile != null)
                            {
                                //upload file
                                string fileName = itemFile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemFile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }



                    _db.tbl_Room.Add(item);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    //return RedirectToAction("Rooms");
                    return RedirectToAction("HotelsEdit", new { id = item.HotelId });
                }
                catch
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            //ViewBag.CountryId = new SelectList(_db.tbl_countries.OrderBy(d => d.country_name).ToList(), "country_id", "country_name", model.CountryId);
            //ViewBag.ProvinceId = new SelectList(_db.tbl_provinces.Where(d => d.IsDelete == false).OrderBy(d => d.province_name).ToList(), "province_id", "province_name", model.ProvinceId);
            //ViewBag.Type = new SelectList(_db.tbl_hotel_level, "hotel_type", "hotel_type_name", model.Type);

            //ViewBag.Notify = notify;
            return View(viewModel);
            //return RedirectToAction("Hotels");
        }

        public ActionResult RoomsEdit(Guid id)
        {
            List<tbl_Room> model = new List<tbl_Room>();
            model = _db.tbl_Room.Where(x => x.Id == id).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }

            //Hotel viewModel = Mapper.MapFrom(tbl_hotel_restaurant);
            var viewModel = model.Select(x => new RoomViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                RoomName = x.RoomName,
                AvailableRoom = x.AvailableRoom,
                RoomArea = x.RoomArea,
                Direction = x.Direction,
                SingleBed = x.SingleBed,
                DoubleBed = x.DoubleBed,
                MaxPeople = x.MaxPeople,
                HotelConvenientType = x.HotelConvenientType,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).FirstOrDefault();


            //dll Hotel
            ViewBag.HotelId = new SelectList(
                    _db.tbl_Hotel.Where(d => d.IsDelete == false).OrderBy(d => d.HotelName).ToList(), "Id", "HotelName",
                    viewModel.HotelId);

            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", viewModel.HotelId); ;

            //dll Hướng
            ViewBag.DirectionType = new SelectList(_db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList(), "Id", "DirectionName", viewModel.Direction);

            //dll Tien ich
            string[] convenientTypeIds = { };
            if (viewModel.HotelConvenientType != "" && viewModel.HotelConvenientType != null)
            {
                convenientTypeIds = viewModel.HotelConvenientType.Split(new char[] { ',' });
            }

            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false && x.Type == 1).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                foreach (var check in convenientTypeIds)
                {
                    if (item.Id == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            //Seesion imageOld
            if (viewModel.Image != null)
            {
                TempData["ImageRoomOld"] = viewModel.Image.ToString();
            }
            else
            {
                TempData["ImageRoomOld"] = null;
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult RoomsEdit(RoomViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {

                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Room()
                    {
                        Id = viewModel.Id,
                        HotelId = viewModel.HotelId,
                        RoomName = viewModel.RoomName,
                        AvailableRoom = viewModel.AvailableRoom,
                        RoomArea = viewModel.RoomArea,
                        Direction = viewModel.Direction,
                        SingleBed = viewModel.SingleBed,
                        DoubleBed = viewModel.DoubleBed,
                        MaxPeople = viewModel.MaxPeople,
                        Image = viewModel.Image,
                        IsDelete = false,
                        HotelConvenientType = sllConvenientTypeId,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemfile in files)
                    {
                        try
                        {
                            if (itemfile != null)
                            {
                                //upload file
                                string fileName = itemfile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemfile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    //Xoa di anh cu trong Folder
                    string imgPath = UploadFolderFile + "\\" + (string)TempData["ImageHotelOld"];
                    //FileInfo fi = new FileInfo(imgPath);
                    FileInfo fi = new FileInfo(Server.MapPath(imgPath));
                    if (fi.Exists)
                    {
                        fi.Delete();
                    }

                    TempData["Notify"] = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    //return RedirectToAction("Rooms");
                    return RedirectToAction("HotelsEdit", new { id = item.HotelId });

                }
                catch (Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            ViewBag.Notify = notify;
            return RedirectToAction("Hotels"); ;
        }

        public ActionResult DeleteRoom(Guid id)
        {
            try
            {

                tbl_Room room = _db.tbl_Room.Find(id);

                room.IsDelete = true;
                _db.Entry(room).State = EntityState.Modified;
                _db.SaveChanges();

                TempData["Notify"] = Notify.NotifySuccess("Xóa thành công.");
                return RedirectToAction("HotelsEdit", new { id = room.HotelId });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region SaveImages Room
        public ActionResult _AddRoomImages(Guid Id, Guid HotelId)
        {
            ViewBag.HotelId = HotelId;
            ViewBag.RoomId = Id;
            ViewBag.Types = new SelectList(XMLUtils.BindData("imagetypehotel"), "value", "text", null);

            return PartialView();
        }

        public ActionResult _GetListRoomImage(Guid RoomId)
        {
            ViewBag.List = _db.tbl_ProductImage.Where(p => p.RoomId == RoomId && p.IsDelete == null || p.IsDelete == false).OrderBy(x => x.Order).ThenBy(x => x.DateCreate);

            return PartialView();
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult _FunSaveRoomImage(Guid RoomId, Guid HotelId, int? Order = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = new tbl_ProductImage()
                    {
                        Id = Guid.NewGuid(),
                        HotelId = HotelId,
                        RoomId = RoomId,
                        Type = 4,
                        Order = Order,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                    };

                    #region Upload hình
                    string fileName = "";

                    foreach (string file in Request.Files)
                    {
                        var postedFile = Request.Files[file];

                        string fileExtension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf("."));

                        string newName = Guid.NewGuid().ToString();

                        fileName = "img_" + DateTime.Now.ToString("ddMMyyyy") + newName + fileExtension;

                        #region Check file chính hay file phụ
                        if (file.Contains("ImageSlide_"))
                        {
                            postedFile.SaveAs(Server.MapPath(string.Format("~/{0}/", "FileUploads/Image/Hotels")) + fileName);
                            item.ImageUrl = fileName;

                            var image = WebImage.GetImageFromRequest(file);
                            if (image != null)
                            {
                                //resize ảnh small
                                image.Resize(1171, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathSmall = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "hotel_" + fileName); //small
                                image.Save(pathSmall); //Lưu ảnh trên server

                                //ảnh đại diện
                                image.Resize(191, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathThumb = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "t_" + fileName); //thumbnail
                                image.Save(pathThumb); //Lưu ảnh trên server
                            }
                        }
                        #endregion
                    }
                    #endregion

                    _db.tbl_ProductImage.Add(item);

                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult _FunXoaRoomImage(Guid Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = _db.tbl_ProductImage.FirstOrDefault(p => p.Id == Id);

                    item.DateUpdate = DateTime.Now;
                    item.UserUpdate = User.UserId;
                    item.IsDelete = true;
                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        #region bình luận
        public ActionResult HotelComments()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var model = HotelDA.HotelCommentSyn();

            var viewModel = model.Select(x => new HotelCommentAreaViewModel()
            {
                HotelId = x.Id,
                HotelName = x.HotelName,
                NumAll = x.NumAll,
                NumCongTac = x.NumCongTac,
                NumCapDoi = x.NumCapDoi,
                NumGiaDinh = x.NumGiaDinh,
                NumBanBe = x.NumBanBe,
                NumCaNhan = x.NumCaNhan,
                PointLocation = x.PointLocation,
                PointServe = x.PointServe,
                PointConvenient = x.PointConvenient,
                PointCost = x.PointCost,
                PointClean = x.PointClean,
                //PointAvg = x.PointAvg,
                //RankText = x.RankText
            }).ToList();

            foreach (var item in viewModel)
            {
                item.PointAvg = (item.PointClean + item.PointConvenient + item.PointCost + item.PointLocation + item.PointServe) / 5;

                if(item.NumAll == 0)
                {
                    item.RankText = "Chưa có đánh giá";
                }
                else
                {
                    if (item.PointAvg >= 0 && item.PointAvg < 5)
                    {
                        item.RankText = "Kém";
                    }
                    else if (item.PointAvg >= 5 && item.PointAvg < 7)
                    {
                        item.RankText = "Trung Bình";
                    }
                    else if (item.PointAvg >= 7 && item.PointAvg < 8)
                    {
                        item.RankText = "Tốt";
                    }
                    else if (item.PointAvg >= 8 && item.PointAvg < 9)
                    {
                        item.RankText = "Rẩt tốt";
                    }
                    else if (item.PointAvg >= 9 && item.PointAvg <= 10)
                    {
                        item.RankText = "Tuyệt vời";
                    }
                    else
                    {
                        item.RankText = "Đánh giá";
                    }
                }

            }

            //ViewBag.HotelComments = viewModel;

            return View(viewModel);
        }

        public ActionResult HotelCommentDetail(string hotelId)
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var idH = Guid.Parse(hotelId);

            var hotelItem = _db.tbl_Hotel.Where(d => d.Id == idH).FirstOrDefault();

            ViewBag.HotelName = hotelItem.HotelName;

            var model = HotelDA.HotelCommentByHotelId(hotelId);

            var viewModel = model.Select(x => new HotelCommentAdminViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                HotelName = x.HotelName,
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName,
                Content = x.Content,
                DateTimeComment = x.DateTimeComment,
                PointLocation = x.PointLocation,
                PointServe = x.PointServe,
                PointConvenient = x.PointConvenient,
                PointCost = x.PointCost,
                PointClean = x.PointClean,
                Type = x.Type,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                item.CustomerName = _db.tbl_Customer.Where(d => d.CustomerId == item.CustomerId).Select(d => d.CustomerName).FirstOrDefault();
                item.PointAvg = (item.PointClean + item.PointConvenient + item.PointCost + item.PointLocation + item.PointServe) / 5;
            }

            ViewBag.Comment = viewModel;

            //dll khach hang
            //ViewBag.CustommerList = new SelectList(_db.tbl_Customer.Where(x => x.IsDelete == false).OrderBy(x => x.CustomerName).ToList(), "CustomerId", "CustomerName - Email");

            //dll Khach hang
            //ViewBag.DirectionType = new SelectList(_db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList(), "Id", "DirectionName");
            var sllCustommer = new List<SelectListItem>();
            var dbModelCustommer = _db.tbl_Customer.Where(x => x.IsDelete == false).OrderBy(x => x.CustomerName).ToList();

            foreach (var item in dbModelCustommer)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.CustomerId.ToString();
                selectItem.Text = item.CustomerName + "-" + item.Email;

                sllCustommer.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.CustommerList = sllCustommer;

            return View(new tbl_HotelComment(){ 
                HotelId = hotelItem.Id}
            );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult HotelCommentsCreate(tbl_HotelComment model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Id = Guid.NewGuid();
                    model.DateTimeComment = DateTime.Now;
                    model.DateCreate = DateTime.Now;
                    model.UserCreate = User.UserId;
                    model.IsDelete = false;

                    _db.tbl_HotelComment.Add(model);
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("HotelCommentDetail", new { hotelId = model.HotelId });
                }
                catch (Exception e)
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("HotelComments");
                }
            }

            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("HotelComments");
        }

        public ActionResult _HotelCommentsEdit(Guid id)
        {
            tbl_HotelComment hotelComment = _db.tbl_HotelComment.FirstOrDefault(d => d.Id == id);
            if (hotelComment == null)
            {
                return HttpNotFound();
            }
            var sllCustommer = new List<SelectListItem>();
            var dbModelCustommer = _db.tbl_Customer.Where(x => x.IsDelete == false).OrderBy(x => x.CustomerName).ToList();

            foreach (var item in dbModelCustommer)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.CustomerId.ToString();
                selectItem.Text = item.CustomerName + "-" + item.Email;

                if (item.CustomerId == hotelComment.CustomerId)
                {
                    selectItem.Selected = true;
                }

                sllCustommer.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.CustommerList = sllCustommer;

            return PartialView(hotelComment);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HotelCommentsEdit(tbl_HotelComment model, string ContentEdit)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    tbl_HotelComment oldItem = _db.tbl_HotelComment.FirstOrDefault(t => t.Id == model.Id);
                    oldItem.CustomerId = model.CustomerId;
                    oldItem.Content = ContentEdit;
                    //oldItem.DateTimeComment = model.DateTimeComment;
                    oldItem.PointLocation = model.PointLocation;
                    oldItem.PointServe = model.PointServe;
                    oldItem.PointConvenient = model.PointConvenient;
                    oldItem.PointCost = model.PointCost;
                    oldItem.PointClean = model.PointClean;
                    oldItem.Type = model.Type;
                    oldItem.DateUpdate = DateTime.Now;
                    oldItem.UserUpdate = User.UserId;
                    
                    _db.Entry(oldItem).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("HotelCommentDetail", new { hotelId = oldItem.HotelId });
                }
                catch (Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("HotelComments");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("HotelComments");
        }

        [HttpPost]
        public JsonResult HotelCommentsDelete(Guid id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelComment.FirstOrDefault(d => d.Id == id);
                model.IsDelete = true;

                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }
        #endregion

        #endregion

        #region Thanh toán
        #region Loại thanh toán

        public ActionResult PaymentTypes()
    {
        if (TempData["Notify"] != null)
        {
            Notify notify = (Notify)TempData["Notify"];
            ViewBag.Notify = notify;
        }

        var applications = _db.tbl_PaymentType.ToList();
        ViewBag.PaymentTypes = applications;

        return View(new tbl_PaymentType());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult PaymentTypesCreate(tbl_PaymentType model)
    {
        Notify notify;
        if (ModelState.IsValid)
        {
            try
            {
                int count = 1;
                var types = _db.tbl_PaymentType.ToList();
                if (types != null && types.Count() > 0)
                    count = types.Max(d => d.Id) + 1;

                model.Id = byte.Parse(count.ToString());
                model.IsDelete = false;
                _db.tbl_PaymentType.Add(model);
                _db.SaveChanges();

                notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                TempData["Notify"] = notify;

                return RedirectToAction("PaymentTypes");
            }
            catch (Exception e)
            {
                notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                TempData["Notify"] = notify;
                return RedirectToAction("PaymentTypes");
            }
        }

        notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
        TempData["Notify"] = notify;

        return RedirectToAction("PaymentTypes");
    }

    public ActionResult _PaymentTypesEdit(int id = 0)
    {
        tbl_PaymentType paymentType = _db.tbl_PaymentType.FirstOrDefault(d => d.Id == id);
        if (paymentType == null)
        {
            return HttpNotFound();
        }
        return PartialView(paymentType);
    }

    [HttpPost]
    public ActionResult PaymentTypesEdit(tbl_PaymentType model)
    {
        Notify notify;
        if (ModelState.IsValid)
        {
            try
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();

                notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                TempData["Notify"] = notify;

                return RedirectToAction("PaymentTypes");
            }
            catch
            {
                notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                TempData["Notify"] = notify;
                return RedirectToAction("PaymentTypes");
            }
        }
        notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
        TempData["Notify"] = notify;

        return RedirectToAction("PaymentTypes");
    }

    [HttpPost]
    public JsonResult PaymentTypesDelete(int id)
    {
        bool check;
        try
        {
            var model = _db.tbl_PaymentType.FirstOrDefault(d => d.Id == id);
            _db.tbl_PaymentType.Remove(model);
            _db.SaveChanges();

            check = true;
        }
        catch
        {
            check = false;
        }
        return Json(check);
    }

        #endregion
        #endregion

        #region hướng tbl_Direction

        public ActionResult Direction()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            var direction = _db.tbl_Direction.OrderBy(d => d.DirectionName).ToList();
            ViewBag.Direction = direction;

            return View(new tbl_Direction());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DirectionCreate(tbl_Direction model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //model.DistrictId = Guid.NewGuid();
                    _db.tbl_Direction.Add(model);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    return RedirectToAction("Direction");
                }
                catch
                {
                    TempData["Notify"] = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");

                    return RedirectToAction("Direction");
                }
            }

            TempData["Notify"] = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            return RedirectToAction("Direction");
        }

        public ActionResult _DirectionEdit(int id)
        {
            var direction = _db.tbl_Direction.FirstOrDefault(d => d.Id == id);

            return PartialView(direction);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DirectionEdit(tbl_Direction model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("Direction");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("Direction");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("Direction");
        }

        [HttpPost]
        public JsonResult DirectionDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_Direction.FirstOrDefault(d => d.Id == id);
                _db.tbl_Direction.Remove(model);
                //_db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        [HttpGet]
        public JsonResult _GetDirection(int provinceid)
        {
            List<SelectListItem> sllDirection = new List<SelectListItem>();
            sllDirection.Add(new SelectListItem { Value = "0", Text = "---   Chọn hướng   ---" });
            var direction = _db.tbl_Direction.OrderBy(d => d.DirectionName).ToList();
            foreach (var item in direction)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.Id.ToString();
                sllItem.Text = item.DirectionName;
                sllDirection.Add(sllItem);
            }
            return Json(sllDirection, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Tin tức

        #region Danh mục tin tức

        public ActionResult NewsCategory()
        {
            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            ViewBag.NewsCategories = _db.NewsCategories.ToList();            
            return View(new NewsCategory());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewsCategoryCreate(NewsCategory model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    model.CategoryID = _db.NewsCategories.Max(d => d.CategoryID) + 1;
                    _db.NewsCategories.Add(model);
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("NewsCategory");
                }
                catch (Exception e)
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("NewsCategory");
                }
            }

            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("NewsCategory");
        }

        public ActionResult _NewsCategoryEdit(int id)
        {
            NewsCategory newsCategory = _db.NewsCategories.FirstOrDefault(d => d.CategoryID == id);
            if (newsCategory == null)
            {
                return HttpNotFound();
            }
            return PartialView(newsCategory);
        }

        [HttpPost]
        public ActionResult NewsCategoryEdit(NewsCategory model)
        {
            Notify notify;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    TempData["Notify"] = notify;

                    return RedirectToAction("NewsCategory");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    TempData["Notify"] = notify;
                    return RedirectToAction("NewsCategory");
                }
            }
            notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            TempData["Notify"] = notify;

            return RedirectToAction("NewsCategory");
        }

        [HttpPost]
        public JsonResult NewsCategoryDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.NewsCategories.FirstOrDefault(d => d.CategoryID == id);
                _db.NewsCategories.Remove(model);
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        #endregion

        #endregion
    }
}