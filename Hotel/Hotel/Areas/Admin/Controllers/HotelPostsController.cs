﻿using Hotel.EntityFramework.Models;
using Hotel.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Custom.Security;
using System.Data.Entity;
using System.Net;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles ="Admin")]
    public class HotelPostsController : BaseController
    {
        private readonly HotelEntities db = new HotelEntities();
        private const string UploadFolderFile = "~/FileUploads/News";

        private Notify notify = new Notify();
        // GET: Admin/HotelPosts
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            LoadDataDropDownList(int.MinValue);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(HotelNews hotelNew, List<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                hotelNew.HotelNewsID = Guid.NewGuid();
                hotelNew.CrtBy = User.UserId.ToString();
                hotelNew.CrtDate = DateTime.Now;
                hotelNew.CompID = 1;
                hotelNew.NewsTypeID = 2;
                //Update attach files                    
                foreach (HttpPostedFileBase item in files)
                {
                    try
                    {
                        if (item != null)
                        {
                            //upload file
                            string fileName = item.FileName;
                            string extension = Path.GetExtension(fileName);

                            if (Utilities.Utility.AllowedExtensions.Contains(extension))
                            {
                                string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                hotelNew.FileAttchment = item.SaveIn(UploadFolderFile, fileName);
                            }
                        }
                    }
                    catch { }
                }

                db.HotelNews.Add(hotelNew);
                db.SaveChanges();
                return RedirectToAction("Index", "HotelNews");
            }

            LoadDataDropDownList(hotelNew.NewsTypeID);
            return View(hotelNew);
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelNews hotelNew = db.HotelNews.Find(id);
            if (hotelNew == null)
            {
                return HttpNotFound();
            }

            LoadDataDropDownList(hotelNew.NewsTypeID);

            return View(hotelNew);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(HotelNews hotelNew, List<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                var news = db.HotelNews.SingleOrDefault(n => n.HotelNewsID == hotelNew.HotelNewsID);

                //Update attach files                    
                foreach (HttpPostedFileBase item in files)
                {
                    try
                    {
                        if (item != null)
                        {
                            //upload file
                            string fileName = item.FileName;
                            string extension = Path.GetExtension(fileName);

                            if (Utilities.Utility.AllowedExtensions.Contains(extension))
                            {
                                string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                hotelNew.FileAttchment = item.SaveIn(UploadFolderFile, fileName);
                            }
                        }
                    }
                    catch { }
                }

                if (string.IsNullOrEmpty(hotelNew.FileAttchment))
                {
                    hotelNew.FileAttchment = news.FileAttchment;
                }

                hotelNew.CrtBy = news.CrtBy;
                hotelNew.CrtDate = news.CrtDate;
                hotelNew.CompID = news.CompID;
                hotelNew.UpdBy = User.UserId.ToString();
                hotelNew.UpdDate = DateTime.Now;

                db.Entry(news).State = EntityState.Detached;
                db.Entry(hotelNew).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","HotelNews");
            }
            else
            {
                //var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                //notify = Notify.NotifyDanger(errors.ToString());
                //ViewBag.Notify = notify;
            }

            LoadDataDropDownList(hotelNew.NewsTypeID);
            return View(hotelNew);
        }


        protected void LoadData()
        {

        }

        protected void LoadDataDropDownList(int? NewsTypeID)
        {
            ViewBag.NewsTypeID = db.NewsTypes.Where(n => n.Status == true).Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.NewsTypeID.ToString(),
                Selected = s.NewsTypeID == NewsTypeID ? true : false
            }).ToList<SelectListItem>();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}