﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.EntityFramework.Models;
using Admin.Helpers;
using System.Net.Mail;
using Custom.Security;
using Hotel.Models;
using Aspose.Cells;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class ReportController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult BaoCaoDanhSachBookingBySearch()
        {
            List<BookingReportViewModel> models = new List<BookingReportViewModel>();
            var search = new SearchBookingViewModel();
            try
            {
                if (Session["SearchBooking"] != null)
                {
                    search = (SearchBookingViewModel)Session["SearchBooking"];
                }
            }
            catch (Exception)
            {
            }
            
            var query = _db.tbl_Booking.Select(d => d);

            query = GetQueryBooking(query, search);

            var booking = query.OrderBy(d => d.CheckIn).ThenBy(d => d.DateCreate).Take(5000).ToList();

            //Get bookingViewModel
            foreach (var item in booking)
            {
                var bkReport = new BookingReportViewModel();
                bkReport.Init(item);
                bkReport.HotelName = _db.tbl_Hotel.Where(d => d.Id == item.HotelId).Select(d => d.HotelName).FirstOrDefault();
                bkReport.RoomName = _db.tbl_Room.Where(d => d.Id == item.RoomId).Select(d => d.RoomName).FirstOrDefault();
                bkReport.UserConfirmName = _db.tbl_User.Where(d => d.UserId == bkReport.UserConfirm).Select(d => d.LoginName).FirstOrDefault();
                bkReport.UserAlloted = _db.tbl_BookingAllot.Where(d => d.BookingId == item.BookingId).Select(d => d.UserId).FirstOrDefault();
                bkReport.UserAllotedName = bkReport.UserAlloted != null ?_db.tbl_User.Where(d => d.UserId == bkReport.UserAlloted).Select(d => d.LoginName).FirstOrDefault() : "-";

                models.Add(bkReport);
            }

            if (Request.HttpMethod == "POST")
            {
                string FileName = InitExcelDanhSachBooking(models);

                if (!string.IsNullOrEmpty(FileName))
                    return Redirect("/FileUploads/Reports/" + FileName);
            }

            ViewBag.Search = search;
            return PartialView(models);
        }


        private string InitExcelDanhSachBooking(List<BookingReportViewModel> models)
        {
            try
            {
                string fileIn = Request.PhysicalApplicationPath + @"FileUploads\Template\Report\DanhSachBooking.xls";

                //Init Workbook
                Workbook workbook = new Workbook(fileIn);
                //workbook.IsHScrollBarVisible = true;
                //workbook.IsVScrollBarVisible = true;

                Worksheet worksheet = workbook.Worksheets[0];
                Cells cells = worksheet.Cells;
                //cells[5, 0].PutValue(tour.destination);
                
                //cells[10, 5].PutValue(tour.arrivalofend_date.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm"));

                int irow = 3;
                int j = 0, stt = 1;
                worksheet.Cells.InsertRows(irow + 1, models.Count() - 1);

                foreach (var item in models)
                {
                    string status = "";
                    cells[irow, j++].PutValue(stt++);
                    cells[irow, j++].PutValue(item.CustomerName);
                    cells[irow, j++].PutValue(item.CustomerPhone);
                    cells[irow, j++].PutValue(item.HotelName);
                    cells[irow, j++].PutValue(item.RoomName);
                    cells[irow, j++].PutValue(item.RoomCount);
                    cells[irow, j++].PutValue(item.CountDate);
                    cells[irow, j++].PutValue(item.DateCheckIn.ToString("dd/MM/yyyy"));
                    cells[irow, j++].PutValue(item.DateCheckOut.ToString("dd/MM/yyyy"));
                    cells[irow, j++].PutValue(item.TotalAmount);

                    if (item.Status == (int)Parameters.Booking.BookingStatus.DaHuy)
                    {
                        status = "Đã huỷ";
                    }
                    else
                    {
                        status = item.IsPaymentSuccess ? "Đã thanh toán" : (item.CustomerCheckPayment ? "KH đã xác nhận" : "Chưa thanh toán");
                    }
                                                    
                    cells[irow, j++].PutValue(status);
                    cells[irow, j++].PutValue(item.UserConfirmName ?? item.UserAllotedName);                    
                    
                    irow++;
                    j = 0;
                }

                string FileName = "DSBooking_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls";
                string fileOut = Request.PhysicalApplicationPath + @"FileUploads\Reports\" + FileName;

                //workbook.Save(fileOut, FileFormatType.Excel2007Xlsx);
                workbook.Save(fileOut);

                return FileName;
            }
            catch(Exception ex)
            {
                return "";
            }
        }
        #region Private
        private IQueryable<tbl_Booking> GetQueryBooking(IQueryable<tbl_Booking> query, SearchBookingViewModel search)
        {
            DateTime now = DateTime.Now.Date;
            //IQueryable<tbl_Booking> query = Enumerable.Empty<tbl_Booking>().AsQueryable();
            if (!string.IsNullOrEmpty(search.BookingCode))
            {
                query = query.Where(m => m.BookingCode == search.BookingCode.ToUpper());
            }

            if (!string.IsNullOrEmpty(search.HotelName))
            {
                var hotelId = _db.tbl_Hotel.Where(d => d.HotelName.ToLower().Contains(search.HotelName.ToLower())).Select(d => d.Id).FirstOrDefault();
                if (hotelId != null)
                {
                    query = query.Where(m => m.HotelId == hotelId);
                }
            }
            if (search.UserAlloted != null && search.UserAlloted != Guid.Empty)
            {
                query = query.Join(_db.tbl_BookingAllot.Where(d => d.UserId == search.UserAlloted),
                    b => b.BookingId,
                    a => a.BookingId,
                     (b, a) => b);
            }
            if (!string.IsNullOrEmpty(search.CustomerName))
            {
                query = query.Where(m => m.CustomerName.ToLower().Contains(search.CustomerName.ToLower()));
            }
            if (!string.IsNullOrEmpty(search.CustomerEmail))
            {
                query = query.Where(m => m.CustomerEmail.ToLower().Contains(search.CustomerEmail.ToLower()));
            }
            if (!string.IsNullOrEmpty(search.CustomerPhone))
            {
                query = query.Where(m => m.CustomerPhoneNumber.Contains(search.CustomerPhone));
            }
            if (search.FromDate != null)
            {
                query = query.Where(m => m.CheckIn >= search.FromDate.Value);
            }
            if (search.ToDate != null)
            {
                query = query.Where(m => m.CheckIn <= search.ToDate.Value);
            }
            if (search.FromDate == null && search.ToDate == null)
            {
                query = query.Where(m => m.CheckOut >= now && !m.IsDelete);
            }
            if (search.Status != -1)
            {
                switch (search.Status)
                {
                    case (int)Parameters.Booking.BookingStatus.ChuaPhanBo:
                        var bookingList = query.Select(d => d.BookingId).ToList();
                        var exceptionList = _db.tbl_BookingAllot.Where(d => bookingList.Contains(d.BookingId) && !d.IsDelete)
                            .Select(d => d.BookingId).ToList();
                        query = query.Where(d => !exceptionList.Contains(d.BookingId));
                        break;
                    case (int)Parameters.Booking.BookingStatus.KHDaThanhToan:
                        query = query.Where(m => m.CustomerCheckPayment == true);
                        break;
                    case (int)Parameters.Booking.BookingStatus.AdminXNThanhToan:
                        query = query.Where(m => m.IsPaymentSuccess == true);
                        break;
                    case (int)Parameters.Booking.BookingStatus.DaHuy:
                        query = query.Where(m => m.CustomerCheckPayment == true);
                        break;

                    default:
                        break;
                }
            }
            return query;
        }
        #endregion
    }
}