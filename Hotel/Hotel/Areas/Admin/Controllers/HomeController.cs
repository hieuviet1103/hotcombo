﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.EntityFramework.Models;
using Admin.Helpers;
using System.Net.Mail;
using Custom.Security;
using Hotel.Models;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index","Hotels");
        }

        [HttpPost]
        public JsonResult SentEmail(SentCommunication model)
        {
            MailControl mailControl = new MailControl();
            Email mail = new Email();
            mail.To = model.MailTo;
            mail.Body = model.Body;
            mail.Subject = model.Subject;
            mailControl.Send(mail);
            mailControl.Send(mail, "hieuviet.doc@gmail.com", "01685460097", "hieuviet.doc@gmail.com", "hieuviet.doc@gmail.com");
            var send = new System.Net.Mail.MailMessage("hieuviet.doc@gmail.com", "hieuviet.1103@gmail.com", "Subject", "Body");            
            return null;
        }
    }
}