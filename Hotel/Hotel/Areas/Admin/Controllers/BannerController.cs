﻿using Hotel.Models;
using Hotel.EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.Utilities;
using System.Data.Entity;
using Custom.Security;
using System.IO;
using System.Web.Mvc.Html;
using System.Net;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.Web.Helpers;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class BannerController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        private const string UploadFolderFile = "~/FileUploads/Image";
        // GET: Admin/Banner
       public ActionResult Index(string banner_pos = "", string active = "", string banner_name = "")
        {
            var list = _db.tbl_banner.ToList();
            if (banner_pos != "")
            {
                list = list.Where(m => m.banner_pos == Convert.ToByte(banner_pos)).ToList();
            }
            if (active != "")
            {
                list = list.Where(m => m.active == Convert.ToByte(active)).ToList();
            }
            if (banner_name != "")
            {
                list = list.Where(m => m.banner_name.ToLower().Contains(banner_name.ToLower())).ToList();
            }
            ViewBag.sllPosition = new SelectList(_db.tbl_Banner_Position.ToList(), "Id", "Name").ToList();
            return View(list);
        }

        //
        // GET: /Admin/Banner/Create

        public ActionResult Create()
        {
            ViewBag.sllPosition = new SelectList(_db.tbl_Banner_Position.ToList(), "Id", "Name").ToList();
            return View();
        }

        //
        // POST: /Admin/Banner/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_banner model)
        {
            try
            {
                var fileUpload = Request.Files["uplImage"];
                if (fileUpload.HasFile())
                {
                    string fileName = "bn_" + DateTime.Now.ToString("yyMMdd") + "_" + fileUpload.FileName;
                    fileUpload.SaveIn("~/FileUploads/Banner/", fileName);
                    model.banner_src = fileName;
                }

                if (ModelState.IsValid)
                {
                    _db.tbl_banner.Add(model);
                    _db.SaveChanges();
                    return Json(new { status = 1, message = "Thêm mới thành công" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Thêm mới thất bại" });
            }
            return null;
        }

        //
        // GET: /Admin/Banner/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tbl_banner model = _db.tbl_banner.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }

            ViewBag.sllPosition = new SelectList(_db.tbl_Banner_Position.ToList(), "Id", "Name").ToList();
            return View(model);
        }

        //
        // POST: /Admin/Banner/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_banner model)
        {
            var fileUpload = Request.Files["uplImage"];
            if (fileUpload.HasFile())
            {
                string fileName = "bn_" + DateTime.Now.ToString("yyMMdd") + "_" + fileUpload.FileName;
                fileUpload.SaveIn("~/FileUploads/Banner/", fileName);
                model.banner_src = fileName;
            }

            //if (ModelState.IsValid)
            try
            {
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
                //ViewBag.Status = "Success";
                return Json(new { status = 1, message = "Cập nhật thành công" });
            }
            catch (Exception ex)
            {
                //ViewBag.Status = "Error";
                //ViewBag.Message = "Vui lòng kiểm tra dữ liệu đầu vào!";
                return Json(new { status = 0, message = "Cập nhật thất bại" });
            }

            //ViewBag.sllPosition = new SelectList(_db.tbl_Banner_Position.ToList(), "Id", "Name").ToList();
            //return View(model);
        }

        public ActionResult DeleteBanner(int id)
        {
            tbl_banner model = _db.tbl_banner.Find(id);
            _db.tbl_banner.Remove(model);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}