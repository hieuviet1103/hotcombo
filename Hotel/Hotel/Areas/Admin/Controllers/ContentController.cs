﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Custom.Security;
using Hotel.Models;
using Hotel.Controllers;
using Hotel.EntityFramework.Models;


namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class ContentController : BaseController
    {
        private HotelEntities db = new HotelEntities();
        int RecordsPerPage = 15;
        // GET: Admin/Content
        public ActionResult Index(int page = 1, string ContenCode = "", string ContentName = "")
        {
            var list = db.tbl_Content.ToList();
            if (ContenCode != "")
            {
                list = list.Where(m => m.ContenCode.Contains(ContenCode)).ToList();
            }
            if (ContentName != "")
            {
                list = list.Where(m => m.ContentName.ToLower().Contains(ContentName.ToLower())).ToList();
            }

            //Init Pager
            Pager pager = new Pager();
            pager.PageUrlFormat = "";
            pager.TotalRecords = list.ToList().Count;
            pager.TotalRecordsPerPage = RecordsPerPage;
            pager.TotalSlots = 10;
            pager.CurrentPage = page;
            pager.CssClass = "pager_simple_orange";
            pager.CssClassCurrentPage = "active";
            pager.FirstPageDisplayText = "<<";
            pager.LastPageDisplayText = ">>";

            ViewBag.Pager = pager.GetHtml();
            list = list.Skip((page - 1) * RecordsPerPage).Take(RecordsPerPage).ToList();

            return View(list);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_Content tbl_content)
        {
            if (ModelState.IsValid)
            {
                tbl_content.DateCreate = DateTime.Now;
                tbl_content.DateUpdate = DateTime.Now;
                db.tbl_Content.Add(tbl_content);
                db.SaveChanges();
                return RedirectToAction("Index");
                //return Content("Thêm mới thành công");
            }

            return View(tbl_content);
        }
        public ActionResult Edit(int id = 0)
        {
            tbl_Content tbl_content = db.tbl_Content.Find(id);
            if (tbl_content == null)
            {
                return HttpNotFound();
            }
            return View(tbl_content);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_Content tbl_content)
        {
            if (ModelState.IsValid)
            {
                tbl_content.DateUpdate = DateTime.Now;
                db.Entry(tbl_content).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
                //return Content("Cập nhật thành công");
            }
            return View(tbl_content);
        }


        public ActionResult Delete(int id)
        {
            tbl_Content tbl_content = db.tbl_Content.Find(id);
            db.tbl_Content.Remove(tbl_content);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}