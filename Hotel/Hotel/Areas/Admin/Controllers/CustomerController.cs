﻿using Custom.Security;
using Hotel.EntityFramework.Models;
using Hotel.Models;
using Hotel.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hotel.Areas.Admin.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        // GET: Admin/Customer
        public ActionResult Index()
        {
            try
            {
                if (TempData["Notify"] != null)
                {
                    Notify notify = (Notify)TempData["Notify"];
                    ViewBag.Notify = notify;
                }
                ViewBag.Status = new SelectList(XMLUtils.BindData("status"), "value", "text", null);
                var viewModel = new List<CustomerAdminViewModel>();
                return View(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetCustomerList(string CustomerName = "", string Email = "", string PhoneNumber = "", int? Status = null)
        {
            bool statusRep = false;
            if (Status == 1)
            {
                statusRep = true;
            }

            var hotels = _db.tbl_Customer.Where(x => x.IsDelete == false
            && (CustomerName == "" || x.CustomerName.ToLower().Contains(CustomerName.ToLower()))
            && (Email == "" || x.Email.ToLower().Contains(Email.ToLower()))
            && (PhoneNumber == "" || x.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()))
            && (Status == null || x.IsActive == statusRep)
            ).ToList();
            var viewModel = hotels.Select(x => new CustomerAdminViewModel()
            {
                CustomerId = x.CustomerId,
                CustomerNo = x.CustomerNo,
                IsActive = x.IsActive,
                CustomerName = x.CustomerName,
                Dob = x.Dob,
                Gender = x.Gender,
                Nationality = x.Nationality,
                IdCard = x.IdCard,
                DateOfIssue = x.DateOfIssue,
                PlaceOfIssue = x.PlaceOfIssue,
                Address = x.Address,
                CountryId = x.CountryId,
                ProvinceId = x.ProvinceId,
                DistrictId = x.DistrictId,
                PhoneNumber = x.PhoneNumber,
                Email = x.Email,
                Note = x.Note,
                Password = x.Password,
                TotalPoint = x.TotalPoint,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                var CountryIdtxt = item.CountryId.ToString();
                var country = _db.tbl_Country.FirstOrDefault(d => d.CountryId == item.CountryId);
                var province = _db.tbl_Province.FirstOrDefault(d => d.ProvinceId == item.ProvinceId);
                var district = _db.tbl_District.FirstOrDefault(d => d.DistrictId == item.DistrictId);

                if (country != null)
                    item.CountryName = country.CountryName;

                if (province != null)
                    item.ProvinceName = province.ProvinceName;

                if (district != null)
                    item.ProvinceName = district.DistrictName;
            }

            return PartialView("_CustomerList", viewModel);
        }

        public ActionResult Edit(Guid id)
        {
            List<tbl_Customer> model = new List<tbl_Customer>();
            model = _db.tbl_Customer.Where(x => x.CustomerId == id).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }


            //Hotel viewModel = Mapper.MapFrom(tbl_hotel_restaurant);
            var viewModel = model.Select(x => new CustomerAdminViewModel()
            {
                CustomerId = x.CustomerId,
                CustomerNo = x.CustomerNo,
                IsActive = x.IsActive,
                IsActiveInt = x.IsActive == true ? 1 : 0,
                CustomerName = x.CustomerName,
                Dob = x.Dob,
                Gender = x.Gender,
                Nationality = x.Nationality,
                IdCard = x.IdCard,
                DateOfIssue = x.DateOfIssue,
                PlaceOfIssue = x.PlaceOfIssue,
                Address = x.Address,
                CountryId = x.CountryId,
                ProvinceId = x.ProvinceId,
                DistrictId = x.DistrictId,
                PhoneNumber = x.PhoneNumber,
                Email = x.Email,
                Note = x.Note,
                Password = x.Password,
                TotalPoint = x.TotalPoint,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).FirstOrDefault();

         
            ////dll Quoc gia
            //ViewBag.CountryId = new SelectList(_db.tbl_Country.OrderBy(d => d.CountryName).ToList(), "CountryId", "CountryName", viewModel.CountryId);

            ////dll Tinh Thanh
            //ViewBag.ProvinceId = new SelectList(_db.tbl_Province.Where(d => d.IsDelete == false && d.CountryId == viewModel.CountryId.ToString()).OrderBy(d => d.ProvinceName).ToList(), "ProvinceId", "ProvinceName", viewModel.ProvinceId);

            ////dll Quan Huyen
            //ViewBag.DistrictId = new SelectList(_db.tbl_District.Where(d => d.IsDelete == false && d.ProvinceId == viewModel.ProvinceId).OrderBy(d => d.DistrictName).ToList(), "DistrictId", "DistrictName", viewModel.DistrictId);

            ViewBag.ddlStatus = new SelectList(XMLUtils.BindData("status"), "value", "text", null);
            ViewBag.ddlGender = new SelectList(XMLUtils.BindData("gender"), "value", "text", null);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(CustomerAdminViewModel viewModel)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {
                    if(viewModel.IsActiveInt == 1)
                    {
                        viewModel.IsActive = true;
                    }
                    else
                    {
                        viewModel.IsActive = false;
                    }
                    var item = new tbl_Customer()
                    {
                        CustomerId = viewModel.CustomerId,
                        CustomerNo = viewModel.CustomerNo,
                        IsActive = viewModel.IsActive,
                        CustomerName = viewModel.CustomerName,
                        Dob = viewModel.Dob,
                        Gender = viewModel.Gender,
                        Nationality = viewModel.Nationality,
                        IdCard = viewModel.IdCard,
                        DateOfIssue = viewModel.DateOfIssue,
                        PlaceOfIssue = viewModel.PlaceOfIssue,
                        Address = viewModel.Address,
                        CountryId = viewModel.CountryId,
                        ProvinceId = viewModel.ProvinceId,
                        DistrictId = viewModel.DistrictId,
                        PhoneNumber = viewModel.PhoneNumber,
                        Email = viewModel.Email,
                        Note = viewModel.Note,
                        Password = viewModel.Password,
                        TotalPoint = viewModel.TotalPoint,
                        IsDelete = viewModel.IsDelete,
                        UserCreate = viewModel.UserCreate,
                        DateCreate = viewModel.DateCreate,
                        UserUpdate = viewModel.UserUpdate,
                        DateUpdate = viewModel.DateUpdate
                    };

                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            ViewBag.Notify = notify;
            return RedirectToAction("Index"); ;
        }
    }
}