﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Hotel.Models;
using Custom.Security;
using Hotel.EntityFramework.Models;
using Hotel.Utilities;
using System.IO;
using System.Net;
using System.Data.Entity;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class HotelNewsController : BaseController
    {
        private readonly HotelEntities db = new HotelEntities();
        // GET: Admin/Articles

        private const string UploadFolderFile = "~/FileUploads/News";
        
        private Notify notify = new Notify();

        public ActionResult Index()
        {
            NewsListViewModel viewModel = new NewsListViewModel();

            LoadDataDropDownList(int.MinValue);

            viewModel.News = db.HotelNews.OrderByDescending(n => n.NewsTypeID).ToList();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(NewsListViewModel viewModel, int page = 1)
        {
            var NewsList = (from n in db.HotelNews
                            where (viewModel.CrtDateFrom <= n.CrtDate && n.CrtDate <= viewModel.CrtDateTo)
                                && (n.NewsTypeID == viewModel.NewsTypeID || viewModel.NewsTypeID == 0)
                                && (n.Title.Contains(viewModel.Title) || String.IsNullOrEmpty(viewModel.Title))
                                && (n.Source.Contains(viewModel.Source) || String.IsNullOrEmpty(viewModel.Source))
                            select n
                ).OrderByDescending(n => n.OrderID).ToList();

            viewModel.News = NewsList;

            LoadDataDropDownList(int.MinValue);

            return View(viewModel);
        }
        public ActionResult Details(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelNews hotelNews = db.HotelNews.Find(id);
            if (hotelNews == null)
            {
                return HttpNotFound();
            }
            return View(hotelNews);
        }

        // GET: News/Create
        public ActionResult Create()
        {
            LoadDataDropDownList(int.MinValue);
            //dll Tinh Thanh
            ViewBag.ProvinceId = new SelectList(db.tbl_Province.Where(d => d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList(), "ProvinceId", "ProvinceName", null);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(HotelNews hotelNew, List<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                hotelNew.HotelNewsID = Guid.NewGuid();
                
                hotelNew.CrtBy = User.UserId.ToString() ;
                hotelNew.CrtDate = DateTime.Now;
                hotelNew.CompID = 1;

                //Update attach files                    
                foreach (HttpPostedFileBase item in files)
                {
                    try
                    {
                        if (item != null)
                        {
                            //upload file
                            string fileName = item.FileName;
                            string extension = Path.GetExtension(fileName);

                            if (Utilities.Utility.AllowedExtensions.Contains(extension))
                            {
                                string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                hotelNew.FileAttchment = item.SaveIn(UploadFolderFile, fileName);
                            }
                        }
                    }
                    catch { }
                }

                db.HotelNews.Add(hotelNew);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            LoadDataDropDownList(hotelNew.NewsTypeID);
            return View(hotelNew);
        }

        // GET: WHSafeNews/Edit/5
        public ActionResult Edit(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelNews hotelNew = db.HotelNews.Find(id);

            //dll Tinh Thanh
            var myListItem = new SelectList(db.tbl_Province.Where(d => d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList(), "ProvinceId", "ProvinceName" , hotelNew.ProvinceID);
         
            ViewBag.TinhThanh = myListItem;

            if (hotelNew == null)
            {
                return HttpNotFound();
            }

            LoadDataDropDownList(hotelNew.NewsTypeID);

            //ddlCategory
            List<SelectListItem> ddlNewsCategories = new List<SelectListItem>();
            ddlNewsCategories = db.NewsCategories.Where(d => d.IsActive).Select(d => new SelectListItem
            {
                Value = d.CategoryID.ToString(),
                Text = d.CategoryName.ToString(),
                Selected = db.NewsAndCategories.Any(m => m.NewsId == id && m.CategoryId == d.CategoryID)
            }).ToList();

            ViewBag.ddlNewsCategories = ddlNewsCategories;

            return View(hotelNew);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(HotelNews hotelNew, List<HttpPostedFileBase> files, List<int> NewsCategories)
        {
            if (ModelState.IsValid)
            {
                var news = db.HotelNews.SingleOrDefault(n => n.HotelNewsID == hotelNew.HotelNewsID);

                //Update attach files                    
                foreach (HttpPostedFileBase item in files)
                {
                    try
                    {
                        if (item != null)
                        {
                            //upload file
                            string fileName = item.FileName;
                            string extension = Path.GetExtension(fileName);

                            if (Utilities.Utility.AllowedExtensions.Contains(extension))
                            {
                                string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                hotelNew.FileAttchment = item.SaveIn(UploadFolderFile, fileName);
                            }
                        }
                    }
                    catch { }
                }

                if (string.IsNullOrEmpty(hotelNew.FileAttchment))
                {
                    hotelNew.FileAttchment = news.FileAttchment;
                }

                hotelNew.CrtBy = news.CrtBy;
                hotelNew.CrtDate = news.CrtDate;
                hotelNew.CompID = news.CompID;
                hotelNew.UpdBy = User.UserId.ToString();
                hotelNew.UpdDate = DateTime.Now;                

                db.Entry(news).State = EntityState.Detached;
                db.Entry(hotelNew).State = EntityState.Modified;
                db.SaveChanges();

                // cập nhật thông tin NewsAndCategory
                //Xoá thông tin cũ
                db.NewsAndCategories.RemoveRange(db.NewsAndCategories.Where(d => d.NewsId == hotelNew.HotelNewsID));
                foreach (var item in NewsCategories)
                {
                    db.NewsAndCategories.Add(new NewsAndCategories()
                    {
                        CategoryId = item,
                        NewsId = hotelNew.HotelNewsID,
                    });
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                //var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
                //notify = Notify.NotifyDanger(errors.ToString());
                //ViewBag.Notify = notify;
            }

            LoadDataDropDownList(hotelNew.NewsTypeID);
            return View(hotelNew);
        }

        // GET: WHSafeNews/Delete/5
        public ActionResult Delete(Guid id)
        {
            HotelNews hotelNew = db.HotelNews.Find(id);
            db.HotelNews.Remove(hotelNew);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //// POST: WHSafeNews/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(Guid id)
        //{
        //    HotelNews hotelNew = db.HotelNews.Find(id);
        //    db.HotelNews.Remove(hotelNew);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected void LoadData()
        {

        }

        protected void LoadDataDropDownList(int? NewsTypeID)
        {
            ViewBag.NewsTypeID = db.NewsTypes.Where(n => n.Status == true).Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.NewsTypeID.ToString(),
                Selected = s.NewsTypeID == NewsTypeID ? true : false
            }).ToList<SelectListItem>();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}