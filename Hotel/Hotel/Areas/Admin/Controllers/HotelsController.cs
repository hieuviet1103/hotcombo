﻿using Hotel.Models;
using Hotel.EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.Utilities;
using System.Data.Entity;
using Custom.Security;
using System.IO;
using System.Web.Mvc.Html;
using System.Net;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.Web.Helpers;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class HotelsController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        private const string UploadFolderFile = "~/FileUploads/Image";
        int RecordsPerPage = 10;
        // GET: Admin/Hotels
        #region Khach san

        public ActionResult Index()
        {
            try
            {
                if (TempData["Notify"] != null)
                {
                    Notify notify = (Notify)TempData["Notify"];
                    ViewBag.Notify = notify;
                }

                ViewBag.CountryId = new SelectList(_db.tbl_Country.ToList(), "CountryId", "CountryName", null);

                ViewBag.HotelType = new SelectList(_db.tbl_HotelType.OrderBy(d => d.HotelTypeName).ToList(), "Id", "HotelTypeName", null);

                ViewBag.Status = new SelectList(XMLUtils.BindData("status"), "value", "text", null);

                var viewModel = new List<HotelViewModel>();
                return View(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetHotelsList(string HotelName = "", string Address = "", int? HotelType = null, string Phone = "", int? CountryId = null, int? ProvinceId = null, int? DistrictId = null, int? Status = null)
        {
            //var hotels = _db.tbl_Hotel.Where(x => x.HotelName.ToLower().Contains(HotelName.ToLower()) 
            //&& Address == "" || x.Address.ToLower().Contains(Address.ToLower())
            //&& x.HotelTypeId == HotelType).ToList();
            var hotels = _db.tbl_Hotel.Where(x => x.IsDelete == false
            && (HotelName == "" || x.HotelName.ToLower().Contains(HotelName.ToLower()))
            && (Address == "" || x.Address.ToLower().Contains(Address.ToLower()))
            && (HotelType == null || x.HotelTypeId == HotelType)
            && (Phone == "" || x.Telephone == Phone)
            && (CountryId == null || x.CountryId == CountryId)
            && (ProvinceId == 0 || x.ProvinceId == ProvinceId)
            && (DistrictId == 0 || x.DistrictId == DistrictId)
            && (Status == null || x.IsActive == Status)
            ).ToList();
            var viewModel = hotels.Select(x => new HotelViewModel()
            {
                Id = x.Id,
                HotelName = x.HotelName,
                CountryId = x.CountryId,
                ProvinceId = x.ProvinceId,
                DistrictId = x.DistrictId,
                AreaId = x.AreaId,
                HotelRatingTypeId = x.HotelRatingTypeId,
                HotelTypeId = x.HotelTypeId,
                HotelConvenientType = x.HotelConvenientType,
                Address = x.Address,
                Telephone = x.Telephone,
                Mobile = x.Mobile,
                Longitude = x.Longitude,
                Latitude = x.Latitude,
                Content = x.Content,
                Email = x.Email,
                Url = x.Url,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                var country = _db.tbl_Country.FirstOrDefault(d => d.CountryId == item.CountryId);
                var province = _db.tbl_Province.FirstOrDefault(d => d.ProvinceId == item.ProvinceId);
                var district = _db.tbl_District.FirstOrDefault(d => d.DistrictId == item.DistrictId);

                if (country != null)
                    item.CountryName = country.CountryName;

                if (province != null)
                    item.ProvinceName = province.ProvinceName;

                if (district != null)
                    item.ProvinceName = district.DistrictName;
            }

            return PartialView("_HotelsList", viewModel);
        }

        public ActionResult GetRoomsListInEditHotel(Guid HotelId)
        {
            var rooms = _db.tbl_Room.Where(x => x.IsDelete == false
            && x.HotelId == HotelId
            ).ToList();
            var viewModel = rooms.Select(x => new RoomViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                RoomName = x.RoomName,
                AvailableRoom = x.AvailableRoom,
                RoomArea = x.RoomArea,
                Direction = x.Direction,
                SingleBed = x.SingleBed,
                DoubleBed = x.DoubleBed,
                MaxPeople = x.MaxPeople,
                HotelConvenientType = x.HotelConvenientType,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            //foreach (var item in viewModel)
            //{
            //    var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId);
            //    var direction = _db.tbl_Direction.FirstOrDefault(d => d.Id == item.Direction);


            //    if (hotel != null)
            //        item.HotelName = hotel.HotelName;

            //    if (direction != null)
            //        item.DirectionName = direction.DirectionName;
            //}

            return PartialView("_RoomList", viewModel);
        }

        public ActionResult GetHotelNoticesListInEditHotel(Guid HotelId)
        {
            var hotelNotices = _db.tbl_HotelNotice.Where(x => x.IsDelete == false
            && x.HotelId == HotelId
            ).ToList();
            var viewModel = hotelNotices.Select(x => new HotelNoticeViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                Content = x.Content,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId);

                if (hotel != null)
                    item.HotelName = hotel.HotelName;

            }

            return PartialView("_HotelNoticeList", viewModel);
        }

        public ActionResult GetHotelSurchargePolicysListInEditHotel(Guid HotelId)
        {
            var hotelSurcharges = _db.tbl_HotelSurchargePolicy.Where(x => x.IsDelete == false
            && x.HotelId == HotelId
            ).ToList();
            var viewModel = hotelSurcharges.Select(x => new HotelSurchargePolicyViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                Condition = x.Condition,
                Amount = x.Amount,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId);

                if (hotel != null)
                    item.HotelName = hotel.HotelName;

            }

            return PartialView("_HotelSurchargePolicyList", viewModel);
        }

        public ActionResult EditRoomPopup(Guid id)
        {
            var model = _db.tbl_Room.Where(x => x.Id == id).ToList();
            var viewModel = model.Select(s => new RoomViewModel()
            {
                Id = s.Id,
                HotelId = s.HotelId,
                RoomName = s.RoomName,
                AvailableRoom = s.AvailableRoom,
                RoomArea = s.RoomArea,
                Direction = s.Direction,
                SingleBed = s.SingleBed,
                DoubleBed = s.DoubleBed,
                MaxPeople = s.MaxPeople,
                HotelConvenientType = s.HotelConvenientType,
                Image = s.Image,
                IsDelete = s.IsDelete,
                UserCreate = s.UserCreate,
                DateCreate = s.DateCreate,
                UserUpdate = s.UserUpdate,
                DateUpdate = s.DateUpdate
            }).FirstOrDefault();

            //dll Hotel
            ViewBag.HotelId = new SelectList(
                    _db.tbl_Hotel.Where(d => d.IsDelete == false).OrderBy(d => d.HotelName).ToList(), "Id", "HotelName",
                    viewModel.HotelId);

            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", viewModel.HotelId); ;

            //dll Hướng
            ViewBag.DirectionType = new SelectList(_db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList(), "Id", "DirectionName", viewModel.Direction);

            //dll Tien ich
            string[] convenientTypeIds = { };
            if (viewModel.HotelConvenientType != "" && viewModel.HotelConvenientType != null)
            {
                convenientTypeIds = viewModel.HotelConvenientType.Split(new char[] { ',' });
            }

            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false && x.Type == 1).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                foreach (var check in convenientTypeIds)
                {
                    if (item.Id == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            //Seesion imageOld
            if (viewModel.Image != null)
            {
                TempData["ImageRoomOld"] = viewModel.Image.ToString();
            }
            else
            {
                TempData["ImageRoomOld"] = null;
            }

            return PartialView("_EditRoom", viewModel);
        }

        public ActionResult Create()
        {
            var modelView = new HotelViewModel();

            //dll Loai khach san
            ViewBag.HotelType = new SelectList(_db.tbl_HotelType.Where(x => x.IsDelete == false).OrderBy(x => x.HotelTypeName).ToList(), "Id", "HotelTypeName");
            //dll Hang sao
            ViewBag.HotelRatingType = new SelectList(_db.tbl_HotelRatingType.Where(x => x.IsDelete == false).OrderBy(x => x.NumberStar).ToList(), "Id", "Name");

            //dll Quoc gia
            ViewBag.CountryId = new SelectList(_db.tbl_Country.OrderBy(d => d.CountryName).ToList(), "CountryId", "CountryName", null);

            //dll Tinh Thanh
            ViewBag.ProvinceId = new SelectList(_db.tbl_Province.Where(d => d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList(), "ProvinceId", "ProvinceName", null);

            //dll Quan Huyen
            ViewBag.DistrictId = new SelectList(_db.tbl_District.Where(d => d.IsDelete == false).OrderBy(d => d.DistrictName).ToList(), "DistrictId", "DistrictName", null);

            //dll Khu vuc
            var sllAreaType = new List<SelectListItem>();
            var dbModelAreaType = _db.tbl_Area.Where(x => x.IsDelete == false).OrderBy(x => x.AreaName).ToList();

            foreach (var item in dbModelAreaType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.AreaId.ToString();
                selectItem.Text = item.AreaName;

                sllAreaType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlAreaType = sllAreaType;

            //dll Tien ich
            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            ViewBag.UrlImage = "~/FileUploads/Image/default-image.jpg";

            ViewBag.ddlStatus = new SelectList(XMLUtils.BindData("status"), "value", "text", null);

            return View(modelView);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(HotelViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null, int?[] sllAreaTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {
                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    string sllAreaTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllAreaTypeID"]))
                    {
                        sllAreaTypeId = Request["sllAreaTypeID"];
                        string[] areaTypeIds = sllAreaTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Hotel()
                    {
                        Id = Guid.NewGuid(),
                        HotelName = viewModel.HotelName,
                        Address = viewModel.Address,
                        CountryId = viewModel.CountryId,
                        ProvinceId = viewModel.ProvinceId,
                        DistrictId = viewModel.DistrictId,
                        AreaId = sllAreaTypeId,
                        HotelTypeId = viewModel.HotelTypeId,
                        HotelRatingTypeId = viewModel.HotelRatingTypeId,
                        Telephone = viewModel.Telephone,
                        Mobile = viewModel.Mobile,
                        Email = viewModel.Email,
                        Url = viewModel.Url,
                        Content = viewModel.Content,
                        Image = viewModel.Image,
                        HotelConvenientType = sllConvenientTypeId,
                        IsActive = viewModel.IsActive,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                        CancelTime = viewModel.CancelTime
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemFile in files)
                    {
                        try
                        {
                            if (files != null)
                            {
                                //upload file
                                string fileName = itemFile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemFile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }



                    _db.tbl_Hotel.Add(item);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    return RedirectToAction("Index");
                }
                catch
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            //ViewBag.CountryId = new SelectList(_db.tbl_countries.OrderBy(d => d.country_name).ToList(), "country_id", "country_name", model.CountryId);
            //ViewBag.ProvinceId = new SelectList(_db.tbl_provinces.Where(d => d.IsDelete == false).OrderBy(d => d.province_name).ToList(), "province_id", "province_name", model.ProvinceId);
            //ViewBag.Type = new SelectList(_db.tbl_hotel_level, "hotel_type", "hotel_type_name", model.Type);

            //ViewBag.Notify = notify;
            return View(viewModel);
            //return RedirectToAction("Hotels");
        }

        public ActionResult Edit(Guid id)
        {
            List<tbl_Hotel> model = new List<tbl_Hotel>();
            List<tbl_Room> modelRoom = new List<tbl_Room>();
            model = _db.tbl_Hotel.Where(x => x.Id == id).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }

            modelRoom = _db.tbl_Room.Where(x => x.HotelId == id).ToList();

            //Hotel viewModel = Mapper.MapFrom(tbl_hotel_restaurant);
            var viewModel = model.Select(x => new HotelViewModel()
            {
                Id = x.Id,
                HotelName = x.HotelName,
                CountryId = x.CountryId,
                ProvinceId = x.ProvinceId,
                DistrictId = x.DistrictId,
                AreaId = x.AreaId,
                HotelRatingTypeId = x.HotelRatingTypeId,
                HotelTypeId = x.HotelTypeId,
                HotelConvenientType = x.HotelConvenientType,
                Address = x.Address,
                Telephone = x.Telephone,
                Mobile = x.Mobile,
                Longitude = x.Longitude ?? "106.69913481294886",
                Latitude = x.Latitude ?? "10.78082576853423",
                Content = x.Content,
                Email = x.Email,
                Url = x.Url,
                Image = x.Image,
                IsActive = x.IsActive,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate,
                CancelTime = x.CancelTime
            }).FirstOrDefault();

            //dll Loai khach san
            ViewBag.HotelType = new SelectList(_db.tbl_HotelType.Where(x => x.IsDelete == false).OrderBy(x => x.HotelTypeName).ToList(), "Id", "HotelTypeName", viewModel.HotelTypeId);
            //dll Hang sao
            ViewBag.HotelRatingType = new SelectList(_db.tbl_HotelRatingType.Where(x => x.IsDelete == false).OrderBy(x => x.NumberStar).ToList(), "Id", "Name", viewModel.HotelRatingTypeId);

            //dll Quoc gia
            ViewBag.CountryId = new SelectList(_db.tbl_Country.OrderBy(d => d.CountryName).ToList(), "CountryId", "CountryName", viewModel.CountryId);

            //dll Tinh Thanh
            ViewBag.ProvinceId = new SelectList(_db.tbl_Province.Where(d => d.IsDelete == false && d.CountryId == viewModel.CountryId).OrderBy(d => d.ProvinceName).ToList(), "ProvinceId", "ProvinceName", viewModel.ProvinceId);

            //dll Quan Huyen
            ViewBag.DistrictId = new SelectList(_db.tbl_District.Where(d => d.IsDelete == false && d.ProvinceId == viewModel.ProvinceId).OrderBy(d => d.DistrictName).ToList(), "DistrictId", "DistrictName", viewModel.DistrictId);

            //dll Khu vuc
            string[] areaTypeIds = { };
            if (viewModel.AreaId != "" && viewModel.AreaId != null)
            {
                areaTypeIds = viewModel.AreaId.Split(new char[] { ',' });
            }

            var sllAreaType = new List<SelectListItem>();
            var dbModelAreaType = _db.tbl_Area.Where(x => x.IsDelete == false).OrderBy(x => x.AreaName).ToList();

            foreach (var item in dbModelAreaType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.AreaId.ToString();
                selectItem.Text = item.AreaName;

                foreach (var check in areaTypeIds)
                {
                    if (item.AreaId == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllAreaType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlAreaType = sllAreaType;

            //dll Tien ich
            string[] convenientTypeIds = { };
            if (viewModel.HotelConvenientType != "" && viewModel.HotelConvenientType != null)
            {
                convenientTypeIds = viewModel.HotelConvenientType.Split(new char[] { ',' });
            }

            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                foreach (var check in convenientTypeIds)
                {
                    if (item.Id == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            ViewBag.ddlStatus = new SelectList(XMLUtils.BindData("status"), "value", "text", null);

            //Seesion imageOld
            if (viewModel.Image != null)
            {
                TempData["ImageHotelOld"] = viewModel.Image.ToString();
            }
            else
            {
                TempData["ImageHotelOld"] = null;
            }


            viewModel.Rooms = new List<RoomViewModel>();
            viewModel.HotelNotices = new List<HotelNoticeViewModel>();
            viewModel.HotelSurchargePolicys = new List<HotelSurchargePolicyViewModel>();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(HotelViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null, int?[] sllAreaTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {

                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    string sllAreaTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllAreaTypeID"]))
                    {
                        sllAreaTypeId = Request["sllAreaTypeID"];
                        string[] areaTypeIds = sllAreaTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Hotel()
                    {
                        Id = viewModel.Id,
                        HotelName = viewModel.HotelName,
                        Address = viewModel.Address,
                        CountryId = viewModel.CountryId,
                        ProvinceId = viewModel.ProvinceId,
                        DistrictId = viewModel.DistrictId,
                        AreaId = sllAreaTypeId,
                        HotelTypeId = viewModel.HotelTypeId,
                        HotelRatingTypeId = viewModel.HotelRatingTypeId,
                        Telephone = viewModel.Telephone,
                        Mobile = viewModel.Mobile,
                        Email = viewModel.Email,
                        Url = viewModel.Url,
                        IsActive = viewModel.IsActive,
                        Content = viewModel.Content,
                        Image = viewModel.Image,
                        HotelConvenientType = sllConvenientTypeId,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                        CancelTime = viewModel.CancelTime
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemfile in files)
                    {
                        try
                        {
                            if (itemfile != null)
                            {
                                //upload file
                                string fileName = itemfile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemfile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    if (files[0] != null)
                    {
                        //Xoa di anh cu trong Folder
                        string imgPath = UploadFolderFile + "\\" + (string)TempData["ImageHotelOld"];
                        //FileInfo fi = new FileInfo(imgPath);
                        FileInfo fi = new FileInfo(Server.MapPath(imgPath));
                        if (fi.Exists)
                        {
                            fi.Delete();
                        }
                    }

                    TempData["Notify"] = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            ViewBag.Notify = notify;
            return RedirectToAction("Index"); ;
        }
        public ActionResult Copy(Guid hotelID)
        {
            Notify notify = null;

            if (hotelID != null && hotelID != Guid.Empty)
            {
                try
                {

                    var oldHotel = _db.tbl_Hotel.FirstOrDefault(t=> t.Id == hotelID);
                    Guid newID = Guid.NewGuid();
                    var item = new tbl_Hotel();
                    Utilities.Utility.CopyObject(oldHotel, item);
                    item.Id = newID;
                    item.HotelName = item.HotelName + " - Copy";
                    foreach (var policies in item.tbl_HotelCancellationPolicy)
                    {
                        policies.HotelId = hotelID;
                    }
                    foreach (var notices in item.tbl_HotelNotice)
                    {
                        notices.HotelId = hotelID;
                    }
                    foreach (var surcharge in item.tbl_HotelSurchargePolicy)
                    {
                        surcharge.HotelId = hotelID;
                    }
                    //foreach (var room in item.tbl_Room)
                    //{
                    //    room.HotelId = hotelID;
                    //    room.tbl_RoomPrice = null;
                    //}
                    item.tbl_Room = null;
                    item.tbl_HotelComment = null;
                    item.IsActive = 0;
                    item.IsDelete = false;
                    _db.tbl_Hotel.Add(item);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Copy dữ liệu thành công");
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                }
            }
            ViewBag.Notify = notify;
            return RedirectToAction("Index"); ;
        }
        public ActionResult DeleteHotel(Guid id)
        {
            try
            {
                tbl_Hotel hotel = _db.tbl_Hotel.Find(id);

                hotel.IsDelete = true;
                _db.Entry(hotel).State = EntityState.Modified;
                _db.SaveChanges();

                TempData["Notify"] = Notify.NotifySuccess("Xóa thành công.");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region SaveImages Hotel
        public ActionResult _AddImages(Guid Id)
        {
            ViewBag.HotelId = Id;
            ViewBag.Types = new SelectList(XMLUtils.BindData("imagetypehotel"), "value", "text", null);

            return PartialView();
        }

        public ActionResult _GetListImage(Guid HotelId)
        {
            ViewBag.List = _db.tbl_ProductImage.Where(p => p.HotelId == HotelId && p.Type != 4 && (p.IsDelete == null || p.IsDelete == false)).OrderBy(x => x.Type).ThenBy(x => x.Order).ThenBy(x => x.DateCreate);

            return PartialView();
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult _FunSaveImage(Guid HotelId, int? Types = null, int? Order = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = new tbl_ProductImage()
                    {
                        Id = Guid.NewGuid(),
                        HotelId = HotelId,
                        Type = Types,
                        Order = Order,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                    };

                    #region Upload hình
                    string fileName = "";

                    foreach (string file in Request.Files)
                    {
                        var postedFile = Request.Files[file];

                        string fileExtension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf("."));

                        string newName = Guid.NewGuid().ToString();

                        fileName = "img_" + DateTime.Now.ToString("ddMMyyyy") + newName + fileExtension;

                        #region Check file chính hay file phụ
                        if (file.Contains("ImageSlide_"))
                        {
                            postedFile.SaveAs(Server.MapPath(string.Format("~/{0}/", "FileUploads/Image/Hotels")) + fileName);
                            item.ImageUrl = fileName;

                            var image = WebImage.GetImageFromRequest(file);
                            if (image != null)
                            {
                                //resize ảnh small
                                image.Resize(1171, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathSmall = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "hotel_" + fileName); //small
                                image.Save(pathSmall); //Lưu ảnh trên server

                                //ảnh đại diện
                                image.Resize(191, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathThumb = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "t_" + fileName); //thumbnail
                                image.Save(pathThumb); //Lưu ảnh trên server
                            }
                        }
                        #endregion
                    }
                    #endregion

                    _db.tbl_ProductImage.Add(item);

                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult _FunXoaImage(Guid Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = _db.tbl_ProductImage.FirstOrDefault(p => p.Id == Id);

                    item.DateUpdate = DateTime.Now;
                    item.UserUpdate = User.UserId;
                    item.IsDelete = true;
                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        #region Phòng

        public ActionResult Rooms()
        {
            try
            {
                if (TempData["Notify"] != null)
                {
                    Notify notify = (Notify)TempData["Notify"];
                    ViewBag.Notify = notify;
                }

                ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", null);


                var viewModel = new List<RoomViewModel>();
                return View(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetRoomsList(Guid? HotelId)
        {
            //var hotel = _db.tbl_Hotel.Find(HotelId);

            var rooms = _db.tbl_Room.Where(x => x.IsDelete == false
            ).ToList();
            var viewModel = rooms.Select(x => new RoomViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                RoomName = x.RoomName,
                AvailableRoom = x.AvailableRoom,
                RoomArea = x.RoomArea,
                Direction = x.Direction,
                SingleBed = x.SingleBed,
                DoubleBed = x.DoubleBed,
                MaxPeople = x.MaxPeople,
                HotelConvenientType = x.HotelConvenientType,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).ToList();

            foreach (var item in viewModel)
            {
                var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == item.HotelId);

                if (hotel != null)
                    item.HotelName = hotel.HotelName;

            }

            return PartialView("_RoomsList", viewModel);
        }

        public ActionResult RoomsCreate(Guid hotelId)
        {
            var modelView = new RoomViewModel();



            ////dll Khach san
            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", null);

            //dll Hướng
            //ViewBag.DirectionType = new SelectList(_db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList(), "Id", "DirectionName");
            var sllDirectionType = new List<SelectListItem>();
            var dbModelDirectionType = _db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList();

            foreach (var item in dbModelDirectionType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.DirectionName;

                sllDirectionType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlDirectionType = sllDirectionType;

            //dll Tien ich
            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false && x.Type == 1).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            //dll Dich vu dinh kem

            var sllServiceType = new List<SelectListItem>();
            var dbModelServiceType = _db.tbl_HotelServiceType.Where(x => x.IsDelete == false).OrderBy(x => x.ServiceName).ToList();
            foreach (var item in dbModelServiceType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ServiceName;

                sllServiceType.Add(selectItem);
            }
            ViewBag.ddlServiceType = sllServiceType;

            ViewBag.UrlImage = "~/FileUploads/Image/default-image.jpg";

            modelView.HotelId = hotelId;

            return View(modelView);
        }

        [HttpPost]
        public ActionResult RoomsCreate(RoomViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null, int?[] sllDirectionTypeID = null, int?[] sllServiceTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {
                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    string sllDirectionTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllDirectionTypeID"]))
                    {
                        sllDirectionTypeId = Request["sllDirectionTypeID"];
                        //string[] convenientTypeIds = sllDirectionTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Room()
                    {
                        Id = Guid.NewGuid(),
                        HotelId = viewModel.HotelId,
                        RoomName = viewModel.RoomName,
                        AvailableRoom = viewModel.AvailableRoom,
                        RoomArea = viewModel.RoomArea,
                        Direction = sllDirectionTypeId,
                        SingleBed = viewModel.SingleBed,
                        DoubleBed = viewModel.DoubleBed,
                        MaxPeople = viewModel.MaxPeople,
                        Image = viewModel.Image,
                        IsDelete = false,
                        HotelConvenientType = sllConvenientTypeId,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemFile in files)
                    {
                        try
                        {
                            if (itemFile != null)
                            {
                                //upload file
                                string fileName = itemFile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemFile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }



                    _db.tbl_Room.Add(item);
                    _db.SaveChanges();

                    TempData["Notify"] = Notify.NotifySuccess("Thêm mới dữ liệu thành công");
                    //return RedirectToAction("Rooms");
                    return RedirectToAction("Edit", new { id = item.HotelId });
                }
                catch
                {
                    notify = Notify.NotifyDanger("Thêm mới dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            //ViewBag.CountryId = new SelectList(_db.tbl_countries.OrderBy(d => d.country_name).ToList(), "country_id", "country_name", model.CountryId);
            //ViewBag.ProvinceId = new SelectList(_db.tbl_provinces.Where(d => d.IsDelete == false).OrderBy(d => d.province_name).ToList(), "province_id", "province_name", model.ProvinceId);
            //ViewBag.Type = new SelectList(_db.tbl_hotel_level, "hotel_type", "hotel_type_name", model.Type);

            //ViewBag.Notify = notify;
            return View(viewModel);
            //return RedirectToAction("Hotels");
        }

        public ActionResult RoomsEdit(Guid id)
        {
            List<tbl_Room> model = new List<tbl_Room>();
            model = _db.tbl_Room.Where(x => x.Id == id).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }

            //Hotel viewModel = Mapper.MapFrom(tbl_hotel_restaurant);
            var viewModel = model.Select(x => new RoomViewModel()
            {
                Id = x.Id,
                HotelId = x.HotelId,
                RoomName = x.RoomName,
                AvailableRoom = x.AvailableRoom,
                RoomArea = x.RoomArea,
                Direction = x.Direction,
                SingleBed = x.SingleBed,
                DoubleBed = x.DoubleBed,
                MaxPeople = x.MaxPeople,
                HotelConvenientType = x.HotelConvenientType,
                Image = x.Image,
                IsDelete = x.IsDelete,
                UserCreate = x.UserCreate,
                DateCreate = x.DateCreate,
                UserUpdate = x.UserUpdate,
                DateUpdate = x.DateUpdate
            }).FirstOrDefault();


            //dll Hotel
            ViewBag.HotelId = new SelectList(
                    _db.tbl_Hotel.Where(d => d.IsDelete == false).OrderBy(d => d.HotelName).ToList(), "Id", "HotelName",
                    viewModel.HotelId);

            //ViewBag.HotelId = new SelectList(_db.tbl_Hotel.OrderBy(d => d.HotelName).ToList(), "Id", "HotelName", viewModel.HotelId); ;

            //dll Hướng
            //ViewBag.DirectionType = new SelectList(_db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList(), "Id", "DirectionName", viewModel.Direction);
            string[] directionTypeIds = { };
            if (viewModel.Direction != "" && viewModel.Direction != null)
            {
                directionTypeIds = viewModel.Direction.Split(new char[] { ',' });
            }

            var sllDirectionType = new List<SelectListItem>();
            var dbModelDirectionType = _db.tbl_Direction.Where(x => x.IsDelete == false).OrderBy(x => x.DirectionName).ToList();

            foreach (var item in dbModelDirectionType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.DirectionName;

                foreach (var check in directionTypeIds)
                {
                    if (item.Id == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllDirectionType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlDirectionType = sllDirectionType;


            //dll Tien ich
            string[] convenientTypeIds = { };
            if (viewModel.HotelConvenientType != "" && viewModel.HotelConvenientType != null)
            {
                convenientTypeIds = viewModel.HotelConvenientType.Split(new char[] { ',' });
            }

            var sllConvenientType = new List<SelectListItem>();
            var dbModelConvenientType = _db.tbl_HotelConvenientType.Where(x => x.IsDelete == false && x.Type == 1).OrderBy(x => x.ConvenientName).ToList();

            foreach (var item in dbModelConvenientType)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.Id.ToString();
                selectItem.Text = item.ConvenientName;

                foreach (var check in convenientTypeIds)
                {
                    if (item.Id == int.Parse(check))
                    {
                        selectItem.Selected = true;
                    }
                }

                sllConvenientType.Add(selectItem);
            }
            //var sllDepartmentId = new List<SelectListItem>();
            ViewBag.ddlConvenientType = sllConvenientType;

            //Seesion imageOld
            if (viewModel.Image != null)
            {
                TempData["ImageRoomOld"] = viewModel.Image.ToString();
            }
            else
            {
                TempData["ImageRoomOld"] = null;
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult RoomsEdit(RoomViewModel viewModel, List<HttpPostedFileBase> files, int?[] sllConvenientTypeID = null, int?[] sllDirectionTypeID = null)
        {
            Notify notify = null;

            if (ModelState.IsValid)
            {
                try
                {
                    //Tien ich
                    string sllConvenientTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllConvenientTypeID"]))
                    {
                        sllConvenientTypeId = Request["sllConvenientTypeID"];
                        string[] convenientTypeIds = sllConvenientTypeId.Split(new char[] { ',' });

                    }

                    //Huong
                    string sllDirectionTypeId = "";
                    if (!string.IsNullOrEmpty(Request["sllDirectionTypeID"]))
                    {
                        sllDirectionTypeId = Request["sllDirectionTypeID"];
                        //string[] directionTypeIds = sllDirectionTypeId.Split(new char[] { ',' });

                    }

                    var item = new tbl_Room()
                    {
                        Id = viewModel.Id,
                        HotelId = viewModel.HotelId,
                        RoomName = viewModel.RoomName,
                        AvailableRoom = viewModel.AvailableRoom,
                        RoomArea = viewModel.RoomArea,
                        Direction = sllDirectionTypeId,
                        SingleBed = viewModel.SingleBed,
                        DoubleBed = viewModel.DoubleBed,
                        MaxPeople = viewModel.MaxPeople,
                        Image = viewModel.Image,
                        IsDelete = false,
                        HotelConvenientType = sllConvenientTypeId,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                    };

                    //Update attach files                    
                    foreach (HttpPostedFileBase itemfile in files)
                    {
                        try
                        {
                            if (itemfile != null)
                            {
                                //upload file
                                string fileName = itemfile.FileName;
                                string extension = Path.GetExtension(fileName);

                                if (Utilities.Utility.AllowedExtensions.Contains(extension))
                                {
                                    string name = fileName.Substring(0, fileName.LastIndexOf("."));
                                    name = HtmlHelperExtentions.LoaiBoDauTiengViet(name);
                                    fileName = DateTime.Now.ToString("ddMMyyyy_hhmmsstt") + "_" + name + extension;
                                    item.Image = itemfile.SaveIn(UploadFolderFile, fileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    //Xoa di anh cu trong Folder
                    string imgPath = UploadFolderFile + "\\" + (string)TempData["ImageRoomOld"];
                    //FileInfo fi = new FileInfo(imgPath);
                    FileInfo fi = new FileInfo(Server.MapPath(imgPath));
                    if (fi.Exists)
                    {
                        fi.Delete();
                    }

                    TempData["Notify"] = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    //return RedirectToAction("Rooms");
                    return RedirectToAction("Edit", new { id = viewModel.HotelId });

                }
                catch (Exception ex)
                {
                    notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                }
            }

            if (notify == null)
                notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");

            ViewBag.Notify = notify;
            //return RedirectToAction("Hotels");
            return RedirectToAction("Edit", new { id = viewModel.HotelId });
        }

        public ActionResult DeleteRoom(Guid id)
        {
            try
            {

                tbl_Room room = _db.tbl_Room.Find(id);

                room.IsDelete = true;
                _db.Entry(room).State = EntityState.Modified;
                _db.SaveChanges();

                TempData["Notify"] = Notify.NotifySuccess("Xóa thành công.");
                return RedirectToAction("Edit", new { id = room.HotelId });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region SaveImages Room
        public ActionResult _AddRoomImages(Guid Id, Guid HotelId)
        {
            ViewBag.HotelId = HotelId;
            ViewBag.RoomId = Id;
            ViewBag.Types = new SelectList(XMLUtils.BindData("imagetypehotel"), "value", "text", null);

            return PartialView();
        }

        public ActionResult _GetListRoomImage(Guid RoomId)
        {
            ViewBag.List = _db.tbl_ProductImage.Where(p => p.RoomId == RoomId && p.Type == 4 && (p.IsDelete == null || p.IsDelete == false)).OrderBy(x => x.Order).ThenBy(x => x.DateCreate);

            return PartialView();
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult _FunSaveRoomImage(Guid RoomId, Guid HotelId, int? Order = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = new tbl_ProductImage()
                    {
                        Id = Guid.NewGuid(),
                        HotelId = HotelId,
                        RoomId = RoomId,
                        Type = 4,
                        Order = Order,
                        UserCreate = User.UserId,
                        DateCreate = DateTime.Now,
                        UserUpdate = User.UserId,
                        DateUpdate = DateTime.Now,
                    };

                    #region Upload hình
                    string fileName = "";

                    foreach (string file in Request.Files)
                    {
                        var postedFile = Request.Files[file];

                        string fileExtension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf("."));

                        string newName = Guid.NewGuid().ToString();

                        fileName = "img_" + DateTime.Now.ToString("ddMMyyyy") + newName + fileExtension;

                        #region Check file chính hay file phụ
                        if (file.Contains("ImageSlide_"))
                        {
                            postedFile.SaveAs(Server.MapPath(string.Format("~/{0}/", "FileUploads/Image/Hotels")) + fileName);
                            item.ImageUrl = fileName;

                            var image = WebImage.GetImageFromRequest(file);
                            if (image != null)
                            {
                                //resize ảnh small
                                image.Resize(1171, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathSmall = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "hotel_" + fileName); //small
                                image.Save(pathSmall); //Lưu ảnh trên server

                                //ảnh đại diện
                                image.Resize(191, 2000);
                                image.Crop(1, 1, 0, 0);
                                var pathThumb = Path.Combine(Server.MapPath("~/FileUploads/Image/Hotels"), "t_" + fileName); //thumbnail
                                image.Save(pathThumb); //Lưu ảnh trên server
                            }
                        }
                        #endregion
                    }
                    #endregion

                    _db.tbl_ProductImage.Add(item);

                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult _FunXoaRoomImage(Guid Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = _db.tbl_ProductImage.FirstOrDefault(p => p.Id == Id);

                    item.DateUpdate = DateTime.Now;
                    item.UserUpdate = User.UserId;
                    item.IsDelete = true;
                    _db.Entry(item).State = EntityState.Modified;
                    _db.SaveChanges();

                    return Json(new { status = 1, title = "", text = "Cập nhật thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = -1, title = "", text = ex.Message, obj = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = -2, title = "", text = "Cập nhật không thành công.", obj = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion
        [HttpGet]
        public JsonResult _GetProvince(int countryid)
        {
            List<SelectListItem> sllProvince = new List<SelectListItem>();
            sllProvince.Add(new SelectListItem { Value = "0", Text = "---   Chọn tỉnh thành   ---" });
            var provinces = _db.tbl_Province.Where(d => d.CountryId == countryid && d.IsDelete == false).OrderBy(d => d.ProvinceName).ToList();
            foreach (var item in provinces)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.ProvinceId.ToString();
                sllItem.Text = item.ProvinceName;

                sllProvince.Add(sllItem);
            }
            return Json(sllProvince, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult _GetDistrict(int provinceid)
        {
            List<SelectListItem> sllDistrict = new List<SelectListItem>();
            sllDistrict.Add(new SelectListItem { Value = "0", Text = "---   Chọn quận / huyện   ---" });
            var districts = _db.tbl_District.Where(d => d.ProvinceId == provinceid).OrderBy(d => d.DistrictName).ToList();
            foreach (var item in districts)
            {
                SelectListItem sllItem = new SelectListItem();
                sllItem.Value = item.DistrictId.ToString();
                sllItem.Text = item.DistrictName;
                sllDistrict.Add(sllItem);
            }
            return Json(sllDistrict, JsonRequestBehavior.AllowGet);
        }

        #region HotelNotice
        public ActionResult _HotelNoticesEdit(int id)
        {
            var hotelNotice = _db.tbl_HotelNotice.FirstOrDefault(d => d.Id == id);

            var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == hotelNotice.HotelId);


            ViewBag.HotelId = new SelectList(
                _db.tbl_Hotel.Where(d => d.IsDelete == false).ToList(), "Id", "HotelName",
                hotel.Id);


            return PartialView(hotelNotice);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult HotelNoticesEdit(tbl_HotelNotice model)
        {
            //Notify notify;
            bool check = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //var model = _db.tbl_HotelNotice.Find(Id);
                    //model.Content = ContentNotice;
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();

                    //notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    //TempData["Notify"] = notify;

                    //return RedirectToAction("Edit", new { id = model.HotelId });
                    check = true;
                }
                catch
                {
                    //notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    //TempData["Notify"] = notify;
                    //return RedirectToAction("Edit", new { id = model.HotelId });
                    check = false;
                }
            }
            //notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            //TempData["Notify"] = notify;

            //return RedirectToAction("Edit", new { id = model.HotelId });
            return Json(check);
        }

        [HttpPost]
        public JsonResult HotelNoticesCreate(Guid Id, string ContentNotice)
        {
            bool check;
            try
            {
                var model = new tbl_HotelNotice();
                model.HotelId = Id;
                model.Content = ContentNotice;
                model.UserCreate = User.UserId;
                model.DateCreate = DateTime.Now;
                //model.DistrictId = Guid.NewGuid();
                _db.tbl_HotelNotice.Add(model);
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        [HttpPost]
        public JsonResult HotelNoticesDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelNotice.FirstOrDefault(d => d.Id == id);
                model.IsDelete = true;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
                //_db.tbl_HotelNotice.Remove(model);
                //_db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }
        #endregion

        #region Phu thu
        public ActionResult _SurchargePolicysEdit(int id)
        {
            var surchargePolicy = _db.tbl_HotelSurchargePolicy.FirstOrDefault(d => d.Id == id);

            var hotel = _db.tbl_Hotel.FirstOrDefault(d => d.Id == surchargePolicy.HotelId);


            ViewBag.HotelId = new SelectList(
                _db.tbl_Hotel.Where(d => d.IsDelete == false).ToList(), "Id", "HotelName",
                hotel.Id);

            return PartialView(surchargePolicy);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SurchargePolicysEdit(tbl_HotelSurchargePolicy model)
        {
            //Notify notify;
            bool check = false;
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(model).State = EntityState.Modified;
                    _db.SaveChanges();
                    check = true;
                    //notify = Notify.NotifySuccess("Cập nhật dữ liệu thành công");
                    //TempData["Notify"] = notify;

                    //return RedirectToAction("Edit", new { id = model.HotelId });
                }
                catch
                {
                    check = false;
                    //notify = Notify.NotifyDanger("Cập nhật dữ liệu không thành công");
                    //TempData["Notify"] = notify;
                    //return RedirectToAction("Edit", new { id = model.HotelId });
                }
            }
            return Json(check);
            //notify = Notify.NotifyWarning("Thông tin nhập không hợp lệ");
            //TempData["Notify"] = notify;

            //return RedirectToAction("Edit", new { id = model.HotelId });
        }

        [HttpPost]
        public JsonResult HotelSurchargesCreate(Guid Id, string ConditionSurcharge, double? AmountSurcharge)
        {
            bool check;
            try
            {
                var model = new tbl_HotelSurchargePolicy();
                model.HotelId = Id;
                model.Condition = ConditionSurcharge;
                model.Amount = AmountSurcharge;
                model.UserCreate = User.UserId;
                model.DateCreate = DateTime.Now;
                //model.DistrictId = Guid.NewGuid();
                _db.tbl_HotelSurchargePolicy.Add(model);
                _db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }

        [HttpPost]
        public JsonResult HotelSurchargesDelete(int id)
        {
            bool check;
            try
            {
                var model = _db.tbl_HotelSurchargePolicy.FirstOrDefault(d => d.Id == id);
                model.IsDelete = true;
                _db.Entry(model).State = EntityState.Modified;
                _db.SaveChanges();
                //_db.tbl_HotelNotice.Remove(model);
                //_db.SaveChanges();

                check = true;
            }
            catch
            {
                check = false;
            }
            return Json(check);
        }
        #endregion

    }
}