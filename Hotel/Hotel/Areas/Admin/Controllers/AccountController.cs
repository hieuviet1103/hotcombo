﻿using Hotel.Models;
using Hotel.EntityFramework.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.Utilities;
using System.Data.Entity;
using Custom.Security;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class AccountController : BaseController
    {
        private readonly HotelEntities _db = new HotelEntities();
        // GET: Admin/Account
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            AccountUpdateViewModel model = new AccountUpdateViewModel();

            var roles = _db.tbl_Role.Select(d => new { id = d.RoleId, name = d.Name }).ToList();
            var sllRoleId = new List<SelectListItem>();
            foreach (var item in roles)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.id.ToString();
                selectItem.Text = item.name;
                sllRoleId.Add(selectItem);
            }
            ViewBag.RoleId = sllRoleId;

            //var departures = _db.Select(d => new { id = d.departure_id, name = d.departure_name }).ToList();
            //var sllDepartureId = new List<SelectListItem>();
            //foreach (var item in departures)
            //{
            //    var selectItem = new SelectListItem();
            //    selectItem.Value = item.id.ToString();
            //    selectItem.Text = item.name;
            //    sllDepartureId.Add(selectItem);
            //}
            //ViewBag.DepartmentId = sllDepartureId;
            //ViewBag.GroupApplicationId = new SelectList(_db.GroupApplications.ToList(), "GroupApplicationId", "Name");

            return View(model);
        }

        [HttpPost]
        public JsonResult Create(AccountUpdateViewModel model)//Guid userId
        {
            if (ModelState.IsValid)
            {
                model.UserId = Guid.NewGuid();
                try
                {
                    if (_db.tbl_User.Any(d => d.UserId == model.UserId || d.LoginName == model.LoginName || d.Email == model.Email))
                    {
                        return Json(new { status = "nook", id = -1, message = "Đã tồn tại nhân sự với thông tin như trên, xin vui lòng kiểm tra lại !" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var user = new AccountUpdateViewModel().MapTo(model);
                        user.LoginName = model.LoginName;
                        user.FullName = model.FullName;
                        var passEndcode = BaseFunctions.EncodeString(model.Password);
                        user.Password = passEndcode;
                        user.CodeStaff = "";
                        user.PhoneNumber = "";
                        user.DateCreate = DateTime.Now;
                        user.UserCreate = User.UserId;
                        //user.CodeStaff = model.CodeStaff;
                        user.Active = 1;                        

                        _db.tbl_User.Add(user);
                        //_db.SaveChanges();

                        //Insert new user role
                        if (!string.IsNullOrEmpty(Request["sllRoleId"]))
                        {
                            var lstUserRole = new List<tbl_User_Role>();
                            string sllRoleId = Request["sllRoleId"];
                            string[] roleIds = sllRoleId.Split(new char[] { ',' });
                            foreach (var item in roleIds)
                            {
                                if (item != ((Guid)Parameters.HotelAll).ToString())
                                {
                                    var usersRoles = new tbl_User_Role();
                                    usersRoles.Id = Guid.NewGuid();
                                    usersRoles.UserId = user.UserId;
                                    usersRoles.RoleId = Int32.Parse(item);
                                    usersRoles.IsActive = true;
                                    lstUserRole.Add(usersRoles);
                                }
                            }
                            _db.tbl_User_Role.AddRange(lstUserRole);                            
                            _db.SaveChanges();
                        }                       

                        return Json(new { status = "ok", id = 1, message = "Cập nhật tài khoản thành công !" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    return Json(new { status = "nook", id = -1, message = "Có lỗi xảy ra, cập nhật thất bại !" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = "nook", id = 0, message = "Vui lòng nhập đầy đủ dữ liệu !" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(Guid id)
        {

            var user = _db.tbl_User.Find(id);
            if (user == null)
                return HttpNotFound();

            AccountUpdateViewModel model = new AccountUpdateViewModel().MapFrom(user);

            var roles = _db.tbl_Role.Select(d => new { id = d.RoleId, name = d.Name }).ToList();
            var sllRoleId = new List<SelectListItem>();
            foreach (var item in roles)
            {
                var selectItem = new SelectListItem();
                selectItem.Value = item.id.ToString();
                selectItem.Text = item.name;
                foreach (var usersRole in user.tbl_User_Role)
                {
                    if (selectItem.Value == usersRole.RoleId.ToString())
                    {
                        selectItem.Selected = true;
                    }
                }
                sllRoleId.Add(selectItem);
            }
            ViewBag.RoleId = sllRoleId;

            //var departures = _db.tbl_departure.Select(d => new { id = d.departure_id, name = d.departure_name }).ToList();
            //var sllDepartureId = new List<SelectListItem>();
            //foreach (var item in departures)
            //{
            //    var selectItem = new SelectListItem();
            //    selectItem.Value = item.id.ToString();
            //    selectItem.Text = item.name;
            //    foreach (var usersDeparture in user.Users_Departures)
            //    {
            //        if (selectItem.Value == usersDeparture.DepartureId.ToString())
            //        {
            //            selectItem.Selected = true;
            //        }
            //    }
            //    sllDepartureId.Add(selectItem);
            //}
            //ViewBag.DepartmentId = sllDepartureId;
            //ViewBag.GroupApplicationId = new SelectList(_db.GroupApplications.ToList(), "GroupApplicationId", "Name", user.GroupApplicationId);

            return View(model);
        }

        [HttpPost]
        public JsonResult Edit(AccountUpdateViewModel model)//Guid userId
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (_db.tbl_User.Any(d => d.UserId != model.UserId && (d.LoginName == model.LoginName || d.Email == model.Email)))
                    {
                        return Json(new { status = "nook", id = -1, message = "Đã tồn tại nhân sự với thông tin như trên, xin vui lòng kiểm tra lại !" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var userOld = _db.tbl_User.FirstOrDefault(d => d.UserId == model.UserId);

                        string password = userOld.Password;

                        //var user = new AccountUpdateViewModel().MapTo(model);
                        userOld.DateUpdate = DateTime.Now;
                        userOld.UserUpdate = User.UserId;
                        //user.DateCreate = userOld.DateCreate;
                        //user.UserCreate = userOld.UserCreate;

                        //update pass
                        if (model.Password != password)
                        {
                            var passEndcode = BaseFunctions.EncodeString(model.Password);
                            userOld.Password = passEndcode;
                        }
                        //remove old user role
                        if (_db.tbl_User_Role.Any(d => d.UserId == model.UserId))
                        {
                            _db.tbl_User_Role.RemoveRange(_db.tbl_User_Role.Where(d => d.UserId == model.UserId));
                            _db.SaveChanges();
                        }

                        //Insert new user role
                        if (!string.IsNullOrEmpty(Request["sllRoleId"]))
                        {
                            var lstUserRole = new List<tbl_User_Role>();
                            string sllRoleId = Request["sllRoleId"];
                            string[] roleIds = sllRoleId.Split(new char[] { ',' });
                            foreach (var item in roleIds)
                            {
                                if (item != ((Guid)Parameters.HotelAll).ToString())
                                {
                                    var usersRoles = new tbl_User_Role();
                                    usersRoles.Id = Guid.NewGuid();
                                    usersRoles.UserId = userOld.UserId;
                                    usersRoles.RoleId = Int32.Parse(item);
                                    usersRoles.IsActive = true;
                                    lstUserRole.Add(usersRoles);
                                }
                            }
                            _db.tbl_User_Role.AddRange(lstUserRole);
                            _db.SaveChanges();
                        }
                        //_db.tbl_User.Remove(userOld);
                        _db.tbl_User.Add(userOld);
                        _db.Entry(userOld).State = System.Data.Entity.EntityState.Modified;
                        _db.SaveChanges();

                        return Json(new { status = "ok", id = 1, message = "Cập nhật tài khoản thành công !" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    return Json(new { status = "nook", id = -1, message = "Có lỗi xảy ra, cập nhật thất bại !" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = "nook", id = 0, message = "Vui lòng nhập đầy đủ dữ liệu !" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ToggleUserStatus(Guid userId)
        {
            bool check = false;

            tbl_User user = new tbl_User();
            try
            {
                user = _db.tbl_User.FirstOrDefault(d => d.UserId == userId);

                user.Active = 1;// (user.IsActive == 1 ? byte.Parse("0") : byte.Parse("1"));
                _db.Entry(user).State = EntityState.Modified;
                _db.SaveChanges();

                check = true;
            }
            catch { }

            if (check)
                return Json(new { status = 1, message = "Thành công", resultStatus = user.Active });
            else
                return Json(new { status = -1, message = "Thất bại" });
        }
        public ActionResult Manager(SearchUser search)
        {
            var session = HttpContext.Session;
            if (session["SearchAccount"] != null && Request.HttpMethod == "GET")
            {
                search = session["SearchAccount"] as SearchUser;
            }

            if (TempData["Notify"] != null)
            {
                Notify notify = (Notify)TempData["Notify"];
                ViewBag.Notify = notify;
            }

            if (Request.HttpMethod == "GET")
            {
                search.Status = 1;
            }

            var listUsers = _db.tbl_User.Where(d => 1 == 1);

            if (!string.IsNullOrEmpty(search.Fullname))
            {
                listUsers = listUsers.Where(d => !string.IsNullOrEmpty(d.FullName) && d.FullName.ToLower().Trim().Contains(search.Fullname.Trim().ToLower()));
            }

            if (!string.IsNullOrEmpty(search.Email))
            {
                listUsers = listUsers.Where(d => !string.IsNullOrEmpty(d.Email) && d.Email.ToLower().Trim().Contains(search.Email.Trim().ToLower()));
            }

            if (!string.IsNullOrEmpty(search.Username))
            {
                listUsers = listUsers.Where(d => !string.IsNullOrEmpty(d.LoginName) && d.LoginName.ToLower().Trim().Contains(search.Username.Trim().ToLower()));
            }

            if (search.Status >= 0)
            {
                //listUsers = listUsers.Where(d => d.IsActive == search.Status);
            }

            List<int> listRoleId = new List<int>();
            if (Request["RoleId"] != null && Request["RoleId"] != "")
            {
                try
                {
                    string requestRoleId = Request["RoleId"];
                    session["RoleId"] = requestRoleId;

                    List<string> listStrRoleId = requestRoleId.Split(new char[] { ',' }).ToList();

                    foreach (var item in listStrRoleId)
                    {
                        int itemRoleId = int.Parse(item);
                        listRoleId.Add(itemRoleId);
                    }
                }
                catch
                {

                }
            }
            else if (Request.HttpMethod == "GET" && session["RoleId"] != null)
            {
                string requestRoleId = session["RoleId"] as string;
                List<string> listStrRoleId = requestRoleId.Split(new char[] { ',' }).ToList();

                foreach (var item in listStrRoleId)
                {
                    int itemRoleId = int.Parse(item);
                    listRoleId.Add(itemRoleId);
                }
            }
            if (listRoleId.Count() > 0)
            {
                if (search.StatusRoleId == 0)
                {
                    listUsers = listUsers.Where(d => d.tbl_User_Role.Any(p => listRoleId.Contains(p.RoleId)));
                }
                else if (search.StatusRoleId == 1)
                {
                    listUsers = listUsers.Where(d => d.tbl_User_Role.Where(p => listRoleId.Contains(p.RoleId)).Select(m => m.RoleId).Distinct().Count() == listRoleId.Count());
                }
            }

            List<int> listDepartureId = new List<int>();
            if (Request["DepartureId"] != null && Request["DepartureId"] != "")
            {
                try
                {
                    string requestDepartureId = Request["DepartureId"];
                    session["DepartureId"] = requestDepartureId;
                    List<string> listStrDepartureId = requestDepartureId.Split(new char[] { ',' }).ToList();
                    foreach (var item in listStrDepartureId)
                    {
                        int itemDepartureId = int.Parse(item);
                        listDepartureId.Add(itemDepartureId);
                    }
                }
                catch
                {

                }
            }
            else if (Request.HttpMethod == "GET" && session["DepartureId"] != null)
            {
                string requestDepartureId = session["DepartureId"] as string;
                List<string> listStrDepartureId = requestDepartureId.Split(new char[] { ',' }).ToList();
                foreach (var item in listStrDepartureId)
                {
                    int itemDepartureId = int.Parse(item);
                    listDepartureId.Add(itemDepartureId);
                }
            }
            if (listDepartureId.Count() > 0)
            {
                if (search.StatusDepartureId == 0)
                {
                    //listUsers = listUsers.Where(d => d.de.Any(p => listDepartureId.Contains(p.DepartureId)));
                }
                else if (search.StatusDepartureId == 1)
                {
                    //listUsers = listUsers.Where(d => d.Users_Departures.Where(p => listDepartureId.Contains(p.DepartureId)).Select(m => m.DepartureId).Distinct().Count() == listDepartureId.Count());
                }
            }


            if (search.GroupApplicationId != null && search.GroupApplicationId > 0)
            {
                //listUsers = listUsers.Where(d => d.GroupApplicationId == search.GroupApplicationId);
            }

            List<SelectListItem> sllStatus = new List<SelectListItem>
            {
                new SelectListItem { Text = "---Tất cả---", Value = "-1" },
                new SelectListItem { Text = "Hoạt động", Value = "1" },
                new SelectListItem { Text = "Không hoạt động", Value = "0" }
            };
            ViewBag.Status = new SelectList(sllStatus, "Value", "Text", search.Status);

            List<SelectListItem> sllStatusQuerry = new List<SelectListItem>
            {
                new SelectListItem { Text = "Chứa trong", Value = "0" },
                new SelectListItem { Text = "Tất cả", Value = "1" }
            };
            ViewBag.StatusRoleId = new SelectList(sllStatusQuerry, "Value", "Text", search.StatusRoleId);

            ViewBag.StatusDepartureId = new SelectList(sllStatusQuerry, "Value", "Text", search.StatusDepartureId);

            var roles = _db.tbl_Role;
            List<SelectListItem> sllRoles = new List<SelectListItem>();
            foreach (var item in roles)
            {
                SelectListItem itemSLL = new SelectListItem();
                itemSLL.Text = item.Name;
                itemSLL.Value = item.RoleId.ToString();
                if (listRoleId.Contains(item.RoleId))
                {
                    itemSLL.Selected = true;
                }

                sllRoles.Add(itemSLL);
            }
            ViewBag.ListRoles = sllRoles;

            //var departures = _db.tbl_departure;
            //List<SelectListItem> sllDepartures = new List<SelectListItem>();
            //foreach (var item in departures)
            //{
            //    SelectListItem itemSLL = new SelectListItem();
            //    itemSLL.Text = item.departure_name;
            //    itemSLL.Value = item.departure_id.ToString();
            //    if (listDepartureId.Contains(item.departure_id))
            //    {
            //        itemSLL.Selected = true;
            //    }

            //    sllDepartures.Add(itemSLL);
            //}
            //ViewBag.ListDepartures = sllDepartures;

            session["SearchAccount"] = search;
            ViewBag.Search = search;

            //var groupApplications = _db.GroupApplications;
            //ViewBag.GroupApplicationId = new SelectList(groupApplications, "GroupApplicationId", "Name", search.GroupApplicationId);

            return View(listUsers.ToList());
        }

    }
}