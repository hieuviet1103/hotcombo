﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Custom.Security;
using Hotel.Models;
using System.Globalization;
using Hotel.Controllers;
using Hotel.EntityFramework.Models;
using Newtonsoft.Json;

namespace Hotel.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class RoomAvailableController : BaseController //Controller
    {
        private readonly HotelEntities _db = new HotelEntities();

        // GET: AdminHotel/RoomAvailable

        public ActionResult Create(string id = "")
        {

            Guid hotelId = Guid.Empty;
            Guid roomId = Guid.Empty;

            Guid.TryParse(id, out hotelId);

            this.LoadDropDownList(hotelId, roomId, 0);


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoomPrice model)
        {
            if (ModelState.IsValid)
            {
                if (this.SaveRoomPriceSchedule(model))
                {
                    ViewBag.Status = "Success";
                }
                else
                {
                    ViewBag.Status = "Error";
                }
            }
            else
            {
                ViewBag.Status = "Error";
            }

            this.LoadDropDownList(model.HotelId, model.RoomId, 0);

            return View(model);
        }

        //[HttpGet]
        public ActionResult _GetCalendarData(Guid roomId, string dateFrom, string dateTo)
        {
            Session["RoomId"] = roomId;
            // Initialization.
            JsonResult result = new JsonResult();
            List<CalendarDataView> calendarDataList = new List<CalendarDataView>();
            try
            {
                // Loading.                
                if (string.IsNullOrEmpty(dateFrom))
                {
                    dateFrom = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy HH:mm:ss");
                }
                else
                {
                    dateFrom = dateFrom + " 00:00:00";
                }

                if (string.IsNullOrEmpty(dateTo))
                {
                    dateTo = DateTime.Now.AddDays(30).ToString("dd/MM/yyyy HH:mm:ss");
                }
                else
                {
                    dateTo = dateTo + " 23:59:59";
                }

                DateTime _dateFrom = DateTime.Parse(dateFrom);
                DateTime _dateTo = DateTime.Parse(dateTo);

                var products = _db.tbl_RoomPrice.Include(p => p.tbl_Room.tbl_Hotel)
                    .Where(
                        p => p.RoomId == roomId
                        //&& _dateFrom <= p.DateAvailable
                        //&& p.DateAvailable <= _dateTo
                        && p.IsDelete != true
                    ).ToList();

                if (products != null)
                {
                    foreach (var p in products)
                    {
                        string checkBox = "\n <label>Hoạt động</label> <input id='{0}' onchange=\"ChangeStatusPrice('{1}',this)\" style='cursor: pointer;' type='checkbox' {2} />  ";
                        var _title = "SL còn lại: " + p.AvailableRoom.ToString()
                                + "\nGiá : " + p.PriceContract.ToString("#,###")
                                + "\nGiá KM: " + p.PricePromotion?.ToString("#,###")
                                + string.Format(checkBox, p.Id.ToString(), p.Id.ToString(), (p.IsActive.Value == true ? "checked='checked'" : ""));

                        var _description = "";

                        if (p.RoomDate >= DateTime.Now.Date)
                        {
                            if (p.IsActive.Value)
                            {
                                calendarDataList.Add(new CalendarDataView
                                {
                                    title = MvcHtmlString.Create(_title).ToHtmlString(),
                                    description = _description,
                                    start = (DateTime)p.RoomDate,
                                    end = (DateTime)p.RoomDate,
                                    backgroundColor = "#0ab80a",
                                    borderColor = "#00b300"
                                });
                            }
                            else
                            {
                                calendarDataList.Add(new CalendarDataView
                                {
                                    title = MvcHtmlString.Create(_title).ToHtmlString(),
                                    description = _description,
                                    start = (DateTime)p.RoomDate,
                                    end = (DateTime)p.RoomDate,
                                    backgroundColor = "#ff1b1b",
                                    borderColor = "#00b300"
                                });
                            }
                        }
                        else
                        {
                            calendarDataList.Add(new CalendarDataView
                            {
                                title = _title,
                                description = _description,
                                start = (DateTime)p.RoomDate,
                                end = (DateTime)p.RoomDate,
                                backgroundColor = "#556655",
                                borderColor = "#00b300"
                            });
                        }
                    }
                }

                // Processing.
                result = this.Json(calendarDataList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
                result = this.Json(calendarDataList, JsonRequestBehavior.AllowGet);

            }

            // Return info.
            return result;
        }

        public ActionResult _ChangeStatusPrice(Guid PriceId, bool Status)
        {
            var products = _db.tbl_RoomPrice.Include(p => p.tbl_Room.tbl_Hotel)
                    .FirstOrDefault(
                        p => p.Id == PriceId
                        && p.IsDelete != true
                    );

            if (products != null)
            {
                //Change Status
                products.IsActive = Status;
                products.UserUpdate = User.UserId;
                products.DateUpdate = DateTime.Now;
                _db.SaveChanges();
                return this.Json(new { id = 1, message = "Đã lưu điều chỉnh" }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new { id = 0, message = "Điều chỉnh không thành công" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult _GetHotelRoom(Guid hotelId)
        {
            var roomList = _db.tbl_Room.Where(r => r.HotelId == hotelId && r.IsDelete != true).ToList().Select(d => new { d.Id, d.RoomName, d.HotelId });
            Session["HotelId"] = hotelId;
            return Json(roomList, JsonRequestBehavior.AllowGet);
        }

        protected bool SaveRoomPriceSchedule(RoomPrice product)
        {
            try
            {
                if (product.DateType == "DateFromTo")
                {
                    if (product.DateFrom != null && product.DateTo != null)
                    {
                        DateTime dateAvailableFrom = (DateTime)product.DateFrom;
                        DateTime dateAvailableTo = (DateTime)product.DateTo;
                        double roomPriceContract = product.PriceContract;
                        double roomPricePromotion = product.PricePromotion.Value;

                        if (dateAvailableFrom < DateTime.Now.Date)
                        {
                            dateAvailableFrom = DateTime.Now.Date;
                        }

                        for (DateTime date = dateAvailableFrom.Date; date <= dateAvailableTo.Date; date = date.AddDays(1.0))
                        {
                            SavePrice(product, date);
                        }

                        return true;
                    }
                }
                if (product.DateType == "DateSelect")
                {
                    foreach (var date in product.DateList.Split(','))
                    {
                        SavePrice(product, Convert.ToDateTime(date));
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        protected void SavePrice(RoomPrice product, DateTime date)
        {
            var roomPriceExist = _db.tbl_RoomPrice.Where(p => p.RoomId == product.RoomId && p.RoomDate == date).SingleOrDefault();

            if (roomPriceExist != null)
            {
                //Exist roomPrice: update                          
                roomPriceExist.HotelPriceTypeId = (int)Parameters.HotelPriceType.GiaThuong; //product.HotelPriceTypeId;
                roomPriceExist.PriceContract = product.PriceContract;
                roomPriceExist.PricePromotion = product.PricePromotion;
                roomPriceExist.PercentPromotion = Convert.ToDouble(String.Format("{0:0.00}", ((product.PriceContract - product.PricePromotion) / product.PriceContract * 100)));
                roomPriceExist.AvailableRoom = product.AvailableRoom;
                roomPriceExist.IsActive = true;
                roomPriceExist.UserUpdate = User.UserId;
                roomPriceExist.DateUpdate = DateTime.Now;

                _db.Entry(roomPriceExist).State = EntityState.Modified;
            }
            else
            {
                //Not exist product: insert new
                var roomPrice = new tbl_RoomPrice()
                {
                    Id = Guid.NewGuid(),
                    RoomId = product.RoomId,
                    HotelId = product.HotelId,
                    RoomDate = date,
                    AvailableRoom = product.AvailableRoom,
                    HotelPriceTypeId = (int)Parameters.HotelPriceType.GiaThuong, //product.HotelPriceTypeId,
                    PriceContract = product.PriceContract,
                    PricePromotion = product.PricePromotion,
                    PercentPromotion = Convert.ToDouble(String.Format("{0:0.00}", ((product.PriceContract - product.PricePromotion) / product.PriceContract * 100))),
                    IsActive = true,
                    IsDelete = false,
                    DateCreate = DateTime.Now,
                    UserCreate = User.UserId,

                };
                _db.tbl_RoomPrice.Add(roomPrice);
            }

            _db.SaveChanges();
        }

        protected void LoadDropDownList(Guid defaultHotelId, Guid defaultRoomId, int defaultRoomPriceTypeId)
        {
            ViewBag.HotelId = _db.tbl_Hotel.Where(d => d.IsDelete == false).Select(item => new SelectListItem
            {
                Value = item.Id.ToString(),
                Text = item.HotelName,
                Selected = item.Id == defaultHotelId
            }).OrderBy(d => d.Text).ToList();

            ViewBag.sllRoom = _db.tbl_Room.Include(b => b.tbl_Hotel).Where(r => r.HotelId == defaultHotelId && r.IsDelete != true).Select(item => new SelectListItem
            {
                Value = item.Id.ToString(),
                Text = item.RoomName,
                Selected = item.Id == defaultRoomId
            }).OrderBy(d => d.Value).ToList();

            ViewBag.RoomPriceType = _db.tbl_HotelPriceType.Select(item => new SelectListItem
            {
                Value = item.Id.ToString(),
                Text = item.PriceTypeName,
                Selected = item.Id == defaultRoomPriceTypeId
            }).OrderBy(d => d.Text).ToList();
        }
    }
}