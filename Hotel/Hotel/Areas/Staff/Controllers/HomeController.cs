﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.EntityFramework.Models;
using Admin.Helpers;
using System.Net.Mail;
using Custom.Security;
using Hotel.Models;

namespace Hotel.Areas.Staff.Controllers
{
    [CustomAuthorize(Roles = "Admin,Staff")]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToActionPermanent("Index", "Booking");
            //return View();
        }
    }
}