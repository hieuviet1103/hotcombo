﻿using Hotel.Common.Utilities;
using Hotel.EntityFramework.Models;
using Hotel.IDataAccess;
using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Hotel.DataAccess
{
    public class HotelDataAccess : IHotelDataAccess
    {
		public List<HotelModel> HotelDanhSach(SearchViewModel model)
		{
			try
			{
				var listParameter = new List<SqlParameter>
				{
					new SqlParameter("@typeInput", model.TypeInput),
					new SqlParameter("@itemId", model.ItemId),
                    new SqlParameter("@startDate", model.StartDate),
					new SqlParameter("@endDate", model.EndDate),
					new SqlParameter("@numberRoom", model.NumberRoom),
					new SqlParameter("@numberAdul", model.NumberAdul),
					new SqlParameter("@numberChildren", model.NumberChildren),
					new SqlParameter("@priceFrom", model.PriceFrom * 1000),
					new SqlParameter("@priceTo", model.PriceTo * 1000),
					new SqlParameter("@hotelPriceType", model.HotelPriceType),
					new SqlParameter("@hotelRatingType", model.HotelRatingType),
					new SqlParameter("@hotelType", model.HotelType),
					new SqlParameter("@hotelServiceType", model.HotelServiceType),
					new SqlParameter("@hotelConvenientType", model.HotelConvenientType),
					new SqlParameter("@hotelArea", model.HotelArea),
					new SqlParameter("@orderPrice", model.OrderPrice),
					new SqlParameter("@orderRating", model.OrderRating)
				};
				return DBUtils.ExecuteSPList<HotelModel>("sp_hotel_list", listParameter);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public List<CommentModel> HotelCommentAvg(string id)
		{
			try
			{
				var listParameter = new List<SqlParameter>
				{
					new SqlParameter("@id", id)
				};
				return DBUtils.ExecuteSPList<CommentModel>("sp_hotel_review", listParameter);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

        public List<HotelCommentSynModel> HotelCommentSyn()
        {
            try
            {
				var listParameter = new List<SqlParameter>
				{
				};
				return DBUtils.ExecuteSPList<HotelCommentSynModel>("sp_hotel_comment", listParameter);
			}
			catch(Exception e)
            {
				Console.WriteLine(e);
				throw;
			}
        }

        public List<HotelCommentModel> HotelCommentByHotelId(string id)
        {
            try
            {
				var listParameter = new List<SqlParameter>
				{
					new SqlParameter("@id", id)
				};
				return DBUtils.ExecuteSPList<HotelCommentModel>("sp_hotel_comment_by_hotelid", listParameter);
			}
			catch(Exception e)
            {
				Console.WriteLine(e);
				throw;
			}
        }
    }
}