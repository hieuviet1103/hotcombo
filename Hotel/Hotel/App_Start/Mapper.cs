﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hotel.Models;
using Hotel.EntityFramework.Models;

namespace Hotel
{
    public static class Mapper
    {
        private static IMapper _mapper;
        public static void RegisterMapping()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                #region Hotel
                cfg.CreateMap<tbl_Hotel, HotelViewModel>();
                //.ForMember(dto => dto.Id, otp => otp.MapFrom(src => src.Id));
                //.ForMember(dto => dto.HotelName, otp => otp.MapFrom(src => src.HotelName))
                //.ForMember(dto => dto.CountryId, otp => otp.MapFrom(src => src.CountryId))
                //.ForMember(dto => dto.ProvinceId, otp => otp.MapFrom(src => src.ProvinceId))
                //.ForMember(dto => dto.DistrictId, otp => otp.MapFrom(src => src.DistrictId))
                //.ForMember(dto => dto.AreaId, otp => otp.MapFrom(src => src.AreaId))
                //.ForMember(dto => dto.HotelRatingTypeId, otp => otp.MapFrom(src => src.HotelRatingTypeId))
                //.ForMember(dto => dto.HotelTypeId, otp => otp.MapFrom(src => src.HotelTypeId))
                //.ForMember(dto => dto.HotelConvenientType, otp => otp.MapFrom(src => src.HotelConvenientType))
                //.ForMember(dto => dto.Address, otp => otp.MapFrom(src => src.Address))
                //.ForMember(dto => dto.Telephone, otp => otp.MapFrom(src => src.Telephone))
                //.ForMember(dto => dto.Mobile, otp => otp.MapFrom(src => src.Mobile))
                //.ForMember(dto => dto.Longitude, otp => otp.MapFrom(src => src.Longitude))
                //.ForMember(dto => dto.Latitude, otp => otp.MapFrom(src => src.Latitude))
                //.ForMember(dto => dto.Content, otp => otp.MapFrom(src => src.Content))
                //.ForMember(dto => dto.Email, otp => otp.MapFrom(src => src.Email))
                //.ForMember(dto => dto.Url, otp => otp.MapFrom(src => src.Url))
                //.ForMember(dto => dto.Image, otp => otp.MapFrom(src => src.Image))
                //.ForMember(dto => dto.IsActive, otp => otp.MapFrom(src => src.IsActive))
                //.ForMember(dto => dto.IsDelete, otp => otp.MapFrom(src => src.IsDelete))
                //.ForMember(dto => dto.UserCreate, otp => otp.MapFrom(src => src.UserCreate))
                //.ForMember(dto => dto.DateCreate, otp => otp.MapFrom(src => src.DateCreate))
                //.ForMember(dto => dto.UserUpdate, otp => otp.MapFrom(src => src.UserUpdate))
                //.ForMember(dto => dto.DateUpdate, otp => otp.MapFrom(src => src.DateUpdate))
                //.ForMember(dto => dto.PriceFrom, otp => otp.MapFrom(src => src.PriceFrom))
                //.ForMember(dto => dto.PricePromotion, otp => otp.MapFrom(src => src.PricePromotion));

                cfg.CreateMap<HotelViewModel, tbl_Hotel>();
                    //.ForMember(dto => dto.Id, otp => otp.MapFrom(src => src.Id))
                    //.ForMember(dto => dto.HotelName, otp => otp.MapFrom(src => src.HotelName))
                    //.ForMember(dto => dto.CountryId, otp => otp.MapFrom(src => src.CountryId))
                    //.ForMember(dto => dto.ProvinceId, otp => otp.MapFrom(src => src.ProvinceId))
                    //.ForMember(dto => dto.DistrictId, otp => otp.MapFrom(src => src.DistrictId))
                    //.ForMember(dto => dto.AreaId, otp => otp.MapFrom(src => src.AreaId))
                    //.ForMember(dto => dto.HotelRatingTypeId, otp => otp.MapFrom(src => src.HotelRatingTypeId))
                    //.ForMember(dto => dto.HotelTypeId, otp => otp.MapFrom(src => src.HotelTypeId))
                    //.ForMember(dto => dto.HotelConvenientType, otp => otp.MapFrom(src => src.HotelConvenientType))
                    //.ForMember(dto => dto.Address, otp => otp.MapFrom(src => src.Address))
                    //.ForMember(dto => dto.Telephone, otp => otp.MapFrom(src => src.Telephone))
                    //.ForMember(dto => dto.Mobile, otp => otp.MapFrom(src => src.Mobile))
                    //.ForMember(dto => dto.Longitude, otp => otp.MapFrom(src => src.Longitude))
                    //.ForMember(dto => dto.Latitude, otp => otp.MapFrom(src => src.Latitude))
                    //.ForMember(dto => dto.Content, otp => otp.MapFrom(src => src.Content))
                    //.ForMember(dto => dto.Email, otp => otp.MapFrom(src => src.Email))
                    //.ForMember(dto => dto.Url, otp => otp.MapFrom(src => src.Url))
                    //.ForMember(dto => dto.Image, otp => otp.MapFrom(src => src.Image))
                    //.ForMember(dto => dto.IsActive, otp => otp.MapFrom(src => src.IsActive))
                    //.ForMember(dto => dto.IsDelete, otp => otp.MapFrom(src => src.IsDelete))
                    //.ForMember(dto => dto.UserCreate, otp => otp.MapFrom(src => src.UserCreate))
                    //.ForMember(dto => dto.DateCreate, otp => otp.MapFrom(src => src.DateCreate))
                    //.ForMember(dto => dto.UserUpdate, otp => otp.MapFrom(src => src.UserUpdate))
                    //.ForMember(dto => dto.DateUpdate, otp => otp.MapFrom(src => src.DateUpdate))
                    //.ForMember(dto => dto.PriceFrom, otp => otp.MapFrom(src => src.PriceFrom))
                    //.ForMember(dto => dto.PricePromotion, otp => otp.MapFrom(src => src.PricePromotion));
                #endregion
                #region Room
                cfg.CreateMap<tbl_Room, RoomViewModel>()
                    .ForMember(dto => dto.Id, otp => otp.MapFrom(src => src.Id))
                    .ForMember(dto => dto.HotelId, otp => otp.MapFrom(src => src.HotelId))
                    .ForMember(dto => dto.RoomName, otp => otp.MapFrom(src => src.RoomName))
                    .ForMember(dto => dto.AvailableRoom, otp => otp.MapFrom(src => src.AvailableRoom))
                    .ForMember(dto => dto.RoomArea, otp => otp.MapFrom(src => src.RoomArea))
                    .ForMember(dto => dto.Direction, otp => otp.MapFrom(src => src.Direction))
                    .ForMember(dto => dto.SingleBed, otp => otp.MapFrom(src => src.SingleBed))
                    .ForMember(dto => dto.DoubleBed, otp => otp.MapFrom(src => src.DoubleBed))
                    .ForMember(dto => dto.MaxPeople, otp => otp.MapFrom(src => src.MaxPeople))
                    .ForMember(dto => dto.HotelConvenientType, otp => otp.MapFrom(src => src.HotelConvenientType))
                    .ForMember(dto => dto.Image, otp => otp.MapFrom(src => src.Image))
                    .ForMember(dto => dto.HotelServiceType, otp => otp.MapFrom(src => src.HotelServiceType))
                    .ForMember(dto => dto.IsCancel, otp => otp.MapFrom(src => src.IsCancel))
                    .ForMember(dto => dto.IsSurcharge, otp => otp.MapFrom(src => src.IsSurcharge))
                    .ForMember(dto => dto.IsDelete, otp => otp.MapFrom(src => src.IsDelete))
                    .ForMember(dto => dto.UserCreate, otp => otp.MapFrom(src => src.UserCreate))
                    .ForMember(dto => dto.DateCreate, otp => otp.MapFrom(src => src.DateCreate))
                    .ForMember(dto => dto.UserUpdate, otp => otp.MapFrom(src => src.UserUpdate))
                    .ForMember(dto => dto.DateUpdate, otp => otp.MapFrom(src => src.DateUpdate));

                cfg.CreateMap<RoomViewModel, tbl_Room>()
                .ForMember(dto => dto.Id, otp => otp.MapFrom(src => src.Id))
                .ForMember(dto => dto.HotelId, otp => otp.MapFrom(src => src.HotelId))
                .ForMember(dto => dto.RoomName, otp => otp.MapFrom(src => src.RoomName))
                .ForMember(dto => dto.AvailableRoom, otp => otp.MapFrom(src => src.AvailableRoom))
                .ForMember(dto => dto.RoomArea, otp => otp.MapFrom(src => src.RoomArea))
                .ForMember(dto => dto.Direction, otp => otp.MapFrom(src => src.Direction))
                .ForMember(dto => dto.SingleBed, otp => otp.MapFrom(src => src.SingleBed))
                .ForMember(dto => dto.DoubleBed, otp => otp.MapFrom(src => src.DoubleBed))
                .ForMember(dto => dto.MaxPeople, otp => otp.MapFrom(src => src.MaxPeople))
                .ForMember(dto => dto.HotelConvenientType, otp => otp.MapFrom(src => src.HotelConvenientType))
                .ForMember(dto => dto.Image, otp => otp.MapFrom(src => src.Image))
                .ForMember(dto => dto.HotelServiceType, otp => otp.MapFrom(src => src.HotelServiceType))
                .ForMember(dto => dto.IsCancel, otp => otp.MapFrom(src => src.IsCancel))
                .ForMember(dto => dto.IsSurcharge, otp => otp.MapFrom(src => src.IsSurcharge))
                .ForMember(dto => dto.IsDelete, otp => otp.MapFrom(src => src.IsDelete))
                .ForMember(dto => dto.UserCreate, otp => otp.MapFrom(src => src.UserCreate))
                .ForMember(dto => dto.DateCreate, otp => otp.MapFrom(src => src.DateCreate))
                .ForMember(dto => dto.UserUpdate, otp => otp.MapFrom(src => src.UserUpdate))
                .ForMember(dto => dto.DateUpdate, otp => otp.MapFrom(src => src.DateUpdate));

                #endregion
            });
            _mapper = mapperConfiguration.CreateMapper();
        }


        #region Hotel
        public static HotelViewModel MapFrom(tbl_Hotel data)
        {
            return _mapper.Map<tbl_Hotel, HotelViewModel>(data);
        }
        public static IList<HotelViewModel> MapFrom(IList<tbl_Hotel> data)
        {
            return _mapper.Map<IList<tbl_Hotel>, IList<HotelViewModel>>(data);
        }
        public static tbl_Hotel MapTo(HotelViewModel data)
        {
            return _mapper.Map<HotelViewModel, tbl_Hotel>(data);
        }
        #endregion
        #region Room
        public static RoomViewModel MapFrom(tbl_Room data)
        {
            return _mapper.Map<tbl_Room, RoomViewModel>(data);
        }
        public static IList<RoomViewModel> MapFrom(IList<tbl_Room> data)
        {
            return _mapper.Map<IList<tbl_Room>, IList<RoomViewModel>>(data);
        }
        public static tbl_Room MapTo(RoomViewModel data)
        {
            return _mapper.Map<RoomViewModel, tbl_Room>(data);
        }
        #endregion
    }
}