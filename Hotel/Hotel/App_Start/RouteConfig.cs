﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Hotel
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //khach-san
            routes.MapRoute(
                name: "Danh sách khách sạn",
                url: "khach-san",
                defaults: new { controller = "Hotel", action = "Index", TypeInput = 1, ItemId = "Resfresh" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //khach-san/tp76/khach-san-tai-an-giang.aspx
            routes.MapRoute(
                name: "Khách sạn theo tỉnh thành",
                url: "khach-san/tp{ItemId}/khach-san-tai-{ProvinceName}.aspx",
                defaults: new { controller = "Hotel", action = "Index", TypeInput = 1 },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //khach-san-holiday-v64477619-22da-45d0-9d24-8c63124e0d82.aspx
            routes.MapRoute(
                name: "Chi tiết khách sạn",
                url: "khach-san/{Keyword}-v{ItemId}.aspx",
                defaults: new { controller = "Hotel", action = "Detail", TypeInput = 0 },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //cam-nang-du-lich
            routes.MapRoute(
                name: "Danh sách cẩm nang du lịch",
                url: "cam-nang-du-lich",
                defaults: new { controller = "TravelGuide", action = "Index" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //cam-nang-du-lich/tin-tuc-du-lich-mua-covid-vcd6dfbac-874f-49d6-a594-3b6cececa2fa.aspx
            routes.MapRoute(
                name: "Chi tiết cẩm nang du lịch",
                url: "cam-nang-du-lich/{TravelGuideName}-v{id}.aspx",
                defaults: new { controller = "TravelGuide", action = "Details" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //cam-nang-du-lich/di-san-van-hoa-c16
            routes.MapRoute(
                name: "Danh sách cẩm nang du lịch theo danh mục",
                url: "cam-nang-du-lich/{CategoryName}-c-{Category}",
                defaults: new { controller = "TravelGuide", action = "Index" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //dang-ky-thanh-vien
            routes.MapRoute(
                name: "Đăng ký thành viên",
                url: "dang-ky-thanh-vien",
                defaults: new { controller = "AccountCustomer", action = "Register" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //privacy-cookies.aspx
            routes.MapRoute(
                name: "Static Page",
                url: "{Type}.aspx",
                defaults: new { controller = "StaticPage", action = "Index" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            //lien-he
            routes.MapRoute(
                name: "Liên hệ",
                url: "lien-he",
                defaults: new { controller = "Contact", action = "Index" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            routes.MapRoute(
                name: "LoadContentHTML",
                url: "{ContenCode}.html",
                defaults: new { controller = "staticpage", action = "LoadContent" },
                namespaces: new string[] { "Hotel.Controllers" }
            );

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new[] { "Hotel.Controllers" }
            );

        }
    }
}
