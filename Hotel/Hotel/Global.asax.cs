﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Optimization;
using System.Web.Routing;
using Custom.Security;
using Newtonsoft.Json;

namespace Hotel
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                    CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                    CustomPrincipal newUser = new CustomPrincipal(authTicket.Name)
                    {
                        UserId = serializeModel.UserId,
                        RoleId = serializeModel.RoleId,
                        UserName = serializeModel.UserName,
                        FullName = serializeModel.FullName,
                        Email = serializeModel.Email,
                        ApplicationId = serializeModel.ApplicationId,
                        DepartureId = serializeModel.DepartureId,
                        Roles = serializeModel.Roles
                    };
                    //newUser.Applications.AddRange(serializeModel.Applications);
                    //newUser.Roles.AddRange(serializeModel.Roles);
                    HttpContext.Current.User = newUser;
                }
                catch (Exception ex){ }
            }

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Language"];
            if (cookie != null && cookie.Value != null)
            {
                //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cookie.Value);
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("cy");  //use for format datetime call back from client.
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cookie.Value);
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("cy");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("cy");
            }
        }
        protected void Application_EndRequest()
        {
            //if (Context.Response.StatusCode == 404)
            //{
            //    Response.Clear();

            //    var rd = new RouteData();
            //    rd.DataTokens["area"] = ""; // In case controller is in another area
            //    rd.Values["controller"] = "Errors";
            //    rd.Values["action"] = "NotFound";

            //    IController c = new ErrorsController();
            //    c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
            //}
            //else if (Context.Response.StatusCode == 500)
            //{
            //    Response.Clear();

            //    var rd = new RouteData();
            //    rd.DataTokens["area"] = ""; // In case controller is in another area
            //    rd.Values["controller"] = "Errors";
            //    rd.Values["action"] = "Error";

            //    IController c = new ErrorsController();
            //    c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
            //}
        }
    }
}
