﻿using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.IDataAccess
{
    public interface IHotelDataAccess
    {
        List<HotelModel> HotelDanhSach(SearchViewModel model);
        List<CommentModel> HotelCommentAvg(string id);
        List<HotelCommentSynModel> HotelCommentSyn();
        List<HotelCommentModel> HotelCommentByHotelId(string id);
    }
}
