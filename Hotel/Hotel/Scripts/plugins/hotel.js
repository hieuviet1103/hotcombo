﻿function ShowNotify(_message, _type, _postion) {
    setTimeout(function () {
        $.notify({
            message: _message
        },
        {
            type: _type,
            placement: {
                from: _postion
            },
            animate: {
                enter: "animated fadeInRight",
                exit: "animated fadeOutRight"
            },
            newest_on_top: false
        });
    }, 500);
}

$(function () {
    if ($.validator != null) {
        $.validator.addMethod('date',
        function (value, element) {
            if (this.optional(element)) {
                return true;
            }
            var ok = true;
            try {
                $.datepicker.parseDate('dd/mm/yy', value);
            }
            catch (err) {
                ok = false;
            }
            return ok;
        });
        //$(".datefield").datepicker({ dateFormat: 'dd/mm/yy', changeYear: true });
    }
});

//====== BaseFunction.js ======//
function PrintElement(elem) {
    var mywindow = window.open('', '', 'height=800,width=900');
    mywindow.document.write('<html><head><title></title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write($(elem).html());
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}

function ShowNotify(_message, _type, _postion) {
    setTimeout(function () {
        $.notify({
            message: _message
        }, {
            type: _type,
            placement: {
                from: _postion
            },
            animate: {
                enter: "animated fadeInRight",
                exit: "animated fadeOutRight"
            }
        });
    }, 500);
}

function CreateEditor(controlID) {

    if (CKEDITOR.instances[controlID] != 'undefined') {

        CKEDITOR.replace(controlID,
                {
                    filebrowserBrowseUrl: '../ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '../ckfinder/ckfinder.html?Type=Images',
                    filebrowserFlashBrowseUrl: '../ckfinder/ckfinder.html?Type=Flash',
                    filebrowserUploadUrl: '../ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '../ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl: '../ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash'
                }
        );
    }
}
function Confirm() {
    if (confirm('Bạn có chắc chắn muốn hoàn tất thao tác này?')) {
        return true;
    }
    return false;
}

function ConfirmDelete() {
    if (confirm('Do you really want to delete?')) {
        return true;
    }
    return false;
}

function ConfirmInsert() {
    if (confirm('Bạn có muốn thêm không?')) {
        return true;
    }
    return false;
}

function ConfirmClose() {
    if (confirm('Bạn có muốn tắt cửa sổ này không?')) {
        return true;
    }
    return false;
}
// JScript File

function CheckAll(grid) {
    var grv = document.getElementById(grid);
    for (i = 1 ; i < grv.rows.length ; i++) {
        for (j = 0 ; j < grv.rows[i].cells.length ; j++) {
            for (k = 0 ; k < grv.rows[i].cells[j].childNodes.length ; k++);
            {
                if (window.ActiveXObject) {
                    if (grv.rows[i].cells[j].childNodes[0].nodeType == 1 && grv.rows[i].cells[j].childNodes[0].type == 'checkbox') {
                        if (grv.rows[0].cells[j].childNodes[0].checked) {
                            grv.rows[i].cells[j].childNodes[0].checked = true;
                        }
                        else {
                            grv.rows[i].cells[j].childNodes[0].checked = false;
                        }
                    }

                } else if (document.implementation && document.implementation.createDocument) {
                    if (grv.rows[i].cells[j].childNodes[1].nodeType == 1 && grv.rows[i].cells[j].childNodes[1].type == 'checkbox') {
                        if (grv.rows[0].cells[j].childNodes[1].checked) {
                            grv.rows[i].cells[j].childNodes[1].checked = true;
                        }
                        else {
                            grv.rows[i].cells[j].childNodes[1].checked = false;
                        }
                    }
                } else {
                    alert('Your browser cannot handle this script');
                    return;
                }
                ////////////////////////////////////////////////////////////////////////
            }
        }
    }
}

function SelectAllCheckbox(id) {
    $(function () {
        $(id).click(function () {
            var ckb = this;
            if ($(ckb).is(':checked')) {
                $('[type=\'checkbox\']').attr('checked', 'checked');
            }
            else {
                $('[type=\'checkbox\']').removeAttr('checked');
            }

        });
    });
}

function CheckAllInGrid(grid, flag) {

    $('#' + grid + ' tr').find(':checkbox').attr('checked', flag);
}

function checkchecked() {
    var vari = readCookie('vtv_tour_id');
    if (vari == null) {
        alert('Bạn chưa chọn ảnh mặt định cho sản phẩm.')
    }
}

function addcookie(value) {
    eraseCookie();
    createCookie('vtv_tour_id', value, 1);
}

function DefineBrowser() {
    // Neu trinh duyet la IE thi return "IE"
    // Neu trinh duyet la FireFox thi return "FF"

    return checkbrowser();
}

function checkbrowser() {

    var browsertype = navigator.userAgent.toLowerCase();
    if (browsertype.indexOf('msie') != -1) {
        browser = 'IE';
        return browser;
    }
    else if (browsertype.indexOf('netscape') != -1) {
        browser = 'NS';
        return browser;
    }
    else if (browsertype.indexOf('safari') != -1) {
        browser = 'SF';
        return browser;
    }
    else if (browsertype.indexOf('gecko') != -1) {
        browser = 'FF';
        return browser;
    }
    return browser;
}

function OpenPopup(name, width, height, url) {
    var winW = 800, winH = 600;
    winW = screen.width;
    winH = screen.height;
    var posW = (winW - width) / 2;
    var posH = (winH - height) / 2;
    var attributes = "width=" + width + ", height=" + height + ", top=" + posH + ",left=" + posW + ",menubar=no, resizable=no,scrollbars=yes, status=1";
    window.open(url, name, attributes);
}

//Ham xu ly, chi cho phep go vao nhung ky tu minh muon
function keyRestrict(e, validchars) {
    var key = '', keychar = '';
    key = getKeyCode(e);
    if (key == null) return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();
    validchars = '-+' + validchars.toLowerCase();
    if (validchars.indexOf(keychar) != -1)
        return true;    
    if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27 || (e.ctrlKey && key == 99) || (e.ctrlKey && key == 118))
        return true;
    return false;
}

function keyRestrict_notlist(e, validchars) {
    var key = '', keychar = '';
    key = getKeyCode(e);
    if (key == null) return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();
    validchars = validchars.toLowerCase();
    if (validchars.indexOf(keychar) == -1)
        return true;
    if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
        return true;
    return false;
}

function getKeyCode(e) {
    if (window.event)
        return window.event.keyCode;
    else if (e)
        return e.which;
    else
        return null;
}

function funCheckInt(e) {
    return keyRestrict(e, '-0123456789');
}

function funCheckMoney(e) {
    return keyRestrict(e, '-0123456789');
}

function funCheckTelephone(e) {
    return keyRestrict(e, '-0123456789;');
}

function funCheckAdress(e) {
    return keyRestrict_notlist(e, '.;');
}

function formatnumber(what) {
    var _value = what.value;
    // do replace dau ' bi chi 1 lan thoi
    _value = _value.replace(',', '');
    _value = _value.replace(',', '');
    _value = _value.replace(',', '');
    var temp = '';
    var test = _value.length;
    var count = 0;
    for (i = test - 1; i >= 0; i--) {
        count++
        temp = _value.charAt(i) + temp;
        if (count % 3 == 0 && i > 0) {
            temp = ',' + temp;
        }
    }
    what.value = temp;
}

function formatnumber_ob(what) {
    var _value = what.value;
    // do replace dau ' bi chi 1 lan thoi
    _value = _value.replace('.', '');
    _value = _value.replace('.', '');
    _value = _value.replace('.', '');
    var temp = '';
    var test = _value.length;
    var count = 0;
    for (i = test - 1; i >= 0; i--) {
        count++
        temp = _value.charAt(i) + temp;
        if (count % 3 == 0 && i > 0) {
            temp = '.' + temp;
        }
    }
    what.value = temp;
}

function getObjby(name) {
    if (document.getElementById) {
        return document.getElementById(name);
    } else if (document.all) {
        return document.all[name];
    } else if (document.layers) {
        return document.layers[name];
    }
}

// region show full date, time
function ShowSmartDate(_textbox) {
    var strdate = _textbox.value;
    var _date = new Date();
    var stryear = _date.getFullYear();
    if (strdate.indexOf('/') > 0) {
        var strdaymonth = strdate.split("/");
        if (strdaymonth.length <= 2)
            _textbox.value = fulldaymonth(strdaymonth[0]) + '/' + fulldaymonth(strdaymonth[1]) + '/' + stryear;
        else
            _textbox.value = fulldaymonth(strdaymonth[0]) + '/' + fulldaymonth(strdaymonth[1]) + '/' + fullyear(strdaymonth[2]);
    }
    else {
        //_textbox.focus();
    }
}
function ShowSmartTime(_textbox) {
    var strtime = _textbox.value;
    if (strtime != '') {
        if (strtime.indexOf(':') < 0) {
            _textbox.value = fulldaymonth(strtime) + ':' + '00';
        }
        else {
            var strhourmin = strtime.split(":");
            _textbox.value = fulldaymonth(strhourmin[0]) + ':' + fulldaymonth(strhourmin[1]);
        }
    }
}

function fulldaymonth(_value) {
    return Right('0' + _value, 2);
}

function fullyear(_value) {
    var _date = new Date();
    var stryear = _date.getFullYear() + 1;        // cho nam sau
    if (_value.length >= 4)
        return _value;
    else {
        if (_value <= (Right(stryear, 2) + 1))
            return '20' + Right(_value, 2);
        else
            return '19' + Right(_value, 2);
    }
}

function Right(str, n) {
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else {
        var iLen = String(str).length;
        return String(str).substring(iLen, iLen - n);
    }
}

function getObjby(name) {
    if (document.getElementById) {
        return document.getElementById(name);
    } else if (document.all) {
        return document.all[name];
    } else if (document.layers) {
        return document.layers[name];
    }
}
// end region show full date
//validate dropdownlist
function validatedropdownlist(obj, arg) {
    alert(selectControl);
    if (arg.Value == '')
        arg.IsValid = false;
}
//Chi cho phep go vao nhung ky tu minh muon


function replaceAll(varb, replaceThis, replaceBy) {
    newvarbarray = varb.split(replaceThis);
    newvarb = newvarbarray.join(replaceBy);
    return newvarb;
}

function FormatNumberND(text) {

    return replaceAll(text, ',', '');
}

function FormatNumberNDAfter(_value) {
    // do replace dau ' bi chi 1 lan thoi
    _value = _value.replace(',', '');
    _value = _value.replace(',', '');
    _value = _value.replace(',', '');
    var temp = '';
    var test = _value.length;
    var count = 0;
    for (i = test - 1; i >= 0; i--) {
        count++
        temp = _value.charAt(i) + temp;
        if (count % 3 == 0 && i > 0) {
            temp = ',' + temp;
        }
    }
    return temp;
}

function lowerRound(number, digits) {
    var num = Math.pow(10, digits);
    var result = parseInt(number * num);
    result = result / num;
    return result;
}

function ShowSmartTime1(_textbox) {

    if (_textbox != '') {
        if (_textbox.indexOf(':') < 0) {
            _textbox = fulldaymonth(_textbox) + ':' + '00';
        }
        else {
            var strhourmin = _textbox.split(":");
            _textbox = fulldaymonth(strhourmin[0]) + ':' + fulldaymonth(strhourmin[1]);
        }
    }
    return _textbox;
}

function clearFormatNumber(_value) {
    var temp = '';
    if (isEmpty(_value)) {
        return temp;
    }
    return replaceAll(_value, ',', '');

    //var test = _value.length;
    //var flag = 0;
    //for (i = 0; i < test; i++) {
    //    if (_value.charAt(i) != ',' && !isNaN(_value.charAt(i))) {
    //        temp += _value.charAt(i);
    //    }
    //    if (isNaN(_value.charAt(i)) && _value.charAt(i) != ',') {
    //        flag = 1;
    //    }
    //}
    //if (flag)
    //    alert('Vui lòng nhập số');
    //return temp;
}

Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function formatPrice(n, currency) {
    return currency + " " + n.toFixed(2).replace(/./g, function (c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

//Number.prototype.format = function () {
//    //n, x, s, c
//    //Number(amount_vn).format(0, 3, ',', '.');
//    var n = 0; x = 3, s = ',', c = '.';
//    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
//        num = this.toFixed(Math.max(0, ~~n));
//    //return num.replace(new RegExp(re, 'g'), '$&' + (s || ','));
//    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
//};

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function createCookie(name, value, minutes) {
    if (minutes) {
        var date = new Date();
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return "";
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function close_window() {
    window.close();
}

function sumbmitForm() {
    $('#preloader-wrapper').toggleClass('hide');
    setTimeout(closeloader, 5000);
}

function sumbmitFormReload(id) {    
    $('#preloader-wrapper').toggleClass('hide');
    setTimeout(closeloader, 5000);
}


function closeloader() {
    if ($("#preloader-wrapper").hasClass("hide") != true) {
        $('#preloader-wrapper').toggleClass('hide');
    }
}

function clearForm(formId) {
    var form = document.getElementById(formId);
    form.reset();

    var elements = form.elements;
    for (var i = 0; i < elements.length; i++) {
        field_type = elements[i].type.toLowerCase();
        switch (field_type) {

            case "text":
                elements[i].value = "";
            case "password":
                elements[i].value = "";
            case "textarea":
                elements[i].value = "";
            case "hidden":
                elements[i].value = "";
                break;
            case "radio":
            case "checkbox":
                if (elements[i].checked) {
                    elements[i].checked = false;
                }
                break;
            case "select-one":
                elements[i].selectedIndex = 0;
                break;
            case "select-multi":
                elements[i].selectedIndex = 0;
                break;
            default:
                break;
        }
    }
}

function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

function PopupFullscreen(url, title) {
    var params = [
    'height=' + screen.height,
    'width=' + screen.width,
    'fullscreen=yes' // only works in IE, but here for completeness
    ].join(',');
    // and any other options from
    // https://developer.mozilla.org/en/DOM/window.open

    var popup = window.open(url, title, params);
    popup.moveTo(0, 0);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

function isEmail(str) {
    if (str == '') return true;

    var objRegExp = /^([a-z_][a-z_0-9\-]*)(([\.][a-z_0-9]+)*)@([a-z_][a-z_0-9\-]*)(([\.][a-z_0-9]+)*)([\.][a-z]{2,3})$/i;
    //check for valid email
    return objRegExp.test(str);
}

function HighlightFocus(field, type)
{
    if (type == 'success') {
        $(field).animate({ 'backgroundColor': '#00a65a' }, 300);
        $(field).animate({ 'backgroundColor': '#fff' }, 500);
    }
    else if (type == 'danger')
    {
        $(field).animate({ 'backgroundColor': '#00a65a' }, 300);
        $(field).animate({ 'backgroundColor': '#fff' }, 500);
    }
    else if (type == 'warning') {
        $(field).animate({ 'backgroundColor': '#f39c12' }, 300);
        $(field).animate({ 'backgroundColor': '#fff' }, 500);
    }
}


String.prototype.format = String.prototype.f = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

//====== script.js ======//
$(document).ready(function () {
    $("#sh_money").hide();
    $("#sh_user").hide();
    $('#myTable').on('click', '.clickable-row', function (event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
    });

    $("#ch_money").click(function (e) {
        $("#sh_money").toggle();
        if ($("#ch_money i.money").hasClass("fa-caret-down")) {
            $("#ch_money i.money").removeClass("fa-caret-down");
            $("#ch_money i.money").addClass("fa-caret-up");
        } else {
            $("#ch_money i.money").addClass("fa-caret-down");
            $("#ch_money i.money").removeClass("fa-caret-up");
        }
    });

    $("#ch_close").click(function (e) {
        $("#sh_money").hide();
        if ($("#ch_money i.money").hasClass("fa-caret-down")) {
            $("#ch_money i.money").removeClass("fa-caret-down");
            $("#ch_money i.money").addClass("fa-caret-up");
        } else {
            $("#ch_money i.money").addClass("fa-caret-down");
            $("#ch_money i.money").removeClass("fa-caret-up");
        }
    });

    $("#ch_user").click(function (e) {
        $("#sh_user").toggle();
        if ($("#ch_user i.user").hasClass("fa-caret-down")) {
            $("#ch_user i.user").removeClass("fa-caret-down");
            $("#ch_user i.user").addClass("fa-caret-up");
        } else {
            $("#ch_user i.user").addClass("fa-caret-down");
            $("#ch_user i.user").removeClass("fa-caret-up");
        }
    });

    $("#car").click(function (e) {
        $("#car").prop("checked", true);
        $("#plane").prop("checked", false);
        $(".ttcb").addClass("hidden");
        $(".ttx").removeClass("hidden");
        $(".c-ttx-t").removeClass("hidden");
        $(".c-ttx-i").removeClass("hidden");
    });

    $("#plane").click(function (e) {
        $("#car").prop("checked", false);
        $("#plane").prop("checked", true);
        $(".ttcb").removeClass("hidden");
        $(".ttx").addClass("hidden");
        $(".c-ttx-i").addClass("hidden");
        $(".c-ttx-t").addClass("hidden");
    });

    $(".congty").click(function (e) {
        $("#congty").prop("checked", true);
        $("#sanbay").prop("checked", false);
    });

    $(".sanbay").click(function (e) {
        $("#sanbay").prop("checked", true);
        $("#congty").prop("checked", false);
    });

    $(".tim-add-func").click(function (e) {
        $(".tim-show-func").toggle();
    });

    $("[data-widget='collapse-header']").click(function () {
        //Find the box parent
        var box = $(this).parents(".box").first();
        var boxContent = box.find("> .box-body, > .box-footer");
        var btnCollapse = box.find("> .box-header i");

        if (!box.hasClass("collapsed-box")) {
            //Hide the content
            boxContent.slideUp(300, function () {
                box.addClass("collapsed-box");
            });
            btnCollapse.removeClass("fa-minus").addClass("fa-plus");
        } else {
            //Show the content
            boxContent.slideDown(300, function () {
                box.removeClass("collapsed-box");
            });
            btnCollapse.removeClass("fa-plus").addClass("fa-minus");
        }
    });

    $(".timeline-header").click(function () {
        //Find the box parent
        var box = $(this).parents(".timeline-item").first();
        var boxContent = box.find(".timeline-body");

        if (!box.hasClass("collapsed-box")) {
            //Hide the content
            boxContent.slideUp(300, function () {
                box.addClass("collapsed-box");
            });
        } else {
            //Show the content
            boxContent.slideDown(300, function () {
                box.removeClass("collapsed-box");
            });
        }
    });
});

//focus on first textbox, or textarea
$(document).ready(function (event) {
    $(".wrapper .content-wrapper .content").find(':text, textarea').filter(":visible:enabled").first().focus();
})

//scroll top
$(document).ready(function ($) {
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function () {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration
        );
    });

});

//Home Search
function ChangeValue(element, value) {
    return parseInt(0 + element.value) + value;
}
function SearchChangeRoomNumber(type, button) {
    var RoomNumber = document.getElementById('room-number');
    var PersionNumber = document.getElementById('persion-number');
    var lsRoomNumber = document.getElementsByName('NumberRoom');
    var lsPersionNumber = document.getElementsByName('NumberAdul');

    var lsGuestNum = Array.from(document.getElementsByClassName('guest-num'));
    var lsRoomNum = Array.from(document.getElementsByClassName('room-num'));
    var val = parseInt(0 + RoomNumber.value);
    if (type == "plus") {
        if (val < 9) {
            val += 1;
            //RoomNumber.value = val;
            lsRoomNumber.forEach(function (elm, index) {
                elm.value = val;
            })
            lsRoomNum.forEach(function (elm, index) {
                elm.innerText = val;
            })
            if (val > parseInt(0 + PersionNumber.value)) {
                //PersionNumber.value = val;
                lsPersionNumber.forEach(function (elm, index) {
                    elm.value = val;
                })
                lsGuestNum.forEach(function (elm, index) {
                    elm.innerText = val;
                })
                //document.getElementsByClassName('room-num')[0].innerText = val;
                lsRoomNumber.forEach(function (elm, index) {
                    elm.value = val;
                })
                lsRoomNum.forEach(function (elm, index) {
                    elm.innerText = val;
                })
            }
        }
    }
    if (type == "minus") {
        if (val > 1) {
            val -= 1;
            //RoomNumber.value = val;
            lsRoomNumber.forEach(function (elm, index) {
                elm.value = val;
            })
        }
    }
    //document.getElementsByClassName('room-num')[0].innerText = val;
    lsRoomNum.forEach(function (elm, index) {
        elm.innerText = val;
    })
}
function SearchChangePersionNumber(type, button) {
    var RoomNumber = document.getElementById('room-number');
    var PersionNumber = document.getElementById('persion-number');
    var lsRoomNumber = document.getElementsByName('NumberRoom');
    var lsPersionNumber = document.getElementsByName('NumberAdul');

    var lsGuestNum = Array.from(document.getElementsByClassName('guest-num'));
    var lsRoomNum = Array.from(document.getElementsByClassName('room-num'));
    var val = parseInt(0 + PersionNumber.value);
    if (type == "plus") {
        if (val < 36) {
            val += 1;
            PersionNumber.value = val;
            lsPersionNumber.forEach(function (elm, index) {
                elm.value = val;
            })
        }

    }
    if (type == "minus") {
        if (val > 1) {
            val -= 1;
            //PersionNumber.value = val;
            lsPersionNumber.forEach(function (elm, index) {
                elm.value = val;
            })
            if (val < parseInt(0 + RoomNumber.value)) {
                //RoomNumber.value = val;
                lsRoomNumber.forEach(function (elm, index) {
                    elm.value = val;
                })
                //document.getElementsByClassName('room-num')[0].innerText = val;
                lsRoomNum.forEach(function (elm, index) {
                    elm.innerText = val;
                })
            }
        }
    }
    //document.getElementsByClassName('guest-num')[0].innerText = val;
    lsGuestNum.forEach(function (elm, index) {
        elm.innerText = val;
    })
}


//Home Search
