﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Web.Configuration;
using Admin.Helpers;
using System.Reflection;

/// <summary>
/// Summary description for Basefunctions
/// </summary>
public static class BaseFunctions
{

    #region Properties values
    public static string fileUploadDirectory = "~/UploadImages/";
    public static string fileUploads = "~/FileUploads/";
    public static string UpdateCurrentArticleLanguage = "Please update this language article.";
    public static string DateFormat = "dd/MM/yyyy HH:mm:ss tt";
    public static int PageSize = 50;
    public static int ImageSize = 100;
    public static string RefixThumnail = "small_";
    public static char sliptChar = '§';
    #endregion

    #region Enum

    public static class LoaiTinNhan
    {
        public static string Email = "email";
        public static string Sms = "sms";
        public static string Notify = "notify";
    }

    #endregion


    #region Common Functions

    #region Image Resize
    /// <summary>
    /// Resizes an image
    /// </summary>
    /// <param name="imageFile">the byte array of the file</param>
    /// <param name="targetSize">the target size of the file (may affect width or height) 
    /// depends on orientation of file (landscape or portrait)</param>
    /// <returns>Byte array containing the resized file</returns>
    private static byte[] ResizeImageFile(byte[] imageFile, int targetSize)
    {
        using (System.Drawing.Image oldImage =
            System.Drawing.Image.FromStream(new MemoryStream(imageFile)))
        {
            Size newSize = CalculateDimensions(oldImage.Size, targetSize);

            using (Bitmap newImage =
                new Bitmap(newSize.Width,
                    newSize.Height, PixelFormat.Format24bppRgb))
            {
                using (Graphics canvas = Graphics.FromImage(newImage))
                {
                    canvas.SmoothingMode = SmoothingMode.AntiAlias;
                    canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    canvas.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    canvas.DrawImage(oldImage,
                        new Rectangle(new Point(0, 0), newSize));

                    MemoryStream m = new MemoryStream();
                    newImage.Save(m, ImageFormat.Jpeg);
                    return m.GetBuffer();
                }
            }
        }
    }
    private static Size CalculateDimensions(Size oldSize, int targetSize)
    {
        Size newSize = new Size();
        if (oldSize.Height > oldSize.Width)
        {
            newSize.Width =
                (int)(oldSize.Width * ((float)targetSize / (float)oldSize.Height));
            newSize.Height = targetSize;
        }
        else
        {
            newSize.Width = targetSize;
            newSize.Height =
                (int)(oldSize.Height * ((float)targetSize / (float)oldSize.Width));
        }
        return newSize;
    }
    public static string SaveImage(FileUpload fi, int size, DateTime dt, out string _thumnail)
    {
        string imageURL = string.Empty;
        try
        {
            if (fi.HasFile)
            {
                // get the root of the web site
                string root = HttpContext.Current.Server.MapPath(fileUploadDirectory);

                // clean up the path
                if (!root.EndsWith(@"\"))
                    root += @"\";

                // make a folder to store the images in
                string fileDirectory = root + dt.Day + "-" + dt.Month + "-" + dt.Year + "\\";

                // create the folder if it does not exist
                if (!System.IO.Directory.Exists(fileDirectory))
                    System.IO.Directory.CreateDirectory(fileDirectory);

                // make a link to the new file
                string link = fileUploadDirectory + dt.Day + "-" + dt.Month + "-" + dt.Year + "/";

                // create a byte array to store the file bytes
                byte[] fileBytes = new byte[fi.PostedFile.ContentLength];
                // fill the byte array
                using (System.IO.Stream stream = fi.PostedFile.InputStream)
                {
                    stream.Read(fileBytes, 0, fi.PostedFile.ContentLength);
                }
                // write the original file to the file system
                File.WriteAllBytes(fileDirectory + fi.FileName,
                    fileBytes);

                // write the resized file to the file system
                File.WriteAllBytes(fileDirectory + RefixThumnail + fi.FileName,
                    ResizeImageFile(fileBytes, size));

                _thumnail = RefixThumnail + fi.FileName;
                imageURL = fi.FileName;
                fileBytes = null;
            }
            else
            {
                _thumnail = string.Empty;
            }
        }
        catch
        {
            _thumnail = string.Empty;
        }
        return imageURL;
    }

    public static void DeleteImage(string fileName, DateTime date)
    {
        string fileDirectory = BuildingImageURL(date);
        // get the root of the web site
        string root = HttpContext.Current.Server.MapPath(fileDirectory);
        if (Directory.Exists(root))
        {
            try
            {
                File.Delete(root + "\\" + fileName);
                File.Delete(root + "\\" + RefixThumnail + fileName);
            }
            catch { }
        }
    }

    public static string BuildingImageURL(DateTime dt)
    {
        return string.Format("~/UploadImages/{0}-{1}-{2}/", dt.Day, dt.Month, dt.Year);
    }


    #endregion

    public static string RandomString(int size, bool lowerCase)
    {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        char ch;
        for (int i = 0; i < size; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }
        if (lowerCase)
            return builder.ToString().ToLower();
        return builder.ToString();
    }
    public static int GenerateRandomNo6Digit()
    {
        int _min = 100000;
        int _max = 999999;
        Random _rdm = new Random();
        return _rdm.Next(_min, _max);
    }

    /// <summary>
    /// Get current page name
    /// </summary>
    /// <returns>string page name</returns>
    public static string GetCurrentPageName()
    {
        string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
    /// <summary>
    /// Remove Unicode Of String
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public static string RemoveUnicode(string s)
    {
        string stFormD = s.Normalize(NormalizationForm.FormD);
        StringBuilder sb = new StringBuilder();

        for (int ich = 0; ich < stFormD.Length; ich++)
        {
            UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
            if (uc != UnicodeCategory.NonSpacingMark)
            {
                sb.Append(stFormD[ich]);
            }
        }
        return (sb.ToString().Normalize(NormalizationForm.FormC).ToLower());
    }

    /// <summary>
    /// Remove Injection SQL
    /// </summary>
    /// <param name="strSQL">string input</param>
    /// <returns>Clean string</returns>
    public static string RemoveSQLChar(string strSQL)
    {
        string strCleanSQL = strSQL;
        if (strSQL != null)
        {
            //Array BadCommands = ";,--,create,drop,select,insert,delete,update,union,sp_,xp_".Split(new Char[] { ',' });
            Array BadCommands = ";,--, create , drop , insert , delete , update , union , xp_".Split(new Char[] { ',' });

            int intCommand;
            for (intCommand = 0; intCommand <= BadCommands.Length - 1; intCommand++)
            {
                strCleanSQL = Regex.Replace(strCleanSQL, Convert.ToString(BadCommands.GetValue(intCommand)), " ", RegexOptions.IgnoreCase);
            }

            //strCleanSQL = strCleanSQL.Replace("'", "''");

        }
        return strCleanSQL;
    }
    /// <summary>
    /// Encode String
    /// </summary>
    /// <param name="plainText">parameter string input</param>
    /// <returns></returns>
    public static string EncodeString(string plainText)
    {
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        MD5CryptoServiceProvider hash = new MD5CryptoServiceProvider();

        byte[] hashBytes = hash.ComputeHash(plainTextBytes);

        return Convert.ToBase64String(hashBytes);

    }

    public static void BindingDropDownList(string textField, string valueField, DropDownList drp, DataTable tblSource)
    {
        drp.Items.Clear();
        if (tblSource != null)
        {
            foreach (DataRow item in tblSource.Rows)
            {
                drp.Items.Add(new ListItem(item[textField].ToString(), item[valueField].ToString()));
            }
        }
        drp.Items.Insert(0, new ListItem("---------------", string.Empty));
    }


    //Function so sanh 2 object hoac DataRow co gia tri cua cac property khac nhau
    public static string CompareObiect(Object objOld, Object objCompare)
    {
        string strResult = "";
        int intCheck = 0;

        Type typeOld = objOld.GetType();
        Type typeCompare = objCompare.GetType();
        if (typeOld == typeCompare)
        {
            PropertyInfo[] sourceProperties = typeOld.GetProperties();
            strResult = "Object " + typeOld.Name + " != Object " + typeCompare.Name + " Values : ";
            foreach (PropertyInfo pi in sourceProperties)
            {
                try
                {
                    string valueOld = typeOld.GetProperty(pi.Name).GetValue(objOld, null) != null ? typeOld.GetProperty(pi.Name).GetValue(objOld, null).ToString() : "";
                    string valueNew = typeCompare.GetProperty(pi.Name).GetValue(objCompare, null) != null ? typeCompare.GetProperty(pi.Name).GetValue(objCompare, null).ToString() : "";

                    if (valueOld != valueNew)
                    {
                        strResult += pi.Name + "(" + valueOld + ") != " + pi.Name + "(" + valueNew + ") ; ";
                        intCheck++;
                    }
                }
                catch { }
            }
        }
        else if (typeOld.BaseType == typeCompare)
        {
            PropertyInfo[] sourceProperties = typeOld.BaseType.GetProperties();
            strResult = "Object " + typeOld.BaseType.Name + " != Object " + typeCompare.Name + " Values : ";
            foreach (PropertyInfo pi in sourceProperties)
            {
                try
                {
                    string valueOld = typeOld.BaseType.GetProperty(pi.Name).GetValue(objOld, null) != null ? typeOld.BaseType.GetProperty(pi.Name).GetValue(objOld, null).ToString() : "";
                    string valueNew = typeCompare.GetProperty(pi.Name).GetValue(objCompare, null) != null ? typeCompare.GetProperty(pi.Name).GetValue(objCompare, null).ToString() : "";
                    if (valueOld != valueNew)
                    {
                        strResult += pi.Name + "(" + valueOld + ") != " + pi.Name + "(" + valueNew + ") ; ";
                        intCheck++;
                    }
                }
                catch { }
            }
        }

        if (intCheck == 0)
        {
            strResult = "";
        }

        return strResult;
    }


    public static DataRow Combine(DataRow drMain, DataRow drExtra, string[] arrProperties)
    {
        DataRow result = null;

        return result;
    }

    public static string WriteLog_GetInfo(DataTable item, string[] arrProperties)
    {
        //Building an HTML string.
        string html = string.Empty;
        if (item != null)
        {

            if (item != null && item.Rows.Count > 0)
            {
                //Building the Data rows.
                foreach (DataRow row in item.Rows)
                {
                    foreach (DataColumn column in item.Columns)
                    {
                        foreach (string s in arrProperties)
                        {
                            if (s == column.ColumnName)
                            {
                                html += string.Format("{0}({1}),", column.ColumnName, row[column.ColumnName]);
                            }
                        }
                    }
                    html += "\n";
                }
            }
        }
        return html;
    }

    public static string CompareObiect(DataRow drOld, DataRow drCompare)
    {
        string strResult = "";
        int intCheck = 0;

        //IEqualityComparer<DataRow> comparer = DataRowComparer.Default;

        if (drOld.Table.Columns.Count == drCompare.Table.Columns.Count)
        {
            for (int currentIndex = 0; currentIndex < drOld.ItemArray.Length; currentIndex++)
            {
                // Check all columns except autoincrement columns
                if (drOld.Table.Columns[currentIndex].AutoIncrement == false)
                {
                    if (drOld[currentIndex].ToString() != drCompare[currentIndex].ToString())
                    {
                        strResult += "Index " + currentIndex + " : " + drOld[currentIndex] + "!=" + drCompare[currentIndex] + " ";
                        intCheck++;
                    }
                }
            }
        }

        if (intCheck == 0)
        {
            strResult = "";
        }

        return strResult;
    }
    /*
   public static void WriteLog(Object objOld, Object objCompare, tbl_LogObject tbl, string strIdentity)
   {
       string strResult = CompareObiect(objOld, objCompare);
       if (strResult != "")
       {
           strResult = strIdentity + ":" + strResult;
           tbl_LogObjectBL obj = new tbl_LogObjectBL();
           tbl.ContentUpdate = strResult;
           obj.Insert(tbl);
       }
   }
   public static void WriteLog(Object objOld, Object objCompare, tbl_LogObject tbl, string strIdentity, string textAppend)
   {
       string strResult = CompareObiect(objOld, objCompare);
       if (strResult + textAppend != "")
       {
           strResult = strIdentity + ":" + strResult;
           tbl_LogObjectBL obj = new tbl_LogObjectBL();
           tbl.ContentUpdate = strResult + "\n" + textAppend;
           obj.Insert(tbl);
       }
   }

   public static void WriteLog(DataRow drOld, DataRow drCompare, tbl_LogObject tbl, string strIdentity)
   {
       string strResult = CompareObiect(drOld, drCompare);
       if (strResult != "")
       {
           strResult = strIdentity + ":" + strResult;
           tbl_LogObjectBL obj = new tbl_LogObjectBL();
           tbl.ContentUpdate = strResult;
           obj.Insert(tbl);
       }
   }

   //Menu
   public static void BindingListMenu(DropDownList drp, List<VTV_Menu> list)
   {
       drp.Items.Clear();
       drp.Items.Add(new ListItem("------------", string.Empty));
       ListItem listitem = null;
       foreach (VTV_Menu item in list)
       {

           string space = string.Empty;
           for (int i = 0; i < item.TreeLevel; i++)
           {
               space += "&nbsp;&nbsp;&nbsp;&nbsp;";
           }
           if (item.ParentID < 1)
           {
               listitem = new ListItem(HttpUtility.HtmlDecode(string.Format("+ {0}", space + item.MenuText)), item.MenuID.ToString());
               drp.Items.Add(listitem);
           }
           else
           {
               FindingItemParentMenu(drp, item, space);
           }
       }
   }

   private static void FindingItemParentMenu(DropDownList list, VTV_Menu item, string space)
   {
       ListItem parent = list.Items.FindByValue(item.ParentID.ToString());
       if (parent != null)
       {
           int index = list.Items.IndexOf(parent);
           list.Items.Insert(index + 1, new ListItem(HttpUtility.HtmlDecode(space + "- " + item.MenuText), item.MenuID.ToString()));
       }
       else
       {
           throw new Exception(" Cannot Find Parent Item Value " + item.MenuID);
       }
   }

   public static void DownloadFile(string path, string filename)
   {

       try
       {
           HttpContext.Current.Response.Clear();
           FileStream fs = null;//File.Open(HttpContext.Current.Server.MapPath(path), FileMode.Open);
           if (path.StartsWith("http"))
           {
               HttpContext.Current.Response.Redirect(path);
           }
           else
           {
               fs = File.Open(HttpContext.Current.Server.MapPath(path), FileMode.Open);
           }
           Byte[] bytBytes = new Byte[fs.Length + 1];
           fs.Read(bytBytes, 0, Convert.ToInt32(fs.Length));
           fs.Close();
           HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
           //Context.Current.Response.ContentType = "application/ms-excel";
           System.String disHeader = "Attachment; Filename=\"" + filename + "\"";
           HttpContext.Current.Response.AppendHeader("Content-Disposition", disHeader);
           HttpContext.Current.Response.BinaryWrite(bytBytes);

           HttpContext.Current.Response.WriteFile(HttpContext.Current.Server.MapPath(path));
           //HttpContext.Current.ApplicationInstance.CompleteRequest();
           HttpContext.Current.Response.End();
       }
       catch (System.Exception ex)
       {
           string er = ex.Message.ToString();
       }
   }

 */

    #endregion
    //    using System;
    //using System.Collections;
    //using System.Collections.Generic;
    //using System.Data;
    //using System.Diagnostics;
    #region "NumberReader"
    // Tên Module: NumberReader

    // Người viết: Nguyễn Thị Kim Loan
    // Ngày viết: 04/11/2003

    // Mô tả: Dùng để đổi số sang chữ (Tiếng Việt và Tiếng Anh)
    // VD: 10 -> mười; 11 -> mười một; 1000000 -> một triệu .....
    //     10 -> ten;  11 -> eleven; 1000000 -> one million .....

    // Người kiểm tra:
    // Ngày kiểm tra:
    // Kiểm tra lần:
    // Kết quả:


    private const string constError = "";
    private static string[] KySo_vn = {
        "không",
        "một",
        "hai",
        "ba",
        "bốn",
        "năm",
        "sáu",
        "bảy",
        "tám",
        "chín"
    };
    private static string[] Hang_vn = {
        "nghìn",
        "triệu",
        "tỷ"
    };
    private static string[] KySo_en = {
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine"
    };
    private static string[] HaiKySo1 = {
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen"
    };
    private static string[] HaiKySo2 = {
        "twenty",
        "thirty",
        "forty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety"
    };
    private static string[] Hang_en = {
        "thousand",
        "million",
        "billion",
        "trillion",
        "quadrillion",
        "quintillion"

    };
    
    // Tên Function: SoToChu
    // Mô tả: Đổi số thành chữ
    // Tham số: prmso kiểu Long (Số muốn chuyển sang viết bằng chữ)
    //          prmLanguage kiểu Byte (1: tiếng anh; 2: tiếng việt)
    // Kết quả trả về: Chuỗi đọc được từ tham số prmSo

    public static string SoToChu(long prmSo, byte prmLanguage, string currencyName = "")
    {
        string temp = string.Empty;
        switch (prmLanguage)
        {
            case 1:
                // Đọc theo tiếng anh
                temp = (prmSo < 0 ? "(minus) " + So2Chu_en(Math.Abs(prmSo)) : So2Chu_en(prmSo));
                break;
            case 2:
                // Đọc theo tiếng việt
                if (string.IsNullOrEmpty(currencyName))
                {
                    temp = (prmSo < 0 ? "(Trừ) " + So2Chu_vn(Math.Abs(prmSo)) : So2Chu_vn(prmSo));
                    temp = temp.Trim() + " đồng chẵn";
                }
                else
                {
                    temp = (prmSo < 0 ? "(Trừ) " + So2Chu_vn(Math.Abs(prmSo)) : So2Chu_vn(prmSo));
                    temp = temp.Trim() + " " + currencyName;
                }
                break;
        }
        if ((temp.Length > 0))
        {
            temp = temp.Substring(0, 1).ToUpper() + temp.Substring(1);
        }
        return temp;
    }

    public static string So2Chu_vn(string prmSo, out int soLe)
    {
        string functionReturnValue = "";
        soLe = 0;
        bool isSoAm = false;

        try
        {
            string[] strParts = prmSo.Split(new char[] { '.' });

            long mainPart = 0;
            soLe = 0;

            if (!string.IsNullOrEmpty(strParts[0]))
            {
                mainPart = long.Parse(strParts[0]);
                if (mainPart < 0)
                {
                    isSoAm = true;
                    mainPart = -1 * mainPart;
                }
            }
            if (strParts.Length > 1 && !string.IsNullOrEmpty(strParts[1]))
            {
                soLe = int.Parse(strParts[1]);
            }
            try
            {
                long lctemp = mainPart / 1000000000;
                functionReturnValue = (lctemp > 0 ? ChinSo(lctemp) + " tỷ " : "") + ChinSo(mainPart % 1000000000, 0, lctemp > 0);
                if (isSoAm)
                    functionReturnValue = "âm " + functionReturnValue;
            }
            catch
            {
                functionReturnValue = constError;
            }
        }
        catch { }

        return functionReturnValue;
    }

    // Tên Function: So2Chu_vn
    // Mô tả: Đổi số thành chữ (tiếng việt)
    // Tham số: prmso kiểu Long (Số muốn chuyển sang viết bằng chữ)
    // Kết quả trả về: Chuỗi đọc được từ tham số prmSo

    public static string So2Chu_vn(long prmSo)
    {
        string functionReturnValue = null;
        try
        {
            long lctemp = prmSo / 1000000000;
            functionReturnValue = (lctemp > 0 ? ChinSo(lctemp) + " tỷ " : "") + ChinSo(prmSo % 1000000000, 0, lctemp > 0);
        }
        catch
        {
            functionReturnValue = constError;
        }
        return functionReturnValue;
    }

    // Tên Function: ChinSo
    // Mô tả: Đọc 9 số ra dạng chữ
    // Tham số: prmso kiểu Integer 
    //          prmInc kiểu Byte
    //          prmConTiep kiểu Boolean 
    // Kết quả trả về: Chuỗi đọc được từ 9 số

    private static string ChinSo(long prmSo, int prmInc, bool prmConTiep)
    {
        string strReturn = null;
        try
        {
            if ((prmSo / 1000) > 0)
            {
                strReturn += ChinSo(prmSo / 1000, prmInc + 1) + ((prmSo / 1000) % 1000 > 0 ? " " + Hang_vn[prmInc] + " " : "") + BaSo(prmSo % 1000, true);
            }
            else
            {
                strReturn += BaSo(prmSo % 1000, prmConTiep);
            }
        }
        catch
        {
            strReturn = constError;
        }
        return strReturn;
    }
    private static string ChinSo(long prmSo, int prmInc)
    {
        return ChinSo(prmSo, prmInc, false);
    }
    private static string ChinSo(long prmSo)
    {
        return ChinSo(prmSo, 0, false);
    }
    // Tên Function: BaSo (tiếng việt)
    // Mô tả: Đọc 3 số ra dạng chữ
    // Tham số: prmSo kiểu Integer 
    //          prmConTiep kiểu Boolean 
    // Kết quả trả về: Chuỗi đọc được từ 3 số

    private static string BaSo(long prmSo, bool prmConTiep)
    {
        long lcDonvi = 0;
        long lcChuc = 0;
        long lcTram = 0;
        string strReturn = "";
        if ((prmSo == 0))
        {
            return (prmConTiep ? "" : KySo_vn[0]);
        }

        lcDonvi = prmSo % 10;
        // Hàng đơn vị
        lcChuc = (prmSo / 10) % 10;
        // Hàng chục
        lcTram = prmSo / 100;
        // Hàng trăm

        // Xét trường hợp hàng trăm
        switch (lcTram)
        {
            case 0:
                if (prmConTiep)
                    strReturn = KySo_vn[0] + " trăm ";
                break;
            default:
                strReturn = KySo_vn[lcTram] + " trăm ";
                break;
        }
        // Xét trường hợp hàng chục
        switch (lcChuc)
        {
            case 0:
                if ((lcDonvi > 0 & ((lcTram > 0) | (lcTram == 0 & prmConTiep))))
                    strReturn += "linh ";
                break;
            case 1:
                strReturn += "mười ";
                break;
            default:
                strReturn += KySo_vn[lcChuc] + " mươi ";
                break;
        }
        // Xét trường hợp hàng đơn vị
        switch (lcDonvi)
        {
            case 0:
                strReturn += "";
                break;
            case 1:
                // 1, 11: một  -  21, 31: .... mốt
                strReturn += (lcChuc <= 1 ? KySo_vn[1] : "mốt");
                break;
            case 5:
                // 5: năm   -  15, 25: .... lăm 
                strReturn += (lcChuc == 0 ? KySo_vn[5] : "lăm");
                break;
            default:
                strReturn += KySo_vn[lcDonvi];
                break;
        }
        return strReturn;
    }
    private static string BaSo(int prmSo)
    {
        return BaSo(prmSo, false);
    }
    // Tên Function: So2Chu_en
    // Mô tả: Đổi số thành chữ (tiếng anh)
    // Tham số: prmso kiểu Long (Số muốn chuyển sang viết bằng chữ)
    //          prmInc kiểu Byte
    //          prmConTiep kiểu Boolean 
    // Kết quả trả về: Chuỗi đọc được từ tham số prmSo

    public static string So2Chu_en(long prmSo, int prmInc, bool prmConTiep)
    {
        string strReturn = null;
        try
        {
            if ((prmSo / 1000) > 0)
            {
                strReturn += So2Chu_en(prmSo / 1000, prmInc + 1) + ((prmSo / 1000) % 1000 > 0 ? " " + Hang_en[prmInc] + "  " : "") + Baso_en(prmSo % 1000, true);
            }
            else
            {
                strReturn += Baso_en(prmSo % 1000, prmConTiep);
            }
        }
        catch
        {
            strReturn = constError;
        }
        return strReturn;
    }
    public static string So2Chu_en(long prmSo, int prmInc)
    {
        return So2Chu_en(prmSo, prmInc, false);
    }
    public static string So2Chu_en(long prmSo)
    {
        return So2Chu_en(prmSo, 0, false);
    }
    public static string So2Chu_en(long prmSo, bool prmConTiep)
    {
        return So2Chu_en(prmSo, 0, prmConTiep);
    }
    // Tên Function: BaSo_en (tiếng anh)
    // Mô tả: Đọc 3 số ra dạng chữ
    // Tham số: prmSo kiểu Integer 
    //          prmConTiep kiểu Boolean 
    // Kết quả trả về: Chuỗi đọc được từ 3 số

    private static string Baso_en(long prmSo, bool prmConTiep)
    {
        long lcDonvi = 0;
        long lcChuc = 0;
        long lcTram = 0;
        string strReturn = "";
        if ((prmSo == 0))
        {
            return (prmConTiep ? "" : KySo_en[0]);
        }

        lcDonvi = prmSo % 10;
        // Hàng đơn vị
        lcChuc = (prmSo / 10) % 10;
        // Hàng chục
        lcTram = prmSo / 100;
        // Hàng trăm        

        // Xét trường hợp hàng trăm
        strReturn = (lcTram == 0 ? "" : KySo_en[lcTram] + " hundred ");

        // Xét trường hợp hàng chục và đơn vị
        switch (lcChuc)
        {
            case 0:
                strReturn += (lcDonvi == 0 ? "" : KySo_en[lcDonvi]);
                break;
            case 1:
                strReturn += HaiKySo1[lcDonvi];
                break;
            default:
                strReturn += (lcDonvi == 0 ? HaiKySo2[lcChuc - 2] : HaiKySo2[lcChuc - 2] + " " + KySo_en[lcDonvi]);
                break;
        }
        return strReturn;
    }
    private static string Baso_en(int prmSo)
    {
        return Baso_en(prmSo, false);
    }

    #endregion
    public static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.ToLower().Substring(1);
    }
    public static string ReadTextFile(string PathFile)
    {
        string strOut;
        StreamReader sr = new StreamReader(PathFile);
        strOut = sr.ReadToEnd();
        sr.Close();
        return strOut;
    }

    public static string CountDay(DateTime fromdate, DateTime todate)
    {
        string result = string.Empty;
        if (fromdate == DateTime.MinValue || todate == DateTime.MinValue || fromdate > todate)
        {
            result = "N/A";
        }
        else
        {
            if (fromdate.ToString("yyyyMMdd") == todate.ToString("yyyyMMdd"))
            {
                result = "1 ngày";
            }
            else
            {
                var day = (todate.Date - fromdate.Date).TotalDays;
                result = string.Format("{0}N{1}Đ", day + 1, day);

                //DateTime fDate = fromdate;
                //while (fDate.ToString("yyyyMMdd") != todate.ToString("yyyyMMdd"))
                //{
                //    days++;
                //    fDate = fromdate.AddDays(days);
                //}
                //result = string.Format("{0}N{1}Đ", days + 1, days);
            }
        }
        return result;
    }

    public static string SaveAttachedFile(FileUpload fi, DateTime dt)
    {
        string imageURL = string.Empty;

        try
        {
            if (fi.HasFile)
            {
                // get the root of the web site
                string root = HttpContext.Current.Server.MapPath(fileUploads);

                // clean up the path
                if (!root.EndsWith(@"\"))
                    root += @"\";

                // make a folder to store the images in
                string fileDirectory = root + "\\";

                // create the folder if it does not exist
                if (!System.IO.Directory.Exists(fileDirectory))
                    System.IO.Directory.CreateDirectory(fileDirectory);

                // make a link to the new file
                string link = fileUploads + "/";

                // create a byte array to store the file bytes
                byte[] fileBytes = new byte[fi.PostedFile.ContentLength];
                // fill the byte array
                using (System.IO.Stream stream = fi.PostedFile.InputStream)
                {
                    stream.Read(fileBytes, 0, fi.PostedFile.ContentLength);
                }
                // write the original file to the file system
                File.WriteAllBytes(fileDirectory + fi.FileName,
                    fileBytes);

                imageURL = fi.FileName;
                fileBytes = null;
            }
            else
            {
                imageURL = string.Empty;
            }
        }
        catch
        {
            imageURL = string.Empty;
        }
        return imageURL;
    }

    public static string CutText(string strcuttext, int inumberchar)
    {
        if (strcuttext.Length < inumberchar)
            return strcuttext;
        string temp = "";
        temp = strcuttext.Substring(0, inumberchar);
        for (int i = temp.Length - 1; i > 0; i--)
        {
            if (temp[i].ToString() == " ")
            {
                temp = temp.Substring(0, i);
                break;
            }
        }
        return temp + " ...";
    }

}
