﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Utilities
{
    public class Notify
    {
        public Notify()
        {
            this.Status = -1;
            this.Type = NotifyTypes.Success;
            this.Message = "";
            this.Position = "top";//bottom
        }

        public static Notify NotifySuccess(string message)
        {
            return new Notify { Status = 1, Type = NotifyTypes.Success, Position = "top", Message = string.IsNullOrEmpty(message) ? "Thao tác thực hiện thành công !" : message };
        }

        public static Notify NotifySuccess(string message, string positon)
        {
            return new Notify { Status = 1, Type = NotifyTypes.Success, Position = positon, Message = string.IsNullOrEmpty(message) ? "Thao tác thực hiện thành công !" : message };
        }

        public static Notify NotifyWarning(string message)
        {
            return new Notify { Status = 1, Type = NotifyTypes.Warning, Position = "top", Message = string.IsNullOrEmpty(message) ? "Thao tác thực hiện không thành công !" : message };
        }

        public static Notify NotifyWarning(string message, string positon)
        {
            return new Notify { Status = 1, Type = NotifyTypes.Warning, Position = positon, Message = string.IsNullOrEmpty(message) ? "Thao tác thực hiện không thành công !" : message };
        }

        public static Notify NotifyDanger(string message)
        {
            return new Notify { Status = 1, Type = NotifyTypes.Danger, Position = "top", Message = string.IsNullOrEmpty(message) ? "Thao tác thực hiện không thành công !" : message };
        }

        public static Notify NotifyDanger(string message, string positon)
        {
            return new Notify { Status = 1, Type = NotifyTypes.Danger, Position = positon, Message = string.IsNullOrEmpty(message) ? "Thao tác thực hiện không thành công !" : message };
        }

        public int Status { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public string Position { get; set; }
    }

    public static class NotifyTypes
    {
        public static string Info = "info";
        public static string Success = "success";
        public static string Warning = "warning";
        public static string Danger = "danger";
    }
}