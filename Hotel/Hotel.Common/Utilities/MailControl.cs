﻿using Hotel.EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Configuration;
using System.Configuration;

namespace Admin.Helpers
{
    public class MailControl
    {
        public bool Send(Email model)
        {
            // Kết nối đến gmail
            var smtp = new SmtpClient();
            model.From = ConfigurationManager.AppSettings["From"].ToString();
            model.FromName = WebConfigurationManager.AppSettings["FromName"].ToString();

            // Tạo email
            var mail = new MailMessage();
            mail.From = new MailAddress(model.From, model.FromName);

            if (!string.IsNullOrEmpty(model.To.ToString()))
                mail.To.Add(new MailAddress(model.To, model.ToName));

            foreach (var mailAddress in model.MailTo_Addresses)
            {
                mail.To.Add(mailAddress);
            }

            foreach (var item in model.AttachFiles)
            {
                mail.Attachments.Add(new Attachment(item));
            }

            foreach (var mailAddress in model.MailCC_Addresses)
            {
                mail.CC.Add(mailAddress);
            }

            foreach (var mailAddress in model.MailBCC_Addresses)
            {
                mail.Bcc.Add(mailAddress);
            }

            mail.Subject = model.Subject;
            mail.IsBodyHtml = true;
            mail.Body = model.Body;
            mail.ReplyToList.Add(mail.From);

            #region Xử lý hình trong mail
            if (!string.IsNullOrEmpty(model.ImageData))
            {
                var imageData = Convert.FromBase64String(model.ImageData);
                var contentId = "Hinh";
                var linkedResource = new LinkedResource(new MemoryStream(imageData), "image/jpeg");
                linkedResource.ContentId = contentId;
                linkedResource.TransferEncoding = TransferEncoding.Base64;

                var htmlView = AlternateView.CreateAlternateViewFromString(model.Body, null, "text/html");
                htmlView.LinkedResources.Add(linkedResource);
                mail.AlternateViews.Add(htmlView);
            }
            #endregion

            // Gửi email
            try
            {
                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool SendCSKH(Email model)
        {
            // Kết nối đến gmail
            var smtp = new SmtpClient();
            model.From = ConfigurationManager.AppSettings["From"].ToString();
            model.FromName = WebConfigurationManager.AppSettings["FromName"].ToString();

            model.To = ConfigurationManager.AppSettings["FromCSKH"].ToString();
            model.ToName = WebConfigurationManager.AppSettings["FromNameCSKH"].ToString();

            // Tạo email
            var mail = new MailMessage();
            mail.From = new MailAddress(model.From, model.FromName);

            if (!string.IsNullOrEmpty(model.To.ToString()))
                mail.To.Add(new MailAddress(model.To, model.ToName));

            foreach (var mailAddress in model.MailTo_Addresses)
            {
                mail.To.Add(mailAddress);
            }

            foreach (var item in model.AttachFiles)
            {
                mail.Attachments.Add(new Attachment(item));
            }

            foreach (var mailAddress in model.MailCC_Addresses)
            {
                mail.CC.Add(mailAddress);
            }

            foreach (var mailAddress in model.MailBCC_Addresses)
            {
                mail.Bcc.Add(mailAddress);
            }

            mail.Subject = model.Subject;
            mail.IsBodyHtml = true;
            mail.Body = model.Body;
            mail.ReplyToList.Add(mail.From);

            #region Xử lý hình trong mail
            if (!string.IsNullOrEmpty(model.ImageData))
            {
                var imageData = Convert.FromBase64String(model.ImageData);
                var contentId = "Hinh";
                var linkedResource = new LinkedResource(new MemoryStream(imageData), "image/jpeg");
                linkedResource.ContentId = contentId;
                linkedResource.TransferEncoding = TransferEncoding.Base64;

                var htmlView = AlternateView.CreateAlternateViewFromString(model.Body, null, "text/html");
                htmlView.LinkedResources.Add(linkedResource);
                mail.AlternateViews.Add(htmlView);
            }
            #endregion

            // Gửi email
            try
            {
                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Send(Email model, string userEmail, string passEmail, string from, string fromName)
        {
            // Kết nối đến gmail
            var smtp = new SmtpClient();

            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = userEmail;
            NetworkCred.Password = passEmail;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;          

            model.From = from;
            model.FromName = fromName;

            // Tạo email
            var mail = new MailMessage();
            mail.From = new MailAddress(model.From, model.FromName);

            if (!string.IsNullOrEmpty(model.To.ToString()))
                mail.To.Add(new MailAddress(model.To, model.ToName));

            foreach (var mailAddress in model.MailTo_Addresses)
            {
                mail.To.Add(mailAddress);
            }

            foreach (var item in model.AttachFiles)
            {
                mail.Attachments.Add(new Attachment(item));
            }

            foreach (var mailAddress in model.MailCC_Addresses)
            {
                mail.CC.Add(mailAddress);
            }

            foreach (var mailAddress in model.MailBCC_Addresses)
            {
                mail.Bcc.Add(mailAddress);
            }

            mail.Subject = model.Subject;
            mail.IsBodyHtml = true;
            mail.Body = model.Body;
            mail.ReplyToList.Add(mail.From);

            #region Xử lý hình trong mail
            if (!string.IsNullOrEmpty(model.ImageData))
            {
                var imageData = Convert.FromBase64String(model.ImageData);
                var contentId = "Hinh";
                var linkedResource = new LinkedResource(new MemoryStream(imageData), "image/jpeg");
                linkedResource.ContentId = contentId;
                linkedResource.TransferEncoding = TransferEncoding.Base64;

                var body = string.Format("<img src=\"cid:{0}\" />", contentId);
                var htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
                htmlView.LinkedResources.Add(linkedResource);
                mail.AlternateViews.Add(htmlView);
            }
            #endregion

            // Gửi email
            try
            {
                smtp.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool AddToSend(Email model, HotelEntities _db)
        {            
            var mailSent = new SentCommunication()
            {                
                Id = Guid.NewGuid(),
                Type = BaseFunctions.LoaiTinNhan.Email,
                Subject = model.Subject,
                Body = model.Body,
                IsCheck = false,
                IsSent = false,
                LogError = "",
                DateCreate = DateTime.Now
            };

            mailSent.MailFrom = WebConfigurationManager.AppSettings["From"].ToString();

            if (!string.IsNullOrEmpty(model.To))
            {
                mailSent.MailTo = model.To;
            }

            foreach (var mailAddress in model.MailTo_Addresses)
            {
                if (mailSent.MailTo == "")
                {
                    mailSent.MailTo = mailAddress.Address;
                }
                else
                {
                    mailSent.MailTo = mailSent.MailTo + ";" + mailAddress.Address;
                }
            }

            foreach (var mailAddress in model.MailCC_Addresses)
            {
                if (mailSent.MailCc == "")
                {
                    mailSent.MailCc = mailAddress.Address;
                }
                else
                {
                    mailSent.MailCc = mailSent.MailCc + ";" + mailAddress.Address;
                }
            }

            foreach (var mailAddress in model.MailBCC_Addresses)
            {
                if (mailSent.MailBcc == "")
                {
                    mailSent.MailBcc = mailAddress.Address;
                }
                else
                {
                    mailSent.MailBcc = mailSent.MailBcc + ";" + mailAddress.Address;
                }
            }

            try
            {
                _db.SentCommunication.Add(mailSent);

                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static SentCommunication MakeToSend(Email model)
        {
            var mailSent = new SentCommunication()
            {
                Id = Guid.NewGuid(),
                Type = BaseFunctions.LoaiTinNhan.Email,
                Subject = model.Subject,
                Body = model.Body,
                IsSent = false,
                IsCheck = false,
                LogError = "",
                DateCreate = DateTime.Now
            };

            mailSent.MailFrom = WebConfigurationManager.AppSettings["From"].ToString();

            if (!string.IsNullOrEmpty(model.To))
            {
                mailSent.MailTo = model.To;
            }

            foreach (var mailAddress in model.MailTo_Addresses)
            {
                if (string.IsNullOrEmpty(mailSent.MailTo))
                {
                    if (!string.IsNullOrEmpty(mailAddress.Address))
                    {
                        mailSent.MailTo = mailAddress.Address;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(mailAddress.Address))
                    {
                        mailSent.MailTo = mailSent.MailTo + ";" + mailAddress.Address;
                    }
                }
            }

            foreach (var mailAddress in model.MailCC_Addresses)
            {
                if (string.IsNullOrEmpty(mailSent.MailCc))
                {
                    if (!string.IsNullOrEmpty(mailAddress.Address))
                    {
                        mailSent.MailCc = mailAddress.Address;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(mailAddress.Address))
                    {
                        mailSent.MailCc = mailSent.MailCc + ";" + mailAddress.Address;
                    }
                }
            }

            foreach (var mailAddress in model.MailBCC_Addresses)
            {
                if (string.IsNullOrEmpty(mailSent.MailBcc))
                {
                    if (!string.IsNullOrEmpty(mailAddress.Address))
                    {
                        mailSent.MailBcc = mailAddress.Address;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(mailAddress.Address))
                    {
                        mailSent.MailBcc = mailSent.MailBcc + ";" + mailAddress.Address;
                    }
                }
            }

            return mailSent;
        }
    }

    public class Email
    {
        public Email()
        {
            AttachFiles = new List<string>();
            MailTo_Addresses = new List<MailAddress>();
            MailCC_Addresses = new List<MailAddress>();
            MailBCC_Addresses = new List<MailAddress>();
        }

        public String From { get; set; }
        public String To { get; set; }
        public String Subject { get; set; }
        public String Body { get; set; }
        public String ImageData { get; set; }
        public String FromName { get; set; }
        public String ToName { get; set; }

        public List<MailAddress> MailTo_Addresses { get; set; }
        public List<MailAddress> MailCC_Addresses { get; set; }
        public List<MailAddress> MailBCC_Addresses { get; set; }
        public List<string> AttachFiles { get; set; }

        public void addMailTo(string address, string displayName)
        {
            if (!MailTo_Addresses.Any(d => d.Address == address && d.DisplayName == displayName))
                MailTo_Addresses.Add(new MailAddress(address, displayName));
        }

        public void addMailCC(string address, string displayName)
        {
            if (!MailCC_Addresses.Any(d => d.Address == address && d.DisplayName == displayName))
                MailCC_Addresses.Add(new MailAddress(address, displayName));
        }

        public void addMailBCC(string address, string displayName)
        {
            if (!MailBCC_Addresses.Any(d => d.Address == address && d.DisplayName == displayName))
                MailBCC_Addresses.Add(new MailAddress(address, displayName));
        }
    }
}