﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Common.Utilities
{
    public class TripleDES
    {
        public string Encrypt(string encryptionKey, string textToEncrypt)
        {
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = Encoding.ASCII.GetBytes(encryptionKey.Substring(0, 24));
            tdes.IV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            byte[] stringBytes = Encoding.UTF8.GetBytes(textToEncrypt);
            string st = Convert.ToBase64String(tdes.CreateEncryptor().TransformFinalBlock(stringBytes, 0, stringBytes.Length));

            byte[] toBytes = Encoding.ASCII.GetBytes(st);
            string s = BitConverter.ToString(toBytes).Replace("-", "").ToLower();
            return s;
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public string hashCheckSum(string encryptionKey, string textToEncrypt)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(encryptionKey + textToEncrypt + encryptionKey))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }
    }
}
