﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.WebPages;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using System.Configuration;
using System.Web.WebPages.Html;
using System.Web.Mvc;
namespace System.Web.Mvc.Html
{
    public static class HtmlHelperExtentions
    {
        #region CKEditorHelper
        public static HtmlString HtmlEditor(this HtmlHelper html, string parName, string parText)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<textarea id='{0}' name='{0}' style='width:750px;' rows='100'>{1}</textarea>", parName, parText);
            sb.AppendFormat("<script type='text/javascript'>var editor = CKEDITOR.replace('{0}', {{fullPage : false, toolbarStartupExpanded: true}});</script>", parName);// CKFinder.setupCKEditor( editor, '/Content/plugins/ckfinder/' );
            //
            return new HtmlString(sb.ToString());
        }

        public static HtmlString HtmlEditor(this HtmlHelper html, string parName, string parText, string style)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<textarea id='{0}' name='{0}' style='{2}'>{1}</textarea>", parName, parText, style);
            sb.AppendFormat("<script type='text/javascript'>CKEDITOR.replace('{0}', {{ fullPage : false, toolbarStartupExpanded: true }}); </script>", parName);
            return new HtmlString(sb.ToString());
        }
        #endregion

        #region Menu Link - Active link
        public static MvcHtmlString MenuLink(this HtmlHelper htmlHelper, string itemText, string actionName, string controllerName, MvcHtmlString[] childElements = null)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
            string finalHtml;
            var linkBuilder = new TagBuilder("a");
            var liBuilder = new TagBuilder("li");

            if (childElements != null && childElements.Length > 0)
            {
                linkBuilder.MergeAttribute("href", "#");
                //linkBuilder.AddCssClass("dropdown-toggle");
                linkBuilder.InnerHtml = itemText;// +" <b class=\"caret\"></b>";
                //linkBuilder.MergeAttribute("data-toggle", "dropdown");
                var ulBuilder = new TagBuilder("ul");
                ulBuilder.AddCssClass("treeview-menu");
                ulBuilder.MergeAttribute("role", "menu");

                bool isChild_clicked = false;
                foreach (var item in childElements)
                {
                    ulBuilder.InnerHtml += item + "\n";
                    if (item.ToString().Contains(currentAction) == true)
                    {
                        isChild_clicked = true;
                    }
                }

                liBuilder.InnerHtml = linkBuilder + "\n" + ulBuilder;
                //liBuilder.AddCssClass("dropdown");
                if ((controllerName == currentController && actionName == currentAction) || isChild_clicked == true)
                {
                    liBuilder.AddCssClass("active");
                }

                finalHtml = liBuilder.ToString() + ulBuilder;
            }
            else
            {
                var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext, htmlHelper.RouteCollection);
                linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, controllerName));
                //linkBuilder.SetInnerText(itemText);
                linkBuilder.InnerHtml = itemText;
                liBuilder.InnerHtml = linkBuilder.ToString();
                if (controllerName == currentController && actionName == currentAction)
                {
                    liBuilder.AddCssClass("active");
                }

                finalHtml = liBuilder.ToString();
            }

            return new MvcHtmlString(finalHtml);
        }

        public static string IsActive(this HtmlHelper htmlHelper, string action, string controller)
        {
            var routeData = htmlHelper.ViewContext.RouteData;

            var routeAction = routeData.Values["action"].ToString();
            var routeController = routeData.Values["controller"].ToString();
            var returnActive = false;

            var actions = action.Split(',').ToArray();
            var controllers = controller.Split(',').ToArray();

            if (controllers.Contains(routeController) && (actions.Contains(routeAction) || action.Length == 0))
            {
                returnActive = true;
            }
            //returnActive = (controller == routeController && (action == routeAction || action.Length == 0));

            return returnActive ? "active" : "";
        }
        #endregion

        #region Custom Dropdow list
        //public class CustomSelectItem : SelectListItem
        //{
        //    public string Class { get; set; }
        //    public string Disabled { get; set; }
        //    public string SelectedValue { get; set; }
        //}

        public static MvcHtmlString CustomDropdownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> list, object htmlAttributes = null)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            string name = ExpressionHelper.GetExpressionText((LambdaExpression)expression);
            return CustomDropdownList(htmlHelper, name, list, "", null, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString CustomDropdownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> list, string optionLabel, object htmlAttributes = null)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            //ModelMetadata metadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string name = ExpressionHelper.GetExpressionText((LambdaExpression)expression);
            return CustomDropdownList(htmlHelper, name, list, "", optionLabel, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString CustomDropdownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> list, string selectedValue, string optionLabel, object htmlAttributes = null)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            string name = ExpressionHelper.GetExpressionText((LambdaExpression)expression);
            return CustomDropdownList(htmlHelper, name, list, selectedValue, optionLabel, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString CustomDropdownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> list, object htmlAttributes = null)
        {
            return CustomDropdownList(htmlHelper, name, list, "", "", HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString CustomDropdownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> list, string optionLabel, object htmlAttributes = null)
        {
            return CustomDropdownList(htmlHelper, name, list, "", optionLabel, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        private static MvcHtmlString CustomDropdownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> list, string selectedValue, string optionLabel, IDictionary<string, object> htmlAttributes)
        {
            string fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            if (String.IsNullOrEmpty(fullName))
            {
                throw new ArgumentException("name");
            }

            TagBuilder dropdown = new TagBuilder("select");
            dropdown.Attributes.Add("name", fullName);
            dropdown.Attributes.Add("id", fullName);
            //dropdown.MergeAttribute("data-val", "true");
            //dropdown.MergeAttribute("data-val-required", "Mandatory field.");
            //dropdown.MergeAttribute("data-val-number", "The field must be a number.");
            dropdown.MergeAttributes(htmlAttributes);

            StringBuilder options = new StringBuilder();

            // Make optionLabel the first item that gets rendered.
            if (optionLabel != null)
                options = options.Append("<option value='" + String.Empty + "'>" + optionLabel + "</option>");

            if (list != null)
            {
                foreach (var item in list)
                {
                    options = options.Append("<option value='" + item.Value + "' " + (item.Selected ? "selected" : "") + ">" + item.Text + "</option>");
                }
            }

            dropdown.InnerHtml = options.ToString();
            return MvcHtmlString.Create(dropdown.ToString(TagRenderMode.Normal));
        }
        #endregion

        public static HtmlString HtmlUpdateStatus(this HtmlHelper html, string status = "", string message = "")
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(status))
            {
                if (status.Equals("1"))
                {
                    sb.AppendFormat(@"<div class=""alert alert-block alert-success fade in"">");
                    sb.AppendFormat(@"<button data-dismiss=""alert"" class=""close"" type=""button"">×</button>");
                    sb.AppendFormat(@"<p><i class=""fa fa-times-circle fa-lg""></i> Cập nhật thành công</p></div>");
                }
                else if (status.Equals("0"))
                {
                    sb.AppendFormat(@"<div class=""alert alert-block alert-danger fade in"">");
                    sb.AppendFormat(@"<button data-dismiss=""alert"" class=""close"" type=""button"">×</button>");
                    sb.AppendFormat(@"<p><i class=""fa fa-times-circle fa-lg""></i> Cập nhật thất bại: " + message + "</p></div>");
                }
            }
            return new HtmlString(sb.ToString());
        }

        public static string ResolveUrl(this HtmlHelper helper, string relativeUrl)
        {
            if (VirtualPathUtility.IsAppRelative(relativeUrl))
            {
                return VirtualPathUtility.ToAbsolute(relativeUrl);
            }
            else
            {
                var curPath = WebPageContext.Current.Page.TemplateInfo.VirtualPath;
                var curDir = VirtualPathUtility.GetDirectory(curPath);
                return VirtualPathUtility.ToAbsolute(VirtualPathUtility.Combine(curDir, relativeUrl));

            }
        }

        //public static string GenerateHtmlFromYourControl(this HtmlHelper helper, string id)
        //{
        //    //http://stackoverflow.com/questions/6317317/mvc-3-add-usercontrol-to-razor-view
        //    var _FCKeditor = new FCKeditor();
        //    //_FCKeditor.BasePath = "~/Content/fckeditor/";
        //    //_FCKeditor.ToolbarSet = "MyToolbar";
        //    //_FCKeditor.ToolbarStartExpanded = true;
        //    //_FCKeditor.Height = 400;
        //    //_FCKeditor.Width = 700;
        //    //_FCKeditor.Value = "Test content";

        //    _FCKeditor.ID = id;

        //    return _FCKeditor.CreateHtml();

        //    //var htmlWriter = new HtmlTextWriter(new StringWriter());

        //    //_FCKeditor.RenderControl(htmlWriter);

        //    //return htmlWriter.InnerWriter.ToString();
        //}


        public static Uri UrlOriginal(this HttpRequestBase request)
        {
            //string hostHeader = request.Headers["host"];

            //string RawUrl = request.RawUrl;
            //if (request.RawUrl.Split('?').Count() > 1)
            //{
            //    RawUrl = request.RawUrl.Split('?')[0];
            //}

            return new Uri(string.Format("{0}://{1}{2}",
               request.Url.Scheme,
               request.Headers["host"],
               request.Url.AbsolutePath));
        }

        public static MvcForm BeginMultipartForm(this HtmlHelper htmlHelper)
        {
            return htmlHelper.BeginForm(null, null, FormMethod.Post,
             new Dictionary<string, object>() { { "enctype", "multipart/form-data" } });
        }

        public static MvcForm BeginMultipartForm(this HtmlHelper htmlHelper, string action, string control)
        {
            return htmlHelper.BeginForm(action, control, FormMethod.Post,
             new Dictionary<string, object>() { { "enctype", "multipart/form-data" } });
        }

        public static MvcForm BeginFormAutoComplete(this HtmlHelper htmlHelper, string action, string control)
        {
            return htmlHelper.BeginForm(action, control, FormMethod.Post,
             new Dictionary<string, object>() { { "autocomplete", "on" } });
        }

        //Lọc dấu tiếng Việt - ToAscii
        #region Lọc dấu tiếng Việt - ToAscii
        public static string convertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static String convertToUnSign2(this String s)
        {
            string[] values = {"[áàảãạăắằẳẵặâấầẩẫậ]",
               	"đ",
               	"[éèẻẽẹêếềểễệ]",
               	"[íìỉĩị]",
               	"[óòỏõọôốồổỗộơớờởỡợ]",
               	"[úùủũụưứừửữự]",
               	"[ýỳỷỹỵ]",
               	"[\\s&]+"
          	};
            string[] key = { "a", "d", "e", "i", "o", "u", "y", "-" };

            for (int i = 0; i < values.Length; i++)
            {
                s = Regex.Replace(s.ToLower(), values[i], key[i]);
            }
            return s;
        }

        public static string convertToUnSign3(this HtmlHelper htmlHelper, string noiDung)
        {
            string temp = "";

            if (!string.IsNullOrEmpty(noiDung))
            {
                try
                {
                    Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                    temp = noiDung.Normalize(NormalizationForm.FormD).Trim();
                    temp = regex.Replace(temp, String.Empty);
                }
                catch { temp = ""; }
            }
            return temp;
        }

        /*== Java script ==
         * function locdau(obj) { 
         * var str; if (eval(obj)) str = eval(obj).value; else str = obj; 
         * str = str.toLowerCase(); str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a"); 
         * str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e"); str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i"); 
         * str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o"); 
         * str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u"); str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y"); 
         * str = str.replace(/đ/g, "d"); //str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-"); 
         * \/* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - *\/ 
         * //str= str.replace(/-+-/g,"-"); 
         * //thay thế 2- thành 1- str = str.replace(/^\-+|\-+$/g, ""); 
         * //cắt bỏ ký tự - ở đầu và cuối chuỗi eval(obj).value = str.toUpperCase(); 
         * }
         */
        #endregion

        public static String UppercaseFirstEach(this String s)
        {
            char[] a = s.Trim().ToLower().ToCharArray();

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];

            }
            return new string(a);
        }

        public static string ChangeDateToDayOfWeek(this DateTime date)
        {
            string result = "";
            string dayofweek = date.Date.DayOfWeek.ToString();
            switch (dayofweek)
            {
                case "Monday":
                    result = "THỨ HAI";
                    break;
                case "Tuesday":
                    result = "THỨ BA";
                    break;
                case "Wednesday":
                    result = "THỨ TƯ";
                    break;
                case "Thursday":
                    result = "THỨ NĂM";
                    break;
                case "Friday":
                    result = "THỨ SÁU";
                    break;
                case "Saturday":
                    result = "THỨ BẢY";
                    break;
                case "Sunday":
                    result = "CHỦ NHẬT";
                    break;
                default:
                    break;
            }
            return result;
        }

        public static string LoaiBoDauTiengViet(this HtmlHelper htmlHelper, string noiDung)
        {
            string temp = "";

            if (!string.IsNullOrEmpty(noiDung))
            {
                try
                {
                    Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                    temp = noiDung.Normalize(NormalizationForm.FormD).Trim();
                    temp = regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
                }
                catch { temp = ""; }
            }
            return temp;
        }

        public static string LoaiBoDauTiengViet(string noiDung)
        {
            string temp = "";

            if (!string.IsNullOrEmpty(noiDung))
            {
                try
                {
                    Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                    temp = noiDung.Normalize(NormalizationForm.FormD).Trim();
                    Array BadCommands = ";,--,create,drop,select,insert,delete,update,union,sp_,xp_".Split(new Char[] { ',' });
                    temp = (regex.Replace(temp, String.Empty)
                        .Replace('\u0111', 'd')
                        .Replace('\u0110', 'D')
                        .Replace(",", "-")
                        .Replace(".", "-")
                        .Replace(":", "-")
                        .Replace("!", "")
                        .Replace(";", "-")
                        .Replace("/", "-")
                        .Replace("&", "va")
                        .Replace("%", "ptram")
                        .Replace('"', '-')
                        .Replace("*", "-")
                        .Replace("?", "")
                        .Replace(' ', '-'));
                    temp = FormatRemoveSQL(temp.Replace(" ", "")).ToLower();
                }
                catch { temp = ""; }
            }
            return temp;
        }

        public static string FormatRemoveSQL(string strSQL)
        {
            string strCleanSQL = strSQL;
            if (strSQL != null)
            {
                Array BadCommands = ";,--,create,drop,select,insert,delete,update,union,sp_,xp_".Split(new Char[] { ',' });

                int intCommand;
                for (intCommand = 0; intCommand <= BadCommands.Length - 1; intCommand++)
                {
                    strCleanSQL = Regex.Replace(strCleanSQL, Convert.ToString(BadCommands.GetValue(intCommand)), " ", RegexOptions.IgnoreCase);
                }

                strCleanSQL = strCleanSQL.Replace("'", "''");

            }
            return strCleanSQL;
        }

        public static string VersionedContent(this UrlHelper urlHelper, string contentPath)
        {
            var applicationVersion = ConfigurationManager.AppSettings["ApplicationVersion"];
            var versionedPath = new StringBuilder(contentPath);
            versionedPath.AppendFormat("{0}v={1}",
                                       contentPath.IndexOf('?') >= 0 ? '&' : '?',
                                       applicationVersion);
            return urlHelper.Content(versionedPath.ToString());
        }

        public static string GetTextImageType(this HtmlHelper helper, int? type)
        {
            string colorStatus = "Khác";
            if (type == 1)
            {
                colorStatus = "Sảnh lễ tân";
            }
            else if (type == 2)
            {
                colorStatus = "Ảnh ngoại cảnh";
            }
            else if (type == 3)
            {
                colorStatus = "Khác";
            }

            return colorStatus;
        }
    }
}
