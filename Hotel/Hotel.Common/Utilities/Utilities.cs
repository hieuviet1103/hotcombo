﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Hotel.Utilities
{
    public static class XUpload
    {
        public static bool HasFile(this HttpPostedFileBase fileUpload)
        {
            return (fileUpload != null && fileUpload.ContentLength > 0);
        }

        public static String SaveIn(this HttpPostedFileBase fileUpload, String folder = "~/", String fileName = null)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                fileName = fileUpload.FileName;
            }
            //else
            //{
            //    fileName += DateTime.Now.ToString("ddMMyyyy") + fileUpload.FileName.Substring(fileUpload.FileName.LastIndexOf("."));
            //}
            var path = Path.Combine(folder, fileName);
            fileUpload.SaveAs(HttpContext.Current.Server.MapPath(path));
            return fileName;
        }
    }
}
