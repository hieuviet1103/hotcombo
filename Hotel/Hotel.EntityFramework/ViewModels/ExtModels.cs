﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Models
{
    public class SearchUser
    {
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
        public string DepartureId { get; set; }
        public int StatusDepartureId { get; set; }
        public string RoleId { get; set; }
        public int StatusRoleId { get; set; }
        public int? GroupApplicationId { get; set; }
    }    
}
