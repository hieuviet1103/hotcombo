﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class ResultResponse
    {
        public ResultResponse()
        {
            status = true;
            message = "Success";
        }

        public bool status { get; set; }
        public string message { get; set; }
    }

}
