﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Hotel.Models
{
    public static class Parameters
    {
        public static string UltimatePassword = "Admin2021@ultimate";
        public enum ApplicationType
        {
            User = 0,
            Admin = 1,
            Staff = 2
        }
        public enum Roles
        {
            User = 0,
            Admin = 1,
            Staff = 2
        }

        public static List<SelectListItem> RolesDescription()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "Quản trị"},
                new SelectListItem { Value = "2", Text = "Nhân viên"},
                new SelectListItem { Value = "3", Text = "Nhập liệu"},
            };
        }

        public static List<SelectListItem> RoleNames()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "Admin"},
                new SelectListItem { Value = "2", Text = "Staff"},
                new SelectListItem { Value = "3", Text = "NhapLieu"},
            };
        }
        public static Guid HotelAll = Guid.Parse("11111111-1111-1111-1111-111111111111"); //option chọn tất cả khách sạn khi bán tour

        public enum NewsType
        {
            TinTuc = 1,
            CamNangDuLich = 2,
            BannerTop = 3,
            BannerBottom = 4

        }

        public enum Country
        {
            VietNam = 1,
        }

        public enum HotelPriceType
        {
            UuDaiDacBiet = 1,
            DatSomGiaTot = 2,
            GiaShockGioChot = 3,
            GiaThuong = 4
        }

        public class Banner
        {
            public enum Position
            {
                LogoCongTy = 1,
                SlideBannerTrangChu = 2,
                DiemDenDuLich = 3,
                SlideFooterTrangChu = 4,
                PopupNgoaiTrangChu = 5,
                LogoWhyChoose = 6
            }
        }

        public class Booking
        {
            public enum BookingStatus
            {
                TaoThanhCong = 1,
                KHDaThanhToan = 2,
                AdminXNThanhToan = 3,
                DaHuy = 4,
                ChuaPhanBo = 5
            }
            public enum PaymentTypes
            {
                TienMat = 1,
                ChuyenKhoan = 2,
            }
        }

    }
}
