//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hotel.EntityFramework.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_PaymentType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_PaymentType()
        {
            this.tbl_Receipt = new HashSet<tbl_Receipt>();
        }
    
        public int Id { get; set; }
        public string PayTypeCode { get; set; }
        public string PayTypeName { get; set; }
        public Nullable<int> Order { get; set; }
        public Nullable<int> Status { get; set; }
        public bool IsDelete { get; set; }
        public string UserCreate { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public string UserUpdate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Receipt> tbl_Receipt { get; set; }
    }
}
