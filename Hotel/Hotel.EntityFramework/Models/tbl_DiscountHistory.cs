//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hotel.EntityFramework.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DiscountHistory
    {
        public System.Guid Id { get; set; }
        public System.Guid CustomerId { get; set; }
        public System.Guid BookingId { get; set; }
        public Nullable<double> Discount { get; set; }
        public string CustomerName { get; set; }
        public Nullable<System.Guid> UserCreate { get; set; }
        public System.DateTime DateCreate { get; set; }
    }
}
