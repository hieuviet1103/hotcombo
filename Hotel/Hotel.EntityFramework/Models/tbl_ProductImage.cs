//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hotel.EntityFramework.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ProductImage
    {
        public System.Guid Id { get; set; }
        public Nullable<System.Guid> HotelId { get; set; }
        public Nullable<System.Guid> RoomId { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> Order { get; set; }
        public Nullable<System.Guid> UserCreate { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.Guid> UserUpdate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public Nullable<bool> IsDelete { get; set; }
    }
}
